﻿using System;
using UnityEngine;

namespace Commons
{
    public class ShipThrusterController : MonoBehaviour
    {
        public enum ThrusterPosition
        {
            Left,
            Center,
            Right,
        }

        [Serializable]
        public struct Thruster
        {
            public Transform transform;
            public float maxScale;
            public ThrusterPosition position;
        }

        [SerializeField] private Thruster[] thrusters;
        [SerializeField] private bool onAwakeEnabled = true;
        
        private bool thrustersEnabled = true;

        public bool ThrustersEnabled
        {
            get => thrustersEnabled;
            set
            {
                thrustersEnabled = value;

                foreach (var t in thrusters)
                {
                    t.transform.gameObject.SetActive(value);
                }
            }
        }

        private void Awake()
        {
            ThrustersEnabled = onAwakeEnabled;
        }

        public void SetThrusters(float value)
        {
            foreach (var t in thrusters)
            {
                var scaleOriginal = t.transform.localScale;
                t.transform.localScale = new Vector3(
                    scaleOriginal.x, t.maxScale * value, scaleOriginal.z);
            }
        }
    }
}