﻿using UnityEngine;

namespace Commons
{
    public struct RaycastCursorTarget
    {
        public static readonly RaycastCursorTarget None = new RaycastCursorTarget(
            false, Vector2.zero, Vector3.zero, null);

        public static RaycastCursorTarget Hit(RaycastHit hit)
        {
            return new RaycastCursorTarget(true, GeneralUtils.V3ToV2(hit.point), hit.normal, hit.transform.gameObject);
        }

        public readonly bool Success;
        public readonly Vector2 Point;
        public readonly Vector3 Normal;
        public readonly GameObject GameObject;

        private RaycastCursorTarget(bool success, Vector2 point, Vector3 normal, GameObject gameObject)
        {
            Success = success;
            Point = point;
            Normal = normal;
            GameObject = gameObject;
        }
    }
    
    public interface IPlaneRaycaster
    {
        Vector3 CursorPosition();
        RaycastCursorTarget RaycastCursor();
    }
}