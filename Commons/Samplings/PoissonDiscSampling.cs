﻿using System.Collections.Generic;
using Game.Extensions;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Commons.Samplings
{
    public static class PoissonDiscSampling
    {
        public static List<int2> GeneratePoints(int radius, int2 sampleRegionSize, int numSamplesBeforeRejection = 30)
        {
            var cellSize = radius / Mathf.Sqrt(2);

            var grid = new int[Mathf.CeilToInt(sampleRegionSize.x / cellSize),
                Mathf.CeilToInt(sampleRegionSize.y / cellSize)];
            var points = new List<int2>();
            var spawnPoints = new List<int2> {sampleRegionSize / 2};

            while (spawnPoints.Count > 0)
            {
                var spawnIndex = Random.Range(0, spawnPoints.Count);
                var spawnCentre = new Vector2(spawnPoints[spawnIndex].x, spawnPoints[spawnIndex].y);
                var candidateAccepted = false;

                for (var i = 0; i < numSamplesBeforeRejection; i++)
                {
                    var angle = Random.value * Mathf.PI * 2;
                    var dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
                    var candidateV2 = spawnCentre + dir * Random.Range(radius, 2 * radius);
                    var candidate = new int2(
                        Mathf.FloorToInt(candidateV2.x),
                        Mathf.FloorToInt(candidateV2.y)
                        );
                    if (IsValid(candidate, sampleRegionSize, cellSize, radius, points, grid))
                    {
                        points.Add(candidate);
                        spawnPoints.Add(candidate);
                        grid[(int) (candidateV2.x / cellSize), (int) (candidateV2.y / cellSize)] = points.Count;
                        candidateAccepted = true;
                        break;
                    }
                }

                if (!candidateAccepted)
                {
                    spawnPoints.RemoveAt(spawnIndex);
                }
            }

            return points;
        }

        static bool IsValid(int2 candidate, int2 sampleRegionSize, float cellSize, float radius,
            List<int2> points, int[,] grid)
        {
            if (candidate.x >= 0 && candidate.x < sampleRegionSize.x && candidate.y >= 0 &&
                candidate.y < sampleRegionSize.y)
            {
                var cellX = (int) (candidate.x / cellSize);
                var cellY = (int) (candidate.y / cellSize);
                var searchStartX = Mathf.Max(0, cellX - 2);
                var searchEndX = Mathf.Min(cellX + 2, grid.GetLength(0) - 1);
                var searchStartY = Mathf.Max(0, cellY - 2);
                var searchEndY = Mathf.Min(cellY + 2, grid.GetLength(1) - 1);

                for (var x = searchStartX; x <= searchEndX; x++)
                {
                    for (var y = searchStartY; y <= searchEndY; y++)
                    {
                        var pointIndex = grid[x, y] - 1;
                        if (pointIndex != -1)
                        {
                            var sqrDst = (candidate - points[pointIndex]).sqrMagnitude();
                            if (sqrDst < radius * radius)
                            {
                                return false;
                            }
                        }
                    }
                }

                return true;
            }

            return false;
        }
    }
}