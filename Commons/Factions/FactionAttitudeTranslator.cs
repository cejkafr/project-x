﻿using System;
using UnityEngine;
using RangeInt = Game.Types.RangeInt;

namespace Commons.Factions
{
    [CreateAssetMenu(menuName = "Factions/Attitude Translator")]
    public class FactionAttitudeTranslator : ScriptableObject
    {
        [Serializable]
        public struct TitleFromValue
        {
            public int toValue;
            public string title;
        }

        [SerializeField] private RangeInt valueRange;
        [SerializeField] private Gradient color;
        [SerializeField] private TitleFromValue[] titles;

        public Color ValueToColor(int value)
        {
            var floatValue = Mathf.InverseLerp(valueRange.min, valueRange.max, value);
            return color.Evaluate(floatValue);
        }

        public string ValueToTitle(int value)
        {
            foreach (var title in titles)
            {
                if (title.toValue > value)
                {
                    return title.title;
                }
            }

            return "Unknown";
        }
    }
}