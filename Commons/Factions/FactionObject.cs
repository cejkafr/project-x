﻿using UnityEngine;

namespace Commons.Factions
{
    [CreateAssetMenu(menuName = "Factions/Faction")]
    public class FactionObject : ScriptableObject
    {
        public string title;
        public string description;
        public Sprite icon;
        public int attitudeTowardsPlayer;
        public bool isHuman;
        public bool isPirate;
    }
}