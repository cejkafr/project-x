﻿using UnityEngine;

namespace Commons.Factions
{
    public interface IFaction
    {
        string Name { get; }
        Color Color { get; }
        bool IsHuman { get; }
        int Attitude { get; }
        string AttitudeTitle { get; }

    }
}