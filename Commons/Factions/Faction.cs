﻿using UnityEngine;

namespace Commons.Factions
{
    public class Faction : IFaction
    {
        private readonly FactionObject template;
        private readonly FactionAttitudeTranslator attitudeTranslator;

        public string Name => template.title;
        public Color Color => IsHuman ? Color.blue : attitudeTranslator.ValueToColor(Attitude);
        public bool IsHuman => template.isHuman;
        public bool IsPirate => template.isPirate;
        public int Attitude { get; set; }
        public string AttitudeTitle => attitudeTranslator.ValueToTitle(Attitude);

        public Faction(FactionObject template, FactionAttitudeTranslator attTranslator)
        {
            this.template = template;

            Attitude = this.template.attitudeTowardsPlayer;
            attitudeTranslator = attTranslator;
        }
    }
}