﻿using Commons.Sectors;
using UnityEngine;

namespace Commons.Presenters
{
    public class CachedObjectPresenter : IObjectPresenter
    {
        private readonly IBulkDisableObjectCache cache;

        public CachedObjectPresenter(IBulkDisableObjectCache cache)
        {
            this.cache = cache;
        }

        public void Present(Presenter settings, SamplerItem item)
        {
            var library = settings.prefabLibrary;
            var randomValue = Random.Range(0, library.levels[item.Value].prefabs.Length);
            var prefab = library.levels[item.Value].prefabs[randomValue];
            var pos = new Vector3(item.Position.x, 0f, item.Position.y);
            cache.FetchOrInstantiate(prefab, pos, Random.rotation);
        }
    }
}