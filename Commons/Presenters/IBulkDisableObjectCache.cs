﻿using UnityEngine;

namespace Commons.Presenters
{
    public interface IBulkDisableObjectCache
    {
        GameObject FetchOrInstantiate(
            GameObject prefab, Vector3 position, Quaternion rotation);
        void DisableAll();
    }
}