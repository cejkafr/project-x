﻿using Commons.Procedural;
using Commons.Procedural.Lights;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

namespace Commons.Presenters
{
    public class CachedLightSourcePresenter : ILightSourcePresenter
    {
        private readonly IBulkDisableObjectCache cache;
        private readonly GameObject lightPrefab;

        public CachedLightSourcePresenter(IBulkDisableObjectCache cache, GameObject lightPrefab)
        {
            this.cache = cache;
            this.lightPrefab = lightPrefab;
        }

        public void Spawn(LightSource[] sources)
        {
            foreach (var t in sources)
            {
                InitLight(t);
            }
        }

        private void InitLight(LightSource source)
        {
            var inst = cache.FetchOrInstantiate(lightPrefab, Vector3.zero, source.rotation);

            var light = inst.GetComponent<Light>();
            light.type = LightType.Directional;
            light.color = source.color;
            light.intensity = source.intensity;

            var hdLight = inst.GetComponent<HDAdditionalLightData>();
            hdLight.type = HDLightType.Directional;
            hdLight.color = source.color;
            hdLight.SetIntensity(source.intensity, LightUnit.Lux);
            hdLight.interactsWithSky = false;
            hdLight.angularDiameter = 0f;
            hdLight.EnableShadows(false);
        }
    }
}