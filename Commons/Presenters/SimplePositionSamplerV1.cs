﻿using Commons.Sectors;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Commons.Presenters
{
    public class SimplePositionSamplerV1 : IPositionSampler
    {
        public bool HasValue(PositionSampler settings, int x, int y, float value)
        {
            return value > 0.001f;
        }

        public SamplerItem Generate(PositionSampler settings, int x, int y, float value)
        {
            var level = settings.intensityLevels.Length - 1;
            
            for (var i = 0; i < settings.intensityLevels.Length; i++)
            {
                var intensity = settings.intensityLevels[i];
                if (!(value < intensity))
                {
                    continue;
                }
                level = i;
                break;
            }

            var halfTileSize = settings.tileSize / 2;
            return new SamplerItem(RandomPosition(x * settings.tileSize + halfTileSize, y * settings.tileSize + halfTileSize), level);
        }

        public static int2 RandomPosition(int x, int y)
        {
            return new int2(x + Random.Range(-50, 50), y + Random.Range(-50, 50));
        }
    }
}