﻿using Commons.Procedural;
using Commons.Procedural.Lights;

namespace Commons.Presenters
{
    public interface ILightSourcePresenter
    {
        void Spawn(LightSource[] sources);
    }
}