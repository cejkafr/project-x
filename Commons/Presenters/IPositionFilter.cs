﻿using System.Collections.Generic;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;

namespace Commons.Presenters
{
    public readonly struct SamplerItem
    {
        public readonly int2 Position;
        public readonly int Value;

        public SamplerItem(int2 position, int value)
        {
            Position = position;
            Value = value;
        }
    }
    
    public interface IPositionSampler
    {
        bool HasValue(PositionSampler settings, int x, int y, float value);
        SamplerItem Generate(PositionSampler settings, int x, int y, float value);
    }
}