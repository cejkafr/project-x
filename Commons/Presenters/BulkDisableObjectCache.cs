﻿using System.Collections.Generic;
using UnityEngine;

namespace Commons.Presenters
{
    public class BulkDisableObjectCache : IBulkDisableObjectCache
    {
        private readonly Dictionary<GameObject, Queue<GameObject>> free;
        private readonly Dictionary<GameObject, List<GameObject>> used;
        private readonly Transform cacheParent;

        private GameObject gameObject;

        public BulkDisableObjectCache(Transform cacheParent)
        {
            free = new Dictionary<GameObject, Queue<GameObject>>(10);
            used = new Dictionary<GameObject, List<GameObject>>(10);
            this.cacheParent = cacheParent;
        }

        public GameObject FetchOrInstantiate(
            GameObject prefab, Vector3 position, Quaternion rotation)
        {
            if (!free.ContainsKey(prefab))
            {
                free.Add(prefab, new Queue<GameObject>(10));
                used.Add(prefab, new List<GameObject>(10));
            }

            if (free[prefab].Count == 0)
            {
                free[prefab].Enqueue(Object.Instantiate(prefab, cacheParent));
            }

            gameObject = free[prefab].Dequeue();
            // gameObject.transform.parent = parent;
            gameObject.transform.position = position;
            gameObject.transform.rotation = rotation;
            gameObject.SetActive(true);
            
            used[prefab].Add(gameObject);

            return gameObject;
        }

        public void DisableAll()
        {
            foreach (var usedKeyPair in used)
            {
                foreach (var item in usedKeyPair.Value)
                {
                    // item.transform.parent = cacheParent;
                    item.SetActive(false);
                    free[usedKeyPair.Key].Enqueue(item);
                }
                
                usedKeyPair.Value.Clear();
            }
        }
    }
}