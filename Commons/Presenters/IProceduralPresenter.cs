﻿using Commons.Procedural.NoiseMaps;
using Commons.Sectors;

namespace Commons.Presenters
{
    public interface IProceduralPresenter
    {
        void Present(int seed, NoiseMap layer, ProceduralLayer[] settings);
    }
}