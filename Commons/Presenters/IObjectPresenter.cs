﻿using Commons.Sectors;

namespace Commons.Presenters
{
    public interface IObjectPresenter
    {
        void Present(Presenter settings, SamplerItem item);
    }
}