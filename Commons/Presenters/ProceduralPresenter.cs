﻿using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Game.Extensions;
using UnityEngine;

namespace Commons.Presenters
{
    public class ProceduralPresenter : IProceduralPresenter
    {
        private readonly IPositionSampler sampler;
        private readonly IObjectPresenter presenter;

        public ProceduralPresenter(IPositionSampler sampler, IObjectPresenter presenter)
        {
            this.sampler = sampler;
            this.presenter = presenter;
        }

        public void Present(int seed, NoiseMap noise, ProceduralLayer[] settings)
        {
            Random.InitState(seed);
            
            noise.Map.IterateOver((layer, x, y, value) =>
            {
                if (!sampler.HasValue(settings[layer].positionSampler, x, y, value))
                {
                    return;
                }

                var item = sampler.Generate(settings[layer].positionSampler, x, y, value);
                presenter.Present(settings[layer].presenter, item); 
            });
        }
    }
}