﻿namespace Commons.Procedural.NoiseMaps
{
    public readonly struct NoiseMapSettings
    {
        public readonly int Size;
        public readonly NoiseMapLayerSettings[] Layers;

        public NoiseMapSettings(int size, NoiseMapLayerSettings[] layers)
        {
            Size = size;
            Layers = layers;
        }
    }

    public readonly struct NoiseMap
    {
        public readonly NoiseMapSettings Settings;
        public readonly float[,,] Map;

        public NoiseMap(NoiseMapSettings settings, float[,,] map)
        {
            Settings = settings;
            Map = map;
        }
    }
    
    public interface INoiseMapGenerator
    {
        NoiseMap Generate(NoiseMapSettings settings);
    }
}