﻿using System.Collections.Generic;

namespace Commons.Procedural.NoiseMaps
{
    public readonly struct NoiseMapLayerSettings
    {
        public readonly struct NoiseLayer
        {
            public readonly float Scale;
            public readonly float MinValue;

            public NoiseLayer(float scale, float minValue)
            {
                Scale = scale;
                MinValue = minValue;
            }
        }

        public readonly int Size;
        public readonly NoiseLayer[] NoiseLayers;

        public NoiseMapLayerSettings(int size, NoiseLayer[] noiseLayers)
        {
            Size = size;
            NoiseLayers = noiseLayers;
        }
    }

    public interface INoiseMapLayerGenerator
    {
        float Generate(int x, int y);
    }
}