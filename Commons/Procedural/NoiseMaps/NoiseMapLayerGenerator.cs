﻿using Commons.Procedural.Noises;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Commons.Procedural.NoiseMaps
{
    public class NoiseMapLayerGenerator : INoiseMapLayerGenerator
    {
        private readonly NoiseMapLayerSettings settings;
        private readonly INoiseGenerator[] noiseGenerators;

        public NoiseMapLayerGenerator(NoiseMapLayerSettings settings)
        {
            this.settings = settings;

            var offset = new int2(
                Random.Range(1000000, 9999999),
                Random.Range(1000000, 9999999));
            noiseGenerators = new INoiseGenerator[settings.NoiseLayers.Length];
            PrepareNoiseGenerators(offset);
        }

        private void PrepareNoiseGenerators(int2 offset)
        {
            for (var i = 0; i < settings.NoiseLayers.Length; i++)
            {
                var noiseSettings = settings.NoiseLayers[i];
                var noiseSeed = Random.Range(int.MinValue, int.MaxValue);

                noiseGenerators[i] = new PerlinNoiseGeneratorV1(noiseSeed, offset,
                    settings.Size, noiseSettings.Scale, noiseSettings.MinValue);
            }
        }
        
        public float Generate(int x, int y)
        {
            return GenerateTileMap(x, y);
        }

        private float GenerateTileMap(int x, int y)
        {
            var value = 0f;
            foreach (var noiseLayer in noiseGenerators)
            {
                value = noiseLayer.Generate(x, y);
                if (value > 0)
                {
                    break;
                }
            }

            return value;
        }
    }
}