﻿using Game.Extensions;

namespace Commons.Procedural.NoiseMaps
{
    public class NoiseMapGenerator : INoiseMapGenerator
    {
        public NoiseMap Generate(NoiseMapSettings settings)
        {
            var generators = InitGenerators(settings);
            
            var map = new float[settings.Layers.Length, settings.Size, settings.Size];
            map.IterateOver((layer, x, y, value) 
                => map[layer, x, y] = generators[layer].Generate(x, y));

            return new NoiseMap(settings, map);
        }

        private INoiseMapLayerGenerator[] InitGenerators(NoiseMapSettings settings)
        {
            var generators = new INoiseMapLayerGenerator[settings.Layers.Length];
            for (var i = 0; i < settings.Layers.Length; i++)
            {
                generators[i] = new NoiseMapLayerGenerator(settings.Layers[i]);
            }
            return generators;
        }
    }
}