﻿using System;
using Unity.Mathematics;
using UnityEngine;

namespace Commons.Procedural.Lights
{
    [Serializable]
    public struct LightSourceSettings
    {
        public MainLight[] mains;
        public RimLight mirrorMainRim;
        public RimLight fixedTopRim; 

        [Serializable]
        public struct MainLight
        {
            public Gradient color;
            public float2 intensity;
        }

        [Serializable]
        public struct RimLight
        {
            public Gradient color;
            public float intensity;
        }
    }
    
    [Serializable]
    public struct LightSource
    {
        public Color color;
        public float intensity;
        public quaternion rotation;
    }

    public interface ILightSourceGenerator
    {
        LightSource[] Generate(LightSourceSettings settings);
    }
}