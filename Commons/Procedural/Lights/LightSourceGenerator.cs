﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Commons.Procedural.Lights
{
    public class LightSourceGenerator : ILightSourceGenerator
    {
        public LightSource[] Generate(LightSourceSettings settings)
        {
            var lightSources = new LightSource[settings.mains.Length + 2];

            for (var i = 0; i < settings.mains.Length; i++)
            {
                var setting = settings.mains[i];
                lightSources[i] = new LightSource
                {
                    color = setting.color.Evaluate(Random.Range(0f, 1f)),
                    intensity = Random.Range(setting.intensity.x, setting.intensity.y),
                    rotation = Quaternion.Euler(0f,
                        Random.Range(0, 360),
                        Random.Range(0, 180)),
                };
            }
            
            var mirrorMain = settings.mirrorMainRim;
            lightSources[settings.mains.Length] = new LightSource
            {
                color = mirrorMain.color.Evaluate(Random.Range(0f, 1f)),
                intensity = mirrorMain.intensity,
                rotation = Quaternion.Inverse(lightSources[0].rotation),
            };
          
            var fixedTop = settings.fixedTopRim;
            lightSources[settings.mains.Length + 1] = new LightSource
            {
                color = fixedTop.color.Evaluate(Random.Range(0f, 1f)),
                intensity = fixedTop.intensity,
                rotation = quaternion.Euler(0f, 0f , 90f),
            };
            
            return lightSources;
        }
    }
}