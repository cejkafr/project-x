﻿using Unity.Mathematics;
using UnityEngine;

namespace Commons.Procedural.Noises
{
    public class PerlinNoiseGeneratorV1 : INoiseGenerator
    {
        private readonly float seed;
        private readonly int2 offset;
        private readonly int tileCount;
        private readonly float scale;
        private readonly float minValue;

        private float xCord;
        private float yCord;
        private float value;

        public PerlinNoiseGeneratorV1(int seed, int2 offset, int tileCount, float scale, float minValue)
        {
            this.seed = seed.GetHashCode() * 0.001f;
            this.offset = offset;
            this.tileCount = tileCount;
            this.scale = scale;
            this.minValue = minValue;
        }

        public float Generate(int x, int y)
        {
            xCord = x + offset.x;
            yCord = y + offset.y;

            xCord /= tileCount;
            yCord /= tileCount;
            
            xCord *= scale;
            yCord *= scale;
            
            xCord += seed;
            yCord += seed;

            value = Mathf.PerlinNoise(xCord, yCord);
            value = Mathf.Clamp01(value);

            return value < minValue ? 0 : value;
        }
    }
}