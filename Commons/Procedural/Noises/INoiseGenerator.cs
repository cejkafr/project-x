﻿namespace Commons.Procedural.Noises
{
    public interface INoiseGenerator
    {
        float Generate(int x, int y);
    }
}