﻿namespace Commons.Procedural.TileMaps
{
    public interface ITileMapGenerator
    {
        int[,] Generate(int count, float ratio);
    }
}