﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Commons.Procedural.TileMaps
{
    public class DumbTileMapGenerator : ITileMapGenerator
    {
        private readonly bool diagonally;

        private int[,] map;
        private List<int2> free;

        public DumbTileMapGenerator(bool diagonally)
        {
            this.diagonally = diagonally;
        }

        public int[,] Generate(int count, float ratio)
        {
            Reset(Mathf.RoundToInt(Mathf.Ceil(Mathf.Sqrt(count / ratio))));
            GenerateTileCoords(count);

            return map;
        }

        private void Reset(int count)
        {
            map = new int[count, count];
            free = new List<int2>(count * count);

            for (var y = 0; y < count; y++)
            {
                for (var x = 0; x < count; x++)
                {
                    map[x, y] = -1;
                    free.Add(new int2(x, y));
                }
            }
        }

        private void GenerateTileCoords(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var coords = free[Random.Range(0, free.Count)];
                map[coords.x, coords.y] = i;
                MarkAdjacentAsFree(coords);
                ReInitFree();
            }
        }

        private void MarkAdjacentAsFree(int2 coords)
        {
            if (diagonally)
            {
                MarkAdjacentAsFreeDiagonally(coords);
            }

            MarkAsFreeIfEmpty(coords.x - 1, coords.y);
            MarkAsFreeIfEmpty(coords.x + 1, coords.y);
            MarkAsFreeIfEmpty(coords.x, coords.y - 1);
            MarkAsFreeIfEmpty(coords.x, coords.y + 1);
        }

        private void MarkAdjacentAsFreeDiagonally(int2 coords)
        {
            MarkAsFreeIfEmpty(coords.x - 1, coords.y - 1);
            MarkAsFreeIfEmpty(coords.x - 1, coords.y + 1);
            MarkAsFreeIfEmpty(coords.x + 1, coords.y + 1);
            MarkAsFreeIfEmpty(coords.x + 1, coords.y - 1);
        }

        private void MarkAsFreeIfEmpty(int x, int y)
        {
            if (!IsCoordValid(x, y)) return;
            if (map[x, y] != -1) return;
            map[x, y] = -2;
        }

        private bool IsCoordValid(int x, int y)
        {
            return x > 0 && y > 0 && x < map.GetLength(0) && y < map.GetLength(1);
        }

        private void ReInitFree()
        {
            free.Clear();

            for (var y = 0; y < map.GetLength(1); y++)
            {
                for (var x = 0; x < map.GetLength(0); x++)
                {
                    if (map[x, y] == -2)
                    {
                        free.Add(new int2(x, y));
                    }
                }
            }
        }
    }
}