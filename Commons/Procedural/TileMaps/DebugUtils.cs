﻿using System.Text;
using UnityEngine;

namespace Commons.Procedural.TileMaps
{
    public static class DebugUtils
    {
        public static void PrintMap(int[,] map)
        {
            var sb = new StringBuilder();
            var width = map.GetLength(0);
            var height = map.GetLength(1);

            LineByWidth(sb, width, 4);
            for (var y = 0; y < height; y++)
            {
                sb.Append("|");
                for (var x = 0; x < width; x++)
                {
                    sb.Append(map[x, y] < 0 ? $"         |" : $"   {map[x, y]}   |");
                }

                sb.AppendLine();
                LineByWidth(sb, width, 4);
            }

            Debug.Log(sb.ToString());
        }

        public static void PrintLinkedMap(LinkedMapTile?[,] map)
        {
            var sb = new StringBuilder();
            var width = map.GetLength(0);
            var height = map.GetLength(1);

            LineByWidth(sb, width, 9);
            for (var y = 0; y < height; y++)
            {
                sb.Append("|");
                for (var x = 0; x < width; x++)
                {
                    if (!map[x, y].HasValue)
                    {
                        sb.Append("                  |");
                    }
                    else
                    {
                        var val = map[x, y].Value;
                        var gates = new bool[4];
                        foreach (var link in val.Links)
                        {
                            var dir = val.Coords - link.Coords;

                            if (dir.y == 1) gates[0] = true;
                            if (dir.x == -1) gates[1] = true;
                            if (dir.y == -1) gates[2] = true;
                            if (dir.x == 1) gates[3] = true;
                        }

                        sb.Append(
                            $" " +
                            $"{(gates[3] ? "<" : "   ")} " +
                            $"{(gates[2] ? "!" : "   ")} " +

                            $"{val.Index} " +

                            $"{(gates[0] ? "^" : "   ")} " +
                            $"{(gates[1] ? ">" : "   ")} |");
                    }
                }

                sb.AppendLine();
                LineByWidth(sb, width, 9);
            }

            Debug.Log(sb.ToString());
        }

        private static void LineByWidth(StringBuilder sb, int width, int size)
        {
            for (var i = 0; i < width; i++)
            {
                sb.Append("+");
                for (var j = 0; j < size; j++)
                {
                    sb.Append("-");
                }
            }

            sb.AppendLine("+");
        }
    }
}