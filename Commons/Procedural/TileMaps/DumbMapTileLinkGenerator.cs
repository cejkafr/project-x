﻿using System.Collections.Generic;
using Game.Extensions;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Commons.Procedural.TileMaps
{
    public class DumbMapTileLinkGenerator : IMapTileLinkGenerator
    {
        private readonly bool diagonally;

        private LinkedMapTile?[,] linkedMap;

        public DumbMapTileLinkGenerator(bool diagonally)
        {
            this.diagonally = diagonally;
        }

        public LinkedMapTile?[,] Generate(int[,] map)
        {
            InitLinkedMap(map);
            map.IterateOver((x, y, index) =>
            {
                if (!linkedMap[x, y].HasValue)
                {
                    return;
                }

                var tile = linkedMap[x, y].Value;
                var adjacent = GetAdjacent(tile);
                var desiredCount = Random.Range(1, adjacent.Count);
                if (desiredCount == 0 || adjacent.Count == 0 || tile.Links.Count > desiredCount)
                {
                    return;
                }

                for (var i = 0; i < desiredCount; i++)
                {
                    var random = Random.Range(0, adjacent.Count);
                    tile.Links.Add(adjacent[random]);
                    adjacent[random].Links.Add(tile);
                    adjacent.RemoveAt(random);
                }
            });

            return linkedMap;
        }

        private void InitLinkedMap(int[,] map)
        {
            linkedMap = new LinkedMapTile?[map.GetLength(0), map.GetLength(1)];
            map.IterateOver((x, y, index) =>
            {
                if (index < 0)
                {
                    linkedMap[x, y] = null;
                    return;
                }

                linkedMap[x, y] = new LinkedMapTile(index, new int2(x, y), diagonally ? 8 : 4);
            });
        }

        private List<LinkedMapTile> GetAdjacent(LinkedMapTile tile)
        {
            var adjacent = new List<LinkedMapTile>(diagonally ? 8 : 4);
            Resolve(adjacent, tile.Links, tile.Coords.x + 1, tile.Coords.y);
            Resolve(adjacent, tile.Links, tile.Coords.x - 1, tile.Coords.y);
            Resolve(adjacent, tile.Links, tile.Coords.x, tile.Coords.y + 1);
            Resolve(adjacent, tile.Links, tile.Coords.x, tile.Coords.y - 1);

            return adjacent;
        }

        private void Resolve(ICollection<LinkedMapTile> adjacent, ICollection<LinkedMapTile> links, int x, int y)
        {
            if (!IsValidSector(x, y))
            {
                return;
            }

            if (links.Contains(linkedMap[x, y].Value))
            {
                return;
            }

            adjacent.Add(linkedMap[x, y].Value);
        }

        private bool IsValidSector(int x, int y)
        {
            // return IsCoordValid(x, y) && linkedMap[x, y].HasValue;
            if (!IsCoordValid(x, y))
            {
                return false;
            }

            if (!linkedMap[x, y].HasValue)
            {
                return false;
            }

            return true;
        }

        private bool IsCoordValid(int x, int y)
        {
            return x > 0 && y > 0 && x < linkedMap.GetLength(0) && y < linkedMap.GetLength(1);
        }
    }
}