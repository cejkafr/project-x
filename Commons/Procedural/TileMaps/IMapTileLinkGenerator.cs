﻿using System.Collections.Generic;
using Unity.Mathematics;

namespace Commons.Procedural.TileMaps
{
    public readonly struct LinkedMapTile
    {
        public readonly int Index;
        public readonly int2 Coords;
        public readonly List<LinkedMapTile> Links;

        public LinkedMapTile(int index, int2 coords, int maxLinkCount)
        {
            Index = index;
            Coords = coords;
            Links = new List<LinkedMapTile>(maxLinkCount);
        }
    }
    
    public interface IMapTileLinkGenerator
    {
        LinkedMapTile?[,] Generate(int[,] map);
    }
}