﻿namespace Commons
{
    public class Vertex
    {
        public int Key { get; set; } = int.MaxValue;
        public int Parent { get; set; } = -1;
        public int V { get; set; }
        public bool IsProcessed { get; set; }
    }

    public static class MinimumSpanningTree
    {
        public static Vertex[] Evaluate(int[][] graph)
        {
            var queue = new PriorityQueue<Vertex>(true);
            var vertexCount = graph.GetLength(0);
            //listing all vertices
            var vertices = new Vertex[vertexCount];
            for (var i = 0; i < vertexCount; i++)
                vertices[i] = new Vertex() {Key = int.MaxValue, Parent = -1, V = i};
            //setting first one's key to zero
            vertices[0].Key = 0;

            //insertingvertices
            for (var i = 0; i < vertexCount; i++)
                queue.Enqueue(vertices[i].Key, vertices[i]);

            while (queue.Count > 0)
            {
                var minVertex = queue.Dequeue();
                var u = minVertex.V;
                vertices[u].IsProcessed = true;
                //alll edges from vertex u
                var edges = graph[minVertex.V];
                for (var v = 0; v < edges.Length; v++)
                {
                    if (graph[u][v] > 0 && !vertices[v].IsProcessed && graph[u][v] < vertices[v].Key)
                    {
                        vertices[v].Parent = u;
                        vertices[v].Key = graph[u][v];
                        //updating priority in queue since key is priority
                        queue.UpdatePriority(vertices[v], vertices[v].Key);
                    }
                }
            }

            return vertices;
        }
    }
}