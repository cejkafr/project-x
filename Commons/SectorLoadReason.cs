﻿namespace Commons
{
    public enum SectorLoadReason
    {
        LoadGame,
        WarpGate,
        TacticalBattle,
    }
}