﻿using System;
using UnityEngine;

namespace Commons.Sectors
{
    public enum ActionType {
        UseGate, Dock, Attack, Evade
    }

    [Serializable]
    public struct SectorObjectAction
    {
        public new string name;
        public string description;
        public Sprite icon;
        public ActionType type;

        [Header("Rules")]
        public bool useAttitudeMin;
        public int attitudeMin;
        public bool useAttitudeMax;
        public int attitudeMax;
        public bool mustBeEnabled;
        public bool mustBeDisabled;
    }
}