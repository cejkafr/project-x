﻿using System.Collections.Generic;
using Unity.Mathematics;

namespace Commons.Sectors
{
    public readonly struct SectorPath
    {
        public readonly List<int2> Waypoints;

        public SectorPath(List<int2> waypoints)
        {
            Waypoints = waypoints;
        }
    }
}