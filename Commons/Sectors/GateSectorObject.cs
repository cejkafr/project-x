﻿using UnityEngine;

namespace Commons.Sectors
{
    [CreateAssetMenu(menuName = "Sectors/Gate")]
    public class GateSectorObject : ScriptableObject
    {
        public Sprite icon;
        public GameObject gamePrefab;
        public SectorObjectAction[] actions;
    }
}