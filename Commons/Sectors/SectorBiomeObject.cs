﻿using System;
using Commons.DataObjects;
using Commons.Factions;
using Commons.Procedural.Lights;
using UnityEngine;

namespace Commons.Sectors
{
    [Serializable]
    public struct ProceduralLayer
    {
        public string name;
        public ProceduralLayerTag tag;
        public int navCostModifier;
        public NoiseFilter noiseFilter;
        public PositionSampler positionSampler;
        public Presenter presenter;
    }
    
    [Serializable]
    public struct NoiseFilter
    {
        [Serializable]
        public struct NoiseLayer
        {
            public float scale;
            [Range(0, 1)]
            public float minValue;
        }

        public NoiseLayer[] noiseLayers;
    }

    [Serializable]
    public struct PositionSampler
    {
        public int tileSize;
        [Range(0, 1)]
        public float[] intensityLevels;
    }

    [Serializable]
    public struct Presenter
    {
        public LeveledPrefabLibrary prefabLibrary;
    }

    [CreateAssetMenu(menuName = "Sectors/Biome")]
    public class SectorBiomeObject : ScriptableObject
    {
        public new string name;
        public int size;
        public int tileSize;
        public FactionObject faction;
        public ProceduralLayer[] proceduralLayers;
        public LightSourceSettings light;
    }
}