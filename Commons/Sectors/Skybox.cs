﻿using UnityEngine;

namespace Commons.Sectors
{
    public readonly struct Skybox
    {
        public readonly Cubemap Texture;
        public readonly float Exposure;
        public readonly float Rotation;

        public Skybox(Cubemap texture, float exposure, float rotation)
        {
            Texture = texture;
            Exposure = exposure;
            Rotation = rotation;
        }
    }
}