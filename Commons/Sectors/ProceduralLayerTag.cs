﻿namespace Commons.Sectors
{
    public enum ProceduralLayerTag
    {
        Asteroid, Nebula, Debris, Unknown
    }
}