﻿using UnityEngine;

namespace Commons.Sectors
{
    [CreateAssetMenu(menuName = "Sectors/Fleet")]
    public class FleetSectorObject : ScriptableObject
    {
        public Sprite icon;
        public SectorObjectAction[] actions;
    }
}