﻿using UnityEngine;

namespace Commons.Sectors
{
    [CreateAssetMenu(menuName = "Sectors/Station")]
    public class StationSectorObject : ScriptableObject
    {
        public Sprite icon;
        public GameObject gamePrefab;
        public SectorObjectAction[] actions;
    }
}