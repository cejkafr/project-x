﻿using System;
using System.Collections;
using System.Collections.Generic;
using Commons.Ships.Components;
using Game.Commons;
using Game.Commons.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Commons.UI
{
    public class ComponentTooltip : Singleton<ComponentTooltip>, IPointerEnterHandler, IPointerExitHandler
    {
        [Serializable]
        public struct Bindings
        {
            public RectTransform transform;
            public TextMeshProUGUI headerText;
            public RectTransform statsTransform;
            public TextMeshProUGUI contentText;
        }

        private CanvasGroup canvasGroup;
        private Coroutine delayCoroutine;
        private List<TooltipComponentLine> m_Lines = new List<TooltipComponentLine>(10);

        public Bindings bindings;
        public GameObject linePrefab;
        public float showDelaySec = 1f;
        public float hideDelaySec = 0.1f;
        public Vector3 offset = Vector3.zero;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        private void Start()
        {
            Instance.Hide();
        }

        public void Show(IComponent component)
        {
            bindings.transform.gameObject.SetActive(false);

            bindings.headerText.text = component.Name;
            bindings.contentText.text = component.Description;

            HideLines();
        
            var stats = component.Stats;
            for (var i = 0; i < stats.Length; i++)
            {
                if (i >= m_Lines.Count)
                {
                    var inst = Instantiate(linePrefab, bindings.statsTransform);
                    m_Lines.Add(inst.GetComponent<TooltipComponentLine>());
                }
            
                m_Lines[i].SetActive(stats[i].Title, stats[i].Value);
            }
        
            ShowInternal();
        }

        public void Hide()
        {
            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
            delayCoroutine = StartCoroutine(SetActiveAfterDelay(false, hideDelaySec));
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            bindings.transform.gameObject.SetActive(false);
        }

        private void ShowInternal()
        {
            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
            delayCoroutine = StartCoroutine(SetActiveAfterDelay(true, showDelaySec));
        }

        private void HideLines()
        {
            foreach (var line in m_Lines)
            {
                line.gameObject.SetActive(false);
            }
        }

        private IEnumerator SetActiveAfterDelay(bool active, float delaySec)
        {
            yield return new WaitForSecondsRealtime(delaySec);
            bindings.transform.position = Input.mousePosition + offset;
            bindings.transform.gameObject.SetActive(active);
            LayoutRebuilder.ForceRebuildLayoutImmediate(bindings.transform);
        }
    }
}