﻿using Game.Commons.UI;
using Game.Console;
using Games.Consoles;
using UnityEngine;
using UnityEngine.UI;

namespace Commons.UI
{
    public class HomeGameMenuScreen : BaseMenuScreen
    {
        [SerializeField] private BaseMenuScreen settingsScreen;
        [SerializeField] private Button firstButton;

        public override void Open()
        {
            base.Open();
            
            firstButton.Select();
        }

        public override void Back()
        {
            Close();
            GameMenu.Close();
        }

        public void OpenSettings()
        {
            Close();
            settingsScreen.Open();
        }
    
        public void OnResumeBtn()
        {
            ConsoleManager.EnqueueCommandNoHistory("menu 0");
        }

        public void OnRestartBtn()
        {
            ConsoleManager.EnqueueCommandNoHistory("restart");
            ConsoleManager.EnqueueCommandNoHistory("menu 0 0");
        }

        public void OnSettingsBtn()
        {
            OpenSettings();
        }

        public void OnQuitBtn()
        {
            ConsoleManager.EnqueueCommandNoHistory("quit");
        }
    }
}
