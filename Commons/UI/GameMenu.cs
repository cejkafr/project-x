﻿using System;
using System.Globalization;
using Game.Commons;
using Games.Consoles;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Commons.UI
{
    public interface IMenuScreen
    {
        GameMenu GameMenu { get; }
        bool IsOpen { get; }

        void Open();
        void Close();
        void Back();
    }

    public abstract class BaseMenuScreen : MonoBehaviour, IMenuScreen
    {
        public BaseMenuScreen parentScreen;

        public GameMenu GameMenu { get; private set; }
        public bool IsOpen => gameObject.activeInHierarchy;

        private void Start()
        {
            GameMenu = FindObjectOfType<GameMenu>();
        }

        private void Update()
        {
            // REMOVE
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Back();
            }
        }

        public virtual void Open()
        {
            gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            gameObject.SetActive(false);
        }

        public virtual void Back()
        {
            Close();
            parentScreen.Open();
        }
    }

    public class GameMenu : Singleton<GameMenu>
    {
        public enum ShowType
        {
            None,
            InGame
        }
        
        [Serializable]
        public class GameMenuEvent : UnityEvent<bool>
        {
        }

        [SerializeField] private BaseMenuScreen[] screens;
        [SerializeField] private BaseMenuScreen home;
        [SerializeField] private  GameObject fader;

        [Space]
        public GameMenuEvent onMenuOpenStateChanged = new GameMenuEvent();
        public bool IsOpen => fader.activeInHierarchy;

        private void Start()
        {
            fader.SetActive(false);
            CloseAllScreens();

            ConsoleManager.AddCommand("menu", CmdMenu, "Show the main menu.");
        }

        public void Open(ShowType showType, float fadeTime = .0f)
        {
            if (ShowType.None == showType)
            {
                EventSystem.current.sendNavigationEvents = false;
                Close(fadeTime);
            }
            else
            {
                Close();

                EventSystem.current.sendNavigationEvents = true;
                fader.SetActive(true);
                home.Open();

                onMenuOpenStateChanged.Invoke(true);
            }
        }

        public void Close(float fadeTime = .0f)
        {
            fader.SetActive(false);
            CloseAllScreens();

            onMenuOpenStateChanged.Invoke(false);
        }

        public void Back()
        {
            foreach (var screen in screens)
            {
                if (!screen.IsOpen)
                    continue;
                screen.Back();
                return;
            }

            Close();
        }

        private void CloseAllScreens()
        {
            foreach (var screen in screens)
            {
                screen.Close();
            }
        }

        private void CmdMenu(string[] args)
        {
            var fadeTime = 0.0f;
            var show = ShowType.None;
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "0":
                        show = ShowType.None;
                        break;
                    case "2":
                        show = ShowType.InGame;
                        break;
                }
            }

            if (args.Length > 1)
            {
                float.TryParse(args[1], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out fadeTime);
            }

            Open(show, fadeTime);
            ConsoleManager.SetOpen(false);
        }
    }
}