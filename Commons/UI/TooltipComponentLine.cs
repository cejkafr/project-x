﻿using System;
using TMPro;
using UnityEngine;

namespace Game.Commons.UI
{
    public class TooltipComponentLine : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public TextMeshProUGUI titleText;
            public TextMeshProUGUI valueText;
        }

        public Bindings bindings;

        public void SetActive(string title, string value)
        {
            bindings.titleText.text = title;
            bindings.valueText.text = value;
        
            gameObject.SetActive(true);
        }
    }
}
