﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Commons.UI
{
    public class TooltipText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private string title;
        [SerializeField] private string text;

        public void OnPointerEnter(PointerEventData eventData)
        {
            TextTooltip.Instance.Show(title, text);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            TextTooltip.Instance.Hide();
        }
    }
}
