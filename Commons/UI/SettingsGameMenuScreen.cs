﻿using Commons.UI;

namespace Game.Commons.UI
{
    public class SettingsGameMenuScreen : BaseMenuScreen
    {
        public BaseMenuScreen gameSettingsScreen;
        public BaseMenuScreen videoSettingsScreen;
        public BaseMenuScreen graphicSettingsScreen;
        public BaseMenuScreen audioSettingsScreen;
        public BaseMenuScreen keymapSettingsScreen;

        public void OpenGameSettings()
        {
            Close();
            gameSettingsScreen.Open();
        }
    
        public void OpenVideoSettings()
        {
            Close();
            videoSettingsScreen.Open();
        }
    
        public void OpenGraphicSettings()
        {
            Close();
            graphicSettingsScreen.Open();
        }
    
        public void OpenAudioSettings()
        {
            Close();
            audioSettingsScreen.Open();
        }
    
        public void OpenKeymapSettings()
        {
            Close();
            keymapSettingsScreen.Open();
        }
    }
}
