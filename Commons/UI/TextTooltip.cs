﻿using System;
using System.Collections;
using Game.Commons;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Commons.UI
{
    public class TextTooltip : Singleton<TextTooltip>, IPointerEnterHandler, IPointerExitHandler
    {
        [Serializable]
        public struct Bindings
        {
            public RectTransform transform;
            public RectTransform headerTransform;
            public TextMeshProUGUI headerText;
            public TextMeshProUGUI contentText;
        }

        private CanvasGroup canvasGroup;
        private Coroutine delayCoroutine;

        public Bindings bindings;
        public float showDelaySec = 1f;
        public float hideDelaySec = 0.1f;
        public Vector3 offset = Vector3.zero;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            bindings.contentText.lineSpacing = 1f;
        }

        private void Start()
        {
            Instance.Hide();
        }

        public void Show(string title, string text)
        {
            bindings.transform.gameObject.SetActive(false);

            bindings.headerText.text = title;
            bindings.contentText.text = text;

            ShowInternal();
        }

        public void Hide()
        {
            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
            delayCoroutine = StartCoroutine(SetActiveAfterDelay(false, hideDelaySec));
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            bindings.transform.gameObject.SetActive(false);
        }

        private void ShowInternal()
        {
            bindings.headerTransform.gameObject.SetActive(bindings.headerText.text.Length > 0);

            if (delayCoroutine != null) 
                StopCoroutine(delayCoroutine);
            delayCoroutine = StartCoroutine(SetActiveAfterDelay(true, showDelaySec));
        }

        private IEnumerator SetActiveAfterDelay(bool active, float delaySec)
        {
            yield return new WaitForSecondsRealtime(delaySec);
            bindings.transform.position = Input.mousePosition + offset;
            bindings.transform.gameObject.SetActive(active);
            LayoutRebuilder.ForceRebuildLayoutImmediate(bindings.transform);
        }
    }
}