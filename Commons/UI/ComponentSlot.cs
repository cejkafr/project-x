﻿using System;
using Commons.Ships;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Commons.UI
{
    public class ComponentSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
    {
        [Serializable]
        public struct Bindings
        {
            public Image background;
            public Image icon;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private Color highlightedColor;

        private Color normalColor;
        private RectTransform rectTransform;
        private int tileSize;
        private Action<IShipSlot> onPointerDown;
        
        public bool IsShown => gameObject.activeSelf;
        public IShipSlot ShipSlot { get; private set; }

        public void Init(int iTileSize, Action<IShipSlot> onPointerDown)
        {
            normalColor = bindings.background.color;
            tileSize = iTileSize;
            this.onPointerDown = onPointerDown;
        }
        
        public void Show(IShipSlot shipSlot, RectTransform parent)
        {
            ShipSlot = shipSlot;

            if (rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }
            
            rectTransform.SetParent(parent);
            rectTransform.localPosition = ShipSlot.Position * tileSize;

            if (ShipSlot.IsEmpty)
            {
                bindings.icon.sprite = null;
                bindings.icon.color = Color.clear;
            }
            else
            {
                bindings.icon.sprite = ShipSlot.Component.Icon;
                bindings.icon.color = ShipSlot.Component.Rarity.Color; 
            }
            
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            bindings.background.color = highlightedColor;
            
            if (ShipSlot.IsEmpty) return;
            ComponentTooltip.Instance.Show(ShipSlot.Component);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            bindings.background.color = normalColor;
            
            ComponentTooltip.Instance.Hide();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown?.Invoke(ShipSlot);
        }
    }
}