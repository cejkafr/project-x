using System;
using Commons.DataObjects.Ships;
using DataObjects.Ships;
using UnityEngine;
using RangeInt = Game.Types.RangeInt;

namespace Game.Commons.Combat
{
    [Serializable]
    public struct DamageBundle
    {
        public static DamageBundle Single(DamageTypeObject typeObject, RangeInt value)
        {
            return new DamageBundle(Damage.Of(typeObject, value));
        }

        public static DamageBundle Of(params Damage[] values)
        {
            return new DamageBundle(values);
        }
        
        public static readonly DamageBundle Empty =  new DamageBundle();

        [SerializeField] private Damage[] values;

        public Damage[] Values => values;
        public bool IsEmpty => values == null || values.Length == 0;

        public DamageBundle(params Damage[] values)
        {
            this.values = values.Length == 0 ? new Damage[0] : values;
        }
    }
}