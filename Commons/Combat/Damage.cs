using System;
using Commons.DataObjects.Ships;
using DataObjects.Ships;
using Game.Types;
using Random = UnityEngine.Random;

namespace Game.Commons.Combat
{
    [Serializable]
    public struct Damage
    {
        public static Damage Of(DamageTypeObject typeObject, RangeInt value)
        {
            return new Damage(typeObject, value);
        }

        public DamageTypeObject Type;
        public RangeInt Range;

        public float ValueFromRange => Random.Range(Range.min, Range.max);

        public Damage(DamageTypeObject type, RangeInt range)
        {
            Type = type;
            Range = range;
        }
    }
}