﻿using Unity.Mathematics;

namespace Commons.NavProviders
{
    public class Node : IHeapItem<Node>
    {
        public int HeapIndex { get; set; }

        public int2 Position { get; }
        
        public int MovementCost { get; set; }
        public bool Walkable { get; set; } = true;
        public int GCost { get; set; }
        public int HCost { get; set; }
        public Node Parent { get; set; }
        public int FCost => GCost + HCost;

        public Node(int2 position, int movementCost = 0)
        {
            Position = position;
            MovementCost = movementCost;
        }
        
        public int CompareTo(Node nodeToCompare) {
            var compare = FCost.CompareTo(nodeToCompare.FCost);
            if (compare == 0) {
                compare = HCost.CompareTo(nodeToCompare.HCost);
            }
            return -compare;
        }

    }
}