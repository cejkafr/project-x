﻿using System.Collections.Generic;
using UnityEngine;

namespace Commons.NavProviders
{
    public abstract class ABaseGrid : IGrid
    {
        public Node[,] Map { get; }
        public int SizeX => Map.GetLength(0);
        public int SizeY => Map.GetLength(1);

        protected ABaseGrid(Node[,] map, int blurSize)
        {
            Map = map;
            
            BlurPenaltyMap(blurSize);
        }

        private void BlurPenaltyMap(int blurSize)
        {
            var kernelExtents = blurSize * 2 + 1;

            var penaltiesHorizontalPass = new int[SizeX, SizeY];
            var penaltiesVerticalPass = new int[SizeX, SizeY];

            for (var y = 0; y < SizeY; y++)
            {
                for (var x = -kernelExtents; x <= kernelExtents; x++)
                {
                    var sampleX = Mathf.Clamp(x, 0, kernelExtents);
                    penaltiesHorizontalPass[0, y] += Map[sampleX, y].MovementCost;
                }

                for (var x = 1; x < SizeX; x++)
                {
                    var removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, SizeX);
                    var addIndex = Mathf.Clamp(x + kernelExtents, 0, SizeX - 1);

                    penaltiesHorizontalPass[x, y] = penaltiesHorizontalPass[x - 1, y] -
                                                    Map[removeIndex, y].MovementCost +
                                                    Map[addIndex, y].MovementCost;
                }
            }

            for (var x = 0; x < SizeX; x++)
            {
                for (var y = -kernelExtents; y <= kernelExtents; y++)
                {
                    var sampleY = Mathf.Clamp(y, 0, kernelExtents);
                    penaltiesVerticalPass[x, 0] += penaltiesHorizontalPass[x, sampleY];
                }

                var blurredPenalty =
                    Mathf.RoundToInt((float) penaltiesVerticalPass[x, 0] / (kernelExtents * kernelExtents));
                Map[x, 0].MovementCost = blurredPenalty;

                for (var y = 1; y < SizeY; y++)
                {
                    var removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, SizeY);
                    var addIndex = Mathf.Clamp(y + kernelExtents, 0, SizeY - 1);

                    penaltiesVerticalPass[x, y] = penaltiesVerticalPass[x, y - 1] -
                                                  penaltiesHorizontalPass[x, removeIndex] +
                                                  penaltiesHorizontalPass[x, addIndex];
                    blurredPenalty =
                        Mathf.RoundToInt((float) penaltiesVerticalPass[x, y] / (kernelExtents * kernelExtents));
                    Map[x, y].MovementCost = blurredPenalty;
                }
            }
        }

        public List<Node> GetNeighbours(Node node)
        {
            var neighbours = new List<Node>(4);

            for (var x = -1; x <= 1; x++)
            {
                for (var y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    var checkX = node.Position.x + x;
                    var checkY = node.Position.y + y;

                    if (checkX >= 0 && checkX < SizeX && checkY >= 0 && checkY < SizeY)
                    {
                        neighbours.Add(Map[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }
    }
}