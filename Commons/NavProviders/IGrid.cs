﻿using System.Collections.Generic;

namespace Commons.NavProviders
{
    public interface IGrid
    {
        Node[,] Map { get; }
        int SizeX { get; }
        int SizeY { get; }
        List<Node> GetNeighbours(Node node);
    }
}