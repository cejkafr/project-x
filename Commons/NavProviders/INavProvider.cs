﻿using System.Collections.Generic;
using Unity.Mathematics;

namespace Commons.NavProviders
{
    public interface INavProvider
    {
        int2[] FindPathWaypoints(int2 startPos, int2 targetPos, out int length);
    }
}