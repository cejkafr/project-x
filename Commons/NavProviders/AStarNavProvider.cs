﻿using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Commons.NavProviders
{
    public class AStarNavProvider : INavProvider
    {
        private const int DirectCost = 10;
        private const int DiagonalCost = 14;

        private readonly IGrid grid;
        private readonly int diagonalCost;

        public AStarNavProvider(IGrid grid, bool penalizeDiagonal = true)
        {
            this.grid = grid;
            diagonalCost = penalizeDiagonal ? DiagonalCost : DirectCost;
        }

        public int2[] FindPathWaypoints(int2 startPos, int2 targetPos, out int length)
        {
            length = 0;

            var waypoints = new int2[0];
            var pathSuccess = false;

            var startNode = grid.Map[startPos.x, startPos.y];
            var targetNode = grid.Map[targetPos.x, targetPos.y];

            var openSet = new Heap<Node>(grid.SizeX * grid.SizeY);
            var closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (!openSet.IsEmpty())
            {
                var currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    pathSuccess = true;
                    break;
                }

                foreach (var neighbour in grid.GetNeighbours(currentNode))
                {
                    if (!neighbour.Walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    var newMovementCostToNeighbour =
                        currentNode.GCost + GetDistance(currentNode, neighbour) + neighbour.MovementCost;
                    if (newMovementCostToNeighbour < neighbour.GCost || !openSet.Contains(neighbour))
                    {
                        neighbour.GCost = newMovementCostToNeighbour;
                        neighbour.HCost = GetDistance(neighbour, targetNode);
                        neighbour.Parent = currentNode;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                        else
                        {
                            openSet.UpdateItem(neighbour);
                        }
                    }
                }
            }

            if (pathSuccess)
            {
                var path = RetracePath(startNode, targetNode);
                length = path.Count;
                waypoints = SimplifyPath(path);
                Array.Reverse(waypoints);
            }

            return waypoints;
        }

        private int GetDistance(Node nodeA, Node nodeB)
        {
            var dstX = Mathf.Abs(nodeA.Position.x - nodeB.Position.x);
            var dstY = Mathf.Abs(nodeA.Position.y - nodeB.Position.y);

            if (dstX > dstY)
                return diagonalCost * dstY + DirectCost * (dstX - dstY);
            return diagonalCost * dstX + DirectCost * (dstY - dstX);
        }

        private List<Node> RetracePath(Node startNode, Node endNode)
        {
            var path = new List<Node>();
            var currNode = endNode;

            while (currNode != startNode)
            {
                path.Add(currNode);
                currNode = currNode.Parent;
            }

            return path;
        }

        private int2[] SimplifyPath(IReadOnlyList<Node> path)
        {
            var waypoints = new List<int2>();
            var dirOld = int2.zero;

            waypoints.Add(path[0].Position);
            for (var i = 1; i < path.Count; i++)
            {
                var dirNew = path[i - 1].Position - path[i].Position;
                var eq = dirNew == dirOld;
                if (!eq.x || !eq.y)
                {
                    waypoints.Add(path[i].Position);
                }

                dirOld = dirNew;
            }

            waypoints.Add(path[path.Count - 1].Position);
            return waypoints.ToArray();
        }
    }
}