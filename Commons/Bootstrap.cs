﻿using Game;
using Game.DI;
using Games;
using UnityEngine;

namespace Commons
{
    public abstract class Bootstrap : ManagedMonoBehaviour
    {
        public static T LoadObject<T>(string name) where T : ScriptableObject
        {
            return Resources.Load($"Prefabs/{name}") as T;
        }

        public static GameObject LoadPrefab(string name)
        {
            return Resources.Load($"Prefabs/{name}") as GameObject;
        }

        public static GameObject InstantiatePrefab(string name)
        {
            return Instantiate(LoadPrefab(name));
        }

        public static T InstantiatePrefab<T>(string name) where T : Object
        {
            return Instantiate(LoadPrefab(name)) as T;
        }

        protected override void Awake()
        {
            if (FindObjectOfType<GameManager>() == null)
            {
                InitSystem();
            }

            base.Awake();
        }

        private void InitSystem()
        {
            var inst = InstantiatePrefab("GameManager");
        }
    }
}