﻿using UnityEngine;

namespace Commons
{
    public class PlaneRaycaster : MonoBehaviour, IPlaneRaycaster
    {
        private Plane plane = new Plane(Vector3.up, Vector3.zero);
        private Ray ray;

        [SerializeField] private Camera rayCamera;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private float maxDistance;

        public Vector3 CursorPosition()
        {
            ray = rayCamera.ScreenPointToRay(Input.mousePosition);
            return plane.Raycast(ray, out var enter) ? ray.GetPoint(enter) : Vector3.zero;
        }

        public RaycastCursorTarget RaycastCursor()
        {
            ray = rayCamera.ScreenPointToRay(Input.mousePosition);
            return !Physics.Raycast(ray, out var hit, maxDistance, layerMask,
                QueryTriggerInteraction.Ignore)
                ? RaycastCursorTarget.None
                : RaycastCursorTarget.Hit(hit);
        }
    }
}