﻿using UnityEngine;

namespace Commons.DataObjects
{
    [CreateAssetMenu(menuName = "Game/Rarity")]
    public class Rarity : ScriptableObject
    {
        [SerializeField] private new string name;
        [SerializeField] private Color color;

        public string Name => name;
        public Color Color => color;
    }
}
