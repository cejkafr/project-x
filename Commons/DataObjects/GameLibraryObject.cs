﻿using Commons.DataObjects.Ships;
using Commons.Factions;
using Commons.Sectors;
using Commons.Ships;
using Commons.Ships.Components;
using UnityEngine;

namespace Commons.DataObjects
{
    [CreateAssetMenu(menuName = "Game/Library")]
    public class GameLibraryObject : ScriptableObject
    {
        public FactionObject playerFaction;
        public FactionObject[] factions;
        public FactionAttitudeTranslator factionAttitudeTranslator;
        public SectorBiomeObject[] biomes;
        public GateSectorObject gate;
        public FleetSectorObject fleet;
        public StationSectorObject station;
        public ShipClassObject[] shipClasses;
        public AComponentObject[] shipComponents;
        public GameObject[] bgObjectPrefabs;
        public GameObject[] asteroidPrefabs;
        public GameObject[] nebulaPrefabs;
        public Cubemap[] skyboxes;
        public string musicTrackPrefix;
        public int musicTrackCount;
    }
}