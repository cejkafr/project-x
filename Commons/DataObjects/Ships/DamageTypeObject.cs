using UnityEngine;

namespace Commons.DataObjects.Ships
{
    [CreateAssetMenu(menuName = "Ships/Damage Type")]
    public class DamageTypeObject : ScriptableObject
    {
        public string title;
        public float shieldMultiplier = 1f;
        public float armorMultiplier = 1f;
        public float hullMultiplier = 1f;
    }
}