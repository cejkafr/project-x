﻿using UnityEngine;

namespace Commons.DataObjects.Ships
{
    [CreateAssetMenu(menuName = "Ships/Name List")]
    public class NameListObject : ScriptableObject
    {
        public string[] first;
        public string[] second;
    }
}