﻿using Commons.DataObjects.Ships;
using UnityEngine;

namespace DataObjects.Ships
{
    [CreateAssetMenu(menuName = "Ship/Crew Member")]
    public class CrewMemberObject : APersonObject
    {
        
    }
}