﻿using UnityEngine;

namespace Commons.DataObjects.Ships
{
    public abstract class APersonObject : ScriptableObject
    {
        public new string name;
        [TextArea]
        public string description;

        public string race;
    }
}