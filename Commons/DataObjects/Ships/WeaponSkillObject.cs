﻿using Commons.DataObjects.Ships;
using UnityEngine;

namespace DataObjects.Ships
{
    [CreateAssetMenu(menuName = "Ship/Weapon Skill")]
    public class WeaponSkillObject : SkillObject
    {
        public float cooldownSec;
    }
}