﻿using UnityEngine;

namespace Commons.DataObjects.Ships
{
    public abstract class SkillObject : ScriptableObject
    {
        public string title;
        public string description;
        public Sprite icon;
    }
}
