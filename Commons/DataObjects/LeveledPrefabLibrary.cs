﻿using System;
using UnityEngine;

namespace Commons.DataObjects
{
    [CreateAssetMenu(menuName = "Game/Prefab Library")]
    public class LeveledPrefabLibrary : ScriptableObject
    {
        [Serializable]
        public struct PrefabList
        {
            public GameObject[] prefabs;
        }

        public PrefabList[] levels;
    }
}
