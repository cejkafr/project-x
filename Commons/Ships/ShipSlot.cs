﻿using System;
using Commons.Ships.Components;
using UnityEngine;

namespace Commons.Ships
{
    public interface IShipSlot
    {
        ShipSlotSize Size { get; }
        Vector2 Position { get; }
        Type ComponentType { get; }
        IComponent Component { get; }
        bool IsEmpty { get; }

        bool CanEquip(IComponent component);
        bool Equip(IComponent component);
        bool UnEquip();
    }
    
    public class ShipSlot<T> : IShipSlot where T : IComponent
    {
        private readonly ShipSlotObject template;

        public ShipSlotSize Size => template.size;
        public Vector2 Position => template.position;
        public Type ComponentType => typeof(T);
        public IComponent Component => ComponentTyped;
        public T ComponentTyped { get; private set; }
        public bool IsEmpty => Component == null;

        public ShipSlot(ShipSlotObject template)
        {
            this.template = template;
        }

        public bool CanEquip(IComponent component)
        {
            if (!ComponentType.IsInstanceOfType(component)
                && !component.GetType().IsAssignableFrom(ComponentType))
                return false;
            return component.Size == Size;
        }
        
        public bool CanEquip(T component)
        {
            return component.Size == Size;
        }

        public bool Equip(IComponent component)
        {
            if (!IsEmpty || !CanEquip(component))
                return false;
            ComponentTyped = (T) component;
            return true;
        }
        
        public bool Equip(T component)
        {
            if (!IsEmpty || !CanEquip(component))
                return false;
            ComponentTyped = component;
            return true;
        }

        public bool UnEquip()
        {
            ComponentTyped = default;
            return true;
        }
    }
}