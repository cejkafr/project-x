﻿using Games.Audio;
using UnityEngine;

namespace Commons.Ships
{
    [CreateAssetMenu(menuName = "Ship/Pilot")]
    public class PilotObject : ScriptableObject
    {
        [Header("SFX")]
        public WeakSoundDef quoteSelect;
        public WeakSoundDef quoteCommand;
        public WeakSoundDef quoteAttack;
        public WeakSoundDef quoteEnemyDetected;
        public WeakSoundDef quoteDestroyed;
    }
}