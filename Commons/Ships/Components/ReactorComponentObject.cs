﻿using UnityEngine;

namespace Commons.Ships.Components
{
    [CreateAssetMenu(menuName = "Ship/Component/Reactor")]
    public class ReactorComponentObject : AComponentObject
    {
        [Header("Reactor")]
        public int power;
        public int capacity;
    }
}