﻿using System.Globalization;
using DataObjects.Ships;
using Game.Commons.Combat;
using UnityEngine;

namespace Commons.Ships.Components
{
    public class WeaponComponent : Component<WeaponComponentObject>, IWeaponComponent
    {
        public int Range => Template.range;
        public float AttackDelaySec => Template.attackDelaySec;
        public WeaponPrecision[] Precision => Template.precision;
        public int CriticalChance => Template.criticalChance;
        public int ShieldPenetrationChance => Template.shieldPenetrationChance;
        public bool Burst => BurstCount > 1;
        public int BurstCount => Template.burstCount;
        public float BurstDelaySec => Template.burstDelaySec;
        public bool BurstCycleOutputs => Template.burstCycleOutputs;
        public bool BurstPlaySoundOnce => Template.burstPlaySoundOnce;
        public WeaponVisualisationMethod Visualisation => Template.visualisation;
        public float ProjectileSpeed => Template.projectileSpeed;
        public float ProjectileLiveTimeSec => Template.projectileLiveTimeSec;
        public GameObject WeaponPrefab => Template.weaponPrefab;
        public GameObject ProjectilePrefab => Template.projectilePrefab;
        public DamageBundle Damage => Template.projectileDamage;
        public WeaponSkillObject[] Skills => Template.skills;
        public bool HasSkills => Skills.Length != 0;

        public override ComponentStat[] Stats => new[]
        {
            new ComponentStat("Range", Range.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Damage",
                $"{Damage.Values[0].Type.title} {Damage.Values[0].Range.min.ToString(CultureInfo.InvariantCulture)} - {Damage.Values[0].Range.max.ToString(CultureInfo.InvariantCulture)}"),
            new ComponentStat("Attack delay", AttackDelaySec.ToString(CultureInfo.CurrentCulture)),
            // new ComponentStat("Precision", $"{Precision.min.ToString(CultureInfo.InvariantCulture)} - {Precision.max.ToString(CultureInfo.InvariantCulture)}"),
            new ComponentStat("Critical chance", CriticalChance.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Shield penetration", ShieldPenetrationChance.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Energy consumption", EnergyConsumption.ToString()),
        };

        public WeaponComponent(WeaponComponentObject template) : base(template)
        {
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}";
        }
    }
}