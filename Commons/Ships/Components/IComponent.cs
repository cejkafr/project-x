﻿using Commons.DataObjects;
using Commons.Items;
using UnityEngine;

namespace Commons.Ships.Components
{
    public interface IComponent : IItem, IInventoryItem, IEnergyConsumer
    {
        Rarity Rarity { get; }
        ShipSlotSize Size { get; }
        ComponentStat[] Stats { get; }
    }
}