﻿namespace Commons.Ships.Components
{
    public interface ISensorsComponent : ISensors, IAuxiliaryComponent
    {

    }
}