﻿using DataObjects.Ships;

namespace Commons.Ships.Components
{
    public class ReactorComponent : Component<ReactorComponentObject>, IReactorComponent
    {
        public int Power => Template.power;
        public int Capacity => Template.capacity;

        public override ComponentStat[] Stats => new[]
        {
            new ComponentStat("Power", Power.ToString()),
            new ComponentStat("Capacity", Capacity.ToString()),
            new ComponentStat("Energy consumption", EnergyConsumption.ToString()),
        };

        public ReactorComponent(ReactorComponentObject template) : base(template)
        {
        }
    }
}