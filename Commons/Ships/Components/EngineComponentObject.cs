﻿using Games.Audio;
using UnityEngine;

namespace Commons.Ships.Components
{
    [CreateAssetMenu(menuName = "Ship/Component/Engine")]
    public class EngineComponentObject : AComponentObject
    {
        [Header("Engine")]
        public float speed;
        public float acceleration;
        public float rotation;
                
        [Header("SFX")]
        public WeakSoundDef loopSfx;   
    }
}