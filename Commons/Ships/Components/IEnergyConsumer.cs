﻿namespace Commons.Ships.Components
{
    public interface IEnergyConsumer
    {
        int EnergyConsumption { get; }
    }
}