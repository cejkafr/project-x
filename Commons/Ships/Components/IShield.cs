﻿namespace Commons.Ships.Components
{
    public interface IShield : IEnergyConsumer
    {
        int Capacity { get; }
        float RechargeRate { get; }
        float RechargeDelay { get; }
    }
}