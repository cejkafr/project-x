﻿namespace Commons.Ships.Components
{
    public interface ISensors : IAuxiliary
    {
        int ShortRange { get; }
        int LongRange { get; }
    }
}