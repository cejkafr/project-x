﻿namespace Commons.Ships.Components
{
    public interface IWeaponComponent : IWeapon, IComponent
    {
    }
}