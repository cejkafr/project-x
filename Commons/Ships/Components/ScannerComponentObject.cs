﻿using Commons.Ships.Components;
using UnityEngine;

namespace Commons.DataObjects.Ships
{
    [CreateAssetMenu(menuName = "Ship/Component/Scanner")]
    public class ScannerComponentObject : AAuxiliaryComponentObject
    {
        [Header("Scanner")]
        public int longRange;
        public int shortRange;
    }
}