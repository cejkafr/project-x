﻿using UnityEngine;

namespace Commons.Ships.Components
{
    [CreateAssetMenu(menuName = "Ship/Component/Shield")]
    public class ShieldComponentObject : AComponentObject
    {
        [Header("Shield")]
        public int capacity;
        public float rechargeRate;
        public float rechargeDelay;
    }
}