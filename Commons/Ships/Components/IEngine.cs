﻿using Games.Audio;

namespace Commons.Ships.Components
{
    public interface IEngine : IEnergyConsumer
    {
        float MaxSpeed { get; }
        float Acceleration { get; }
        float RotationSpeedRad { get; }

        WeakSoundDef LoopSfx { get; }
    }
}