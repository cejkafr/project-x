﻿namespace Commons.Ships.Components
{
    public abstract class AuxiliaryComponent : Component<AAuxiliaryComponentObject>, IAuxiliaryComponent
    {
        protected AuxiliaryComponent(AAuxiliaryComponentObject template) : base(template)
        {
        }
    }
}