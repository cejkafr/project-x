﻿using Commons.DataObjects.Ships;

namespace Commons.Ships.Components
{
    public class SensorsComponent : AuxiliaryComponent, ISensorsComponent
    {
        private new ScannerComponentObject Template { get; }
        public int ShortRange => Template.shortRange;
        public int LongRange => Template.longRange;

        public override ComponentStat[] Stats => new[]
        {
            new ComponentStat("Short range", ShortRange.ToString()),
            new ComponentStat("Long range", LongRange.ToString()),
            new ComponentStat("Energy consumption", EnergyConsumption.ToString()),
        };

        public SensorsComponent(ScannerComponentObject template) : base(template)
        {
            Template = template;
        }
    }
}