﻿using Commons.DataObjects;
using Commons.Items;

namespace Commons.Ships.Components
{
    public abstract class AComponentObject : AItemObject
    {
        public Rarity rarity;
        public short tier = 1;
        public ShipSlotSize size;
        public int energyConsumption;
    }
}