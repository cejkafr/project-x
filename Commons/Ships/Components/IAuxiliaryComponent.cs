﻿namespace Commons.Ships.Components
{
    public interface IAuxiliaryComponent : IAuxiliary, IComponent
    {
    }
}