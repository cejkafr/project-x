﻿using System.Globalization;

namespace Commons.Ships.Components
{
    public class ShieldComponent : Component<ShieldComponentObject>, IShieldComponent
    {
        public int Capacity => Template.capacity;
        public float RechargeRate => Template.rechargeRate;
        public float RechargeDelay => Template.rechargeDelay;

        public override ComponentStat[] Stats => new[]
        {
            new ComponentStat("Capacity", Capacity.ToString()),
            new ComponentStat("Recharge rate", RechargeRate.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Recharge delay", RechargeDelay.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Energy consumption", EnergyConsumption.ToString()),
        };

        public ShieldComponent(ShieldComponentObject template) : base(template)
        {
        }
    }
}