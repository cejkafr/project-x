﻿namespace Commons.Ships.Components
{
    public interface IReactor
    {
        int Power { get; }
        int Capacity { get; }
    }
}