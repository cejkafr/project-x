﻿using System;
using DataObjects.Ships;
using Game.Commons.Combat;
using UnityEngine;

namespace Commons.Ships.Components
{
    public enum WeaponType
    {
        Physical,
        Laser,
        Energy,
        Missile,
    }

    public enum WeaponVisualisationMethod
    {
        ConeOfFire,
        Trajectory,
    }

    [Serializable]
    public struct WeaponPrecision
    {
        public int distance;
        public int precision;
    }
    
    [CreateAssetMenu(menuName = "Ship/Component/Weapon")]
    public class WeaponComponentObject : AComponentObject
    {
        [Header("Weapon")]
        public WeaponType type;
        public WeaponVisualisationMethod visualisation;
        public int range;
        public float attackDelaySec;
        public WeaponPrecision[] precision;
        public int criticalChance;
        public int shieldPenetrationChance;
        public GameObject weaponPrefab;

        [Header("Projectile")]
        public GameObject projectilePrefab;
        public float projectileSpeed = 200f;
        public float projectileLiveTimeSec = 10f;
        public DamageBundle projectileDamage;

        [Header("Burst")]
        public int burstCount = 1;
        public float burstDelaySec = .2f;
        public bool burstCycleOutputs;
        public bool burstPlaySoundOnce;

        [Header("Skills")]
        public WeaponSkillObject[] skills;
    }
}