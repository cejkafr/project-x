﻿namespace Commons.Ships.Components
{
    public interface IEngineComponent : IComponent, IEngine
    {

    }
}