﻿using DataObjects.Ships;
using Game.Commons.Combat;
using UnityEngine;

namespace Commons.Ships.Components
{
    public interface IWeapon : IEnergyConsumer
    {
        int Range { get; }
        float AttackDelaySec { get; }
        WeaponPrecision[] Precision { get; }
        int CriticalChance { get; }
        int ShieldPenetrationChance { get; }
        bool Burst { get; }
        int BurstCount { get; }
        float BurstDelaySec { get; }
        bool BurstCycleOutputs { get; }
        bool BurstPlaySoundOnce { get; }
        WeaponVisualisationMethod Visualisation { get; }
        float ProjectileSpeed { get; }
        float ProjectileLiveTimeSec { get; }
        GameObject WeaponPrefab { get; }
        GameObject ProjectilePrefab { get; }
        DamageBundle Damage { get; }
        WeaponSkillObject[] Skills { get; }
        bool HasSkills { get; }
    }
}