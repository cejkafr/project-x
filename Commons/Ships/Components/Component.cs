﻿using Commons.DataObjects;
using Commons.Items;
using UnityEngine;

namespace Commons.Ships.Components
{
    public readonly struct ComponentStat
    {
        public readonly string Title;
        public readonly string Value;

        public ComponentStat(string title, string value)
        {
            Title = title;
            Value = value;
        }
    }

    public abstract class Component<T> : IComponent where T : AComponentObject
    {
        public AItemObject ItemTemplate => Template;
        public T Template { get; }
        public string Name => Template.name;
        public string Description => Template.description;
        public bool Stackable => false;
        public int Amount => 1;
        public Sprite Icon => Template.icon;
        public Rarity Rarity => Template.rarity;
        public ShipSlotSize Size => Template.size;
        public int Value => Template.value;
        public int EnergyConsumption => Template.energyConsumption;
        public virtual ComponentStat[] Stats => new ComponentStat[0];

        protected Component(T template)
        {
            Template = template;
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Description)}: {Description}, {nameof(Size)}: {Size}";
        }
    }
}