﻿using System;
using Commons.DataObjects.Ships;

namespace Commons.Ships.Components
{
    public static class ComponentFactory
    {
        public static IComponent Create(AComponentObject aComponentObject)
        {
            if (aComponentObject == null)
            {
                return null;
            }

            switch (aComponentObject)
            {
                case ReactorComponentObject c:
                    return new ReactorComponent(c);
                case EngineComponentObject c:
                    return new EngineComponent(c);
                case ShieldComponentObject c:
                    return new ShieldComponent(c);
                case ScannerComponentObject c:
                    return new SensorsComponent(c);
                case WeaponComponentObject c:
                    return new WeaponComponent(c);
                default:
                    throw new ArgumentException("Unknown component.");
            }
        }
    }
}