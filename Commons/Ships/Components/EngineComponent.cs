﻿using System.Globalization;
using Games.Audio;
using UnityEngine;

namespace Commons.Ships.Components
{
    public class EngineComponent : Component<EngineComponentObject>, IEngineComponent
    {
        public float MaxSpeed => Template.speed;
        public float Acceleration => Template.acceleration;
        public float RotationSpeedRad => Template.rotation * Mathf.Deg2Rad;
        public WeakSoundDef LoopSfx => Template.loopSfx;

        public override ComponentStat[] Stats => new[]
        {
            new ComponentStat("Max Speed", MaxSpeed.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Acceleration", Acceleration.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Rotation speed", Template.rotation.ToString(CultureInfo.CurrentCulture)),
            new ComponentStat("Energy consumption", EnergyConsumption.ToString()),
        };

        public EngineComponent(EngineComponentObject template) : base(template)
        {
        }
    }
}