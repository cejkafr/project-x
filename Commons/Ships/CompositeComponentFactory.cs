﻿using Commons.Ships.Components;

namespace Commons.Ships
{
    public static class CompositeComponentFactory
    {
        public static CompositeReactor Create(ShipSlot<IReactorComponent>[] slots)
        {
            var composite = new CompositeReactor(slots.Length);
            for (var i = 0; i < slots.Length; i++)
            {
                if (slots[i].IsEmpty) continue;
                composite.Set(i, slots[i].ComponentTyped);
            }

            return composite;
        }   
        
        public static CompositeEngine Create(ShipSlot<IEngineComponent>[] slots)
        {
            var composite = new CompositeEngine(slots.Length);
            for (var i = 0; i < slots.Length; i++)
            {
                if (slots[i].IsEmpty) continue;
                composite.Set(i, slots[i].ComponentTyped);
            }

            return composite;
        } 
        
        public static CompositeShield Create(ShipSlot<IShieldComponent>[] slots)
        {
            var composite = new CompositeShield(slots.Length);
            for (var i = 0; i < slots.Length; i++)
            {
                if (slots[i].IsEmpty) continue;
                composite.Set(i, slots[i].ComponentTyped);
            }

            return composite;
        } 
        
        public static CompositeSensors Create(ShipSlot<IAuxiliaryComponent>[] slots)
        {
            var composite = new CompositeSensors(slots.Length);
            for (var i = 0; i < slots.Length; i++)
            {
                if (slots[i].IsEmpty) continue;
                composite.Set(i, slots[i].Component as ISensorsComponent);
            }

            return composite;
        } 
    }
}