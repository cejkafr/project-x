﻿using System.Collections.Generic;
using System.Linq;
using Commons.Factions;
using Commons.Ships.Components;
using Games.Types;
using UnityEngine;

namespace Commons.Ships
{
    public class Ship : IShip
    {
        private readonly ShipClassObject shipClass;
        public string Name { get; }
        public IFaction Faction { get; }

        public string ClassName => shipClass.title;
        public ShipClassSize ClassSize => shipClass.size;
        public PilotObject Pilot { get; }
        public ShipClassSize Size => shipClass.size;
        public Sprite IconSide => shipClass.iconSide;
        public Sprite IconTop => shipClass.iconTop;
        public GameObject GamePrefab => shipClass.gamePrefab;

        public ShipInventory Inventory { get; }

        public ShipSlot<IReactorComponent>[] ReactorSlots { get; }
        public ShipSlot<IEngineComponent>[] EngineSlots { get; }
        public ShipSlot<IShieldComponent>[] ShieldSlots { get; }
        public ShipSlot<IAuxiliaryComponent>[] AuxiliarySlots { get; }
        public WeaponShipSlot[] WeaponSlots { get; }

        public MaxValue HullPoints { get; }
        public MaxValue FrontArmour { get; }
        public MaxValue SideArmour { get; }
        public MaxValue RearArmour { get; }

        public IEnumerable<StanceSkillObject> Stances => shipClass.stances;

        public Ship(string name, PilotObject pilot, ShipClassObject shipClass, IFaction faction)
        {
            Name = name;
            Pilot = pilot;
            this.shipClass = shipClass;
            Faction = faction;

            HullPoints = new MaxValue(shipClass.hullPoints);
            FrontArmour = new MaxValue(shipClass.frontArmour);
            SideArmour = new MaxValue(shipClass.sideArmour);
            RearArmour = new MaxValue(shipClass.rearArmour);

            Inventory = new ShipInventory(shipClass.cargoCapacity);

            ReactorSlots = new ShipSlot<IReactorComponent>[shipClass.reactorSlots.Length];
            for (var i = 0; i < ReactorSlots.Length; i++)
                ReactorSlots[i] = new ShipSlot<IReactorComponent>(shipClass.reactorSlots[i]);
            EngineSlots = new ShipSlot<IEngineComponent>[shipClass.engineSlots.Length];
            for (var i = 0; i < EngineSlots.Length; i++)
                EngineSlots[i] = new ShipSlot<IEngineComponent>(shipClass.engineSlots[i]);
            ShieldSlots = new ShipSlot<IShieldComponent>[shipClass.shieldSlots.Length];
            for (var i = 0; i < ShieldSlots.Length; i++)
                ShieldSlots[i] = new ShipSlot<IShieldComponent>(shipClass.shieldSlots[i]);
            AuxiliarySlots = new ShipSlot<IAuxiliaryComponent>[shipClass.auxiliarySlots.Length];
            for (var i = 0; i < AuxiliarySlots.Length; i++)
                AuxiliarySlots[i] = new ShipSlot<IAuxiliaryComponent>(shipClass.auxiliarySlots[i]);
            WeaponSlots = new WeaponShipSlot[shipClass.weaponSlots.Length];
            for (var i = 0; i < WeaponSlots.Length; i++)
                WeaponSlots[i] = new WeaponShipSlot(shipClass.weaponSlots[i]);
        }

        public Ship(string name, PilotObject pilot, ShipClassObject shipClass, IFaction faction,
            float hullPoints, float frontArmour, float sideArmours, float rearArmour) :
            this(name, pilot, shipClass, faction)
        {
            HullPoints.SetWithoutNotify(hullPoints);
            FrontArmour.SetWithoutNotify(frontArmour);
            SideArmour.SetWithoutNotify(sideArmours);
            RearArmour.SetWithoutNotify(rearArmour);
        }

        public List<IComponent> GetComponents()
        {
            var list = new List<IComponent>(ReactorSlots.Length + EngineSlots.Length +
                                            ShieldSlots.Length + WeaponSlots.Length + AuxiliarySlots.Length);
            list.AddRange(from slot in ReactorSlots where !slot.IsEmpty select slot.Component);
            list.AddRange(from slot in EngineSlots where !slot.IsEmpty select slot.Component);
            list.AddRange(from slot in ShieldSlots where !slot.IsEmpty select slot.Component);
            list.AddRange(from slot in AuxiliarySlots where !slot.IsEmpty select slot.Component);
            list.AddRange(from slot in WeaponSlots where !slot.IsEmpty select slot.Component);
            return list;
        }

        public override string ToString()
        {
            return $"{nameof(ClassName)}: {ClassName}, {nameof(Size)}: {Size}, {nameof(Faction)}: {Faction}";
        }
    }
}