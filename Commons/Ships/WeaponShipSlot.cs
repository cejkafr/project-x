﻿using Commons.Ships.Components;

namespace Commons.Ships
{
    public class WeaponShipSlot : ShipSlot<IWeaponComponent>
    {
        public float FireAngle { get; }
        
        public WeaponShipSlot(WeaponShipSlotObject template) : base(template)
        {
            FireAngle = template.maxFiringAngle;
        }
    }
}