﻿using System;
using Commons.Ships.Components;
using UnityEngine;

namespace Commons.Ships
{
    public enum ShipClassSize
    {
        Fighter = 0,
        Bomber = 1,
        Cruiser = 2,
        Carrier = 3,
    }
    
    public enum ShipWeaponSlotType
    {
        Cannon = 0,
        Turret = 1,
    }

    public enum ShipSlotSize
    {
        S = 0,
        M = 1,
        L = 2,
    }

    [Serializable]
    public class ShipSlotObject
    {
        public ShipSlotSize size;
        public Vector2 position;

        public override string ToString()
        {
            return $"{nameof(size)}: {size}";
        }
    }
        
    [Serializable]
    public class WeaponShipSlotObject : ShipSlotObject
    {
        public ShipWeaponSlotType type;
        public float maxFiringAngle = 90f;
    }

    [Serializable]
    public struct ShipEquipmentVariant
    {
        public ReactorComponentObject[] reactors;
        public EngineComponentObject[] engines;
        public ShieldComponentObject[] shields;
        public AAuxiliaryComponentObject[] auxiliary;
        public WeaponComponentObject[] weapons;
    }

    [CreateAssetMenu(menuName = "Ship/Class")]
    public class ShipClassObject : ScriptableObject
    {
        public string title;
        [TextArea]
        public string description;
        public ShipClassSize size;
        public int value;
        public Sprite iconSide;
        public Sprite iconTop;
        public GameObject gamePrefab;
        public int cargoCapacity;
        
        [Header("Combat")]
        public int hullPoints;
        public int frontArmour;
        public int sideArmour;
        public int rearArmour;

        [Header("Crew")] 
        public int crewTotalCount;
        public int commandingOfficerCount;
        public int scienceOfficerCount;
        public int engineerOfficerCount;
        public int weaponOfficerCount;
        public int pilotCount;

        [Header("Slots")]
        public ShipSlotObject[] reactorSlots;
        public ShipSlotObject[] engineSlots;
        public ShipSlotObject[] shieldSlots;
        public ShipSlotObject[] auxiliarySlots;
        public WeaponShipSlotObject[] weaponSlots;
        
        [Header("Other")]
        public StanceSkillObject[] stances;
        public ShipEquipmentVariant[] equipmentVariants;

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(title)}: {title}, {nameof(size)}: {size}";
        }
    }
}