﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Ships.Components;
using Games.Types;
using UnityEngine;

namespace Commons.Ships
{
    public interface IShip
    {
        string Name { get; }

        IFaction Faction { get; }
        string ClassName { get; }
        ShipClassSize ClassSize { get; }
        PilotObject Pilot { get; }
        ShipClassSize Size { get; }
        Sprite IconSide { get; }
        Sprite IconTop { get; }
        GameObject GamePrefab { get; }

        MaxValue HullPoints { get; }
        MaxValue FrontArmour { get; }
        MaxValue SideArmour { get; }
        MaxValue RearArmour { get; }

        ShipInventory Inventory { get; }
        
        IEnumerable<StanceSkillObject> Stances { get; }

        ShipSlot<IReactorComponent>[] ReactorSlots { get; }
        ShipSlot<IEngineComponent>[] EngineSlots { get; }
        ShipSlot<IShieldComponent>[] ShieldSlots { get; }
        ShipSlot<IAuxiliaryComponent>[] AuxiliarySlots { get; }
        WeaponShipSlot[] WeaponSlots { get; }

        List<IComponent> GetComponents();
    }
}