﻿using System.Collections.Generic;
using Commons.Ships.Components;
using Games.Audio;

namespace Commons.Ships
{
    public abstract class AComposite<T> where T : IComponent
    {
        public T[] Components { get; }

        public AComposite(int size)
        {
            Components = new T[size];
        }

        public virtual void Set(int index, T component)
        {
            if (Components[index] != null)
            {
                Remove(index);
            }

            AddWithoutNotify(index, component);

            OnComponentAdd(index, component);
            OnComponentChange();
        }

        public virtual T Remove(int index)
        {
            if (Components[index] == null)
            {
                return default;
            }

            var c = RemoveWithoutNotify(index);

            OnComponentRemove(index, c);
            OnComponentChange();

            return c;
        }

        protected virtual void AddWithoutNotify(int index, T component)
        {
            Components[index] = component;
        }

        protected virtual T RemoveWithoutNotify(int index)
        {
            var c = Components[index];
            Components[index] = default;
            return c;
        }

        protected virtual void OnComponentAdd(int index, T component)
        {
        }

        protected virtual void OnComponentRemove(int index, T component)
        {
        }

        protected virtual void OnComponentChange()
        {
        }
    }

    public abstract class ACompositeEnergyConsumer<T> : AComposite<T> where T : IComponent
    {
        public int EnergyConsumption { get; private set; }

        protected ACompositeEnergyConsumer(int size) : base(size)
        {
        }

        protected override void AddWithoutNotify(int index, T component)
        {
            base.AddWithoutNotify(index, component);
            EnergyConsumption += component.EnergyConsumption;
        }

        protected override T RemoveWithoutNotify(int index)
        {
            var c = base.RemoveWithoutNotify(index);
            EnergyConsumption -= c.EnergyConsumption;
            return c;
        }
    }

    public class CompositeReactor : AComposite<IReactorComponent>, IReactor
    {
        public int Power { get; private set; }
        public int Capacity { get; private set; }

        public CompositeReactor(int size) : base(size)
        {
        }

        protected override void OnComponentAdd(int i, IReactorComponent c)
        {
            Power += c.Power;
            Capacity += c.Capacity;
        }

        protected override void OnComponentRemove(int i, IReactorComponent c)
        {
            Power -= c.Power;
            Capacity -= c.Capacity;
        }
    }

    public class CompositeEngine : ACompositeEnergyConsumer<IEngineComponent>, IEngine
    {
        public float MaxSpeed { get; private set; }
        public float Acceleration { get; private set; }
        public float RotationSpeedRad { get; private set; }
        public WeakSoundDef LoopSfx { get; private set; }

        public CompositeEngine(int size) : base(size)
        {
        }

        protected override void OnComponentAdd(int i, IEngineComponent c)
        {
            MaxSpeed += c.MaxSpeed;
            Acceleration += c.Acceleration;
            RotationSpeedRad += c.RotationSpeedRad;
            LoopSfx = c.LoopSfx;
        }

        protected override void OnComponentRemove(int i, IEngineComponent c)
        {
            MaxSpeed -= c.MaxSpeed;
            Acceleration -= c.Acceleration;
            RotationSpeedRad -= c.RotationSpeedRad;
            LoopSfx = c.LoopSfx;
        }
    }

    public class CompositeShield : ACompositeEnergyConsumer<IShieldComponent>, IShield
    {
        public int Capacity { get; private set; }
        public float RechargeRate { get; private set; }
        public float RechargeDelay { get; private set; }

        public CompositeShield(int size) : base(size)
        {
        }

        protected override void OnComponentAdd(int i, IShieldComponent c)
        {
            Capacity += c.Capacity;
            RechargeRate += c.RechargeRate;
            RechargeDelay += c.RechargeDelay;
        }

        protected override void OnComponentRemove(int i, IShieldComponent c)
        {
            Capacity -= c.Capacity;
            RechargeRate -= c.RechargeRate;
            RechargeDelay += c.RechargeDelay;
        }
    }

    public class CompositeSensors : ACompositeEnergyConsumer<ISensorsComponent>, ISensors
    {
        public int LongRange { get; private set; } = 0;
        public int ShortRange { get; private set; } = 0;

        public CompositeSensors(int size) : base(size)
        {
        }

        protected override void OnComponentAdd(int i, ISensorsComponent c)
        {
            LongRange += c.LongRange;
            ShortRange += c.ShortRange;
        }

        protected override void OnComponentRemove(int i, ISensorsComponent c)
        {
            LongRange -= c.LongRange;
            ShortRange -= c.ShortRange;
        }
    }
}