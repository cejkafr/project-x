﻿using System;
using Random = UnityEngine.Random;

namespace Commons.Ships
{
    public class Generator
    {
        private readonly NameGenerator mNameGenerator;

        public Generator(NameGenerator nameGenerator)
        {
            mNameGenerator = nameGenerator;
        }

        public ShipObject Generate(ShipClassObject classObject)
        {
            var shipObject = new ShipObject();
            shipObject.name = mNameGenerator.Generate();
            shipObject.@class = classObject;

            var equipmentVariant = GenerateEquipment(classObject.equipmentVariants);
            
            shipObject.weapons = equipmentVariant.weapons;

            return shipObject;
        }

        public ShipEquipmentVariant GenerateEquipment(ShipEquipmentVariant[] equipmentVariants)
        {
            if (equipmentVariants.Length == 0)
                throw new ArgumentException("Cannot generate equipment, no variants defined.");

            return equipmentVariants[Random.Range(0, equipmentVariants.Length)];
        }
    }
}