﻿using System;
using Commons.DataObjects.Ships;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Commons.Ships
{
    public class NameGenerator
    {
        public static T LoadObject<T>(string name) where T : ScriptableObject
        {
            return Resources.Load($"Prefabs/{name}") as T;
        }
        
        private readonly string[] firstnameList;
        private readonly string[] surnameList;

        public NameGenerator(string resource)
        {
            var nameObject = LoadObject<NameListObject>(resource);
            if (nameObject == null)
                throw new ArgumentException($"Unknown ShipNameObject '{resource}'.");

            firstnameList = nameObject.first;
            surnameList = nameObject.second;
        }

        public string Generate()
        {
            var value0 = firstnameList[Random.Range(0, firstnameList.Length)];
            var value1 = surnameList[Random.Range(0, surnameList.Length)];

            return $"{value0} {value1}";
        }
    }
}