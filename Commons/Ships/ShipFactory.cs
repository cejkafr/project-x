﻿using System.Collections.Generic;
using Commons.DataObjects;
using Commons.DataObjects.Ships;
using Commons.Factions;
using Commons.Items;
using Commons.Ships.Components;
using DataObjects.Ships;

namespace Commons.Ships
{
    public static class ShipFactory
    {
        public static IShip Create(ShipObject shipObject, IFaction faction)
        {
            var hullPoints = shipObject.hullPoints > 0f ? shipObject.hullPoints : shipObject.@class.hullPoints;
            var fontArmour = shipObject.frontArmour > 0f ? shipObject.frontArmour : shipObject.@class.frontArmour;
            var sideArmour = shipObject.sideArmour > 0f ? shipObject.sideArmour : shipObject.@class.sideArmour;
            var rearArmour = shipObject.rearArmour > 0f ? shipObject.rearArmour : shipObject.@class.rearArmour;
            var ship = new Ship(shipObject.title, shipObject.pilot, shipObject.@class, faction, 
                hullPoints, fontArmour, sideArmour, rearArmour);

            CreateCargo(ship, shipObject.cargo);
            
            CreateComponents(ship.ReactorSlots, shipObject.reactors);
            CreateComponents(ship.EngineSlots, shipObject.engines);
            CreateComponents(ship.ShieldSlots, shipObject.shields);
            CreateComponents(ship.AuxiliarySlots, shipObject.auxiliary);
            CreateComponents(ship.WeaponSlots, shipObject.weapons);

            return ship;
        }

        private static void CreateComponents(IReadOnlyList<IShipSlot> slots, IReadOnlyList<AComponentObject> components)
        {
            for (var i = 0; i < components.Count; i++)
            {
                if (components[i] == null) continue;
                slots[i].Equip(ComponentFactory.Create(components[i]));
            }
        }

        private static void CreateCargo(IShip ship, IEnumerable<AItemObject> items)
        {
            foreach (var item in items)
            {
                if (item is AComponentObject component)
                {
                    ship.Inventory.Add(ComponentFactory.Create(component));
                }
                else
                {
                    ship.Inventory.Add(new GenericInventoryItem(item, 1));
                }
            }
        }
    }
}