﻿using System;
using System.Collections.Generic;
using Commons.Items;
using Commons.Ships.Components;

namespace Commons.Ships
{
    public class ShipInventory
    {
        public int Capacity { get; }
        public int Used => Items.Count;
        public int Free => Capacity - Used;

        public List<IInventoryItem> Items { get; } = new List<IInventoryItem>();

        public ShipInventory(int capacity)
        {
            Capacity = capacity;
        }

        public bool Contains(IInventoryItem item)
        {
            return Contains(item.ItemTemplate);
        }
        
        public bool Contains(AItemObject template)
        {
            return Find(template) != null;
        }

        public IInventoryItem Find(IInventoryItem item)
        {
            return Find(item.ItemTemplate);
        }
        
        public IInventoryItem Find(AItemObject template)
        {
            foreach (var inventoryItem in Items)
            {
                if (inventoryItem.ItemTemplate == template)
                {
                    return inventoryItem;
                }
            }

            return null;
        }
        
        public bool Add(IInventoryItem item)
        {
            if (item == null) return true;
            
            if (Used + 1 > Capacity)
            {
                return false;
            }

            Items.Add(item);
            return true;
        }

        public bool Remove(IInventoryItem item)
        {
            if (!Items.Contains(item))
            {
                return false;
            }

            Items.Remove(item);
            return true;
        }

        public List<IComponent> FindComponentsByExample(IComponent example)
        {
            var result = new List<IComponent>();
            foreach (var item in Items)
            {
                if (!(item is IComponent component))
                {
                    continue;
                }
                
                if (!item.GetType().IsInstanceOfType(example))
                {
                    continue;
                }

                if (component.Size != example.Size)
                {
                    continue;
                }
                
                result.Add(component);
            }

            return result;
        }

        public List<IComponent> FindComponents(Type type, ShipSlotSize size)
        {
            var result = new List<IComponent>();
            foreach (var item in Items)
            {
                if (!(item is IComponent component))
                {
                    continue;
                }

                if (!type.IsInstanceOfType(item) && !item.GetType().IsAssignableFrom(type))
                {
                    continue;
                }

                if (component.Size != size)
                {
                    continue;
                }
                
                result.Add(component);
            }

            return result;
        }
    }
}