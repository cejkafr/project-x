﻿using Commons.DataObjects;
using Commons.Items;
using Commons.Ships.Components;
using UnityEngine;

namespace Commons.Ships
{
    [CreateAssetMenu(menuName = "Ship/Ship")]
    public class ShipObject : ScriptableObject
    {
        public string title;
        public int value;
        [TextArea]
        public string description;
        public ShipClassObject @class;
        public PilotObject pilot;
        public AItemObject[] cargo;
        
        [Header("Components")]
        public ReactorComponentObject[] reactors;
        public EngineComponentObject[] engines;
        public ShieldComponentObject[] shields;
        public AAuxiliaryComponentObject[] auxiliary;
        public WeaponComponentObject[] weapons;

        [Header("State")]
        public int hullPoints;
        public int frontArmour;
        public int sideArmour;
        public int rearArmour;

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(title)}: {title}";
        }
    }
}