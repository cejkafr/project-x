﻿using Commons.DataObjects.Ships;
using UnityEngine;

namespace Commons.Ships
{
    [CreateAssetMenu(menuName = "Ship/Stance")]
    public class StanceSkillObject : SkillObject
    {
        public float attackSpeedMultiplier = 1;
        public float attackPrecisionMultiplier = 1;
        public float attackCriticalChanceMultiplier = 1;
        public float collisionDamageMultiplier = 1;
    }
}
