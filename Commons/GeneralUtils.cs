﻿using UnityEngine;

namespace Commons
{
    public static class GeneralUtils
    {
        public static Vector2 V3ToV2(Vector3 v3)
        {
            return new Vector2(v3.x, v3.z);
        }
    
        public static Vector3 V2ToV3(Vector2 v2, float y = 100f)
        {
            return new Vector3(v2.x, y, v2.y);
        }

        public static int FindInArray(object[] toggles, object toggle)
        {
            for (var i = 0; i < toggles.Length; i++)
            {
                if (toggles[i] != toggle) continue;
                return i;
            }
            
            return -1;
        }

        public static float FacingAngleBetween(Vector3 aFacing, Vector3 a, Vector3 b)
        {
            return Vector3.Angle(aFacing, b - a);
        }

        //first-order intercept using absolute target position
        public static Vector3 FirstOrderIntercept
        (
            Vector3 shooterPosition,
            Vector3 shooterVelocity,
            float shotSpeed,
            Vector3 targetPosition,
            Vector3 targetVelocity
        )  {
            var targetRelativePosition = targetPosition - shooterPosition;
            var targetRelativeVelocity = targetVelocity - shooterVelocity;
            var t = FirstOrderInterceptTime
            (
                shotSpeed,
                targetRelativePosition,
                targetRelativeVelocity
            );

            return targetPosition + t*(targetRelativeVelocity);
        }
//first-order intercept using relative target position
        public static float FirstOrderInterceptTime
        (
            float shotSpeed,
            Vector3 targetRelativePosition,
            Vector3 targetRelativeVelocity
        ) {
            var velocitySquared = targetRelativeVelocity.sqrMagnitude;
            if(velocitySquared < 0.001f)
                return 0f;
 
            var a = velocitySquared - shotSpeed*shotSpeed;
 
            //handle similar velocities
            if (Mathf.Abs(a) < 0.001f)
            {
                var t = -targetRelativePosition.sqrMagnitude/
                        (
                            2f*Vector3.Dot
                            (
                                targetRelativeVelocity,
                                targetRelativePosition
                            )
                        );
                return Mathf.Max(t, 0f); //don't shoot back in time
            }
 
            var b = 2f*Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
            var c = targetRelativePosition.sqrMagnitude;
            var determinant = b*b - 4f*a*c;
 
            if (determinant > 0f) { //determinant > 0; two intercept paths (most common)
                float	t1 = (-b + Mathf.Sqrt(determinant))/(2f*a),
                    t2 = (-b - Mathf.Sqrt(determinant))/(2f*a);
                if (t1 > 0f) {
                    if (t2 > 0f)
                        return Mathf.Min(t1, t2); //both are positive
                    else
                        return t1; //only t1 is positive
                } else
                    return Mathf.Max(t2, 0f); //don't shoot back in time
            } else if (determinant < 0f) //determinant < 0; no intercept path
                return 0f;
            else //determinant = 0; one intercept path, pretty much never happens
                return Mathf.Max(-b/(2f*a), 0f); //don't shoot back in time
        }
    }
}