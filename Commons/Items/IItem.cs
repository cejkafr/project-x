﻿using UnityEngine;

namespace Commons.Items
{
    public interface IItem
    {
        AItemObject ItemTemplate { get; }
        string Name { get; }
        string Description { get; }
        Sprite Icon { get; }
        int Value { get; }
    }
}