﻿namespace Commons.Items
{
    public interface IInventoryItem : IItem
    {
        bool Stackable { get; }
        int Amount { get; }
    }
}