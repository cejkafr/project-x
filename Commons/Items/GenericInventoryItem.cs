﻿using UnityEngine;

namespace Commons.Items
{
    public readonly struct GenericInventoryItem : IInventoryItem
    {
        public AItemObject ItemTemplate { get; }
        public string Name => ItemTemplate.name;
        public string Description => ItemTemplate.description;
        public Sprite Icon => ItemTemplate.icon;
        public int Value => ItemTemplate.value;
        public bool Stackable => ItemTemplate.stackable;
        public int Amount { get; }

        public GenericInventoryItem(AItemObject itemTemplate, int amount)
        {
            ItemTemplate = itemTemplate;
            Amount = amount;
        }
    }
}