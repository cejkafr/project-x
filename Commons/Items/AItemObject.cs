﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Commons.Items
{
    public class AItemObject : ScriptableObject
    {
        public new string name;
        [TextArea]
        public string description;
        public Sprite icon;
        public int value;
        public bool stackable;
        public bool consumable;
    }
}