﻿using UnityEngine;

namespace Sectors.Cameras
{
    public interface IFollowCamera
    {
        Camera Camera { get; }
        Transform Target { get; }
        Transform CameraTransform { get; }

        void Rotate(Vector2 delta);
        void Zoom(Vector2 delta);

        Vector3 WorldToScreenPoint(Vector3 position);
    }
}