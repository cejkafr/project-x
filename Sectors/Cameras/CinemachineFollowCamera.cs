﻿using Cinemachine;
using UnityEngine;

namespace Sectors.Cameras
{
    public class CinemachineFollowCamera : MonoBehaviour, IFollowCamera
    {
        [SerializeField] private Camera realCamera;
        [SerializeField] private LayerMask cinematicLayerMask;

        private int standardLayerMask;
        
        public Transform Target { get; private set; }
        public Camera Camera => realCamera;
        public CinemachineFreeLook FreeLookCam { get; private set; }
        public Transform CameraTransform { get; private set; }

        private void Awake()
        {
            FreeLookCam = GetComponent<CinemachineFreeLook>();
            CameraTransform = realCamera.transform;

            standardLayerMask = realCamera.cullingMask;
            
            if (Target != null)
            {
                SetTarget(Target);
            }
        }

        public void SetTarget(Transform target)
        {
            Target = target;

            if (FreeLookCam != null)
            {
                FreeLookCam.Follow = Target;
                FreeLookCam.LookAt = Target;
            }
        }

        public void Rotate(Vector2 delta)
        {
            FreeLookCam.m_XAxis.m_InputAxisValue = delta.x;
        }

        public void Zoom(Vector2 delta)
        {
            FreeLookCam.m_YAxis.m_InputAxisValue = delta.y;
        }

        public Vector3 WorldToScreenPoint(Vector3 position)
        {
            return realCamera.WorldToScreenPoint(position);
        }

        private void OnDisable()
        {
            realCamera.cullingMask = standardLayerMask;
        }

        public void OnCinematicStarted()
        {
            realCamera.cullingMask = cinematicLayerMask;
        }

        public void OnCinematicEnded()
        {
            realCamera.cullingMask = standardLayerMask;
        }
    }
}