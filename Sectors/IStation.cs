﻿using Commons.Factions;
using Commons.Sectors;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public interface IStation : IActionObject
    {
        Transform Transform { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        Faction Faction { get; }
        void Init(Station iStation, Sprite icon, SectorObjectAction[] actions);
    }
}