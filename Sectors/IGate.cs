﻿using Commons.Sectors;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public interface IGate : IActionObject
    {
        Gate BaseObject { get; }
        Transform Transform { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        Gate Destination { get; }
        bool IsActive { get; }
        GameObject WarpTunnel { get; }
        GameObject WarpTunnelGlow { get; }

        void Init(Gate iGate, Sprite icon, SectorObjectAction[] actions);
    }
}