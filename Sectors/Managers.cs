﻿using UnityEngine;

namespace Sectors
{
    public class Managers : MonoBehaviour
    {
        public GateManager Gate { get; private set; }
        public FleetManager Fleet { get; private set; }
        public StationManager Station { get; private set; }

        private void Awake()
        {
            Gate = GetComponent<GateManager>();
            Fleet = GetComponent<FleetManager>();
            Station = GetComponent<StationManager>();
        }
    }
}