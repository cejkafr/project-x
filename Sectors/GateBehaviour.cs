﻿using Commons.Factions;
using Commons.Sectors;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public class GateBehaviour : MonoBehaviour, IGate
    {
        [SerializeField] private GameObject warpTunnelGlow;
        [SerializeField] private GameObject warpTunnel;

        private IAction[] actions;
        
        public SectorObject SectorObject => BaseObject;
        public Gate BaseObject { get; private set; }

        public Transform Transform => transform;
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;
        public Gate Destination => BaseObject.Target;
        public bool IsActive => BaseObject.Active;

        public string Name => BaseObject.Name;
        public Faction Faction => null;
        public bool HasFaction => Faction != null;
        public Sprite Icon { get; private set; }
        public bool IsActionRequired => false;
        
        public GameObject WarpTunnel => warpTunnel;

        public GameObject WarpTunnelGlow => warpTunnelGlow;

        public void Init(Gate iGate, Sprite icon, SectorObjectAction[] objectActions)
        {
            BaseObject = iGate;
            Icon = icon;

            actions = new IAction[objectActions.Length];
            for (var i = 0; i < objectActions.Length; i++)
            {
                var oa = objectActions[i];
                actions[i] = new ObjectAction(oa.name, oa.description, oa.icon, oa.type, false);
            }
        }

        public IAction[] Actions(IFleet iFleet)
        {
            return actions;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}