﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Sectors
{
    public class TimeController : MonoBehaviour
    {
        [Serializable]
        public class TimeChangedEvent : UnityEvent<DateTime>
        {
            
        }

        [SerializeField] private string timeAtStart;
        
        [Space]
        public TimeChangedEvent onTimeChanged = new TimeChangedEvent();
        
        public DateTime CurrentTime { get; private set; }

        private void Awake()
        {
            CurrentTime = DateTime.Parse(timeAtStart);
            enabled = false;

            onTimeChanged.Invoke(CurrentTime);
        }

        private void Update()
        {
            CurrentTime = CurrentTime.AddSeconds(400 * Time.unscaledDeltaTime);
            onTimeChanged.Invoke(CurrentTime);
        }

        public void OnPlayerActive(bool isActive)
        {
            enabled = isActive;
        }
    }
}