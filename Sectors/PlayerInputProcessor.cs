﻿using Commons.UI;
using Game.Extensions;
using Games.Consoles;
using Sectors.Cameras;
using Sectors.UI;
using StrategicMap.UI;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Sectors
{
    public class PlayerInputProcessor : MonoBehaviour
    {
        [SerializeField] private PlayerController playerController;
        [SerializeField] private UniverseUIController uiController;
        [SerializeField] private CinemachineFollowCamera playerCamera;

        private bool cameraRotation;

        public void OnCameraRotation(InputAction.CallbackContext ctx)
        {
            if (!cameraRotation) return;
            playerCamera.Rotate(ctx.ReadValue<Vector2>());
        }

        public void OnCameraRotationToggle(InputAction.CallbackContext ctx)
        {
            cameraRotation = ctx.performed;
            playerCamera.Rotate(float2.zero);
        }

        public void OnCameraZoom(InputAction.CallbackContext ctx)
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;
            playerCamera.Zoom(ctx.ReadValue<Vector2>());
        }

        public void OnFleetMove(InputAction.CallbackContext ctx)
        {
            if (ctx.canceled)
            {
                playerController.Stop();
            }
            else
            {
                var rawDir = ctx.ReadValue<Vector2>();
                var cameraForward = playerCamera.CameraTransform.forward.Flatten();
                var cameraRight = playerCamera.CameraTransform.right.Flatten();
                var dir = rawDir.x * cameraRight + rawDir.y * cameraForward;

                playerController.Move(dir.normalized);
            }
        }

        public void OnWait(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                playerController.Wait(true);
            }
            else if (ctx.canceled)
            {
                playerController.Wait(false);
            }
        }

        public void OnFleetWindowToggle(InputAction.CallbackContext ctx)
        {
            if (!ctx.performed) return;
            uiController.FleetWindow.SetOpen(!uiController.FleetWindow.IsOpen);
        }
        
        public void OnMap(InputAction.CallbackContext ctx)
        {
            if (!ctx.performed) return;
            uiController.SectorMapWindow.SetOpen(!uiController.SectorMapWindow.IsOpen);
        }

        public void OnMenu(InputAction.CallbackContext ctx)
        {
            if (!ctx.performed) return;
            if (uiController.IsWindowOpen)
            {
                uiController.CloseWindows();
                return;
            }

            GameMenu.Instance.Open(GameMenu.ShowType.InGame);
        }

        public void OnConsole(InputAction.CallbackContext ctx)
        {
            if (!ctx.performed) return;
            ConsoleManager.SetOpen(!ConsoleManager.IsOpen());
        }

        public void OnQuickButton(InputAction.CallbackContext ctx)
        {
            if (!ctx.performed) return;
        }
    }
}