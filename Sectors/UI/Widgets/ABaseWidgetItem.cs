﻿using UnityEngine;

namespace Sectors.UI.Widgets
{
    public abstract class ABaseWidgetItem<T> : MonoBehaviour, IWidgetItem<T>
    {
        public T Item { get; private set; }
        public bool IsShown => gameObject.activeSelf;

        public virtual void Show(T item)
        {
            Item = item;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}