﻿using System;
using Commons.Ships;

namespace Sectors.UI.Widgets
{
    public class FleetListWidget : ABaseWidget<FleetListWidgetItem, IShip>
    {
        public void Show(IFleet fleet, Action<IShip> onClickCallback)
        {
            HideAll();

            foreach (var ship in fleet.Ships)
            {
                var widgetItem = Show(ship);
                widgetItem.SetBtnClickCallback(onClickCallback);
            }
        }
    }
}