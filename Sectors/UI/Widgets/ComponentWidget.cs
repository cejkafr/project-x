﻿using System;
using System.Collections.Generic;
using Commons.Ships.Components;

namespace Sectors.UI.Widgets
{
    public class ComponentWidget : ABaseWidget<ComponentWidgetItem, IComponent>
    {
        public void Show(IEnumerable<IComponent> components, bool showUnEquip, Action<IComponent> onSelectedCallback)
        {
            HideAll();

            if (showUnEquip)
            {
                var widgetItem = ShowEmpty();
                widgetItem.SetBtnClickCallback(onSelectedCallback); 
            }
            
            foreach (var component in components)
            {
                var widgetItem = Show(component);
                widgetItem.SetBtnClickCallback(onSelectedCallback);
            }
        }

        private ComponentWidgetItem ShowEmpty()
        {
            var item = FindNotShownOrInstantiate();
            item.ShowEmpty();
            return item;
        }
    }
}