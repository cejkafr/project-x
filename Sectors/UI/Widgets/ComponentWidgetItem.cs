﻿using System;
using Commons.Ships.Components;
using Commons.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sectors.UI.Widgets
{
    public class ComponentWidgetItem : ABaseWidgetItem<IComponent>, IPointerEnterHandler, IPointerExitHandler
    {
        [Serializable]
        public struct Bindings
        {
            public Image icon;
            public TextMeshProUGUI title;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private Sprite unEquipIcon;
        [SerializeField] private string unEquipText = "[remove component]";

        private Action<IComponent> btnClickCallback;

        public override void Show(IComponent component)
        {
            bindings.icon.sprite = component.Icon;
            bindings.icon.color = component.Rarity.Color;
            bindings.title.text = component.Name;

            base.Show(component);
        }

        public void ShowEmpty()
        {
            bindings.icon.sprite = unEquipIcon;
            bindings.icon.color = Color.white;
            bindings.title.text = unEquipText;
            
            base.Show(null);
        }
        
        public void SetBtnClickCallback(Action<IComponent> cb)
        {
            btnClickCallback = cb;
        }
        
        public void OnBtnClick()
        {
            ComponentTooltip.Instance.Hide();
            btnClickCallback?.Invoke(Item);
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Item == null) return;
            ComponentTooltip.Instance.Show(Item);
        }
        
        public void OnPointerExit(PointerEventData eventData)
        {
            ComponentTooltip.Instance.Hide();
        }
    }
}