﻿namespace Sectors.UI.Widgets
{
    public interface IWidgetItem<T>
    {
        T Item { get; }
        bool IsShown { get; }
        void Show(T ship);
        void Hide();
        void Destroy();
    }
}