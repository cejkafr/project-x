﻿using System;
using Commons.Items;
using Commons.Ships.Components;
using Commons.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sectors.UI.Widgets
{
    public class FleetListWidgetItemInventoryWidgetItem : ABaseWidgetItem<IInventoryItem>, IPointerEnterHandler, IPointerExitHandler
    {
        [Serializable]
        public struct Bindings
        {
            public Image icon;
        }

        [SerializeField] private Bindings bindings;

        public override void Show(IInventoryItem item)
        {
            if (item == null)
            {
                bindings.icon.color = Color.clear;
            }
            else
            {
                if (item is IComponent component)
                {
                    bindings.icon.color = component.Rarity.Color;
                }
                else
                {
                    bindings.icon.color = Color.white;
                }
                
                bindings.icon.sprite = item.Icon;
            }

            base.Show(item);
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Item == null) return;

            if (Item is IComponent component)
            {
                ComponentTooltip.Instance.Show(component);
            }
            else
            {
                TextTooltip.Instance.Show(Item.Name, Item.Description);
            }
        }
        
        public void OnPointerExit(PointerEventData eventData)
        {
            TextTooltip.Instance.Hide();
            ComponentTooltip.Instance.Hide();
        }
    }
}