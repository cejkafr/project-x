﻿using System;
using Commons.Ships;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Sectors.UI.Widgets
{
    public class FleetListWidgetItem : ABaseWidgetItem<IShip>
    {
        [Serializable]
        public struct Bindings
        {
            public TextMeshProUGUI name;
            public TextMeshProUGUI clazz;
            public Image image;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private FleetListWidgetItemInventoryWidget fleetListWidgetItemInventoryWidget;

        private Action<IShip> btnClickCallback;

        public override void Show(IShip ship)
        {
            bindings.name.text = ship.Name;
            bindings.clazz.text = ship.ClassName;
            bindings.image.sprite = ship.IconSide;

            fleetListWidgetItemInventoryWidget.Show(ship.Inventory);
            
            base.Show(ship);
        }

        public void SetBtnClickCallback(Action<IShip> cb)
        {
            btnClickCallback = cb;
        }
        
        public void OnBtnClick()
        {
            btnClickCallback?.Invoke(Item);
        }
    }
}