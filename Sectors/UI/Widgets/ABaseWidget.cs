﻿using System.Collections.Generic;
using UnityEngine;

namespace Sectors.UI.Widgets
{
    public abstract class ABaseWidget<T, T1> : MonoBehaviour where T : IWidgetItem<T1>
    {
        [SerializeField] private GameObject widgetItemPrefab;
        [SerializeField] private RectTransform widgetItemParent;
        
        public List<T> Items { get; } = new List<T>();

        public T Show(T1 other)
        {
            var item = FindNotShownOrInstantiate();
            item.Show(other);
            return item;
        } 
        
        public void HideAll()
        {
            foreach (var item in Items)
            {
                item.Hide();
            }
        }

        protected T FindNotShownOrInstantiate()
        {
            foreach (var item in Items)
            {
                if (!item.IsShown)
                {
                    return item;
                }
            }

            var inst = Instantiate(widgetItemPrefab, widgetItemParent);
            var newItem = inst.GetComponent<T>();
            Items.Add(newItem);

            return newItem;
        }
    }
}