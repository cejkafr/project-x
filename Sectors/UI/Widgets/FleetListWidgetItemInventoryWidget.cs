﻿using Commons.Items;
using Commons.Ships;

namespace Sectors.UI.Widgets
{
    public class
        FleetListWidgetItemInventoryWidget : ABaseWidget<FleetListWidgetItemInventoryWidgetItem, IInventoryItem>
    {
        public void Show(ShipInventory inventory)
        {
            HideAll();

            for (var i = 0; i < inventory.Capacity; i++)
            {
                var item = inventory.Items.Count > i ? inventory.Items[i] : null;
                Show(item);
            }
        }
    }
}