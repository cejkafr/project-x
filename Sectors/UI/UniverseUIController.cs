﻿using Sectors.UI.Windows;
using StrategicMap.UI;
using UnityEngine;

namespace Sectors.UI
{
    public class UniverseUIController : MonoBehaviour
    {
        [SerializeField] private SectorMapWindow sectorMap;
        [SerializeField] private FleetWindow fleetWindow;

        public IWindow SectorMapWindow => sectorMap;
        public IWindow FleetWindow => fleetWindow;

        public bool IsWindowOpen => sectorMap.IsOpen;

        public void SetActive(bool value)
        {
            gameObject.SetActive(value); 
        }

        public void CloseWindows()
        {
            sectorMap.SetOpen(false);
            FleetWindow.SetOpen(false);
        }

        public void OnMenuOpened(bool isOpen)
        {
            SetActive(!isOpen);
        }
        
        public void OnCinematicStarted()
        {
            SetActive(false);
        }

        public void OnCinematicEnded()
        {
            SetActive(true);
        }
    }
}