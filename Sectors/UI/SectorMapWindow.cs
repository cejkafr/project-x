﻿using System;
using Commons.Sectors;
using Game.Extensions;
using Sectors.UI.Windows;
using StrategicMap.UI;
using UnityEngine;
using UnityEngine.UI;
using Universes;

namespace Sectors.UI
{
    public class SectorMapWindow : MonoBehaviour, IWindow
    {
        [Serializable]
        public struct Bindings
        {
            public Image mapImage;
            public RectTransform playerMarker;
            public RectTransform[] gates;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private UniverseController universeController;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private Managers managers;
        [SerializeField] private Color[] layerColors;

        public bool IsOpen => gameObject.activeInHierarchy;

        private void Awake()
        {
            SetOpen(false);
        }

        public void SetOpen(bool value)
        {
            gameObject.SetActive(value);
        }

        public void OnCloseBtn()
        {
            SetOpen(false);
        }

        private void OnEnable()
        {
            HideGates();
            RenderMap(universeController.CurrentSector);

            var size = bindings.mapImage.rectTransform.rect.size;
            var halfSize = size / 2;
            var imageUnit = size / universeController.CurrentSector.Size;
            
            var mapUnit = playerController.Fleet.Position / 100f;
            var pos = new Vector2(mapUnit.x, mapUnit.z) * imageUnit;

            bindings.playerMarker.localPosition = pos - halfSize;
            bindings.playerMarker.localRotation =
                Quaternion.Euler(0f, 0f, playerController.Fleet.Rotation.eulerAngles.y * -1);

            for (var i = 0; i < managers.Gate.Objects.Count; i++)
            {
                var gate = managers.Gate.Objects[i];
                var gateMapUnit = gate.Position / 100f;
                var gatePos = new Vector2(gateMapUnit.x, gateMapUnit.z) * imageUnit;
                bindings.gates[i].localPosition = gatePos - halfSize;
                bindings.gates[i].gameObject.SetActive(true);
            }
        }

        public void RenderMap(Sector sector)
        {
            var pixels = new Color[sector.Size * sector.Size];
            sector.ProceduralLayers.Map.IterateOver((layer, x, y, value) =>
            {
                if (layer == 0)
                {
                    pixels[y * sector.Size + x] = Color.Lerp(Color.clear, layerColors[layer], value);
                }
                else
                {
                    pixels[y * sector.Size + x] = Color.Lerp(pixels[y * sector.Size + x], layerColors[layer], value);
                }
            });

            var tex = new Texture2D(sector.Size, sector.Size);
            tex.SetPixels(pixels);
            tex.Apply();

            bindings.mapImage.sprite = Sprite.Create(tex,
                new Rect(0.0f, 0.0f, tex.width, tex.height),
                new Vector2(0.5f, 0.5f), 100);
            bindings.mapImage.color = Color.white;
        }

        public void Invalidate()
        {
            throw new NotImplementedException();
        }

        private void HideGates()
        {
            foreach (var gate in bindings.gates)
            {
                gate.gameObject.SetActive(false);
            }
        }
    }
}