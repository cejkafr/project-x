﻿using Sectors.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace StrategicMap.UI
{
    public class MarkerPOI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public void OnPointerEnter(PointerEventData eventData)
        {
            transform.parent.GetComponent<Marker>().ShowDetail(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            transform.parent.GetComponent<Marker>().ShowDetail(false);
        }
    }
}