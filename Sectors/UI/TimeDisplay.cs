﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Sectors.UI
{
    public class TimeDisplay : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public Text dateTime;
        }

        [SerializeField] private Bindings bindings;
        
        
        public void OnTimeChanged(DateTime dateTime)
        {
            bindings.dateTime.text = $"{dateTime.ToShortDateString()} {dateTime.ToLongTimeString()}";
        }
    }
}