﻿using System;
using System.Collections.Generic;
using Commons.Ships.Components;
using Sectors.UI.Widgets;
using UnityEngine;

namespace Sectors.UI.Windows
{
    public class ComponentWidgetWindow : ABaseWindow
    {
        [SerializeField] private ComponentWidget componentWidget;
        
        protected override void OnDraw()
        {
            
        }

        public void Init(IEnumerable<IComponent> components, bool showUnEquip, Action<IComponent> onSelectedCallback)
        {
            componentWidget.Show(components, showUnEquip, onSelectedCallback);
        }
    }
}