﻿namespace Sectors.UI.Windows
{
    public interface IWindow
    {
        bool IsOpen { get; }

        void SetOpen(bool value);

        void Invalidate();
    }
}