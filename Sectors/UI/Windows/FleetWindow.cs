﻿using Commons.Ships;
using Sectors.UI.Widgets;
using UnityEngine;

namespace Sectors.UI.Windows
{
    public class FleetWindow : ABaseWindow
    {
        [SerializeField] private PlayerController playerController;
        [SerializeField] private FleetListWidget fleetListWidget;
        [SerializeField] private ShipDetail shipDetail;

        private int currentShipIndex;

        protected override void OnDraw()
        {
            fleetListWidget.Show(playerController.Fleet, OnShipBtnClick);
            if (currentShipIndex > playerController.Fleet.Ships.Count)
                currentShipIndex = 0;
            shipDetail.Show(playerController.Fleet, playerController.Fleet.Ships[currentShipIndex]);
        }

        protected override void OnClose()
        {
            currentShipIndex = 0;
        }

        private void OnShipBtnClick(IShip ship)
        {
            currentShipIndex = playerController.Fleet.Ships.IndexOf(ship);
            shipDetail.Show(playerController.Fleet, ship);
        }
    }
}