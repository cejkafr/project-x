﻿using System;
using UnityEngine;

namespace Sectors.UI.Windows
{
    public abstract class ABaseWindow : MonoBehaviour, IWindow
    {
        [SerializeField] private bool onStartOpen;

        private bool isFirstStart = true;
        
        public bool IsOpen => gameObject.activeInHierarchy;

        private void Awake()
        {
            if (isFirstStart)
            {
                if (onStartOpen)
                {
                    OnDraw();
                }
                else
                {
                    SetOpen(false);
                }
            }
            
            isFirstStart = false;
        }

        private void Start()
        {
            
        }

        public void SetOpen(bool value)
        {
            if (value) OnDraw();
            if (!value) OnClose();
            gameObject.SetActive(value);
            isFirstStart = false;
        }

        public void Invalidate()
        {
            OnDraw();
        }

        public void OnCloseBtn()
        {
            SetOpen(false);
        }

        protected abstract void OnDraw();

        protected virtual void OnClose()
        {
            
        }
    }
}