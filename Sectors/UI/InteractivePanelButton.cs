﻿using System;
using Commons.UI;
using Sectors.Actions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sectors.UI
{
    public delegate void InteractivePanelButtonEvent(IAction action);
    
    public class InteractivePanelButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public event InteractivePanelButtonEvent Clicked; 
        
        [Serializable]
        public struct Bindings
        {
            public Image image;
        }

        [SerializeField] private Bindings bindings;

        private IAction action;

        public void SetActive(bool value)
        {
            gameObject.SetActive(value); 
        }

        public void Show(IAction iAction)
        {
            action = iAction;

            bindings.image.sprite = iAction.Icon;
            
            SetActive(true);
        }

        public void OnClick()
        {
            Clicked?.Invoke(action);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            TextTooltip.Instance.Show(action.Name, action.Description);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            TextTooltip.Instance.Hide();
        }
    }
}