﻿using System;
using System.Collections.Generic;
using Sectors.Actions;
using StrategicMap.UI;
using TMPro;
using UnityEngine;

namespace Sectors.UI
{
    public class InteractivePanel : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public Transform buttonList;
            public TextMeshPro name;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private GameObject buttonPrefab;
        [SerializeField] private PlayerController playerController;

        private readonly List<InteractivePanelButton> buttons
            = new List<InteractivePanelButton>(2);

        private IActionObject actionObject;

        private void Start()
        {
            SetActive(false);
            
            // playerController.PlayerFleetCreated += OnPlayerFleetCreated;
            // playerController.PlayerFleetDestroyed += OnPlayerFleetDestroyed;

            if (playerController.Fleet != null)
            {
                OnPlayerFleetCreated(playerController.Fleet);
            }
        }

        public void OnPlayerFleetCreated(IFleet fleet)
        {
            fleet.TriggerInteractiveEnter += OnInteractiveTriggerInteractiveEnter;
            fleet.TriggerInteractiveExit += OnInteractiveTriggerInteractiveExit;
        }
        
        public void OnPlayerFleetDestroyed(IFleet fleet)
        {
            // playerController.Fleet.TriggerInteractiveEnter -= OnInteractiveTriggerInteractiveEnter;
            // playerController.Fleet.TriggerInteractiveExit -= OnInteractiveTriggerInteractiveExit;
        }

        public void OnInteractiveTriggerInteractiveEnter(IFleet iFleet, IActionObject iActionObject)
        {
            actionObject = iActionObject;

            ResetButtons();

            var actions = iActionObject.Actions(iFleet);
            for (var i = 0; i < actions.Length; i++)
            {
                var action = actions[i];

                if (i >= buttons.Count)
                {
                    var inst = Instantiate(buttonPrefab, bindings.buttonList);
                    var buttonInst = inst.GetComponent<InteractivePanelButton>();
                    buttons.Add(buttonInst);
                    buttonInst.Clicked += OnActionTriggered;
                }

                var button = buttons[i];
                button.Show(action);
            }

            SetActive(true);
        }

        public void OnInteractiveTriggerInteractiveExit(IFleet iFleet, IActionObject iActionObject)
        {
            SetActive(false);
        }

        public void OnActionTriggered(IAction action)
        {
            playerController.ExecuteAction(actionObject, action);
        }

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }

        private void ResetButtons()
        {
            foreach (var button in buttons)
            {
                button.SetActive(false);
            }
        }
    }
}