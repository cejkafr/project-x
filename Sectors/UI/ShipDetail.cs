﻿using System;
using System.Collections.Generic;
using Commons.Ships;
using Commons.Ships.Components;
using Commons.UI;
using Sectors.UI.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Sectors.UI
{
    public class ShipDetail : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public TextMeshProUGUI name;
            public Image topDownImage;
            public RectTransform weaponSlotParent;
            public RectTransform systemSlotParent;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private GameObject slotPrefab;
        [SerializeField] private int tileSize = 40;
        [SerializeField] private ComponentWidgetWindow componentWidgetWindow;

        private readonly List<ComponentSlot> slots = new List<ComponentSlot>();
        private IShipSlot currentShipSlot;

        public IFleet Fleet { get; private set; }
        public IShip Ship { get; private set; }

        public void Show(IFleet fleet, IShip ship)
        {
            Fleet = fleet;
            Ship = ship;

            Redraw();
        }

        private void Redraw()
        {
            componentWidgetWindow.SetOpen(false);
            HideAll();

            bindings.name.text = Ship.Name;
            bindings.topDownImage.sprite = Ship.IconTop;

            DrawSlots(Ship.WeaponSlots, true);
            DrawSlots(Ship.ReactorSlots, false);
            DrawSlots(Ship.EngineSlots, false);
            DrawSlots(Ship.ShieldSlots, false);
            DrawSlots(Ship.AuxiliarySlots, false);
        }

        private void DrawSlots(IEnumerable<IShipSlot> shipSlots, bool showInImage)
        {
            foreach (var shipSlot in shipSlots)
            {
                var slot = FindNotShownOrInstantiate();
                var parent = showInImage ? bindings.weaponSlotParent : bindings.systemSlotParent;
                slot.Show(shipSlot, parent);
            }
        }

        private void HideAll()
        {
            foreach (var slot in slots)
            {
                slot.Hide();
            }
        }

        private ComponentSlot FindNotShownOrInstantiate()
        {
            foreach (var slot in slots)
            {
                if (!slot.IsShown)
                {
                    return slot;
                }
            }

            var inst = Instantiate(slotPrefab, bindings.weaponSlotParent);
            var newItem = inst.GetComponent<ComponentSlot>();
            newItem.Init(tileSize, OnPointerDown);
            slots.Add(newItem);

            return newItem;
        }

        private void OnPointerDown(IShipSlot shipSlot)
        {
            currentShipSlot = shipSlot;

            var list = new List<IComponent>();

            foreach (var ship in Fleet.Ships)
            {
                list.AddRange(ship.Inventory.FindComponents(
                    currentShipSlot.ComponentType, currentShipSlot.Size));
            }

            componentWidgetWindow.Init(list, !shipSlot.IsEmpty, OnItemSelected);
            componentWidgetWindow.SetOpen(true);
        }

        private void OnItemSelected(IComponent component)
        {
            if (currentShipSlot == null) return;

            if (component == null)
            {
                if (!currentShipSlot.IsEmpty)
                {
                    Ship.Inventory.Add(currentShipSlot.Component);
                    currentShipSlot.UnEquip();
                }
            }
            else
            {
                if (!currentShipSlot.CanEquip(component)) return;

                foreach (var ship in Fleet.Ships)
                {
                    if (!ship.Inventory.Contains(component)) continue;

                    if (!currentShipSlot.IsEmpty)
                    {
                        ship.Inventory.Add(currentShipSlot.Component);
                        currentShipSlot.UnEquip();
                    }

                    currentShipSlot.Equip(component);
                    ship.Inventory.Remove(component);
                }
            }

            componentWidgetWindow.SetOpen(false);
            FindObjectOfType<UniverseUIController>().FleetWindow.Invalidate();
        }
    }
}