﻿using System.Collections.Generic;
using Game.Extensions;
using Sectors.Actions;
using Sectors.Cameras;
using UnityEngine;

namespace Sectors.UI
{
    public class MarkerManager : MonoBehaviour
    {
        [SerializeField] private GameObject markerPrefab;
        [SerializeField] private CinemachineFollowCamera playerCamera;
        [SerializeField] private Managers managers;

        private readonly Queue<Marker> free = new Queue<Marker>();
        private readonly Dictionary<IActionObject, Marker> allocated
            = new Dictionary<IActionObject, Marker>();

        private bool isSectorLoaded;

        public void OnSectorLoaded()
        {
            isSectorLoaded = true;
            
            foreach (var fleet in managers.Fleet.Objects)
            {
                ShowMarker(fleet);
            }

            foreach (var gate in managers.Gate.Objects)
            {
                ShowMarker(gate);
            }

            foreach (var station in managers.Station.Objects)
            {
                ShowMarker(station);
            }
        }

        public void OnSectorUnloaded()
        {
            isSectorLoaded = false;
            
            foreach (var marker in allocated.Values)
            {
                HideMarker(marker);
            }
        }

        public void OnInteractiveCreated(IActionObject actionObject)
        {
            if (!isSectorLoaded) return;
            ShowMarker(actionObject);
        }
        
        public void OnInteractiveDestroyed(IActionObject actionObject)
        {
            if (!isSectorLoaded) return;
            HideMarker(actionObject);
        }

        private void ShowMarker(IActionObject actionObject)
        {
            if (allocated.ContainsKey(actionObject)
                || (actionObject is IFleet && actionObject.Faction.IsHuman))
            {
                return;
            }

            if (free.IsEmpty())
            {
                InstantiateMarker();
            }

            var marker = free.Dequeue();
            marker.Show(actionObject);
            marker.SetActive(true);
            allocated.Add(actionObject, marker);
        }

        private void InstantiateMarker()
        {
            var inst = Instantiate(markerPrefab, transform);
            var marker = inst.GetComponent<Marker>();
            marker.Init(playerCamera);

            free.Enqueue(marker);
        }

        private void HideMarker(IActionObject actionObject)
        {
            if (!allocated.ContainsKey(actionObject))
            {
                return;
            }

            var marker = allocated[actionObject];
            allocated.Remove(actionObject);
            HideMarker(marker);
        }

        private void HideMarker(Marker marker)
        {
            marker.SetActive(false);
            free.Enqueue(marker);            
        }
    }
}