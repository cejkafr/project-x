﻿using Commons.UI;
using Sectors.UI.Windows;
using UnityEngine;

namespace Sectors.UI
{
    public class ButtonController : MonoBehaviour
    {
        [SerializeField] private FleetWindow fleetWindow;
        
        public void OnFleetClick()
        {
            fleetWindow.SetOpen(!fleetWindow.IsOpen);
        }
        
        public void OnInventoryClick()
        {
            
        }
        
        public void OnMenuClick()
        {
            GameMenu.Instance.Open(GameMenu.ShowType.InGame);
        }
    }
}