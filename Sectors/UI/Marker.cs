﻿using System;
using System.Linq;
using System.Text;
using Commons.DataObjects.Ships;
using Commons.Ships;
using Commons.UI;
using DataObjects.Ships;
using Sectors.Actions;
using Sectors.Cameras;
using UnityEngine;
using UnityEngine.UI;

namespace Sectors.UI
{
    public class Marker : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public RectTransform rectTransform;
            public Image poi;
            public Image icon;
            public GameObject detail;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private float offset = 20;

        private IFollowCamera playerCamera;
        private IActionObject actionObject;

        private void OnGUI()
        {
            var pos = playerCamera.WorldToScreenPoint(actionObject.Position);
            var x = pos.x;
            var y = pos.y + offset;
            var rot = 0f;
            var widthHalf = bindings.rectTransform.rect.width * .5f;

            if (y < 0f || pos.z < 0f)
            {
                y = 0f;
            }
            else if (y > Screen.height)
            {
                y = Screen.height;
                rot = 180;
                x *= -1;
            }

            if (x > Screen.width - widthHalf)
            {
                x = Screen.width;
                rot = 90;
            }
            else if (x < 0f + widthHalf)
            {
                x = 0f;
                rot = 270;
            }

            // print($"pos: {pos} screen: {Screen.width}x{Screen.height}");

            bindings.rectTransform.position = new Vector3(x, y, 0);
            bindings.rectTransform.rotation = Quaternion.Euler(0, 0, rot);
        }

        public void Init(IFollowCamera iCam)
        {
            playerCamera = iCam;
        }
        
        public void Show(IActionObject iActionObject)
        {
            actionObject = iActionObject;

            bindings.detail.SetActive(false);
            bindings.icon.sprite = iActionObject.Icon;

            var color = Color.white;
            if (iActionObject.Faction != null)
            {
                color = iActionObject.Faction.Color;
            }

            SetColor(color);

            SetActive(true);
        }

        public void ShowDetail(bool value)
        {
            if (value)
            {
                var text = new StringBuilder();
                if (actionObject.Faction != null)
                {
                    text.Append($"<size=18>{actionObject.Faction.Name}</size> <color=#{ColorUtility.ToHtmlStringRGB(actionObject.Faction.Color)}>({actionObject.Faction.AttitudeTitle})</color><br>");
                }

                if (actionObject is IGate gate)
                {
                    text.Append($"Leads to: <b>{gate.Destination.Sector.Name}</b>");
                }
                
                if (actionObject is IFleet fleet)
                {
                    var fi = fleet.Ships.Count(ship => ship.ClassSize == ShipClassSize.Fighter);
                    var cr = fleet.Ships.Count(ship => ship.ClassSize == ShipClassSize.Cruiser);
                    var ca = fleet.Ships.Count(ship => ship.ClassSize == ShipClassSize.Carrier);
                    text.Append($"Fi: <b>{fi}</b> / Cr: <b>{cr}</b> / Ca: <b>{ca}</b>");
                }
                
                // text.Append($"Distance: {}m");
                
                TextTooltip.Instance.Show(actionObject.Name, text.ToString());
            }
            else
            {
                TextTooltip.Instance.Hide();
            }
        }

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        private void SetColor(Color color)
        {
            bindings.poi.color = color;
            bindings.icon.color = color;
        }
    }
}