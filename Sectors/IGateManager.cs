﻿using System.Collections.Generic;
using Commons.Sectors;
using Universes.SectorObjects;

namespace Sectors
{
    public interface IGateManager
    {
        List<IGate> Objects { get; }
        
        IGate Create(Gate baseObject);
        void Destroy(IGate targetObject);
        void Register(IGate targetObject);
        void Unregister(IGate targetObject);
        IGate FindByBaseObject(Gate baseObject);
        void DestroyAll();
    }
}