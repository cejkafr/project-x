﻿using Commons.Sectors;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public class StationManager : AObjectManager<IStation, Station>, IStationManager
    {
        [SerializeField] private StationSectorObject settings;
        
        public override IStation Create(Station baseObject)
        {
            var inst = Instantiate(settings.gamePrefab, baseObject.Position, baseObject.Rotation);
            var behaviour = inst.GetComponent<IStation>();
            behaviour.Init(baseObject, settings.icon, settings.actions);
            Register(behaviour);

            return behaviour;
        }

        public override void Destroy(IStation targetObject)
        {
            Unregister(targetObject);
        }
    }
}