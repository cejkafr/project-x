﻿using Commons;
using Commons.Sectors;
using Commons.UI;
using Sectors.Cinematic;
using UnityEngine;
using Universes;

namespace Sectors.Actions.Handlers
{
    public class UseGateActionHandler : IActionHandler
    {
        private readonly PlayerData data;
        private readonly GateEntryDirector director;

        public UseGateActionHandler(PlayerData data, GateEntryDirector director)
        {
            this.data = data;
            this.director = director;
        }

        public bool Supports(IActionObject actionObject, IAction action)
        {
            return actionObject is IGate && action.Type == ActionType.UseGate;
        }

        public void Handle(IFleet fleet, IActionObject actionObject, IAction action)
        {
            var gate = (IGate) actionObject;
            if (!gate.IsActive)
            {
                return;
            }
            
            TextTooltip.Instance.Hide();
            
            data.Player.SetFromGate(gate.Destination);
            director.Play(fleet, gate, OnDirectorComplete);
        }

        private void OnDirectorComplete()
        {
            Object.FindObjectOfType<UniverseController>()
                .LoadSector(data.Player.Sector, SectorLoadReason.WarpGate);
        }
    }
}