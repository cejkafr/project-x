﻿using Commons.Sectors;
using Sectors.Cinematic;
using Unity.Mathematics;
using UnityEngine;
using Universes;

namespace Sectors.Actions.Handlers
{
    public class AttackActionHandler : IActionHandler
    {
        private readonly PlayerData data;
        private readonly BattleEntryDirector director;

        public AttackActionHandler(PlayerData data, BattleEntryDirector director)
        {
            this.data = data;
            this.director = director;
        }

        public bool Supports(IActionObject actionObject, IAction action)
        {
            return actionObject is IFleet && action.Type == ActionType.Attack;
        }

        public void Handle(IFleet fleet, IActionObject actionObject, IAction action)
        {
            var otherFleet = (IFleet) actionObject;
            if (otherFleet.IsDestroyed)
            {
                return;
            }
            
            data.Player.Fleet.Position = fleet.Position;
            data.Player.EngageInTacticalCombat(new PlayerCombat(otherFleet.BaseObject));

            director.Play(fleet, otherFleet, OnDirectorComplete);
        }

        private void OnDirectorComplete()
        {
            var pos3d = data.Player.Fleet.Position;
            var pos = new int2(
                Mathf.RoundToInt(pos3d.x / data.Player.Sector.Biome.tileSize),
                Mathf.RoundToInt(pos3d.x / data.Player.Sector.Biome.tileSize));
            Object.FindObjectOfType<UniverseController>().LoadTacticalBattle(pos);
        }
    }
}