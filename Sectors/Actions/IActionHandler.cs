﻿namespace Sectors.Actions
{
    public interface IActionHandler
    {
        bool Supports(IActionObject actionObject, IAction action);
        void Handle(IFleet fleet, IActionObject actionObject, IAction action);
    }
}