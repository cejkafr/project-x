﻿using System.Linq;
using UnityEngine;

namespace Sectors.Actions
{
    public class ActionHandlerManager
    {
        private readonly IActionHandler[] handlers;

        public ActionHandlerManager(IActionHandler[] handlers)
        {
            this.handlers = handlers;
        }

        public void Handle(IFleet fleet, IActionObject actionObject, IAction action)
        {
            var handler = FindHandler(actionObject, action);
            if (handler == null)
            {
                Debug.LogError($"Failed to find handler for action key {action.Type}.");
                return;
            }
            
            handler.Handle(fleet, actionObject, action);
        }

        private IActionHandler FindHandler(IActionObject actionObject, IAction action)
        {
            return handlers.FirstOrDefault(handler => handler.Supports(actionObject, action));
        }
    }
}