﻿using Commons.Factions;
using Commons.Sectors;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors.Actions
{
    public readonly struct ObjectAction : IAction
    {
        public string Name { get; }
        public string Description { get; }
        public Sprite Icon { get; }
        public ActionType Type { get; }
        public bool IsDisabled { get; }
        public bool IsActive => !IsDisabled;

        public ObjectAction(string name, string description, Sprite icon, ActionType type, bool isDisabled)
        {
            Name = name;
            Description = description;
            Icon = icon;
            Type = type;
            IsDisabled = isDisabled;
        }
    }

    public interface IActionObject
    {
        Vector3 Position { get; }
        SectorObject SectorObject { get; }
        Sprite Icon { get; }
        string Name { get; }
        Faction Faction { get; }
        bool HasFaction { get; }
        bool IsActionRequired { get; }

        IAction[] Actions(IFleet iFleet);
        void Destroy();
    }
}