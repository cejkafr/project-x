﻿using Commons.Sectors;
using UnityEngine;

namespace Sectors.Actions
{
    public interface IAction
    {
        string Name { get; }
        string Description { get; }
        Sprite Icon { get; }
        bool IsActive { get; }
        ActionType Type { get; }
    }
}