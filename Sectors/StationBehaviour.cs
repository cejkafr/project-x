﻿using Commons.Factions;
using Commons.Sectors;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public class StationBehaviour : MonoBehaviour, IStation
    {
        private IAction[] actions;
        
        public SectorObject SectorObject { get; private set; }
        public Transform Transform => transform;
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;
        public Faction Faction => SectorObject.Faction;
        public bool HasFaction => Faction != null;
        public string Name => SectorObject.Name;
        public Sprite Icon { get; private set; }
        public bool IsActionRequired => false;
        
        public void Init(Station iStation, Sprite icon, SectorObjectAction[] objectActions)
        {
            SectorObject = iStation;
            Icon = icon;

            actions = new IAction[objectActions.Length];
            for (var i = 0; i < objectActions.Length; i++)
            {
                var oa = objectActions[i];
                actions[i] = new ObjectAction(oa.name, oa.description, oa.icon, oa.type, false);
            }
        }

        public IAction[] Actions(IFleet iFleet)
        {
            return actions;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}