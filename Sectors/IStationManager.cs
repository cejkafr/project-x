﻿using System.Collections.Generic;
using Commons.Sectors;
using Universes.SectorObjects;

namespace Sectors
{
    public interface IStationManager
    {
        List<IStation> Objects { get; }
        
        IStation Create(Station baseObject);
        void Destroy(IStation targetObject);
        void Register(IStation targetObject);
        void Unregister(IStation targetObject);
        IStation FindByBaseObject(Station baseObject);
        void DestroyAll();
    }
}