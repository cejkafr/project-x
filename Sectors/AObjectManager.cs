﻿using System.Collections.Generic;
using Commons.Sectors;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public abstract class AObjectManager<T, T1> : MonoBehaviour where T : IActionObject where T1 : SectorObject
    {
        public List<T> Objects { get; } = new List<T>();

        public abstract T Create(T1 baseObject);
        public abstract void Destroy(T targetObject);
        
        public virtual void Register(T targetObject)
        {
            Objects.Add(targetObject);
        }
        
        public virtual void Unregister(T targetObject)
        {
            Objects.Remove(targetObject);
        }

        public virtual T FindByBaseObject(T1 baseObject)
        {
            foreach (var o in Objects)
            {
                if (o.SectorObject != baseObject)
                {
                    continue;
                }

                return o;
            }

            return default;
        }
        
        public void DestroyAll()
        {
            foreach (var ob in Objects.ToArray())
            {
                ob.Destroy();
            }
            
            Objects.Clear();
        }
    }
}