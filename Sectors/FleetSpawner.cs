﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Ships;
using Unity.Mathematics;
using UnityEngine;
using Universes;
using Universes.SectorObjects;
using Random = UnityEngine.Random;

namespace Sectors
{
    public class FleetSpawner : MonoBehaviour
    {
        [SerializeField] private ShipObject[] shipTemplates;
        [SerializeField] private UniverseController universeController;

        public Fleet SpawnRandom(int size, int2 position, int rotation, Faction faction)
        {
            var ships = new List<IShip>(size);
            for (var i = 0; i < size; i++)
            {
                var template = shipTemplates[Random.Range(0, shipTemplates.Length)];
                ships.Add(ShipFactory.Create(template, faction));                
            }

            return CreateFleet(universeController.CurrentSector, position, rotation, faction, ships);
        }

        public static Fleet CreateFleet(Sector sector, int2 position, int rotation, Faction faction, List<IShip> ships)
        {
            var pos = new Vector3(position.x, 0f, position.y);
            var rot = Quaternion.Euler(0f, rotation, 0f);
            var fleet = new Fleet("Pirate Fleet", pos, rot, faction, ships);
            sector.RegisterFleet(fleet);
            return fleet;
        }
    }
}