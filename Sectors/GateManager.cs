﻿using Commons.Sectors;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public class GateManager : AObjectManager<IGate, Gate>
    {
        [SerializeField] private GateSectorObject settings;
        
        public override IGate Create(Gate baseObject)
        {
            var inst = Instantiate(settings.gamePrefab, baseObject.Position, baseObject.Rotation);
            var behaviour = inst.GetComponent<IGate>();
            behaviour.Init(baseObject, settings.icon, settings.actions);
            Register(behaviour);

            return behaviour;
        }

        public override void Destroy(IGate targetObject)
        {
            Unregister(targetObject);
        }
    }
}
