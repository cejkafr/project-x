﻿using Commons;
using Sectors.Cinematic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using Universes;

namespace Sectors
{
    public class StrategicMapController : MonoBehaviour
    {
        [SerializeField] private UniverseController universeController;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private Managers managers;
        [SerializeField] private CinematicDirectorManager cinematicDirector;

        private bool isPlayModeActive;
        
        [Space]
        public UnityEvent onPlayModeStarted = new UnityEvent();
        public UnityEvent onPlayModeEnded = new UnityEvent();
        
        public void StartPlayMode()
        {
            isPlayModeActive = true;
            
            universeController.LoadSector(playerController.Player.Sector, SectorLoadReason.LoadGame);
            playerController.StartPlayer();

            FindObjectOfType<FleetSpawner>()
                .SpawnRandom(1, new int2(2000, 2000), 0, universeController.Universe.PirateFaction());
            
            onPlayModeStarted.Invoke();
        }
        
        public void EndPlayMode()
        {
            isPlayModeActive = false;
            
            playerController.StopPlayer();

            onPlayModeEnded.Invoke();
        }

        public void OnUniverseCreated(Universe universe)
        {
            playerController.CreatePlayer(universeController.Universe, managers.Fleet);
        }

        public void OnUniverseDestroyed(Universe universe)
        {
            EndPlayMode();
            playerController.DestroyPlayer();
        }

        public void OnSectorLoaded(Sector sector, SectorLoadReason reason)
        {
            if (!isPlayModeActive) return;
            
            playerController.StartPlayer();

            switch (reason)
            {
                case SectorLoadReason.WarpGate:
                    cinematicDirector.GateExit.Play(playerController.Fleet,
                        managers.Gate.FindByBaseObject(playerController.Player.LastUsedGate));
                    break;
                case SectorLoadReason.TacticalBattle:
                {
                    var enemyFleet = managers.Fleet.FindByBaseObject(playerController.Player.Combat.Fleet);
                    cinematicDirector.BattleExit.Play(playerController.Fleet, enemyFleet, () =>
                    {
                        if (playerController.Player.Combat.Status == PlayerCombatStatus.Won)
                        {
                            universeController.CurrentSector
                                .UnregisterFleet(playerController.Player.Combat.Fleet);   
                        } 
                    });
                    break;
                }
            }
        }

        public void OnSectorUnloaded()
        {
            playerController.StopPlayer();
        }
    }
}