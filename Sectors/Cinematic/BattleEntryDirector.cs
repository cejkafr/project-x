﻿using System;
using System.Collections;
using Cinemachine;
using Game.Commons.UI;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;

namespace Sectors.Cinematic
{
    public class BattleEntryDirector : MonoBehaviour, ICinematicDirector
    {
        public event CinematicStartedEvent PlaybackStarted;
        public event CinematicEndedEvent PlaybackEnded;

        [SerializeField] private float fadeTime = 0.4f;
        [SerializeField] private float eventTriggerOffset = -.4f;
        [SerializeField] private PlayableDirector director;
        [SerializeField] private CinemachineVirtualCamera battleCamera;

        public void Play(IFleet fleet, IFleet target, [CanBeNull] Action onCompleted = null)
        {
            battleCamera.Follow = target.Transform;
            battleCamera.LookAt = target.Transform;

            StartCoroutine(Coroutine(onCompleted));
        }
        
        private IEnumerator Coroutine([CanBeNull] Action onCompleted)
        {
            PlaybackStarted?.Invoke();

            director.Play();
            Loader.Instance.FadeOut(fadeTime);
            yield return new WaitForSeconds((float) director.duration + eventTriggerOffset);

            PlaybackEnded?.Invoke();
            onCompleted?.Invoke();
        }
    }
}