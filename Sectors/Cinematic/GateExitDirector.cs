﻿using System;
using System.Collections;
using Cinemachine;
using Game;
using Game.Commons.UI;
using Game.Extensions;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;

namespace Sectors.Cinematic
{
    public class GateExitDirector : MonoBehaviour, ICinematicDirector
    {
        public event CinematicStartedEvent PlaybackStarted;
        public event CinematicEndedEvent PlaybackEnded;

        private const string TrackWarpTunnelGlow = "WarpTunnel Glow Activation";
        private const string TrackWarpTunnel = "WarpTunnel Activation";
        private const string TrackFleetTranslation = "Fleet Movement Animation";

        [SerializeField] private float fadeTime = 0.8f;
        [SerializeField] private float eventTriggerOffset = -.4f;
        [SerializeField] private PlayableDirector director;
        [SerializeField] private CinemachineVirtualCamera gateCamera;
        [SerializeField] private CinemachineVirtualCamera followCamera;
        [SerializeField] private TextMeshProUGUI exitSectorName;

        public void Play(IFleet fleet, IGate target, [CanBeNull] Action onCompleted = null)
        {
            director.BindTrack(TrackWarpTunnel, target.WarpTunnel);
            director.BindTrack(TrackWarpTunnelGlow, target.WarpTunnelGlow);
            director.BindTrack(TrackFleetTranslation, fleet.Animator);
            
            var pos = target.Position - target.Transform.forward * 100;
            var rot = Quaternion.Euler(0f, target.Rotation.eulerAngles.y, 0f);

            director.SetTrackOffset(TrackFleetTranslation, pos, rot);

            gateCamera.Follow = target.Transform;
            gateCamera.LookAt = target.Transform;
            followCamera.Follow = fleet.Transform;
            followCamera.LookAt = fleet.Transform;

            exitSectorName.text = target.Destination.Sector.Name;

            fleet.OverrideThrusters = true;

            StartCoroutine(GateExitCoroutine(fleet, onCompleted));
        }

        private IEnumerator GateExitCoroutine(IFleet fleet, [CanBeNull] Action onCompleted)
        {
            PlaybackStarted?.Invoke();

            director.Play();
            yield return new WaitForSeconds((float) director.duration + eventTriggerOffset);

            fleet.OverrideThrusters = false;

            PlaybackEnded?.Invoke();
            onCompleted?.Invoke();
        }
    }
}