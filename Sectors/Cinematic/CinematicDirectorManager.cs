﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Sectors.Cinematic
{
    public class CinematicDirectorManager : MonoBehaviour
    {
        [Serializable]
        public class CinematicEvent : UnityEvent
        {
        }

        [SerializeField] private GateEntryDirector gateEntry;
        [SerializeField] private GateExitDirector gateExit;
        [SerializeField] private BattleEntryDirector battleEntry;
        [SerializeField] private BattleExitDirector battleExit;

        [Space]
        public CinematicEvent onCinematicStarted = new CinematicEvent();
        public CinematicEvent onCinematicEnded = new CinematicEvent();

        public GateEntryDirector GateEntry => gateEntry;
        public GateExitDirector GateExit => gateExit;
        public BattleEntryDirector BattleEntry => battleEntry;
        public BattleExitDirector BattleExit => battleExit;

        private void Awake()
        {
            gateEntry.PlaybackStarted += OnCinematicStarted;
            gateEntry.PlaybackEnded += OnCinematicEnded;
            gateExit.PlaybackStarted += OnCinematicStarted;
            gateExit.PlaybackEnded += OnCinematicEnded;
            
            battleEntry.PlaybackStarted += OnCinematicStarted;
            battleEntry.PlaybackEnded += OnCinematicEnded;
            battleExit.PlaybackStarted += OnCinematicStarted;
            battleExit.PlaybackEnded += OnCinematicEnded;
        }

        private void OnCinematicStarted()
        {
            onCinematicStarted.Invoke();
        }

        private void OnCinematicEnded()
        {
            onCinematicEnded.Invoke();
        }
    }
}