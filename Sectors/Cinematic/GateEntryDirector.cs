﻿using System;
using System.Collections;
using Cinemachine;
using Game;
using Game.Commons.UI;
using Game.Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;

namespace Sectors.Cinematic
{
    public class GateEntryDirector : MonoBehaviour, ICinematicDirector
    {
        public event CinematicStartedEvent PlaybackStarted;
        public event CinematicEndedEvent PlaybackEnded;

        private const string TrackWarpTunnelGlow = "WarpTunnel Glow Activation";
        private const string TrackWarpTunnel = "WarpTunnel Activation";
        private const string TrackFleetTranslation = "Fleet Movement Animation";

        [SerializeField] private float fadeTime = 0.8f;
        [SerializeField] private float eventTriggerOffset = -.4f;
        [SerializeField] private PlayableDirector director;
        [SerializeField] private CinemachineVirtualCamera gateCamera;
        [SerializeField] private CinemachineVirtualCamera followCamera;

        public void Play(IFleet fleet, IGate target, [CanBeNull] Action onCompleted = null)
        {
            director.BindTrack(TrackWarpTunnel, target.WarpTunnel);
            director.BindTrack(TrackWarpTunnelGlow, target.WarpTunnelGlow);
            director.BindTrack(TrackFleetTranslation, fleet.Animator);

            var pos = target.Position + target.Transform.forward * 300;
            var rot = Quaternion.Euler(0f, target.Rotation.eulerAngles.y - 180f, 0f);

            director.SetTrackOffset(TrackFleetTranslation, pos, rot);

            gateCamera.Follow = target.Transform;
            gateCamera.LookAt = target.Transform;
            followCamera.Follow = fleet.Transform;
            followCamera.LookAt = fleet.Transform;

            StartCoroutine(GateEnterCoroutine(fleet, onCompleted));
        }


        private IEnumerator GateEnterCoroutine(IFleet fleet, [CanBeNull] Action onCompleted)
        {
            var fadeHalf = fadeTime / 2f;
            
            PlaybackStarted?.Invoke();

            Loader.Instance.FadeOut(fadeHalf);
            yield return new WaitForSeconds(fadeHalf);

            fleet.OverrideThrusters = true;
            director.Play();
            Loader.Instance.FadeIn(fadeHalf);
            yield return new WaitForSeconds((float) director.duration + eventTriggerOffset);

            // PlaybackEnded?.Invoke();
            onCompleted?.Invoke();
        }
    }
}