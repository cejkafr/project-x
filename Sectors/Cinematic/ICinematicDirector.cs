﻿namespace Sectors.Cinematic
{
    public delegate void CinematicStartedEvent();
    public delegate void CinematicEndedEvent();
    
    public interface ICinematicDirector
    {
        event CinematicStartedEvent PlaybackStarted;
        event CinematicEndedEvent PlaybackEnded;
    }
}