﻿using System;
using System.Collections;
using Cinemachine;
using Game;
using Game.Commons.UI;
using Game.Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;

namespace Sectors.Cinematic
{
    public class BattleExitDirector : MonoBehaviour, ICinematicDirector
    {
        public event CinematicStartedEvent PlaybackStarted;
        public event CinematicEndedEvent PlaybackEnded;

        private const string TargetFleet = "Target Fleet";
        
        [SerializeField] private float fadeTime = 0.4f;
        [SerializeField] private float eventTriggerOffset = -.4f;
        [SerializeField] private PlayableDirector director;
        [SerializeField] private CinemachineVirtualCamera battleCamera;
        [SerializeField] private GameObject explosion;

        public void Play(IFleet fleet, IFleet target, [CanBeNull] Action onCompleted = null)
        {
            battleCamera.Follow = target.Transform;
            battleCamera.LookAt = target.Transform;

            explosion.transform.position = target.Position;
            
            director.BindTrack(TargetFleet, target.Transform.gameObject);
            
            StartCoroutine(Coroutine(onCompleted));
        }

        private IEnumerator Coroutine([CanBeNull] Action onCompleted)
        {
            PlaybackStarted?.Invoke();

            Loader.Instance.FadeIn(fadeTime);
            yield return new WaitForSeconds(fadeTime);

            director.Play();

            yield return new WaitForSeconds((float) director.duration + eventTriggerOffset);

            PlaybackEnded?.Invoke();
            onCompleted?.Invoke();
        }
    }
}