﻿using System;
using Commons.Sectors;
using UnityEngine;
using UnityEngine.Events;
using Universes.SectorObjects;

namespace Sectors
{
    public class FleetManager : AObjectManager<IFleet, Fleet>
    {
        [Serializable]
        public class FleetEvent : UnityEvent<IFleet>
        {
            
        }

        [SerializeField] private GameObject fleetPrefab;
        [SerializeField] private GameObject playerFleetPrefab;
        [SerializeField] private FleetSectorObject settings;

        [Space]
        public FleetEvent onFleetCreated;
        public FleetEvent onFleetDestroyed;

        public override IFleet Create(Fleet baseObject)
        {
            var inst = Instantiate(fleetPrefab, baseObject.Position, baseObject.Rotation);
            var behaviour = inst.GetComponent<IFleet>();
            behaviour.Init(baseObject, settings.icon, settings.actions);

            Register(behaviour);

            onFleetCreated.Invoke(behaviour);

            return behaviour;
        }

        public IFleet CreatePlayer(Fleet baseObject)
        {
            var inst = Instantiate(playerFleetPrefab, baseObject.Position, baseObject.Rotation);
            var behaviour = inst.GetComponent<IFleet>();
            behaviour.Init(baseObject, null, new SectorObjectAction[0]);

            onFleetCreated.Invoke(behaviour);

            return behaviour;
        }

        public override void Destroy(IFleet targetObject)
        {
            Unregister(targetObject);
            targetObject.Destroy();
            onFleetDestroyed.Invoke(targetObject);
        }

        public void Destroy(Fleet fleet)
        {
            foreach (var o in Objects)
            {
                if (o.BaseObject == fleet)
                {
                    Destroy(o);
                    return;
                }
            }
            
            Debug.LogError("Failed to find fleet to destroy.");
        }
    }
}