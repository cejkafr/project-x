﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Sectors;
using Commons.Ships;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public delegate void FleetTriggerInteractiveEvent(IFleet fleet, IActionObject i);

    public interface IFleet : IActionObject
    {
        event FleetTriggerInteractiveEvent TriggerInteractiveEnter;
        event FleetTriggerInteractiveEvent TriggerInteractiveExit;

        Transform Transform { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        List<IShip> Ships { get; }
        Faction Faction { get; }
        bool IsDestroyed { get; }
        Fleet BaseObject { get; }
        Vector3 Direction { get; set; }
        bool Accelerate { get; set; }
        Animator Animator { get; }
        bool OverrideThrusters { get; set; }

        void Init(Fleet iFleet, Sprite icon, SectorObjectAction[] actions);
    }
}