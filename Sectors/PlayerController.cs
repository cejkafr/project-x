﻿using System;
using System.Linq;
using Commons.Factions;
using Commons.Ships;
using Sectors.Actions;
using Sectors.Actions.Handlers;
using Sectors.Cameras;
using Sectors.Cinematic;
using UnityEngine;
using UnityEngine.Events;
using Universes;
using Universes.SectorObjects;

namespace Sectors
{
    public class PlayerController : MonoBehaviour
    {
        [Serializable]
        public class PlayerFleetEvent : UnityEvent<IFleet>
        {
        }

        [Serializable]
        public class PlayerActiveEvent : UnityEvent<bool>
        {
        }

        [SerializeField] private PlayerData data;
        [SerializeField] private CinematicDirectorManager cinematicDirector;
        [SerializeField] private CinemachineFollowCamera playerCamera;

        [Space] public PlayerFleetEvent onPlayerFleetCreated = new PlayerFleetEvent();
        public PlayerFleetEvent onPlayerFleetDestroyed = new PlayerFleetEvent();
        public PlayerActiveEvent onPlayerActive = new PlayerActiveEvent();

        private ActionHandlerManager handlerManager;

        public Player Player => data.Player;
        public IFleet Fleet { get; set; }

        private void Awake()
        {
            handlerManager = new ActionHandlerManager(new IActionHandler[]
            {
                new UseGateActionHandler(data, cinematicDirector.GateEntry),
                new AttackActionHandler(data, cinematicDirector.BattleEntry),
            });
        }

        public void StartPlayer()
        {
            gameObject.SetActive(true);
        }

        public void StopPlayer()
        {
            gameObject.SetActive(false);
        }

        public void CreatePlayer(Universe universe, FleetManager fleetManager)
        {
            var sector = universe.Sectors[data.settings.player.sectorIndex];
            var fleet = CreateFleet(data.settings.player.fleet, universe.PlayerFaction(), sector);
            data.CreatePlayer(fleet, sector);
            sector.Fleets.Add(fleet);

            Fleet = fleetManager.CreatePlayer(fleet);
            playerCamera.SetTarget(Fleet.Transform);

            onPlayerFleetCreated.Invoke(Fleet);
        }

        public void DestroyPlayer()
        {
            onPlayerFleetDestroyed.Invoke(Fleet);
        }

        private Fleet CreateFleet(ManualSettingsFleet fleetSettings, Faction faction, Sector sector)
        {
            var ships = fleetSettings.ships
                .Select(shipObject => ShipFactory.Create(shipObject, faction)).ToList();
            return FleetSpawner.CreateFleet(sector, fleetSettings.position, fleetSettings.rotation, faction, ships);
        }

        public void ExecuteAction(IActionObject actionObject, IAction action)
        {
            handlerManager.Handle(Fleet, actionObject, action);
        }

        public void Wait(bool value)
        {
            onPlayerActive.Invoke(value);
        }

        public void Move(Vector3 dir)
        {
            Fleet.Direction = dir;
            Fleet.Accelerate = true;

            onPlayerActive.Invoke(true);
        }

        public void Stop()
        {
            Fleet.Accelerate = false;
            onPlayerActive.Invoke(false);
        }
    }
}