﻿using System.Collections.Generic;
using Commons;
using Commons.Factions;
using Commons.Sectors;
using Commons.Ships;
using Game.Commons.Audio;
using Game.Extensions;
using Games;
using Games.Audio;
using Sectors.Actions;
using UnityEngine;
using Universes.SectorObjects;

namespace Sectors
{
    public class FleetBehaviour : MonoBehaviour, IFleet
    {
        public event FleetTriggerInteractiveEvent TriggerInteractiveEnter;
        public event FleetTriggerInteractiveEvent TriggerInteractiveExit;

        [SerializeField] private float moveSpeed;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private WeakSoundDef moveSFX;

        private ShipThrusterController thrusterController;
        private bool accelerate;
        private SoundSystem.SoundHandle soundHandle;
        private IAction[] actions;

        public Fleet BaseObject { get; private set; }
        public SectorObject SectorObject => BaseObject;
        public Transform Transform => transform;
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;
        public List<IShip> Ships => BaseObject.Ships;
        public Faction Faction => BaseObject.Faction;
        public bool HasFaction => Faction != null;
        public bool IsDestroyed => BaseObject.IsDestroyed;
        public Vector3 Direction { get; set; }

        public bool OverrideThrusters
        {
            get => thrusterController.ThrustersEnabled;
            set
            {
                if (value == false && accelerate) return;
                thrusterController.ThrustersEnabled = value;
            }
        }

        public bool Accelerate
        {
            get => accelerate;
            set
            {
                if (accelerate == value) return;
                accelerate = value;
                OverrideThrusters = accelerate;
                if (accelerate)
                {
                    soundHandle = GameManager.SoundSystem.Play(moveSFX, transform);
                }
                else
                {
                    GameManager.SoundSystem.Stop(soundHandle);
                }
            }
        }

        public Animator Animator => GetComponent<Animator>();
        public string Name => BaseObject.Name;
        public Sprite Icon { get; private set; }
        public bool IsActionRequired => true;

        private void Awake()
        {
            thrusterController = GetComponentInChildren<ShipThrusterController>();
            thrusterController.SetThrusters(1f);
        }

        public void Init(Fleet iFleet, Sprite icon, SectorObjectAction[] objectActions)
        {
            BaseObject = iFleet;
            Icon = icon;

            actions = new IAction[objectActions.Length];
            for (var i = 0; i < objectActions.Length; i++)
            {
                var oa = objectActions[i];
                actions[i] = new ObjectAction(oa.name, oa.description, oa.icon, oa.type, false);
            }
        }

        private void Update()
        {
            if (Accelerate)
            {
                Rotate();
                Move();
            }
        }

        public IAction[] Actions(IFleet iFleet)
        {
            return actions;
        }

        private void Rotate()
        {
            var targetRotation = Quaternion.LookRotation(Direction.Flatten());
            transform.rotation = Quaternion.Slerp(transform.rotation,
                targetRotation, rotationSpeed * Mathf.Deg2Rad * Time.deltaTime);
        }

        private void Move()
        {
            transform.Translate(Vector3.forward * (moveSpeed * Time.deltaTime));
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            var interactive = other.GetComponent<IActionObject>();
            if (interactive == null)
            {
                return;
            }

            TriggerInteractiveEnter?.Invoke(this, interactive);
        }

        private void OnTriggerExit(Collider other)
        {
            var interactive = other.GetComponent<IActionObject>();
            if (interactive == null)
            {
                return;
            }

            TriggerInteractiveExit?.Invoke(this, interactive);
        }
    }
}