﻿using Commons.Sectors;
using Universes;
using Universes.SectorObjects;

namespace Sectors.Presenters
{
    public class SectorObjectPresenter
    {
        private readonly Managers managers;
        private Sector sector;

        public SectorObjectPresenter(Managers managers)
        {
            this.managers = managers;
        }

        public void Present(Sector iSector)
        {
            if (sector == iSector) return;
            if (sector != null) Destroy();

            sector = iSector;

            foreach (var gate in sector.Gates)
            {
                managers.Gate.Create(gate);
            }

            foreach (var station in sector.Stations)
            {
                managers.Station.Create(station);
            }

            foreach (var fleet in sector.Fleets)
            {
                OnFleetRegistered(fleet);
            }

            sector.FleetRegistered += OnFleetRegistered;
            sector.FleetUnregistered += OnFleetUnregistered;
        }

        public void Destroy()
        {
            if (sector == null) return;
            
            sector.FleetRegistered -= OnFleetRegistered;
            sector.FleetUnregistered -= OnFleetUnregistered;

            managers.Gate.DestroyAll();
            managers.Station.DestroyAll();
            managers.Fleet.DestroyAll();

            sector = null;
        }

        public void OnFleetRegistered(Fleet fleet)
        {
            if (fleet.Faction.IsHuman) return;
            managers.Fleet.Create(fleet);
        }

        public void OnFleetUnregistered(Fleet fleet)
        {
            if (fleet.Faction.IsHuman) return;
            managers.Fleet.Destroy(fleet);
        }
    }
}