﻿using System;
using Commons.Presenters;
using Commons.Sectors;
using UnityEngine;
using Universes;
using Universes.Presenters;
using Random = UnityEngine.Random;

namespace Sectors.Presenters
{
    public class SectorPresenter : MonoBehaviour, ISectorPresenter
    {
        [SerializeField] private GameObject lightSourcePrefab;
        [SerializeField] private Managers managers;

        private IBulkDisableObjectCache cache;
        private ISectorEnvPresenter envPresenter;
        private SectorObjectPresenter objectPresenter;

        private void Awake()
        {
            var cacheParent = new GameObject("Object Cache").transform;
            cache = new BulkDisableObjectCache(cacheParent);
            
            var positionSampler = new SimplePositionSamplerV1();
            var envObjectPresenter = new CachedObjectPresenter(cache);
            var layerPresenter = new ProceduralPresenter(positionSampler, envObjectPresenter);
            var lightSourcePresenter = new CachedLightSourcePresenter(cache, lightSourcePrefab);
            var pathPresenter = GetComponent<CachedSectorPathPresenter>();
            pathPresenter.Init(cache);

            envPresenter = new SectorEnvPresenter(layerPresenter, lightSourcePresenter, pathPresenter);   
            
            objectPresenter = new SectorObjectPresenter(managers);
        }

        public void Present(Sector sector)
        {
            envPresenter.Present(sector);
            objectPresenter.Present(sector);

            Random.InitState(Environment.TickCount);
        }

        public void Destroy()
        {
            objectPresenter.Destroy();
            cache.DisableAll();
        }
    }
}