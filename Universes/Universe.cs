﻿using System.Linq;
using Commons.Factions;
using Commons.Sectors;

namespace Universes
{
    public class Universe
    {
        public string Seed { get; }
        public Sector[] Sectors { get; }
        public Sector[,] Map { get; }
        public Faction[] Factions { get; }
        // public List<Fleet> Fleets { get; } = new List<Fleet>();
        // public List<Gate> Gates { get; } = new List<Gate>();
        // public List<Station> Stations { get; } = new List<Station>();

        public Universe(string seed, Sector[] sectors, Sector[,] map, Faction[] factions)
        {
            Seed = seed;
            Sectors = sectors;
            Map = map;
            Factions = factions;
        }

        public Faction PlayerFaction()
        {
            return Factions.FirstOrDefault(faction => faction.IsHuman);
        }
        
        public Faction PirateFaction()
        {
            return Factions.FirstOrDefault(faction => faction.IsPirate);
        }
    }
}