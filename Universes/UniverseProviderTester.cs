﻿using Game.Extensions;
using Unity.Mathematics;
using UnityEngine;

namespace Universes
{
    public class UniverseProviderTester : MonoBehaviour
    {
        [SerializeField] private UniverseController universeController;
        [SerializeField] private Vector2 mapSize;
        [SerializeField] private UniverseMapTester universeMapTester;
        [SerializeField] private string seed;
        [SerializeField] private int2 sectorCount;

        private void Start()
        {
            Regenerate();
        }

        public void Regenerate()
        {
            universeMapTester.Clear();
            
            universeController.Init(seed, sectorCount);
            
            universeController.Universe.Map.IterateOver((x, y, sector) =>
            {
                var pos = new Vector2(
                    (mapSize.x * x + (universeMapTester.Spacing.x * x)),
                    (mapSize.y * y + (universeMapTester.Spacing.y * y)) * -1
                );

                var map = universeMapTester.CreateSectorMap(pos, mapSize);
                map.Init(sector);
            });
        }
    }
}