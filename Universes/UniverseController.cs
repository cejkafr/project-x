﻿using System;
using Commons;
using Commons.DataObjects;
using Commons.Procedural.NoiseMaps;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using Universes.Generators;
using Universes.Presenters;

namespace Universes
{
    public class UniverseController : MonoBehaviour
    {
        [Serializable]
        public class UniverseEvent : UnityEvent<Universe>
        {
        }

        [Serializable]
        public class SectorEvent : UnityEvent<Sector, SectorLoadReason>
        {
        }


        [Serializable]
        public class BattleEvent : UnityEvent<Battle>
        {
        }

        [SerializeField] private GameLibraryObject library;
        [SerializeField] private UniverseProvider universeProvider;
        [SerializeField] private SectorPresenterWrapper sectorPresenterWrapper;
        [SerializeField] private BattlePresenterWrapper battlePresenterWrapper;

        [Space] 
        public UniverseEvent onUniverseCreated = new UniverseEvent();
        public UniverseEvent onUniverseDestroyed = new UniverseEvent();
        public SectorEvent onSectorLoaded = new SectorEvent();
        public SectorEvent onSectorUnloaded = new SectorEvent();
        public BattleEvent onBattleLoaded = new BattleEvent();
        public BattleEvent onBattleUnloaded = new BattleEvent();

        private BattleGenerator battleGenerator;
        
        public Universe Universe { get; private set; }
        public Sector CurrentSector { get; private set; }
        public Battle? CurrentBattle { get; private set; }
        public bool IsUniverseReady => Universe != null;
        public bool IsBattleReady => CurrentBattle.HasValue;

        private void Start()
        {
            universeProvider.InitWithLibrary(library);
            
            var noiseMapGenerator = new NoiseMapGenerator();
            battleGenerator = new BattleGenerator(noiseMapGenerator);
        }

        public void Init(string seed, int2 sectorCount)
        {
            if (CurrentSector != null)
                onSectorUnloaded.Invoke(CurrentSector, SectorLoadReason.LoadGame);
            if (Universe != null)
                onUniverseDestroyed.Invoke(Universe);
            sectorPresenterWrapper.Value.Destroy();

            CurrentSector = null;
            CurrentBattle = null;

            var desc = universeProvider.Generate(seed, sectorCount);
            Universe = UniverseMapper.CreateUniverse(desc, universeProvider.CreateFactions());
            onUniverseCreated.Invoke(Universe);
        }

        public void LoadSector(Sector sector, SectorLoadReason reason)
        {
            if (CurrentSector == sector && !IsBattleReady) return;
            UnloadSector(reason);
            UnloadTacticalBattle();

            CurrentSector = sector;
            sectorPresenterWrapper.Value.Present(sector);
            onSectorLoaded.Invoke(sector, reason);
        }

        private void UnloadSector(SectorLoadReason reason)
        {
            if (CurrentSector == null) return;
            sectorPresenterWrapper.Value.Destroy();
            onSectorUnloaded.Invoke(CurrentSector, reason);
        }

        public void LoadTacticalBattle(int2 position)
        {
            UnloadSector(SectorLoadReason.TacticalBattle);
            UnloadTacticalBattle();

            CurrentBattle = battleGenerator.Generate(CurrentSector, position, new int2(50, 50));
            
            battlePresenterWrapper.Value.Present(CurrentBattle.Value);
            onBattleLoaded.Invoke(CurrentBattle.Value);
        }

        private void UnloadTacticalBattle()
        {
            if (!IsBattleReady) return;
            battlePresenterWrapper.Value.Destroy();
            var battle = CurrentBattle.Value;
            CurrentBattle = null;
            onBattleUnloaded.Invoke(battle);
        }
    }
}