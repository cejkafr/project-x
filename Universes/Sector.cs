﻿using System.Collections.Generic;
using System.Linq;
using Commons.Factions;
using Commons.Procedural.Lights;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;
using Universes.SectorObjects;

namespace Universes
{
    public class Sector
    {
        public delegate void FleetEvent(Fleet fleet);

        public event FleetEvent FleetRegistered;
        public event FleetEvent FleetUnregistered;
        
        public Universe Universe { get; private set; }
        public int Seed { get; }
        public int2 Coords { get; }
        public int Size { get; }
        public string Name { get; }
        public Faction Faction { get; }
        public SectorBiomeObject Biome { get; }
        public Sector[] Links { get; private set; }
        public string Music  { get; }
        public Skybox Skybox { get; }
        public NoiseMap ProceduralLayers { get; }
        public SectorPath[] Paths { get; }
        public LightSource[] Lights { get; }

        public List<Gate> Gates { get; } = new List<Gate>(4);
        public List<Station> Stations { get; } = new List<Station>(2);
        public List<POI> POIs { get; } = new List<POI>();
        public List<Fleet> Fleets { get; } = new List<Fleet>();

        public Sector(int seed, int2 coords, int size, string name, Faction faction, SectorBiomeObject biome, 
            string music, Skybox skybox, NoiseMap proceduralLayers,  SectorPath[] paths, LightSource[] lights)
        {
            Seed = seed;
            Coords = coords;
            Size = size;
            Name = name;
            Faction = faction;
            Biome = biome;
            Music = music;
            Skybox = skybox;
            ProceduralLayers = proceduralLayers;
            Paths = paths;
            Lights = lights;
        }

        public void SetLinks(Sector[] links)
        {
            Links = links;
        }

        public Gate FindGateBySector(Sector sector)
        {
            return (from gate in Gates let b2 = gate.TargetCoords == sector.Coords where b2.x && b2.y select gate).FirstOrDefault();
        }

        public void RegisterFleet(Fleet fleet)
        {
            Fleets.Add(fleet);
            FleetRegistered?.Invoke(fleet);
        }

        public void UnregisterFleet(Fleet fleet)
        {
            Fleets.Remove(fleet);
            FleetUnregistered?.Invoke(fleet);
        }
    }
}