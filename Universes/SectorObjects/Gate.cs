﻿using Unity.Mathematics;
using UnityEngine;

namespace Universes.SectorObjects
{
    public class Gate : SectorObject
    {
        public Sector Sector { get; }
        public int2 TargetCoords { get; }
        public Gate Target { get; private set; }
        public bool Active { get; set; } = true;

        public Gate(string name, Vector3 position, Quaternion rotation, Sector sector, int2 targetCoords) 
            : base(name, position, rotation, null)
        {
            Sector = sector;
            TargetCoords = targetCoords;
        }

        public void SetTarget(Gate target)
        {
            Target = target;
        }
    }
}