﻿using Commons.Factions;
using UnityEngine;

namespace Universes.SectorObjects
{
    public class POI : SectorObject
    {
        public POI(string name, Vector3 position, Quaternion rotation, Faction faction) : base(name, position, rotation, faction)
        {
        }
    }
}