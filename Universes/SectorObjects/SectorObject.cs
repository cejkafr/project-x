﻿using Commons.Factions;
using Unity.Mathematics;
using UnityEngine;

namespace Universes.SectorObjects
{
    public abstract class SectorObject
    {
        public string Name { get; }
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }
        public Faction Faction { get; }
        public bool Visible { get; } = true;
        public int IntelAmount { get; } = 100;

        protected SectorObject(string name, Vector3 position, Quaternion rotation, Faction faction)
        {
            Name = name;
            Position = position;
            Rotation = rotation;
            Faction = faction;
        }
    }
}