﻿using Commons.Factions;
using Commons.Sectors;
using UnityEngine;

namespace Universes.SectorObjects
{
    public class Station : SectorObject
    {
        public Station(string name, Vector3 position, Quaternion rotation, Faction faction) : base(name, position, rotation, faction)
        {
        }
    }
}