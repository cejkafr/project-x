﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Ships;
using UnityEngine;

namespace Universes.SectorObjects
{
    public class Fleet : SectorObject
    {
        public List<IShip> Ships { get; }
        public bool IsDestroyed { get; private set; }

        public Fleet(string name, Vector3 position, Quaternion rotation, Faction faction, List<IShip> ships) 
            : base(name, position, rotation, faction)
        {
            Ships = ships;
        }

        public void Destroy()
        {
            IsDestroyed = true;
        }
    }
}