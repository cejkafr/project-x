﻿using System;
using Commons.DataObjects.Ships;
using Commons.Factions;
using Commons.Sectors;
using Commons.Ships;
using DataObjects.Ships;
using Unity.Mathematics;
using UnityEngine;
using Universes.SectorObjects;

namespace Universes
{
    [Serializable]
    public struct ManualSettingsPlayer
    {
        public FactionObject faction;
        public int sectorIndex;
        public ManualSettingsFleet fleet;
    }

    [Serializable]
    public struct ManualSettingsFleet
    {
        public ShipObject[] ships;
        public int2 position;
        public int rotation;
        public FactionObject faction;
    }

    [Serializable]
    public struct ManualSettings
    {
        public ManualSettingsPlayer player;
    }

    public class PlayerData : MonoBehaviour
    {
        [SerializeField] public ManualSettings settings;

        public Player Player { get; private set; }

        private void Awake()
        {
            if (FindObjectsOfType<PlayerData>().Length > 1)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(this);
        }

        public void CreatePlayer(Fleet fleet, Sector sector)
        {
            Player = new Player(fleet.Faction, fleet, sector);
        }

        // private void CreateFleets(IEnumerable<ManualSettingsFleet> fleets, Sector sector)
        // {
        //     foreach (var fleetSettings in fleets)
        //     {
        //         var faction = new Faction(fleetSettings.faction, library.factionAttitudeTranslator);
        //         Factions.Add(faction);
        //
        //         var fleet = CreateFleet(fleetSettings, faction);
        //         Fleets.Add(fleet);
        //         sector.Fleets.Add(fleet);
        //     }
        // }

        // private void CreateStations(IEnumerable<ManualSettingsStation> stations, Sector sector)
        // {
        //     foreach (var s in stations)
        //     {
        //         var faction = new Faction(s.faction, library.factionAttitudeTranslator);
        //         Factions.Add(faction);
        //
        //         var pos = new Vector3(s.position.x, 0f, s.position.y);
        //         var rot = Quaternion.Euler(0f, s.rotation, 0f);
        //         var station = new Station(s.name, pos, rot, faction);
        //         Stations.Add(station);
        //         sector.Stations.Add(station);
        //     }
        // }
    }
}