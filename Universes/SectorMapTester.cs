﻿using System.Text;
using Commons;
using Commons.Sectors;
using Commons.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Universes
{
    public class SectorMapTester : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField] private Image background;
        [SerializeField] private GameObject[] links;
        [SerializeField] private Image[] layerImages;
        [SerializeField] private Color[] layerColors;
        [SerializeField] private Image objectLayerImage;
        [SerializeField] private Color objectLayerColor;

        private Sector sector;

        public Image[] Layers => layerImages;

        public void Init(Sector iSector)
        {
            sector = iSector;

            if (iSector == null)
            {
                Clear();
                return;
            }

            RenderLayers(sector.ProceduralLayers.Map);

            foreach (var link in sector.Links)
            {
                var dir = sector.Coords - link.Coords;
                if (dir.y == 1) links[0].SetActive(true);
                if (dir.x == -1) links[1].SetActive(true);
                if (dir.y == -1) links[2].SetActive(true);
                if (dir.x == 1) links[3].SetActive(true);
            }

            RenderObjectLayer(objectLayerImage, objectLayerColor);
        }
        
        private void Clear()
        {
            background.color = Color.black;
            foreach (var layerImage in layerImages)
            {
                layerImage.color = Color.clear;
            }

            objectLayerImage.color = Color.clear;
        }
        
        private void RenderLayers(float[,,] map)
        {
            for (var layer = 0; layer < map.GetLength(0); layer++)
            {
                var width = map.GetLength(1);
                var height = map.GetLength(2);
                var pixels = new Color[width * height];

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        var c = Color.Lerp(Color.clear, Color.white, map[layer, x, y]);
                        pixels[y * width + x] = c;
                    }
                }

                RenderPixels(width, height, pixels, layerImages[layer], layerColors[layer]);
            }
        }

        private void RenderPixels(int width, int height, Color[] pixels, Image image, Color color)
        {
            var texture = new Texture2D(width, height);
            texture.SetPixels(pixels);
            texture.Apply();

            var sprite = Sprite.Create(texture,
                new Rect(0.0f, 0.0f, texture.width, texture.height),
                new Vector2(0.5f, 0.5f), 100);

            image.sprite = sprite;
            image.color = color;
        }

        public void RenderObjectLayer(Image image, Color color)
        {
            // var width = sector.ObjectMap.GetLength(0);
            // var height = sector.ObjectMap.GetLength(1);
            // var pixels = new Color[width * height];
            // for (var y = 0; y < height; y++)
            // {
            //     for (var x = 0; x < width; x++)
            //     {
            //         var c = Color.clear;
            //         if (sector.ObjectMap[x, y] != null)
            //         {
            //             c = Color.white;
            //         }
            //         
            //         pixels[y *  width + x] = c;
            //     }
            // }
            //
            // var texture = new Texture2D(width, height);
            // texture.SetPixels(pixels);
            // texture.Apply();
            //
            // var sprite = Sprite.Create(texture,
            //     new Rect(0.0f, 0.0f, texture.width, texture.height),
            //     new Vector2(0.5f, 0.5f), 100);
            //
            // image.sprite = sprite;
            // image.color = color;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (sector == null)
            {
                return;
            }

            var sb = new StringBuilder();
            sb.AppendLine($"Seed: {sector.Seed}");
            sb.Append("Links to: ");
            foreach (var link in sector.Links)
            {
                sb.Append($"{link.Coords.x}x{link.Coords.y}, ");                
            }

            var title = $"{sector.Coords.x}x{sector.Coords.y}";
            TextTooltip.Instance.Show(title, sb.ToString());
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            TextTooltip.Instance.Hide();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (sector == null)
            {
                return;
            }

            FindObjectOfType<UniverseController>().LoadSector(sector, SectorLoadReason.LoadGame);
        }
    }
}