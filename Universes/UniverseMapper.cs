﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Sectors;
using Game.Extensions;
using Unity.Mathematics;
using UnityEngine;
using Universes.Generators;
using Universes.SectorObjects;

namespace Universes
{
    public static class UniverseMapper
    {
        public static Universe CreateUniverse(UniverseDesc desc, Faction[] factions)
        {
            var sectors = new Sector[desc.SectorCount];
            var map = CreateMap(desc.Map, sectors);
            LinkSectors(desc.Map, map);
            return new Universe(desc.Seed, sectors, map, factions);
        }

        private static Sector[,] CreateMap(SectorDesc?[,] descMap, IList<Sector> sectors)
        {
            var map = new Sector[descMap.GetLength(0), descMap.GetLength(1)];
            var i = 0;
            descMap.IterateOver((x, y, desc) =>
            {
                if (!desc.HasValue)
                {
                    map[x, y] = null;
                    return;
                }

                var sector = CreateSector(desc.Value);
                map[x, y] = sector;
                sectors[i] = sector;
                i++;
            });
            return map;
        }

        private static Sector CreateSector(SectorDesc desc)
        {
            var sector = new Sector(desc.Seed, desc.Coords, desc.Size, desc.Name, desc.Faction,
                desc.Biome, desc.Music, desc.Skybox, desc.ProceduralLayers, MapPaths(desc.Paths), desc.Lights);

            desc.ObjectMap.IterateOver((x, y, o) =>
            {
                if (o == null) return;
                var pos = new Vector3(o.Position.x * 100 + 50, 0f, o.Position.y * 100 + 50);
                var rot = Quaternion.Euler(0f, o.Rotation, 0f);
                if (o is SectorDesc.Gate gate)
                {
                    sector.Gates.Add(new Gate("Gate", pos, rot, sector, gate.Destination.Coords));
                } else if (o is SectorDesc.Station station)
                {
                    sector.Stations.Add(new Station("Station", pos, rot, desc.Faction));
                }
                else
                {
                    Debug.LogError("Unknown sector object type.");
                }
            });

            return sector;
        }

        private static SectorPath[] MapPaths(List<SectorDesc.Path> pathDescs)
        {
            var paths = new SectorPath[pathDescs.Count];
            for (var i = 0; i < pathDescs.Count; i++)
            {
                paths[i] = new SectorPath(new List<int2>(pathDescs[i].Waypoints));
            }

            return paths;
        }
        
        private static void LinkSectors(SectorDesc?[,] descMap, Sector[,] map)
        {
            var list = new List<Sector>(4);
            descMap.IterateOver((x, y, desc) =>
            {
                if (!desc.HasValue) return;
                list.Clear();

                var sector = map[desc.Value.Coords.x, desc.Value.Coords.y];

                foreach (var link in desc.Value.Links)
                {
                    list.Add(map[link.Coords.x, link.Coords.y]);
                }

                foreach (var gate in sector.Gates)
                {
                    gate.SetTarget(map[gate.TargetCoords.x, gate.TargetCoords.y].FindGateBySector(sector));
                }

                sector.SetLinks(list.ToArray());
            });
        }
    }
}