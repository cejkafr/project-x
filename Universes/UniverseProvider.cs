﻿using Commons.DataObjects;
using Commons.Factions;
using Commons.Procedural.Lights;
using Commons.Procedural.NoiseMaps;
using Commons.Procedural.TileMaps;
using Unity.Mathematics;
using UnityEngine;
using Universes.Generators;

namespace Universes
{
    public class UniverseProvider : MonoBehaviour
    {
        private GameLibraryObject library;
        private UniverseGenerator universeGenerator;

        public void InitWithLibrary(GameLibraryObject iLibrary)
        {
            library = iLibrary;
            
            var noiseMapGenerator = new NoiseMapGenerator();
            var lightSourceGenerator = new LightSourceGenerator();
            var gateGenerator = new SectorGateGenerator();
            var stationGenerator = new SectorStationGenerator();
            var pathGenerator = new SectorPathGenerator();
            var sectorGenerator = new SectorGenerator(library, noiseMapGenerator, 
                lightSourceGenerator, gateGenerator, stationGenerator, pathGenerator);
            
            var mapLinkGenerator = new DumbMapTileLinkGenerator(false);
            var mapGenerator = new DumbTileMapGenerator(false);
            universeGenerator = new UniverseGenerator(mapGenerator, mapLinkGenerator, sectorGenerator);
        }

        public UniverseDesc Generate(string seed, int2 sectorCount)
        {
            return universeGenerator.Generate(seed, sectorCount);
        }

        public Faction[] CreateFactions()
        {
            var factions = new Faction[library.factions.Length];
            for (var i = 0; i < library.factions.Length; i++)
            {
                var factionObject = library.factions[i];
                factions[i] = new Faction(factionObject, library.factionAttitudeTranslator);
            }

            return factions;
        }
    }
}