﻿using UnityEditor;
using UnityEngine;

namespace Universes
{
    [CustomEditor(typeof(UniverseProviderTester))]
    public class GalaxyProviderTesterEditor : Editor
    {
        private UniverseProviderTester tester;
        
        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                base.OnInspectorGUI();

                if (check.changed)
                {
                    tester.Regenerate();
                }
            }

            if (GUILayout.Button("Generate"))
            {
                tester.Regenerate();
            }

            // DrawSettingsEditor(_planet.shapeSettings, _planet.OnShapeSettingsUpdated, ref _planet.shapeSettingsFoldout, ref _shapeEditor);
            // DrawSettingsEditor(_planet.colorSettings, _planet.OnColorSettingsUpdated, ref _planet.colorSettingsFoldout, ref _colorEditor);
        }

        // private void DrawSettingsEditor(Object settings, Action onSettingsUpdated, ref bool foldout, ref UnityEditor.Editor editor)
        // {
        //     if (settings == null)
        //     {
        //         return;
        //     }
        //
        //     foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);
        //
        //     using (var check = new EditorGUI.ChangeCheckScope())
        //     {
        //         if (!foldout)
        //         {
        //             return;
        //         } 
        //
        //         CreateCachedEditor(settings, null, ref editor);
        //         editor.OnInspectorGUI();
        //
        //         if (check.changed)
        //         {
        //             onSettingsUpdated?.Invoke();
        //         }
        //     }
        // }
    
        private void OnEnable()
        {
            tester = (UniverseProviderTester) target;
        }
    }
}

