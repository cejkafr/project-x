﻿using Commons.Factions;
using Commons.Sectors;
using Universes.SectorObjects;

namespace Universes
{
    public enum PlayerCombatStatus
    {
        Unresolved, Won, Lost, Evaded
    }
    
    public class PlayerCombat
    {
        public Fleet Fleet { get; }
        public PlayerCombatStatus Status { get; set; }

        public PlayerCombat(Fleet fleet)
        {
            Fleet = fleet;
            Status = PlayerCombatStatus.Unresolved;
        }
    }
    
    public class Player
    {
        public Faction Faction { get; }
        public Sector Sector { get; private set; }
        public Fleet Fleet { get; }
        public Gate LastUsedGate { get; private set; }
        
        public PlayerCombat Combat { get; private set; }

        public Player(Faction faction, Fleet fleet, Sector sector)
        {
            Faction = faction;
            Fleet = fleet;
            Sector = sector;
        }

        public void SetFromGate(Gate gate)
        {
            Sector.Fleets.Remove(Fleet);
            Sector = gate.Sector;
            Sector.Fleets.Add(Fleet);
            Fleet.Position = gate.Position;
            Fleet.Rotation = gate.Rotation;
            LastUsedGate = gate;
        }

        public void EngageInTacticalCombat(PlayerCombat combat)
        {
            Combat = combat;
        }
    }
}