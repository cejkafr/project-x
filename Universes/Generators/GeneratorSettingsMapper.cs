﻿using System.Collections.Generic;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;

namespace Universes.Generators
{
    public static class GeneratorSettingsMapper
    {
        public static NoiseMapSettings CreateMapSettings(int size, IList<ProceduralLayer> layers)
        {
            var layerSettings = new NoiseMapLayerSettings[layers.Count];
            for (var i = 0; i < layers.Count; i++)
            {
                var noiseFilter = layers[i].noiseFilter;
                layerSettings[i] = new NoiseMapLayerSettings(size, CreateMapSettingsNoiseLayers(noiseFilter));
            }

            return new NoiseMapSettings(size, layerSettings);
        }

        private static NoiseMapLayerSettings.NoiseLayer[] CreateMapSettingsNoiseLayers(NoiseFilter filter)
        {
            var noiseLayers = new NoiseMapLayerSettings.NoiseLayer[filter.noiseLayers.Length];
            for (var i = 0; i < filter.noiseLayers.Length; i++)
            {
                noiseLayers[i] = new NoiseMapLayerSettings.NoiseLayer(
                    filter.noiseLayers[i].scale, filter.noiseLayers[i].minValue);
            }

            return noiseLayers;
        }
    }
}