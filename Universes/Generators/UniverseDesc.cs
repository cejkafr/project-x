﻿namespace Universes.Generators
{
    public readonly struct UniverseDesc
    {
        public readonly string Seed;
        public readonly int SectorCount;
        public readonly SectorDesc?[,] Map;

        public UniverseDesc(string seed, int sectorCount, SectorDesc?[,] map)
        {
            Seed = seed;
            SectorCount = sectorCount;
            Map = map;
        }
    }
}