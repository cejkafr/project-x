﻿using Commons.Samplings;

namespace Universes.Generators
{
    public class SectorStationGenerator
    {
        public void Generate(SectorDesc sector)
        {
            var count = 3;
            var points = PoissonDiscSampling.GeneratePoints(20, sector.Size - 70);
            for (var i = 0; i < count; i++)
            {
                var pos = points[i] + 30;
                var rot = 0;
                sector.ObjectMap[pos.x, pos.y] = new SectorDesc.Station(pos, rot);
            }
        }
    }
}