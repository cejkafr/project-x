﻿using System.Collections.Generic;
using Commons;
using Commons.NavProviders;
using Game.Extensions;
using Unity.Mathematics;
using UnityEngine;
using Universes.SectorObjects;

namespace Universes.Generators
{
    public class SectorPathGenerator
    {
        private readonly struct PathInfo
        {
            public readonly SectorDesc.IObject sectorObject;
            public readonly Dictionary<SectorDesc.IObject, int2[]> paths;
            public readonly Dictionary<SectorDesc.IObject, int> pathLengths;

            public PathInfo(SectorDesc.IObject sectorObject) : this()
            {
                this.sectorObject = sectorObject;
                paths = new Dictionary<SectorDesc.IObject, int2[]>();
                pathLengths = new Dictionary<SectorDesc.IObject, int>();
            }
        }

        public void Generate(SectorDesc sector)
        {
            var grid = new SectorGrid(sector.Biome, sector.ProceduralLayers, 1);
            var navProvider = new AStarNavProvider(grid);

            var sectorObjects = new List<PathInfo>();
            sector.ObjectMap.IterateOver((x, y, o) =>
            {
                if (o == null) return;
                sectorObjects.Add(new PathInfo(o));
            });

            var graph = new int[sectorObjects.Count][];

            for (var i = 0; i < sectorObjects.Count; i++)
            {
                var from = sectorObjects[i];
                graph[i] = new int[graph.Length];

                for (var x = 0; x < sectorObjects.Count; x++)
                {
                    var to = sectorObjects[x];
                    if (from.sectorObject == to.sectorObject)
                    {
                        graph[i][x] = 0;
                        continue;
                    }

                    if (from.pathLengths.ContainsKey(to.sectorObject))
                    {
                        graph[i][x] = from.pathLengths[to.sectorObject];
                        continue;
                    }

                    var pathWaypoints = navProvider.FindPathWaypoints(
                        from.sectorObject.Position, to.sectorObject.Position, out var length);
                    graph[i][x] = length;

                    from.paths.Add(to.sectorObject, pathWaypoints);
                    from.pathLengths.Add(to.sectorObject, length);

                    to.paths.Add(from.sectorObject, pathWaypoints);
                    to.pathLengths.Add(from.sectorObject, length);
                }
            }

            var result = MinimumSpanningTree.Evaluate(graph);
            for (var i = 0; i < result.Length; i++)
            {
                if (result[i].Parent == -1) continue;
                sector.Paths.Add(new SectorDesc.Path(
                    sectorObjects[result[i].Parent].paths[sectorObjects[i].sectorObject]));
            }
            

            // SectorDesc.IObject prev = null;
            // sector.ObjectMap.IterateOver((x, y, o) =>
            // {
            //     if (o == null) return;
            //     if (prev != null)
            //     {
            //         int length;
            //         var pathWaypoints = navProvider.FindPathWaypoints(prev.Position, o.Position, out length);
            //         sector.Paths.Add(new SectorDesc.Path(pathWaypoints));
            //     }
            //
            //     prev = o;
            // });
        }
    }
}