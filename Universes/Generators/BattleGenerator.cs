﻿using Commons.Procedural.NoiseMaps;
using Unity.Mathematics;
using UnityEngine;

namespace Universes.Generators
{
    public class BattleGenerator
    {
        private readonly INoiseMapGenerator noiseMapGenerator;

        public BattleGenerator(INoiseMapGenerator noiseMapGenerator)
        {
            this.noiseMapGenerator = noiseMapGenerator;
        }

        public Battle Generate(Sector sector, int2 position, int2 size)
        {
            return new Battle(sector, size, "Battle_01", sector.Skybox, new NoiseMap(), sector.Lights);
        }
    }
}