﻿using System.Collections.Generic;
using Commons.Factions;
using Commons.Procedural.Lights;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;

namespace Universes.Generators
{
    public  readonly struct SectorDesc
    {
        public interface IObject
        {
            int2 Position { get; }
            int Rotation { get; }
        }
        
        public readonly struct Gate : IObject
        {
            public int2 Position { get; }
            public int Rotation { get; }
            
            public readonly SectorDesc Destination;

            public Gate(int2 position, int rotation, SectorDesc destination)
            {
                Position = position;
                Rotation = rotation;
                Destination = destination;
            }
        }
        
        public readonly struct Station : IObject
        {
            public int2 Position { get; }
            public int Rotation { get; }

            public Station(int2 position, int rotation)
            {
                Position = position;
                Rotation = rotation;
            }
        }
        
        public readonly struct Path
        {
            public int2[] Waypoints { get; }

            public Path(int2[] waypoints)
            {
                Waypoints = waypoints;
            }
        }

        public readonly int Seed;
        public readonly int2 Coords;
        public readonly int Size;
        public readonly string Name;
        public readonly Faction Faction;
        public readonly SectorBiomeObject Biome;
        public readonly List<SectorDesc> Links;
        public readonly string Music;
        public readonly Skybox Skybox;
        public readonly NoiseMap ProceduralLayers;
        public readonly LightSource[] Lights;
        public readonly IObject[,] ObjectMap;
        public readonly List<Path> Paths;

        public SectorDesc(int seed, int2 coords, int size, string name, Faction faction, SectorBiomeObject biome, 
            string music, Skybox skybox, NoiseMap proceduralLayers, LightSource[] lights, IObject[,] objectMap)
        {
            Seed = seed;
            Coords = coords;
            Size = size;
            Name = name;
            Faction = faction;
            Biome = biome;
            Links = new List<SectorDesc>(4);
            Music = music;
            Skybox = skybox;
            ProceduralLayers = proceduralLayers;
            Lights = lights;
            ObjectMap = objectMap;
            Paths = new List<Path>();
        }
    }
}