﻿using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Universes.Generators
{
    public class SectorGateGenerator
    {
        public void Generate(SectorDesc sector)
        {
            foreach (var targetSector in sector.Links)
            {
                PositionGate(sector, targetSector);
            }
        }

        public void PositionGate(SectorDesc sector, SectorDesc targetSector)
        {
            var size = sector.Size;
            var radius = new int2(85, 100);
            
            var xRange = int2.zero;
            var yRange = int2.zero;
            var yRot = 0;
         
            var dir = sector.Coords - targetSector.Coords;
            
            if (dir.y == 1)// TOP
            {
                xRange = new int2(size - radius.x, radius.x);
                yRange = new int2(radius.x, radius.y);
                yRot = 180;
            } else if (dir.x == -1)// RIGHT
            {
                xRange = new int2( radius.x, radius.y);
                yRange = new int2(size - radius.x, radius.x);
                yRot = 270;
            } else if (dir.y == -1)// BOTTOM
            {
                xRange = new int2(size - radius.x, radius.x);
                yRange = new int2(size - radius.x, size - radius.y);  
            } else if (dir.x == 1)// LEFT
            {
                xRange = new int2(size - radius.x, size - radius.y);  
                yRange = new int2(size - radius.x, radius.x);
                yRot = 90;
            }

            var pos = int2.zero;
            for (var i = 0; i < 30; i++)
            {
                pos = new int2(
                    Random.Range(xRange.x, xRange.y), 
                    Random.Range(yRange.x, yRange.y));
                if (sector.ProceduralLayers.Map[0, pos.x, pos.y] < 0.1f) break;
            }

            sector.ObjectMap[pos.x, pos.y] = new SectorDesc.Gate(pos, yRot, targetSector);
        }
    }
}