﻿using System.Collections.Generic;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;
using UnityEngine;

namespace Universes.Generators
{
    public readonly struct AnalyzedNoiseMap
    {
        public readonly struct Area
        {
            public readonly ProceduralLayerTag Tag;
            public readonly Rect Rect;

            public Area(ProceduralLayerTag tag, Rect rect)
            {
                Tag = tag;
                Rect = rect;
            }
        }
        
        public readonly int Size;
        public readonly Area[] Areas;

        public AnalyzedNoiseMap(int size, Area[] areas)
        {
            Size = size;
            Areas = areas;
        }
    }
    
    public class NoiseMapAnalyzer
    {
        private readonly List<HashSet<int2>> tempList 
            = new List<HashSet<int2>>();
        private int size;
        
        public AnalyzedNoiseMap Analyze(NoiseMap noiseMap)
        {
            size = noiseMap.Settings.Size;
            
            for (var layer = 0; layer < noiseMap.Map.GetLength(0); layer++)
            {
                tempList.Clear();
                
                for (var y = 0; y < noiseMap.Map.GetLength(2); y++)
                {
                    for (var x = 0; x < noiseMap.Map.GetLength(1); x++)
                    {
                        var value = noiseMap.Map[layer, x, y];
                        if (value < .2f) continue;
                        
                        
                    } 
                }          
            }
            
            return new AnalyzedNoiseMap(noiseMap.Settings.Size, new AnalyzedNoiseMap.Area[0]);
        }

        private void EvaluateCoord(int x, int y)
        {
            EvaluateCoordDir(x - 1, y);
            EvaluateCoordDir(x - 1, y - 1);
            EvaluateCoordDir(x, y - 1);
        }

        private void EvaluateCoordDir(int x, int y)
        {
            if (!IsValidCoord(x, y))
            {
                return;
            }

            AddCoordIfInList(x, y);
        }

        private bool IsValidCoord(int x, int y)
        {
            return x >= 0 && y >= 0 && x < size && y < size;
        }
        
        private void AddCoordIfInList(int x, int y)
        {
            foreach (var val in tempList)
            {
                if (!val.Contains(new int2(x, y)))
                {
                    continue;
                }
                
                Debug.Log("WAKA");
            }
        }
    }
}