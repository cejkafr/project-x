﻿using System.Collections.Generic;
using Commons.DataObjects;
using Commons.Factions;
using Commons.Procedural.Lights;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Universes.Generators
{
    public class SectorGenerator
    {
        private readonly GameLibraryObject library;
        private readonly INoiseMapGenerator noiseMapGenerator;
        private readonly LightSourceGenerator lightSourceGenerator;
        private readonly SectorGateGenerator gateGenerator;
        private readonly SectorStationGenerator stationGenerator;
        private readonly SectorPathGenerator pathGenerator;

        public SectorGenerator(GameLibraryObject library, 
            INoiseMapGenerator noiseMapGenerator, LightSourceGenerator lightSourceGenerator, 
            SectorGateGenerator gateGenerator, SectorStationGenerator stationGenerator, SectorPathGenerator pathGenerator)
        {
            this.library = library;
            this.noiseMapGenerator = noiseMapGenerator;
            this.lightSourceGenerator = lightSourceGenerator;
            this.gateGenerator = gateGenerator;
            this.stationGenerator = stationGenerator;
            this.pathGenerator = pathGenerator;
        }

        public SectorDesc Generate(int seed, int2 coords, Faction faction)
        {
            Random.InitState(seed);
            
            var biome = library.biomes[Random.Range(0, library.biomes.Length)];
            var name = $"Sector {seed}";

            var objectMap = new SectorDesc.IObject[biome.size, biome.size];

            var music = GenerateMusic();
            var skybox = GenerateSkybox();
            var noiseMap = GenerateMap(biome.size, biome.proceduralLayers);
            var lights = lightSourceGenerator.Generate(biome.light);

            return new SectorDesc(seed, coords, biome.size, name, 
                faction, biome, music, skybox, noiseMap, lights, objectMap);
        }

        private string GenerateMusic()
        {
            return $"{library.musicTrackPrefix}0{Random.Range(0, library.musicTrackCount)}";
        }

        private Skybox GenerateSkybox()
        {
            return new Skybox(
                library.skyboxes[Random.Range(0, library.skyboxes.Length)],
                0,
                Random.Range(0f, 360f));
        }
        
        private NoiseMap GenerateMap(int size, IList<ProceduralLayer> layers)
        {
            return noiseMapGenerator.Generate(GeneratorSettingsMapper.CreateMapSettings(size, layers));
        }

        public void GenerateObjects(SectorDesc sector)
        {
            stationGenerator.Generate(sector);
            gateGenerator.Generate(sector);
            pathGenerator.Generate(sector);
        }
    }
}