﻿using Commons.NavProviders;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;
using UnityEngine;

namespace Universes.Generators
{
    public class SectorGrid : ABaseGrid
    {
        public SectorGrid(SectorBiomeObject biome, NoiseMap noiseMap, int blurSize) : base(CreateMap(biome, noiseMap), blurSize)
        {
        }

        private static Node[,] CreateMap(SectorBiomeObject biome, NoiseMap noiseMap)
        {
            var map = new Node[noiseMap.Settings.Size, noiseMap.Settings.Size];
            for (var y = 0; y < noiseMap.Settings.Size; y++)
            {
                for (var x = 0; x < noiseMap.Settings.Size; x++)
                {
                    var value = noiseMap.Map[0, x, y];
                    var navCost = 0;
                    if (value > 0)
                    {
                        navCost = Mathf.RoundToInt(biome.proceduralLayers[0].navCostModifier * value);
                    }
                    map[x, y] = new Node(new int2(x, y), navCost);
                }
            }

            return map;
        }
    }
}