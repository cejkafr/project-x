﻿using System;
using System.Collections.Generic;
using Commons.Procedural.TileMaps;
using Game.Extensions;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Universes.Generators
{
    public class UniverseGenerator
    {
        private readonly ITileMapGenerator mapGenerator;
        private readonly IMapTileLinkGenerator linkGenerator;
        private readonly SectorGenerator sectorGenerator;

        public UniverseGenerator(ITileMapGenerator mapGenerator,
            IMapTileLinkGenerator linkGenerator, SectorGenerator sectorGenerator)
        {
            this.mapGenerator = mapGenerator;
            this.linkGenerator = linkGenerator;
            this.sectorGenerator = sectorGenerator;
        }

        public UniverseDesc Generate(string seed, int2 sectorCountRange)
        {
            Random.InitState(seed.GetHashCode());

            var sectorCount = Random.Range(sectorCountRange.x, sectorCountRange.y);
            var basicMap = mapGenerator.Generate(sectorCount, .4f);
            var linkedMap = linkGenerator.Generate(basicMap);

            var sectorSeeds = RandomSectorSeeds(sectorCount);

            var map = new SectorDesc?[linkedMap.GetLength(0), linkedMap.GetLength(1)];
            linkedMap.IterateOver((x, y, tile) =>
            {
                if (!tile.HasValue) return;
                map[x, y] = sectorGenerator.Generate(sectorSeeds.Dequeue(), tile.Value.Coords, null);
            });

            // 5, generate POIs
            
            LinkSectors(map, linkedMap);

            Random.InitState(Environment.TickCount);

            return new UniverseDesc(seed, sectorCount, map);
        }

        private Queue<int> RandomSectorSeeds(int count)
        {
            var result = new Queue<int>(count);
            for (var i = 0; i < count; i++)
            {
                result.Enqueue(Random.Range(int.MinValue, int.MaxValue));
            }

            return result;
        }

        private void LinkSectors(SectorDesc?[,] map, LinkedMapTile?[,] linkedMap)
        {
            map.IterateOver((x, y, sector) =>
            {
                if (!sector.HasValue) return;

                foreach (var link in linkedMap[x, y].Value.Links)
                {
                    sector.Value.Links.Add(map[link.Coords.x, link.Coords.y].Value);
                }
                
                sectorGenerator.GenerateObjects(sector.Value);
            });
        }
    }
}