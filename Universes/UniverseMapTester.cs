﻿using UnityEditor;
using UnityEngine;

namespace Universes
{
    public class UniverseMapTester : MonoBehaviour
    {
        [SerializeField] private GameObject sectorMapPrefab;
        [SerializeField] private Vector2 spacing;

        public Vector2 Spacing => spacing;

        public void Clear()
        {
            foreach (Transform t in transform)
            {
#if UNITY_EDITOR
                if (EditorApplication.isPlaying)
                    Destroy(t.gameObject);
                else
                    DestroyImmediate(t.gameObject);
#else
                Destroy(t.gameObject);
#endif
            }
        }

        public SectorMapTester CreateSectorMap(Vector2 position, Vector2 size)
        {
            var inst = Instantiate(sectorMapPrefab, transform);
            inst.GetComponent<RectTransform>().localPosition = position;
            inst.GetComponent<RectTransform>().sizeDelta = size;
            return inst.GetComponent<SectorMapTester>();
        }
    }
}