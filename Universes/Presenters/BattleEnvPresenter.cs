﻿using Commons.Presenters;
using Commons.Procedural.Lights;
using Commons.Sectors;
using Game;
using Games;
using UnityEngine;
using Skybox = Commons.Sectors.Skybox;

namespace Universes.Presenters
{
    public class BattleEnvPresenter : IBattleEnvPresenter
    {
        private readonly IProceduralPresenter proceduralPresenter;
        private readonly ILightSourcePresenter lightSourcePresenter;

        public BattleEnvPresenter(IProceduralPresenter proceduralPresenter, ILightSourcePresenter lightSourcePresenter)
        {
            this.proceduralPresenter = proceduralPresenter;
            this.lightSourcePresenter = lightSourcePresenter;
        }

        public void Present(Battle battle)
        {
            SpawnLayers(battle);
            SpawnLights(battle.Lights);
            SetSkybox(battle.Skybox);
            SetMusic(battle.Music);
        }
        
        private void SpawnLayers(Battle battle)
        {
            var presenterSeed = Random.Range(int.MinValue, int.MaxValue);

            // var settings = sector.Biome.proceduralLayers;
            // proceduralPresenter.Present(presenterSeed, sector.ProceduralLayers, settings);
        }

        private void SpawnLights(LightSource[] lightSources)
        {
            lightSourcePresenter.Spawn(lightSources);
        }

        private void SetSkybox(Skybox skyboxStruct)
        {
            // GameManager.RenderSettings.SetSkybox(skyboxStruct.Texture, skyboxStruct.Exposure, skyboxStruct.Rotation);
        }

        private void SetMusic(string music)
        {
            GameManager.PlayMusic(music, 0f, 1f);
        }
    }
}