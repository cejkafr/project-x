﻿using Commons.Presenters;
using Commons.Procedural.Lights;
using Commons.Sectors;
using Game;
using Games;
using UnityEngine;
using Skybox = Commons.Sectors.Skybox;

namespace Universes.Presenters
{
    public class SectorEnvPresenter : ISectorEnvPresenter
    {
        private readonly IProceduralPresenter proceduralPresenter;
        private readonly ILightSourcePresenter lightSourcePresenter;
        private readonly ISectorPathPresenter pathPresenter;

        public SectorEnvPresenter(IProceduralPresenter proceduralPresenter, 
            ILightSourcePresenter lightSourcePresenter, ISectorPathPresenter pathPresenter)
        {
            this.proceduralPresenter = proceduralPresenter;
            this.lightSourcePresenter = lightSourcePresenter;
            this.pathPresenter = pathPresenter;
        }

        public void Present(Sector sector)
        {
            Random.InitState(sector.Seed);

            SpawnLayers(sector);
            SpawnPaths(sector);
            SpawnLights(sector.Lights);
            SetSkybox(sector.Skybox);
            SetMusic(sector.Music);
        }

        private void SpawnLayers(Sector sector)
        {
            var presenterSeed = Random.Range(int.MinValue, int.MaxValue);

            var settings = sector.Biome.proceduralLayers;
            proceduralPresenter.Present(presenterSeed, sector.ProceduralLayers, settings);
        }

        private void SpawnPaths(Sector sector)
        {
            foreach (var path in sector.Paths)
            {
                pathPresenter.Present(path);                
            }
        }

        private void SpawnLights(LightSource[] lightSources)
        {
            lightSourcePresenter.Spawn(lightSources);
        }

        private void SetSkybox(Skybox skyboxStruct)
        {
            GameManager.RenderSettings.SetSkybox(skyboxStruct.Texture, skyboxStruct.Exposure, skyboxStruct.Rotation);
        }

        private void SetMusic(string music)
        {
            GameManager.PlayMusic(music, 0f, 1f);
        }

        public void Reset()
        {

        }
    }
}