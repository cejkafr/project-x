﻿using Commons.Sectors;

namespace Universes.Presenters
{
    public interface ISectorEnvPresenter
    {
        void Present(Sector sector);
    }
}