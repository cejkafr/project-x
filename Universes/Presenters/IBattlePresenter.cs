﻿using Commons.Sectors;

namespace Universes.Presenters
{
    public interface IBattlePresenter
    {
        void Present(Battle battle);
        void Destroy();
    }
}