﻿using Commons.Sectors;

namespace Universes.Presenters
{
    public interface ISectorPathPresenter
    {
        void Present(SectorPath path);
    }
}