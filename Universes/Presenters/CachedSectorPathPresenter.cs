﻿using Commons.Presenters;
using Commons.Sectors;
using UnityEngine;

namespace Universes.Presenters
{
    public class CachedSectorPathPresenter : MonoBehaviour, ISectorPathPresenter
    {
        [SerializeField] private GameObject pathPrefab;

        private IBulkDisableObjectCache cache;

        public void Init(IBulkDisableObjectCache cache)
        {
            this.cache = cache;
        }
        
        public void Present(SectorPath path)
        {
            var inst = cache.FetchOrInstantiate(pathPrefab, Vector3.zero, Quaternion.identity);
            var line = inst.GetComponent<LineRenderer>();
            
            var positions = new Vector3[path.Waypoints.Count];
            for (var i = 0; i < path.Waypoints.Count; i++)
            {
                positions[i] = new Vector3(path.Waypoints[i].x * 100 + 50, 0f, path.Waypoints[i].y * 100 + 50);
            }

            line.positionCount = positions.Length;
            line.SetPositions(positions);
        }
    }
}