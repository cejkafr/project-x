﻿using Commons.Sectors;

namespace Universes.Presenters
{
    public interface ISectorPresenter
    {
        void Present(Sector sector);
        void Destroy();
    }
}