﻿using Commons.Sectors;

namespace Universes.Presenters
{
    public interface IBattleEnvPresenter
    {
        void Present(Battle battle);
    }
}