﻿using Commons.Procedural.Lights;
using Commons.Procedural.NoiseMaps;
using Commons.Sectors;
using Unity.Mathematics;

namespace Universes
{
    public readonly struct Battle
    {
        public readonly Sector Sector;
        public readonly int2 Size;
        public readonly string Music;
        public readonly Skybox Skybox;
        public readonly NoiseMap ProceduralLayers;
        public readonly LightSource[] Lights;

        public Battle(Sector sector, int2 size, string music, Skybox skybox, NoiseMap proceduralLayers, LightSource[] lights)
        {
            Sector = sector;
            Size = size;
            Music = music;
            Skybox = skybox;
            ProceduralLayers = proceduralLayers;
            Lights = lights;
        }
    }
}