using System;
using Random = UnityEngine.Random;

namespace Game.Types
{
    [Serializable]
    public struct RangeFloat
    {
        public float min;
        public float max;

        public RangeFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
        
        public float RandomInBetween()
        {
            return Random.Range(min, max);
        }
    }
}