﻿namespace Games.Types
{
    public delegate void MaxValueChange(MaxValue maxValue);

    public class MaxValue
    {
        private readonly bool keepInRange;

        private float max;
        private float value;

        public event MaxValueChange ChangeEvent;
        public event MaxValueChange DepletedEvent;
        public event MaxValueChange OverflowEvent;
        
        public float Min { get; set; }

        public float Max
        {
            get => max;
            set
            {
                if (max == value) return;
                max = value;

                ChangeEvent?.Invoke(this);
            }
        }

        public float Value
        {
            get => value;
            set
            {
                if (this.value == value) return;

                this.value = value;

                if (IsMin)
                {
                    if (keepInRange)
                        ToMin(false);
                    DepletedEvent?.Invoke(this);
                }

                else if (IsMax)
                {
                    if (keepInRange)
                        ToMax(false);
                    OverflowEvent?.Invoke(this);
                }

                ChangeEvent?.Invoke(this);
            }
        }

        public bool IsMin => value <= Min;
        public bool IsMax => value >= Max;

        public MaxValue(float max, bool keepInRange = true, bool startMax = true)
        {
            this.keepInRange = keepInRange;
            this.max = max;

            if (startMax)
            {
                value = this.max;
            }
        }

        public MaxValue(MaxValue other)
        {
            keepInRange = other.keepInRange;
            Min = other.Min;
            max = other.max;
            value = other.value;
        }
        
        public void SetWithoutNotify(float value)
        {
            if (this.value == value) return;

            this.value = value;

            if (!keepInRange) return;

            if (IsMin)
                ToMin(false);
            else if (IsMax)
                ToMax(false);
        }

        public void ToMin(bool notify = true)
        {
            value = Min;

            if (!notify) return;

            DepletedEvent?.Invoke(this);
            ChangeEvent?.Invoke(this);
        }
        
        public void ToMax(bool notify = true)
        {
            value = Max;
            
            if (!notify) return;

            OverflowEvent?.Invoke(this);
            ChangeEvent?.Invoke(this);
        }
    }
}