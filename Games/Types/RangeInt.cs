using System;
using Random = UnityEngine.Random;

namespace Game.Types
{
    [Serializable]
    public struct RangeInt
    {
        public int min;
        public int max;

        public RangeInt(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public int RandomInBetween()
        {
            return Random.Range(min, max);
        }
    }
}