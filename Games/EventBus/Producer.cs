﻿using System;

namespace Game.EventBus
{
    [AttributeUsage(AttributeTargets.Event)]
    public class Producer : Attribute
    {
        public Producer()
        {
        }
    }
}