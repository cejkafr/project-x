﻿using System;

namespace Game.EventBus
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Consumer : Attribute
    {
        public Type EventType { get; }

        public Consumer(Type eventType)
        {
            EventType = eventType;
        }
    }
}