﻿using System;

namespace GalaxyMap.EventBus
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Bus : Attribute
    {
        public Bus()
        {
        }
    }
}