﻿using System;
using System.Collections.Generic;
using System.Reflection;
using GalaxyMap.EventBus;
using Game.DI;
using UnityEngine;

namespace Game.EventBus
{
    public class BusDriver : ManagedMonoBehaviour
    {
        private readonly struct ProducerInfo
        {
            public readonly object Target;
            public readonly EventInfo EventInfo;

            public ProducerInfo(object target, EventInfo eventInfo)
            {
                Target = target;
                EventInfo = eventInfo;
            }
        }
        
        [SerializeField] private DefaultDJInjectorContainer container;

        [PostBinding]
        private void Init()
        {
            var channelProducerDictionary = new Dictionary<Type, List<ProducerInfo>>();
            foreach (var managedObject in container.ManagedObjects.Values)
            {
                foreach (var eventInfo in managedObject.GetType().GetEvents())
                {
                    var attr = eventInfo.GetCustomAttribute<Producer>();
                    if (attr == null)
                    {
                        continue;
                    }

                    if (!channelProducerDictionary.ContainsKey(eventInfo.EventHandlerType))
                    {
                        channelProducerDictionary.Add(eventInfo.EventHandlerType, new List<ProducerInfo>(1));
                    }
                    
                    channelProducerDictionary[eventInfo.EventHandlerType]
                        .Add(new ProducerInfo(managedObject, eventInfo));
                }
            }

            foreach (var managedObject in container.ManagedObjects.Values)
            {
                foreach (var method in managedObject.GetType().GetMethods())
                {
                    var attr = method.GetCustomAttribute<Consumer>();
                    if (attr == null)
                    {
                        continue;
                    }

                    if (!channelProducerDictionary.ContainsKey(attr.EventType))
                    {
                        Debug.LogError($"Unable to find producer channel {attr.EventType}.");
                        continue;
                    }

                    var producers = channelProducerDictionary[attr.EventType];
                    foreach (var producer in producers)
                    {
                        var handler = Delegate.CreateDelegate(producer.EventInfo.EventHandlerType, managedObject, method);
                        producer.EventInfo.AddEventHandler(producer.Target, handler);   
                    }
                }
            }
        }
    }
}