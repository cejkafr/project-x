﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace Game
{
    public class RenderSettings
    {
        private readonly Volume renderSettings;
        private readonly Volume postProcessing;

        public RenderSettings(Volume renderSettings, Volume postProcessing)
        {
            this.renderSettings = renderSettings;
            this.postProcessing = postProcessing;
        }

        public void SetSkybox(Cubemap skybox, float exposure = 0, float rotation = 0)
        {
            renderSettings.profile.TryGet(typeof(HDRISky), out HDRISky sky);
            sky.hdriSky.value = skybox;
            sky.exposure.value = exposure;
            sky.rotation.value = rotation;
        }
        
        public void SetSkybox(string name, float exposure = 0, float rotation = 0)
        {
            var skybox = Resources.Load<Cubemap>($"Resources/Skyboxes/{name}");
            renderSettings.profile.TryGet(typeof(HDRISky), out HDRISky sky);
            sky.hdriSky.value = skybox;
            sky.exposure.value = exposure;
            sky.rotation.value = rotation;
        }
    }
}