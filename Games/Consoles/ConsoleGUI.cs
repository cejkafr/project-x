using System;
using System.Collections.Generic;
using Game;
using Game.Commons;
using Game.Console;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Games.Consoles
{
    // ReSharper disable once InconsistentNaming
    public class ConsoleGUI : MonoBehaviour, IConsoleUI
    {
        [Serializable]
        public class ConsoleEvent : UnityEvent<bool>
        {
        }
        
        [ConfigVar(Name = "console.alpha", DefaultValue = "0.9", Description = "Alpha of console")]
        public static ConfigVar consoleAlpha;

        private readonly List<string> lines = new List<string>();
        private int wantedCaretPosition = -1;

        public Transform panel;
        public InputField input_field;
        public Text text_area;
        public Image text_area_background;
        public KeyCode toggle_console_key;
        public Text buildIdText;



        public GameObject fader;

        [Space]
        public ConsoleEvent onConsoleOpenStateChanged = new ConsoleEvent();

        private void Awake()
        {
            input_field.onEndEdit.AddListener(OnSubmit);
        }

        public void Init()
        {
            buildIdText.text = GameManager.BuildId + " (" + Application.unityVersion + ")";
        }

        public void Shutdown()
        {
        }

        public void OutputString(string s)
        {
            lines.Add(s);
            var count = Mathf.Min(100, lines.Count);
            var start = lines.Count - count;
            text_area.text = string.Join("\n", lines.GetRange(start, count).ToArray());
        }

        public bool IsOpen()
        {
            return panel.gameObject.activeSelf;
        }

        public void SetOpen(bool open)
        {
            panel.gameObject.SetActive(open);
            if (open)
            {
                input_field.ActivateInputField();
            }
            
            onConsoleOpenStateChanged.Invoke(open);
        }

        public void ConsoleUpdate()
        {
            if (Input.GetKeyDown(toggle_console_key) || Input.GetKeyDown(KeyCode.Backslash))
                SetOpen(!IsOpen());

            if (!IsOpen())
                return;

            var c = text_area_background.color;
            c.a = Mathf.Clamp01(consoleAlpha.FloatValue);
            text_area_background.color = c;

            // This is to prevent clicks outside input field from removing focus
            input_field.ActivateInputField();

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (input_field.caretPosition != input_field.text.Length
                    || input_field.text.Length <= 0) return;

                var res = ConsoleManager.TabComplete(input_field.text);
                input_field.text = res;
                input_field.caretPosition = res.Length;
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                input_field.text = ConsoleManager.HistoryUp(input_field.text);
                wantedCaretPosition = input_field.text.Length;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                input_field.text = ConsoleManager.HistoryDown();
                input_field.caretPosition = input_field.text.Length;
            }
        }

        public void ConsoleLateUpdate()
        {
            // This has to happen here because keys like KeyUp will navigate the caret
            // int the UI event handling which runs between Update and LateUpdate
            if (wantedCaretPosition > -1)
            {
                input_field.caretPosition = wantedCaretPosition;
                wantedCaretPosition = -1;
            }
        }

        public void OnSubmit(string value)
        {
            // Only react to this if enter was actually pressed. Submit can also happen by mouse clicks.
            if (!Input.GetKey(KeyCode.Return) && !Input.GetKey(KeyCode.KeypadEnter))
                return;

            input_field.text = "";
            input_field.ActivateInputField();

            ConsoleManager.EnqueueCommand(value);
        }

        public void SetPrompt(string prompt)
        {
        }
    }
}