﻿using System;
using System.Collections;
using System.IO;
using Game;
using Game.Commons;
using Game.Commons.Audio;
using Game.Commons.UI;
using Games.Audio;
using Games.Consoles;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using RenderSettings = Game.RenderSettings;

namespace Games
{
    [DefaultExecutionOrder(-1000)]
    public class GameManager : MonoBehaviour
    {
        public delegate void GameManagerEvent(); 
        
        public static event GameManagerEvent LevelLoaded;

        private const string UserConfigFilename = "user.cfg";
        private const string BootConfigFilename = "boot.cfg";
        private const string LogFBasename = "game";
        
        public static double FrameTime => Time.unscaledTime;
        public static ISoundSystem SoundSystem { get; private set; }
        public static RenderSettings RenderSettings { get; private set; }
        public static LevelManager LevelManager { get; private set; }
        public static EventSystem EventSystem { get; private set; }
        public static string BuildId { get; private set; } = "NoBuild";

        public static void PlayMusic([CanBeNull] string trackName, float fadeInTime = 0f, float fadeOutTime = 0f)
        {
            SoundSystem.Stop(musicHandle, fadeOutTime);

            if (trackName == null)
            {
                return;
            }

            var musicTrack = Resources.Load<SoundDef>("Music/" + trackName);
            if (musicTrack == null)
            {
                return;
            }

            musicHandle = SoundSystem.Play(musicTrack);
        }
        
        private static string FindNewFilename(string pattern)
        {
            for (var i = 0; i < 10000; i++)
            {
                var f = string.Format(pattern, i);
                if (File.Exists(string.Format(pattern, i)))
                    continue;
                return f;
            }

            return null;
        }

        private static string LogFilePath(string[] commandLineArgs)
        {
            // If -logfile was passed, we try to put our own logs next to the engine's logfile
            var engineLogFileLocation = ".";
            var logfileArgIdx = commandLineArgs.IndexOf("-logfile");
            if (logfileArgIdx >= 0 && commandLineArgs.Length >= logfileArgIdx)
            {
                engineLogFileLocation = Path.GetDirectoryName(commandLineArgs[logfileArgIdx + 1]);
            }

            return engineLogFileLocation;
        }

        private static SoundSystem.SoundHandle musicHandle;

        [SerializeField] private ConsoleGUI consoleGui;
        [SerializeField] private Volume renderSettings;
        [SerializeField] private Volume postProcessing;
        [SerializeField] private EventSystem eventSystem;
        [SerializeField] private AudioMixer audioMixer;
        [SerializeField] private SoundBank soundBank;

        private void Awake()
        {
            if (FindObjectsOfType<GameManager>().Length > 1)
            {
                Debug.LogWarning("GameManager already exists, destroying new instance.");
                Destroy(this);
                return;
            }
            
            DontDestroyOnLoad(this);

            var commandLineArgs = Environment.GetCommandLineArgs();

            InitGameDebug(commandLineArgs);
            InitConsole();
            ConfigVar.Init();
            InitSoundSystem();
            InitLevelManager();
            RenderSettings = new RenderSettings(renderSettings, postProcessing);

            ConsoleManager.EnqueueCommandNoHistory("exec -s " + UserConfigFilename);
            if (Array.IndexOf(commandLineArgs, "-noboot") != -1)
                ConsoleManager.EnqueueCommandNoHistory("exec -s " + BootConfigFilename);
            ConsoleManager.ProcessCommandLineArguments(commandLineArgs);
            ConsoleManager.SetOpen(false);

            Application.targetFrameRate = 60;

            EventSystem = eventSystem;
        }

        private void Update()
        {
            ConsoleManager.ConsoleUpdate();
            SoundSystem.Update();
        }

        private void LateUpdate()
        {
            ConsoleManager.ConsoleLateUpdate();
        }

        private void InitGameDebug(string[] commandLineArgs)
        {
            var engineLogFileLocation = LogFilePath(commandLineArgs);
            GameDebug.Init(engineLogFileLocation, LogFBasename);
        }

        private void InitConsole()
        {
            ConsoleManager.Init(consoleGui);
            ConsoleManager.AddCommand("music", CmdMusic, "Play music.");
            ConsoleManager.AddCommand("restart", CmdRestart, "Restart level.");
            ConsoleManager.AddCommand("load", CmdLoad, "Load level.");
            ConsoleManager.AddCommand("quit", CmdQuit, "Quits game.");
            ConsoleManager.AddCommand("screenshot", CmdScreenshot,
                "Capture screenshot. Optional argument is destination folder or filename.");
            ConsoleManager.AddCommand("saveconfig", CmdSaveConfig, "Save the user config variables.");
            ConsoleManager.AddCommand("loadconfig", CmdLoadConfig, "Load the user config variables.");
#if UNITY_STANDALONE_WIN
            ConsoleManager.AddCommand("windowpos", CmdWindowPosition, "Position of window. e.g. windowpos 100,100.");
#endif
        }

        private void InitSoundSystem()
        {
            SoundSystem = new SoundSystem();
            SoundSystem.Init(audioMixer);
            SoundSystem.MountBank(soundBank);
        }

        private void InitLevelManager()
        {
            LevelManager = new LevelManager();
        }
        
        private void CmdMusic(string[] args)
        {
            if (args.Length == 0)
            {
                ConsoleManager.OutputString("Usage: music <track name> <fade-out-time>");
                
                return;
            }

            if (musicHandle.IsValid())
            {
                ConsoleManager.OutputString("Stopping music track.");

                var fadeOutTime = args.Length > 1 ? float.Parse(args[1]) : 0f;
                SoundSystem.Stop(musicHandle, fadeOutTime);
            }

            if (args[0] == "stop")
            {
                return;
            }

            var musicTrack = Resources.Load<SoundDef>("Music/" + args[0]);
            if (musicTrack == null)
            {
                ConsoleManager.OutputString("Invalid music track " + args[0] + ".");
                return;
            }
            
            ConsoleManager.OutputString("Playing music track " + musicTrack + ".");

            musicHandle = SoundSystem.Play(musicTrack);
        }

        private void CmdQuit(string[] args)
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

        private void CmdScreenshot(string[] arguments)
        {
            string filename = null;
            var root = Path.GetFullPath(".");

            switch (arguments.Length)
            {
                case 0:
                    filename = FindNewFilename(root + "/screenshot{0}.png");
                    break;
                case 1:
                {
                    var a = arguments[0];
                    if (Directory.Exists(a))
                        filename = FindNewFilename(a + "/screenshot{0}.png");
                    else if (!File.Exists(a))
                        filename = a;
                    else
                    {
                        ConsoleManager.Write("File " + a + " already exists");
                        return;
                    }

                    break;
                }
            }

            if (filename == null) return;

            GameDebug.Log("Saving screenshot to " + filename);
            ConsoleManager.SetOpen(false);
            ScreenCapture.CaptureScreenshot(filename);
        }

        private void CmdSaveConfig(string[] arguments)
        {
            ConfigVar.Save(UserConfigFilename);
        }

        private void CmdLoadConfig(string[] arguments)
        {
            ConsoleManager.EnqueueCommandNoHistory("exec " + UserConfigFilename);
        }

        private void CmdRestart(string[] args)
        {
            LevelManager.Restart();
            LevelLoaded?.Invoke();
        }

        private void CmdLoad(string[] args)
        {
            if (args.Length == 0)
            {
                return;
            }
            
            try
            {
                var level = (Level) Enum.Parse(typeof(Level), args[0], true);
                ConsoleManager.SetOpen(false);
                StartCoroutine(LoadLevelCoroutine(level));

            }
            catch (Exception ex)
            {
                GameDebug.LogError("Level '" + args[0] + "' not found.");
            }
        }

        private IEnumerator LoadLevelCoroutine(Level level)
        {
            Loader.Instance.FadeOut(0.8f);

            yield return new WaitForSecondsRealtime(0.8f);

            LevelLoaded?.Invoke();
            LevelManager.Load(level);
        }

#if UNITY_STANDALONE_WIN
        private void CmdWindowPosition(string[] arguments)
        {
            if (arguments.Length == 1)
            {
                var cords = arguments[0].Split(',');
                if (cords.Length == 2)
                {
                    int x, y;
                    var xParsed = int.TryParse(cords[0], out x);
                    var yParsed = int.TryParse(cords[1], out y);
                    if (xParsed && yParsed)
                    {
                        WindowsUtil.SetWindowPosition(x, y);
                        return;
                    }
                }
            }

            ConsoleManager.Write("Usage: windowpos <x,y>");
        }
#endif
    }
}