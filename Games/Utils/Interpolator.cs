﻿using UnityEngine;

// Utility class for interpolating values. Usage
//
//    var ival = new Interpolator(0.0f, Interpolator.CurveType.Linear)
//    ival.MoveTo(10.0f, 0.5f); // interpolate from 0 to 10 in 0.5 secs
//
//    ival.GetValue();          // gets current value
//
//    ival.SetValue(12.0f);     // force value and stop animation
//
// Ideas for more curve types: http://sol.gfxile.net/interpolation/ 

namespace Game
{
    public class Interpolator
    {
        public enum CurveType
        {
            Linear,
            SmoothDeparture,
            SmoothArrival,
            SmoothStep
        }

        private readonly CurveType m_Type;
        private float m_StartTime;
        private float m_StartValue;
        private float m_TargetTime;
        
        public float TargetValue { get; private set; }

        public Interpolator(float startValue, CurveType type)
        {
            m_Type = type;
            SetValue(startValue);
        }

        public void SetValue(float value)
        {
            m_StartValue = value;
            TargetValue = value;
            m_StartTime = 0;
            m_TargetTime = 0;
        }

        public void MoveTo(float target, float time)
        {
            m_StartValue = GetValue();
            TargetValue = target;
            m_StartTime = Time.realtimeSinceStartup;
            m_TargetTime = m_StartTime + time;
        }

        public bool IsMoving()
        {
            return Time.realtimeSinceStartup < m_TargetTime;
        }

        public float Direction()
        {
            return Mathf.Sign(m_TargetTime - m_StartValue);
        }

        public void Stop()
        {
            m_StartValue = TargetValue = GetValue();
            m_TargetTime = 0;
            m_StartTime = 0;
        }

        public float GetValue()
        {
            var now = Time.realtimeSinceStartup;
            var timeToLive = m_TargetTime - now;
            if (timeToLive <= 0.0f)
                return TargetValue;

            var t = (now - m_StartTime) / (m_TargetTime - m_StartTime);
            switch (m_Type)
            {
                default:
                case CurveType.Linear:
                    return m_StartValue + (TargetValue - m_StartValue) * t;
                case CurveType.SmoothArrival:
                    var s = 1.0f - t;
                    return m_StartValue + (TargetValue - m_StartValue) * (1.0f - s * s * s * s);
                case CurveType.SmoothDeparture:
                    return m_StartValue + (TargetValue - m_StartValue) * t * t * t * t;
                case CurveType.SmoothStep:
                    return m_StartValue + (TargetValue - m_StartValue) * t * t * (3.0f - 2.0f * t);
            }
        }
    }
}
