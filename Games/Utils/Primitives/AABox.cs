﻿using System;
using Unity.Mathematics;

namespace Game.Primitives
{
	[Serializable]
	public struct AABox
	{
		public float3 center;
		public float3 size;
	}
}

