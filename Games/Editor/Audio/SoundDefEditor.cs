﻿using Game.Commons.Audio;
using UnityEditor;
using UnityEngine;

namespace Core.Editor
{
    [CustomEditor(typeof(SoundDef))]
    public class SoundDefEditor : UnityEditor.Editor
    {
        private static AudioSource TestSource;

        public override void OnInspectorGUI()
        {
            if (TestSource == null)
            {
                var go = new GameObject("testSource")
                {
                    hideFlags = HideFlags.HideAndDontSave
                };
                TestSource = go.AddComponent<AudioSource>();
            }

            var sd = (SoundDef) target;

            // Allow playing audio even when sounddef is readonly
            var oldEnabled = GUI.enabled;
            GUI.enabled = true;
            if (TestSource.isPlaying && GUILayout.Button("Stop []"))
            {
                TestSource.Stop();
            }
            else if (!TestSource.isPlaying && GUILayout.Button("Play >"))
            {
                SoundSystem.StartSource(TestSource, sd);
            }

            GUI.enabled = oldEnabled;

            DrawPropertiesExcluding(serializedObject, new string[] {"m_Script"});

            serializedObject.ApplyModifiedProperties();
        }
    }
}