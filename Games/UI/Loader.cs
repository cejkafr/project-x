﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Commons.UI
{
    public class Loader : Singleton<Loader>
    {
        [Serializable]
        public struct UiBindings
        {
            public CanvasGroup canvasGroup;
            public Image faderImage;
            public GameObject loadingScreen;
            public TextMeshProUGUI customText;
            public Slider progressBar;
        }
    
        private Coroutine faderCoroutine;
        private float faderStep;

        public UiBindings uiBindings;

        private void Awake()
        {
//        DontDestroyOnLoad(this);
        
            uiBindings.canvasGroup.alpha = 0f;
        }

        public void FadeOut(float tSec)
        {
            HideLoader();
            ShowFader();
            uiBindings.canvasGroup.alpha = 0f;

            faderStep = 1f / tSec;
            if (faderCoroutine != null) StopCoroutine(faderCoroutine);
            faderCoroutine = StartCoroutine(FadeCoroutine());
        }

        public void FadeIn(float tSec)
        {
            HideLoader();
            ShowFader();
            uiBindings.canvasGroup.alpha = 1f;

            faderStep = 1f / tSec * -1f;
            
            if (faderCoroutine != null) StopCoroutine(faderCoroutine);
            faderCoroutine = StartCoroutine(FadeCoroutine());
        }
    
        public void ShowLoader(int value, string text)
        {
            HideFader();

            uiBindings.canvasGroup.alpha = 1f;
            uiBindings.loadingScreen.SetActive(true);

            ShowProgressBar(value);
            ShowCustomText(text);
        }

        public void HideLoader()
        {
            uiBindings.canvasGroup.alpha = 0f;
            uiBindings.loadingScreen.SetActive(false);
        
            HideProgressBar();
            HideCustomText();
        }

        public void ShowCustomText(string value)
        {
            uiBindings.customText.gameObject.SetActive(true);
            uiBindings.customText.text = value;
        }

        public void HideCustomText()
        {
            uiBindings.customText.gameObject.SetActive(false);
        }

        public void ShowProgressBar(int value)
        {
            uiBindings.progressBar.gameObject.SetActive(true);
            uiBindings.progressBar.value = value;
        }

        public void HideProgressBar()
        {
            uiBindings.progressBar.gameObject.SetActive(false);
        }

        private void HideFader()
        {
            if (faderCoroutine != null)
            {
                StopCoroutine(faderCoroutine);
            }

            uiBindings.faderImage.gameObject.SetActive(false);
        }
    
        private void ShowFader()
        {
            uiBindings.faderImage.gameObject.SetActive(true);
        }

        private IEnumerator FadeCoroutine()
        {
            var wfeof = new WaitForEndOfFrame();

            while (true)
            {
                uiBindings.canvasGroup.alpha += faderStep * Time.unscaledDeltaTime;

                if (uiBindings.canvasGroup.alpha >= 1f 
                    || uiBindings.canvasGroup.alpha <= 0f)
                {
                    break;
                }

                yield return wfeof;
            }

            if (uiBindings.canvasGroup.alpha <= 0f)
            {
                HideFader();
            }
        }
    }
}
