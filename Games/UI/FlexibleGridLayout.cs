﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Commons.UI
{
    public class FlexibleGridLayout : LayoutGroup
    {
        public enum FitType
        {
            Uniform,
            Width,
            Height,
            FixedRows,
            FixedColumns,
        }
    
        [SerializeField] private FitType fitType;
        [SerializeField] private int rows;
        [SerializeField] private int columns;
        [SerializeField] private Vector2 cellSize;
        [SerializeField] private Vector2 spacing;
        [SerializeField] private bool fitX;
        [SerializeField] private bool fitY;

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();

            if (FitType.Width == fitType || FitType.Height == fitType || FitType.Uniform == fitType)
            {
                fitX = fitY = true;
            
                var sqrt = Mathf.Sqrt(transform.childCount);
                rows = Mathf.CeilToInt(sqrt);
                columns = Mathf.CeilToInt(sqrt);
            }

            switch (fitType)
            {
                case FitType.Width:
                case FitType.FixedColumns:
                    rows = Mathf.CeilToInt(transform.childCount / (float) columns);
                    break;
                case FitType.Height:
                case FitType.FixedRows:
                    columns = Mathf.CeilToInt(transform.childCount / (float) rows);
                    break;
            }

            var rect = rectTransform.rect;
            var parentWidth = rect.width;
            var parentHeight = rect.height;

            var paddingLoc = padding;
            var cellWidth = (parentWidth / (float) columns) - ((spacing.x / (float) columns) * (columns - 1)) -
                            (paddingLoc.left / (float) columns) - (paddingLoc.right / (float) columns);
            var cellHeight = (parentHeight / (float) rows) - ((spacing.y / (float) rows) * (rows - 1)) -
                             (paddingLoc.top / (float) rows) - (paddingLoc.bottom / (float) rows);

            cellSize.x = fitX ? cellWidth : cellSize.x;
            cellSize.y = fitY ? cellHeight : cellSize.y;

            for (var i = 0; i < rectChildren.Count; i++)
            {
                var rowCount = i / columns;
                var columnCount = i % columns;

                var item = rectChildren[i];

                var xPos = (cellSize.x * columnCount) + (spacing.x * columnCount) + paddingLoc.left;
                var yPos = (cellSize.y * rowCount) + (spacing.y * rowCount) + paddingLoc.top;

                SetChildAlongAxis(item, 0, xPos, cellSize.x);
                SetChildAlongAxis(item, 1, yPos, cellSize.y);
            }
        }

        public override void CalculateLayoutInputVertical()
        {
        }

        public override void SetLayoutHorizontal()
        {
        }

        public override void SetLayoutVertical()
        {
        }
    }
}