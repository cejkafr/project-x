﻿using System.Collections.Generic;
using System.Reflection;

namespace Game
{
    public class DefaultPostBindingExecutor : IPostBindingExecutor
    {
        private static readonly BindingFlags BindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;
        
        public void Execute(IEnumerable<object> managedObjects)
        {
            foreach (var value in managedObjects)
            {
                foreach (var method in value.GetType().GetMethods(BindingFlags))
                {
                    var attr = method.GetCustomAttribute<PostBinding>();
                    if (attr == null) continue;
                    method.Invoke(value, new object[] { });
                }
            }
        }   
    }
}