﻿using System.Reflection;
using UnityEngine;

namespace Game
{
    public class DefaultContainerResolver : IContainerResolver
    {
        public IDJInjectorContainer Resolve(object target)
        {
            var container = DJInjector.Default;

            var attr = target.GetType().GetCustomAttribute<ManagedObject>();
            if (attr?.Container == null)
            {
                return container;
            }

            container = DJInjector.FindContainerByName(attr.Container);
            if (container == null)
            {
                Debug.LogError($"Failed to find container by name {attr.Container}.");
            }

            return container;
        }
    }
}