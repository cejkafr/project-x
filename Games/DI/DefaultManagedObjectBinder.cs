﻿using System.Reflection;
using UnityEngine;

namespace Game
{
    public class DefaultManagedObjectBinder : IManagedObjectBinder
    {
        private static readonly BindingFlags BindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;

        private readonly IManagedObjectFieldBinder _fieldBinder;

        public DefaultManagedObjectBinder(IManagedObjectFieldBinder fieldBinder)
        {
            _fieldBinder = fieldBinder;
        }

        public void Bind(object target)
        {
            foreach (var field in target.GetType().GetFields(BindingFlags))
            {
                var attr = field.GetCustomAttribute<Inject>();
                if (attr == null)
                {
                    continue;
                }

                var result = _fieldBinder.TryBind(target, field);
                if (!result && !attr.Nullable)
                {
                    Debug.LogError(
                        $"DefaultManagedObjectBinder: Cannot inject '{target.GetType().FullName}' with unknown value '{field.Name} of type {field.FieldType.FullName}'.");
                }
            }
        }
    }
}