﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [DefaultExecutionOrder(-1900)]
    public class DefaultDJInjectorContainer : MonoBehaviour, IDJInjectorContainer
    {
        public Dictionary<string, object> ManagedObjects { get; } = new Dictionary<string, object>();

        [SerializeField] private string title;
        [SerializeField] private bool isDefault;
        [SerializeField] private bool debug;

        public string Name => title;
        public bool IsDefault => isDefault;

        private void Awake()
        {
            DJInjector.RegisterContainer(this);
        }

        private void OnDestroy()
        {
            DJInjector.UnregisterContainer(this);
        }

        private void Start()
        {
            foreach (var value in ManagedObjects.Values)
            {
                DJInjector.Inject(value);
            }

            DJInjector.PostBindingExecutor.Execute(ManagedObjects.Values);
        }

        public void Register(object target)
        {
            Register(target.GetType().FullName, target);
        }

        public void Register(string name, object target)
        {
            if (debug)
            {
                print($"DJInjectorContainer ({Name}): Registering {name}.");
            }

            if (ManagedObjects.ContainsKey(name))
            {
                throw new ArgumentException($"Managed object of name '{name}' already exists.");
            }

            ManagedObjects.Add(name, target);
        }

        public void Unregister(object o)
        {
            Unregister(o.GetType().FullName);
        }

        public void Unregister(string name)
        {
            if (debug)
            {
                print($"DJInjectorContainer ({Name}): Unregistering {name}.");
            }

            ManagedObjects.Remove(name);
        }
    }
}