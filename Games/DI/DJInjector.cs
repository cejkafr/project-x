﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    [DefaultExecutionOrder(-2000)]
    public class DJInjector : MonoBehaviour
    {
        public static readonly List<IDJInjectorContainer> Containers = new List<IDJInjectorContainer>();
        private static readonly IManagedObjectBinder ObjectBinder 
            = new DefaultManagedObjectBinder(new DefaultManagedObjectFieldBinder());

        public static IDJInjectorContainer Default { get; private set; }
        public static IContainerResolver ContainerResolver { get; } = new DefaultContainerResolver();
        public static IPostBindingExecutor PostBindingExecutor { get; } = new DefaultPostBindingExecutor();
        
        private static DJInjector _instance;
        private static bool _debug;

        [SerializeField] private bool debug;

        private void Awake()
        {
            if (_instance != null)
            {
                Debug.LogError("DInjectorContainerManager already exists in the scene, destroying new one.");
                Destroy(gameObject);
                return;
            }
            
            DontDestroyOnLoad(this);
            _instance = this;
            _debug = debug;
        }

        public static IDJInjectorContainer FindContainerByName(string name)
        {
            return Containers.FirstOrDefault(container => container.Name == name);
        }
        
        public static void RegisterContainer(IDJInjectorContainer container)
        {
            if (_debug)
            {
                print($"DJInjector: Registering container {container.Name}.");
            }

            Containers.Add(container);
            if (container.IsDefault)
            {
                if (Default != null)
                {
                    Debug.LogError("Overwriting existing default container.");
                }

                Default = container;
            }
        }
        
        public static void UnregisterContainer(IDJInjectorContainer container)
        {
            if (_debug)
            {
                print($"DJInjector: Unregistering container {container.Name}.");
            }

            Containers.Remove(container);

            if (container.IsDefault)
            {
                Default = default;
            }
        }

        public static void Inject(object target)
        {
            ObjectBinder.Bind(target);
        }

        // TODO refactor, doesn't belong here
        public static T Create<T>(Type type)
        {
            if (type.IsAssignableFrom(typeof(MonoBehaviour)))
            {
                Debug.LogError("Cannot create MonoBehaviour objects.");
                return default;
            }
            
            var constructors = type.GetConstructors();
            if (constructors.Length != 1)
            {
                Debug.LogError("Created object must have exactly one constructor.");
                return default;
            }

            var c = constructors[0];
            var arr = new object[c.GetParameters().Length];
            for (var i = 0; i < c.GetParameters().Length; i++)
            {
                var p = c.GetParameters()[i];
                var o = FindRegisteredOfType(p.ParameterType);
                if (o == null)
                {
                    Debug.LogError($"{p.Name}, Unable to find registered object of type {p.ParameterType}.");
                    return default;
                }

                arr[i] = o;
            }

            return (T) c.Invoke(arr);
        }
        
        // TODO refactor, doesn't belong here
        public static object FindRegisteredOfType(Type type)
        {
            foreach (var container in Containers)
            {
                foreach (var ob in container.ManagedObjects.Values)
                {
                    if (!type.IsInstanceOfType(ob))
                    {
                        continue;
                    }

                    return ob;
                }   
            }

            return default;
        }
    }
    
    // var assembly = Assembly.GetExecutingAssembly();
    // foreach (var type in assembly.GetTypes())
    // {
    //     var attribute = type.GetCustomAttribute<CoolThing>();
    //     var name = attribute.Name ?? type.FullName;
    //     managed.Add(name, type.);
    //     
    //     if (attribute != null)
    //     {
    //         GameDebug.Log(type.FullName);
    //     }
    // }
}