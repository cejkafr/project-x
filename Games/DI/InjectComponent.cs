﻿using System;

namespace Game
{
    [AttributeUsage(AttributeTargets.Field)]
    public class InjectComponent : Attribute
    {
        public bool Nullable { get; }
        
        public InjectComponent(bool nullable = false)
        {
            Nullable = nullable;
        }
    }
}