﻿using System;
using System.Reflection;
using UnityEngine;

namespace Game.DI
{
    public abstract class ManagedMonoBehaviour : MonoBehaviour
    {
        private IDJInjectorContainer container;

        protected virtual void Awake()
        {
            container = DJInjector.ContainerResolver.Resolve(this);
            container.Register(this);

            InjectComponents();
        }

        protected virtual void OnDestroy()
        {
            container?.Unregister(this);
        }

        private void InjectComponents()
        {
            foreach (var field in GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                var attr = field.GetCustomAttribute<InjectComponent>();
                if (attr == null) continue;
                var tOb = FindRegisteredOfType(field.FieldType);
                if (tOb == null)
                {
                    Debug.LogError($"Cannot inject unknown value.");
                    continue;
                }

                field.SetValue(this, tOb);
            }
        }

        private object FindRegisteredOfType(Type type)
        {
            foreach (var ob in GetComponents(typeof(MonoBehaviour)))
            {
                if (!type.IsInstanceOfType(ob)) continue;
                return ob;
            }

            return default;
        }
    }
}