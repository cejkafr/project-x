﻿namespace Game
{
    public interface IManagedObjectBinder
    {
        void Bind(object target);
    }
}