﻿using System;

namespace Game
{
    [AttributeUsage(AttributeTargets.Field)]
    public class Inject : Attribute
    {
        public string Name { get; }
        public bool Nullable { get; }

        public Inject(string name = null, bool nullable = false)

        {
            Name = name;
            Nullable = nullable;
        }
    }
}