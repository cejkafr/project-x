﻿namespace Game
{
    public interface IContainerResolver
    {
        IDJInjectorContainer Resolve(object target);
    }
}