﻿using System;

namespace Game
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ManagedObject : Attribute
    {
        public string Name { get; }
        public string Container { get; }

        public ManagedObject(string name = null, string container = null)

        {
            Name = name;
            Container = container;
        }
    }
}