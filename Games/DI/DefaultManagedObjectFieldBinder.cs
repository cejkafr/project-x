﻿using System;
using System.Reflection;

namespace Game
{
    public class DefaultManagedObjectFieldBinder : IManagedObjectFieldBinder
    {
        public bool TryBind(object target, FieldInfo field)
        {
            var tOb = DJInjector.FindRegisteredOfType(field.FieldType);
            if (tOb == null)
            {
                return false;
            }

            field.SetValue(target, tOb);
            return true;
        }
    }
}