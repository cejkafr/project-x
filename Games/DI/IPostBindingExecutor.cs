﻿using System.Collections.Generic;

namespace Game
{
    public interface IPostBindingExecutor
    {
        void Execute(IEnumerable<object> managedObjects);
    }
}