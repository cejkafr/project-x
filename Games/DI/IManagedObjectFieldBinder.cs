﻿using System.Reflection;

namespace Game
{
    public interface IManagedObjectFieldBinder
    {
        bool TryBind(object target, FieldInfo field);
    }
}