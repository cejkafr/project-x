﻿using System.Collections.Generic;

namespace Game
{
    public interface IDJInjectorContainer
    {
        Dictionary<string, object> ManagedObjects { get; }
        
        string Name { get; }
        bool IsDefault { get; }

        void Register(object target);
        void Register(string name, object target);
        void Unregister(object o);
        void Unregister(string name);
    }
}