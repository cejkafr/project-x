﻿using System;

namespace Game
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PostBinding : Attribute
    {
        public PostBinding()
        {
        }
    }
}