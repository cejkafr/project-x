﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public enum Level
    {
        None = -1,
        Bootstrap = 0,
        MainMenu = 1,
        GalaxyMap = 2,
        TacticalBattle = 3,
    }
    
    public class LoadResponse
    {
        private readonly AsyncOperation[] scenesLoading;

        public float Progress
        {
            get
            {
                var progress = 0f;
                foreach (var asyncOperation in scenesLoading)
                {
                    progress = asyncOperation.isDone ? 100f : asyncOperation.progress;
                }
            
                return (progress / scenesLoading.Length) * 100f;
            }
        }

        public bool IsDone
        {
            get
            {
                return scenesLoading.All(asyncOperation => asyncOperation.isDone);
            }
        }

        public LoadResponse(params AsyncOperation[] asyncOperations)
        {
            scenesLoading = asyncOperations;
        }
    }
    
    /**
     * TODO: NOT COMPLETE, barely works.
     */
    public class LevelManager
    {
        public static bool IsInitialized { get; private set; }
        public static Level CurrentScene { get; private set; } = Level.None;
        
        public static LoadResponse Init()
        {
            if (IsInitialized)
            {
                throw new ArgumentException("Already initialized.");
            }

            var response = new LoadResponse(
                SceneManager.LoadSceneAsync((int) Level.Bootstrap, LoadSceneMode.Additive));
            IsInitialized = true;
            return response;
        }


        public LevelManager()
        {
            
        }

        public LoadResponse Load(Level level)
        {
//            if (Level.None != CurrentScene)
//            {
//                SceneManager.UnloadSceneAsync((int) CurrentScene);
//            }

            var response = new LoadResponse(
                SceneManager.LoadSceneAsync((int) level, LoadSceneMode.Single));
            CurrentScene = level;

            return response;
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}