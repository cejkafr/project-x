using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Game.Commons
{
    public class ConfigVarAttribute : Attribute
    {
        public string Name = null;
        public string DefaultValue = "";
        public ConfigVar.Flags Flags = ConfigVar.Flags.None;
        public string Description = "";
    }

    public class ConfigVar
    {
        [Flags]
        public enum Flags
        {
            None = 0x0, // None
            Save = 0x1, // Causes the cvar to be save to settings.cfg
            Cheat = 0x2, // Consider this a cheat var. Can only be set if cheats enabled
            User = 0x10, // User created variable
        }

        private static bool _initialized;
        private static readonly Regex ValidateNameRe = new Regex(@"^[a-z_+-][a-z0-9_+.-]*$");

        public static Dictionary<string, ConfigVar> ConfigVars;
        public static Flags DirtyFlags = Flags.None;

        public static void Init()
        {
            if (_initialized)
            {
                return;
            }

            ConfigVars = new Dictionary<string, ConfigVar>();
            InjectAttributeConfigVars();
            _initialized = true;
        }

        public static void ResetAllToDefault()
        {
            foreach (var v in ConfigVars)
            {
                v.Value.ResetToDefault();
            }
        }

        public static void SaveChangedVars(string filename)
        {
            if ((DirtyFlags & Flags.Save) == Flags.None)
            {
                return;
            }

            Save(filename);
        }

        public static void Save(string filename)
        {
            using (var st = File.CreateText(filename))
            {
                foreach (var cvar in ConfigVars.Values)
                {
                    if ((cvar.ConfigFlags & Flags.Save) == Flags.Save)
                        st.WriteLine("{0} \"{1}\"", cvar.Name, cvar.Value);
                }

                DirtyFlags &= ~Flags.Save;
            }

            GameDebug.Log("saved: " + filename);
        }

        public static void RegisterConfigVar(ConfigVar cvar)
        {
            if (ConfigVars.ContainsKey(cvar.Name))
            {
                GameDebug.LogError("Trying to register cvar " + cvar.Name + " twice");
                return;
            }

            if (!ValidateNameRe.IsMatch(cvar.Name))
            {
                GameDebug.LogError("Trying to register cvar with invalid name: " + cvar.Name);
                return;
            }

            ConfigVars.Add(cvar.Name, cvar);
        }

        private static void InjectAttributeConfigVars()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var _class in assembly.GetTypes())
                {
                    if (!_class.IsClass)
                    {
                        continue;
                    }

                    foreach (var field in _class.GetFields(BindingFlags.Instance |
                                                           BindingFlags.Static |
                                                           BindingFlags.NonPublic |
                                                           BindingFlags.Public))
                    {
                        if (!field.IsDefined(typeof(ConfigVarAttribute), false))
                            continue;
                        if (!field.IsStatic)
                        {
                            GameDebug.LogError("Cannot use ConfigVar attribute on non-static fields");
                            continue;
                        }

                        if (field.FieldType != typeof(ConfigVar))
                        {
                            GameDebug.LogError("Cannot use ConfigVar attribute on fields not of type ConfigVar");
                            continue;
                        }

                        var attr = field.GetCustomAttributes(typeof(ConfigVarAttribute),
                            false)[0] as ConfigVarAttribute;
                        var name = attr.Name ?? _class.Name.ToLower() + "." + field.Name.ToLower();
                        if (field.GetValue(null) is ConfigVar cvar)
                        {
                            GameDebug.LogError("ConfigVars (" + name +
                                               ") should not be initialized from code; just marked with attribute");
                            continue;
                        }

                        cvar = new ConfigVar(name, attr.Description, attr.DefaultValue, attr.Flags);
                        cvar.ResetToDefault();
                        RegisterConfigVar(cvar);
                        field.SetValue(null, cvar);
                    }
                }
            }

            // Clear dirty flags as default values shouldn't count as dirtying
            DirtyFlags = Flags.None;
        }

        private string m_StringValue;
        private float m_FloatValue;
        private int m_IntValue;

        public readonly string Name;
        public readonly string Description;
        public readonly string DefaultValue;
        public readonly Flags ConfigFlags;

        public bool Changed;

        public int IntValue => m_IntValue;
        public float FloatValue => m_FloatValue;

        private ConfigVar(string name, string description, string defaultValue, Flags configFlags = Flags.None)
        {
            Name = name;
            ConfigFlags = configFlags;
            Description = description;
            DefaultValue = defaultValue;
        }

        public virtual string Value
        {
            get => m_StringValue;
            set
            {
                if (m_StringValue == value)
                    return;
                DirtyFlags |= ConfigFlags;
                m_StringValue = value;
                if (!int.TryParse(value, out m_IntValue))
                    m_IntValue = 0;
                if (!float.TryParse(value, NumberStyles.Float,
                    CultureInfo.InvariantCulture, out m_FloatValue))
                    m_FloatValue = 0;
                Changed = true;
            }
        }

        private void ResetToDefault()
        {
            Value = DefaultValue;
        }

        public bool ChangeCheck()
        {
            if (!Changed)
            {
                return false;
            }

            Changed = false;
            return true;
        }
    }
}