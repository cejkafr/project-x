﻿using UnityEngine;

namespace Game.Wrappers
{
    public class DirectWrapper<T> : MonoBehaviour
    {
        public T Value => GetComponent<T>();
    }
}