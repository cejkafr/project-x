﻿using UnityEngine;

namespace Game.Wrappers
{
    public class CachedWrapper<T> : MonoBehaviour
    {
        public T Value { get; private set; }

        private void Awake()
        {
            Value = GetComponent<T>();
        }
    }
}