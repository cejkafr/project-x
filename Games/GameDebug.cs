using System;
using System.IO;
using Game.Console;
using Games.Consoles;
using UnityEngine;

//
// Logging of messages
//
// There are three different types of messages:
//
// Debug.Log/Warn/Error coming from unity (or code, e.g. packages, not using GameDebug)
//    These get caught here and sent onto the console and into our log file
// GameDebug.Log/Warn/Error coming from game
//    These gets sent onto the console and into our log file
//    *IF* we are in editor, they are also sent to Debug.* so they show up in editor Console window
// Console.Write
//    Only used for things that should not be logged. Typically reponses to user commands. Only shown on Console.
//

namespace Game.Commons
{
    public static class GameDebug
    {
        private static StreamWriter LogFile;

        private static bool ForwardToDebug = true;
        public static void Init(string logfilePath, string logBaseName)
        {
            ForwardToDebug = Application.isEditor;
            Application.logMessageReceived += LogCallback;

            // Try creating logName; attempt a number of suffixxes
            var name = "";
            for (var i = 0; i < 10; i++)
            {
                name = logBaseName + (i == 0 ? "" : "_" + i) + ".log";
                try
                {
                    LogFile = File.CreateText(logfilePath + "/" + name);
                    LogFile.AutoFlush = true;
                    break;
                }
                catch
                {
                    name = "<none>";
                }
            }
            Log("GameDebug initialized. Logging to " + logfilePath + "/" + name);
        }

        public static void Shutdown()
        {
            Application.logMessageReceived -= LogCallback;
            if (LogFile != null)
                LogFile.Close();
            LogFile = null;
        }

        static void LogCallback(string message, string stack, LogType logtype)
        {
            switch (logtype)
            {
                default:
                case LogType.Log:
                    _Log(message);
                    break;
                case LogType.Warning:
                    _LogWarning(message);
                    break;
                case LogType.Error:
                    _LogError(message);
                    break;
            }
        }

        public static void Log(string message)
        {
            if (ForwardToDebug)
                Debug.Log(message);
            else
                _Log(message);
        }

        static void _Log(string message)
        {
            ConsoleManager.Write(Time.frameCount + ": " + message);
            if (LogFile != null)
                LogFile.WriteLine(Time.frameCount + ": " + message + "\n");
        }

        public static void LogError(string message)
        {
            if (ForwardToDebug)
                Debug.LogError(message);
            else
                _LogError(message);
        }

        static void _LogError(string message)
        {
            ConsoleManager.Write(Time.frameCount + ": [ERR] " + message);
            if (LogFile != null)
                LogFile.WriteLine("[ERR] " + message + "\n");
        }

        public static void LogWarning(string message)
        {
            if (ForwardToDebug)
                Debug.LogWarning(message);
            else
                _LogWarning(message);
        }

        static void _LogWarning(string message)
        {
            ConsoleManager.Write(Time.frameCount + ": [WARN] " + message);
            if (LogFile != null)
                LogFile.WriteLine("[WARN] " + message + "\n");
        }

        public static void Assert(bool condition)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED");
        }

        public static void Assert(bool condition, string msg)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + msg);
        }

        public static void Assert<T>(bool condition, string format, T arg1)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + string.Format(format, arg1));
        }

        public static void Assert<T1, T2>(bool condition, string format, T1 arg1, T2 arg2)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + string.Format(format, arg1, arg2));
        }

        public static void Assert<T1, T2, T3>(bool condition, string format, T1 arg1, T2 arg2, T3 arg3)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + string.Format(format, arg1, arg2, arg3));
        }

        public static void Assert<T1, T2, T3, T4>(bool condition, string format, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + string.Format(format, arg1, arg2, arg3, arg4));
        }

        public static void Assert<T1, T2, T3, T4, T5>(bool condition, string format, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            if (!condition)
                throw new ApplicationException("GAME ASSERT FAILED : " + string.Format(format, arg1, arg2, arg3, arg4, arg5));
        }
    }
}
