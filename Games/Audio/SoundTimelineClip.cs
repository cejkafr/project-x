﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Game.Commons.Audio
{
    public class SoundTimelineClip : PlayableAsset, ITimelineClipAsset
    {
        private readonly SoundTimelineBehaviour m_Template = new SoundTimelineBehaviour();

        public SoundDef sound;
        public SoundTimelineBehaviour.SoundPosition position = SoundTimelineBehaviour.SoundPosition.None;

        public ClipCaps clipCaps => ClipCaps.None;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<SoundTimelineBehaviour>.Create(graph, m_Template);
            var clone = playable.GetBehaviour();
            clone.sound = sound;
            clone.position = position;
            return playable;
        }
    }
}
