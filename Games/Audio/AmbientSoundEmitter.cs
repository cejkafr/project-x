﻿using Games;
using UnityEngine;

namespace Game.Commons.Audio
{
    public class AmbientSoundEmitter : MonoBehaviour
    {
        private SoundSystem.SoundHandle handle;
        
        public SoundDef sound;

        private void Start()
        {
            handle = GameManager.SoundSystem.Play(sound);
        }

        private void OnDisable()
        {
            GameManager.SoundSystem.Stop(handle, 4.0f);
        }
    }
}