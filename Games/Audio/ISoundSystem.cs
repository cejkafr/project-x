﻿using Games.Audio;
using UnityEngine;
using UnityEngine.Audio;

namespace Game.Commons.Audio
{
    public interface ISoundSystem
    {
        void Init(AudioMixer mixer);
        void MountBank(SoundBank bank);
        void SetCurrentListener(AudioListener audioListener);
        SoundSystem.SoundHandle Play(SoundDef soundDef);
        SoundSystem.SoundHandle Play(SoundDef soundDef, Transform parent);
        SoundSystem.SoundHandle Play(SoundDef soundDef, Vector3 position);
        SoundSystem.SoundHandle Play(WeakSoundDef weakSoundDef);
        SoundSystem.SoundHandle Play(WeakSoundDef weakSoundDef, Transform parent);
        void Stop(SoundSystem.SoundHandle sh, float fadeOutTime = 0);
        void UnmountBank(SoundBank bank);
        void Update();
    }
}