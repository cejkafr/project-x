using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Profiling;
using Game;
using Games.Audio;

namespace Game.Commons.Audio
{
    public class SoundSystem : ISoundSystem
    {
        // These are passed to the game code
        public readonly struct SoundHandle
        {
            public static readonly SoundHandle Invalid = new SoundHandle();
            
            public readonly SoundEmitter Emitter;
            public readonly int Seq;

            public bool IsValid()
            {
                return Emitter != null && Emitter.SeqId == Seq;
            }

            public SoundHandle(SoundEmitter e)
            {
                Emitter = e;
                Seq = e?.SeqId ?? -1;
            }
        }

        public struct SoundReq
        {
            public SoundDef Def;
            public bool UsePos;
            public Vector3 Pos;
            public Transform Transform;
        }
        
        // These are internal to the SoundSystem
        public class SoundEmitter
        {
            public AudioSource Source;
            public SoundDef SoundDef;
            public bool Playing;
            public int RepeatCount;
            public Interpolator FadeToKill;
            public int SeqId;

            internal void Kill()
            {
                Source.Stop();
                RepeatCount = 0;
            }
        }

        private static readonly float SOUND_VOL_CUTOFF = -60.0f;
        private static readonly float SOUND_AMP_CUTOFF = Mathf.Pow(2.0f, SOUND_VOL_CUTOFF / 6.0f);
        
        private static AudioMixerGroup[] _mixerGroups;
        
        [ConfigVar(Name = "sound.debug", DefaultValue = "0", Description = "Enable sound debug overlay")]
        public static ConfigVar SoundDebug;

        [ConfigVar(Name = "sound.numemitters", DefaultValue = "48", Description = "Number of sound emitters")]
        public static ConfigVar SoundNumEmitters;

        [ConfigVar(Name = "sound.spatialize", DefaultValue = "1", Description = "Use spatializer")]
        public static ConfigVar SoundSpatialize;

        [ConfigVar(Name = "sound.mute", DefaultValue = "-1",
            Description = "Is audio enabled. -1 causes default behavior (on when window has focus)",
            Flags = ConfigVar.Flags.None)]
        public static ConfigVar SoundMute;

        // Debugging only
        [ConfigVar(Name = "sound.mastervol", DefaultValue = "1", Description = "Master volume",
            Flags = ConfigVar.Flags.None)]
        public static ConfigVar SoundMasterVol;

        // Exposed in options menu
        [ConfigVar(Name = "sound.menuvol", DefaultValue = "1", Description = "Menu volume",
            Flags = ConfigVar.Flags.None)]
        public static ConfigVar SoundMenuVol;

        [ConfigVar(Name = "sound.sfxvol", DefaultValue = "1", Description = "SFX volume", Flags = ConfigVar.Flags.Save)]
        public static ConfigVar SoundSfxVol;

        [ConfigVar(Name = "sound.musicvol", DefaultValue = "1", Description = "Music volume",
            Flags = ConfigVar.Flags.Save)]
        public static ConfigVar SoundMusicVol;

        private static float DecibelFromAmplitude(float amplitude)
        {
            if (amplitude < SOUND_AMP_CUTOFF)
                return -60.0f;
            return 6.0f * Mathf.Log(amplitude) / Mathf.Log(2.0f);
        }

        private static float AmplitudeFromDecibel(float decibel)
        {
            if (decibel <= SOUND_VOL_CUTOFF)
            {
                return 0;
            }

            return Mathf.Pow(2.0f, decibel / 6.0f);
        }
        
                private static void StartEmitter(SoundEmitter emitter)
        {
            var soundDef = emitter.SoundDef;
            var source = emitter.Source;

            StartSource(source, soundDef);
        }

#if UNITY_EDITOR
        public static void StartSource(AudioSource source, SoundDef soundDef)
#else
    static void StartSource(AudioSource source, SoundDef soundDef)
#endif
        {
            Profiler.BeginSample(".Set source clip");
            source.clip = soundDef.clips[Random.Range(0, soundDef.clips.Count)];
            Profiler.EndSample();

            Profiler.BeginSample(".Setup source");
            // Map from halftone space to linear playback multiplier
            source.pitch = Mathf.Pow(2.0f, Random.Range(soundDef.pitchMin, soundDef.pitchMax) / 12.0f);
            source.minDistance = soundDef.distMin;
            source.maxDistance = soundDef.distMax;
            source.volume = AmplitudeFromDecibel(soundDef.volume);
            source.loop = soundDef.loopCount < 1 ? true : false;
            source.rolloffMode = soundDef.rolloffMode;
            var delay = Random.Range(soundDef.delayMin, soundDef.delayMax);
            if (_mixerGroups != null)
                source.outputAudioMixerGroup = _mixerGroups[(int) soundDef.soundGroup];
            source.spatialBlend = soundDef.spatialBlend;
            source.panStereo = Random.Range(soundDef.panMin, soundDef.panMax);
            Profiler.EndSample();

            // soundSpatialize can be null as this is run from editor too
            Profiler.BeginSample(".Setup spatializer");
            if (SoundSpatialize != null && SoundSpatialize.IntValue > 0 && soundDef.spatialBlend > 0.5f)
            {
                source.spatialize = true;
                source.SetSpatializerFloat(0, 8.0f);
                source.SetSpatializerFloat(1, 0.0f);
                //source.SetSpatializerFloat(2, soundDef.distMin);
                //source.SetSpatializerFloat(3, soundDef.distMax);
                source.SetSpatializerFloat(4, 0.0f);
                source.SetSpatializerFloat(5, 0.0f);
                source.spatializePostEffects = false;
                //source.rolloffMode = source.spatialize ? AudioRolloffMode.Linear : source.rolloffMode;
            }
            else
            {
                source.spatialize = false;
            }

            Profiler.EndSample();

            Profiler.BeginSample("AudioSource.Play");
            if (delay > 0.0f)
                source.PlayDelayed(delay);
            else
                source.Play();
            Profiler.EndSample();
        }

        private readonly Dictionary<string, SoundDef> m_SoundDefs = new Dictionary<string, SoundDef>();
        private AudioMixer m_AudioMixer;
        private int m_SequenceId;
        private SoundEmitter[] m_Emitters;
        private GameObject m_SourceHolder;
        private AudioListener m_CurrentListener;
        private Interpolator m_MasterVolume = new Interpolator(1.0f, Interpolator.CurveType.SmoothStep);
        private bool m_Focus = false;

        private AudioSource MakeAudioSource()
        {
            var go = new GameObject("SoundSystemSource");
            go.transform.parent = m_SourceHolder.transform;
            return go.AddComponent<AudioSource>();
        }

        public void Init(AudioMixer mixer)
        {
            m_SourceHolder = new GameObject("SoundSystemSources");
            Object.DontDestroyOnLoad(m_SourceHolder);
            m_AudioMixer = mixer;
            GameDebug.Log("SoundSystem using mixer: " + m_AudioMixer.name);
            m_SequenceId = 0;

            // Create pool of emitters
            m_Emitters = new SoundEmitter[SoundNumEmitters.IntValue];
            for (var i = 0; i < SoundNumEmitters.IntValue; i++)
            {
                var emitter = new SoundEmitter();
                emitter.Source = MakeAudioSource();
                emitter.FadeToKill = new Interpolator(1.0f, Interpolator.CurveType.Linear);
                m_Emitters[i] = emitter;
            }

            // Set up mixer groups
            _mixerGroups = new AudioMixerGroup[(int) SoundMixerGroup._Count];
            _mixerGroups[(int) SoundMixerGroup.Menu] = m_AudioMixer.FindMatchingGroups("Menu")[0];
            _mixerGroups[(int) SoundMixerGroup.Music] = m_AudioMixer.FindMatchingGroups("Music")[0];
            _mixerGroups[(int) SoundMixerGroup.SFX] = m_AudioMixer.FindMatchingGroups("SFX")[0];
        }

        private SoundEmitter AllocEmitter()
        {
            // Look for unused emitter
            foreach (var e in m_Emitters)
            {
                if (e.Playing) continue;
                e.SeqId = m_SequenceId++;
                return e;
            }

            // Hunt down one emitter to kill
            SoundEmitter emitter = null;
            var distance = float.MinValue;
            var listenerPos = m_CurrentListener != null ? m_CurrentListener.transform.position : Vector3.zero;
            foreach (var e in m_Emitters)
            {
                var s = e.Source;

                if (s == null)
                {
                    // Could happen if parent was killed. Not good, but fixable:
                    GameDebug.LogWarning("Soundemitter had its audiosource destroyed. Making a new.");
                    e.Source = MakeAudioSource();
                    e.RepeatCount = 0;
                    s = e.Source;
                }

                // Skip destroyed sources and looping sources
                if (s.loop)
                {
                    continue;
                }

                // Pick closest; assuming 2d sounds very close!
                var dist = 0.0f;
                if (s.spatialBlend > 0.0f)
                {
                    dist = (s.transform.position - listenerPos).magnitude;

                    // if tracking another object assume closer
                    var t = s.transform;
                    if (t.parent != m_SourceHolder.transform)
                        dist *= 0.5f;
                }

                if (dist > distance)
                {
                    distance = dist;
                    emitter = e;
                }
            }

            if (emitter != null)
            {
                emitter.Kill();
                emitter.SeqId = m_SequenceId++;
                return emitter;
            }

            GameDebug.Log("Unable to allocate sound emitter!");
            return null;
        }

        public SoundHandle Play(WeakSoundDef weakSoundDef)
        {
            return m_SoundDefs.TryGetValue(weakSoundDef.guid, out var def) ? Play(def) : new SoundHandle(null);
        }
        
        public SoundHandle Play(WeakSoundDef weakSoundDef, Transform transform)
        {
            return m_SoundDefs.TryGetValue(weakSoundDef.guid, out var def) ? Play(def, transform) : new SoundHandle(null);
        }

        public SoundHandle Play(SoundDef soundDef)
        {
            GameDebug.Assert(soundDef != null);

            var e = AllocEmitter();

            if (e == null)
                return new SoundHandle(null);

            if (soundDef.spatialBlend > 0.0f)
                GameDebug.LogWarning($"Playing 3d {soundDef.name} sound at 0,0,0");

            e.Source.transform.position = new Vector3(0, 0, 0);
            e.RepeatCount = Random.Range(soundDef.repeatMin, soundDef.repeatMax);
            e.Playing = true;
            e.SoundDef = soundDef;
            StartEmitter(e);
            return new SoundHandle(e);
        }

        public SoundHandle Play(SoundDef soundDef, Vector3 position)
        {
            var e = AllocEmitter();

            if (e == null)
                return new SoundHandle(null);

            e.Source.transform.position = position;
            e.RepeatCount = Random.Range(soundDef.repeatMin, soundDef.repeatMax);
            e.Playing = true;
            e.SoundDef = soundDef;
            StartEmitter(e);
            return new SoundHandle(e);
        }

        public SoundHandle Play(SoundDef soundDef, Transform parent)
        {
            Profiler.BeginSample("SoundSystem.AllocEmitter");
            var e = AllocEmitter();
            Profiler.EndSample();

            if (e == null)
                return new SoundHandle(null);

            e.Source.transform.parent = parent;
            e.Source.transform.localPosition = Vector3.zero;
            e.RepeatCount = Random.Range(soundDef.repeatMin, soundDef.repeatMax);
            e.Playing = true;
            e.SoundDef = soundDef;
            Profiler.BeginSample("SoundSystem.StartEmitter");
            StartEmitter(e);
            Profiler.EndSample();
            return new SoundHandle(e);
        }

        public void Stop(SoundHandle sh, float fadeOutTime = 0.0f)
        {
            if (!sh.IsValid())
            {
                GameDebug.LogWarning("SoundSystem.Stop(): invalid SoundHandle");
                return;
            }

            if (fadeOutTime == 0.0f)
                sh.Emitter.FadeToKill.SetValue(0.0f);
            else
            {
                sh.Emitter.FadeToKill.SetValue(1.0f);
                sh.Emitter.FadeToKill.MoveTo(0.0f, fadeOutTime);
            }
        }

        public void Update()
        {
            if (m_Focus != Application.isFocused)
            {
                m_Focus = Application.isFocused;
                if (SoundMute.IntValue == -1)
                    m_MasterVolume.MoveTo(m_Focus ? 1.0f : 0.0f, 0.5f);
            }

            var masterVolume = m_MasterVolume.GetValue();

            if (SoundMute.IntValue == 0)
            {
                masterVolume = 0.0f;
                DebugOverlay.Write(Color.red, DebugOverlay.Width - 10, 2, "{0}", "AUDIO MUTED");
            }
            else if (SoundMute.IntValue == 1)
            {
                masterVolume = 1.0f;
                DebugOverlay.Write(Color.green, DebugOverlay.Width - 10, 2, "{0}", "AUDIO PLAYING");
            }

            m_AudioMixer.SetFloat("MasterVolume",
                DecibelFromAmplitude(Mathf.Clamp(SoundMasterVol.FloatValue, 0.0f, 1.0f) * masterVolume));
            m_AudioMixer.SetFloat("MusicVolume",
                DecibelFromAmplitude(Mathf.Clamp(SoundMusicVol.FloatValue, 0.0f, 1.0f)));
            m_AudioMixer.SetFloat("SFXVolume", DecibelFromAmplitude(Mathf.Clamp(SoundSfxVol.FloatValue, 0.0f, 1.0f)));
            m_AudioMixer.SetFloat("MenuVolume", DecibelFromAmplitude(Mathf.Clamp(SoundMenuVol.FloatValue, 0.0f, 1.0f)));

            // Update running sounds
            var count = 0;
            foreach (var e in m_Emitters)
            {
                if (!e.Playing)
                {
                    continue;
                }

                if (e.Source == null)
                {
                    // Could happen if parent was killed. Not good, but fixable:
                    GameDebug.LogWarning("SoundEmitter had it's AudioSource destroyed. Making a new.");
                    e.Source = MakeAudioSource();
                    e.RepeatCount = 0;
                }

                if (e.FadeToKill.IsMoving())
                {
                    e.Source.volume = AmplitudeFromDecibel(e.SoundDef.volume) * e.FadeToKill.GetValue();
                }
                else if (e.FadeToKill.GetValue() == 0.0f)
                {
                    // kill no matter what
                    e.Kill();
                }

                if (e.Source.isPlaying)
                {
                    count++;
                    continue;
                }

                if (e.RepeatCount > 1)
                {
                    e.RepeatCount--;
                    StartEmitter(e);
                    continue;
                }

                // Reset for reuse
                e.Playing = false;
                e.Source.transform.parent = m_SourceHolder.transform;
                e.Source.enabled = true;
                e.Source.gameObject.SetActive(true);
                e.Source.transform.position = Vector3.zero;
                e.FadeToKill.SetValue(1.0f);
            }

            if (SoundDebug.IntValue > 0)
            {
                // DebugOverlay.Write(30, 1, "Mixer: {0} {1}", m_AudioMixer.GetInstanceID(),
                //     GameManager.Instance.audioMixer.GetInstanceID());
                var ii = 4;
                foreach (var o in Object.FindObjectsOfType<AudioMixerGroup>())
                {
                    DebugOverlay.Write(30, ii++, "group: {0} {1}", o.name, o.GetInstanceID());
                }

                DebugOverlay.Write(1, 1, "Num audios {0}", count);
                for (int i = 0, c = m_Emitters.Length; i < c; ++i)
                {
                    var e = m_Emitters[i];
                    DebugOverlay.Write(1, 3 + i, "Emitter {0:##}  {1} {2} {3}", i,
                        e.Playing ? e.SoundDef.name : "<n/a>", e.Source.gameObject.activeInHierarchy ? "act" : "nact",
                        e.Playing ? "Mixer: " + e.Source.outputAudioMixerGroup.audioMixer.GetInstanceID() : "");
                }

                if (m_CurrentListener == null)
                {
                    DebugOverlay.Write(DebugOverlay.Width / 2 - 5, DebugOverlay.Height, "No AudioListener?");
                    return;
                }

                for (int i = 0, c = m_Emitters.Length; i < c; ++i)
                {
                    var e = m_Emitters[i];
                    if (!e.Playing)
                        continue;
                    var s = e.Source.spatialBlend;
                    var locpos = m_CurrentListener.transform.InverseTransformPoint(e.Source.transform.position);
                    var x = Mathf.Lerp(e.Source.panStereo * 10.0f, Mathf.Clamp(locpos.x, -10, 10), s);

                    var z = Mathf.Lerp(-10.0f, Mathf.Clamp(locpos.z, -10, 10), s);
                    DebugOverlay.Write(Color.Lerp(Color.green, Color.blue, s), DebugOverlay.Width / 2 + x,
                        DebugOverlay.Height / 2 - z, "{0} ({1:##.#})", e.SoundDef.name, locpos.magnitude);
                }
            }
        }

        public void MountBank(SoundBank bank)
        {
            for (int i = 0, c = bank.soundDefGuids.Count; i < c; ++i)
            {
                m_SoundDefs[bank.soundDefGuids[i]] = bank.soundDefs[i];
            }

            GameDebug.Log("Mounted soundbank: " + bank.name + " with " + bank.soundDefGuids.Count + " sounds");
        }

        public void UnmountBank(SoundBank bank)
        {
            GameDebug.Assert(bank.soundDefs.Count == bank.soundDefGuids.Count);
            for (int i = 0, c = bank.soundDefGuids.Count; i < c; ++i)
            {
                m_SoundDefs.Remove(bank.soundDefGuids[i]);
            }

            GameDebug.Log("Unmounted soundbank: " + bank.name + " with " + bank.soundDefGuids.Count + " sounds");
        }

        public void SetCurrentListener(AudioListener audioListener)
        {
            m_CurrentListener = audioListener;
        }
    }
}