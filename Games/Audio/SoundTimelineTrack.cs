﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Game.Commons.Audio
{
    [TrackColor(0.5f,0.8f,0.5f)]
    [TrackClipType(typeof(SoundTimelineClip))]
    [TrackBindingType(typeof(Transform))]
    public class SoundTimelineTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<SoundTimelineMixerBehaviour>.Create(graph, inputCount);
        }
    }
}
