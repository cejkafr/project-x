using System;
using System.Collections.Generic;
using Game.Commons.Audio;
using Game.WeakAssetReference;
using UnityEditor;
using UnityEngine;

namespace Games.Audio
{
    [Serializable]
    public class WeakSoundDef : Weak<SoundDef>
    {
        public bool IsValid()
        {
            return guid != "";
        }
    }

    [CreateAssetMenu(fileName = "Sound", menuName = "Audio/SoundBank", order = 10000)]
    public class SoundBank : ScriptableObject
    {
        public List<string> soundDefGuids;
        
        public List<SoundDef> soundDefs;

        public SoundDef FindByName(string name)
        {
            foreach (var s in soundDefs)
            {
                if (s.name == name)
                    return s;
            }

            return null;
        }

#if UNITY_EDITOR
        void OnValidate()
        {
            soundDefGuids.Clear();
            foreach (var s in soundDefs)
            {
                var p = AssetDatabase.GetAssetPath(s);
                soundDefGuids.Add(AssetDatabase.AssetPathToGUID(p));
            }
        }
#endif
    }
}