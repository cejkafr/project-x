﻿using System.Collections.Generic;

namespace Game.Extensions
{
    public static class DictionaryExtensions
    {
        public static bool IsEmpty<T, T1>(this Dictionary<T, T1> dic)
        {
            return dic.Count == 0;
        }
    }
}