﻿using Unity.Mathematics;

namespace Game.Extensions
{
    public static class int2Extensions
    {
        public static int sqrMagnitude(this int2 original)
        {
            return (original.x * original.x + original.y * original.y);
        }
    }
}