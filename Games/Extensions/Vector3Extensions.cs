﻿using UnityEngine;

namespace Game.Extensions
{
    public static class Vector3Extensions
    {
        public static Vector3 With(this Vector3 original, float? x = null, float? y = null, float? z = null)
        {
            return new Vector3(x ?? original.x, y ?? original.y, z ?? original.z);
        }
        
        public static Vector3 Flatten(this Vector3 original)
        {
            return new Vector3(original.x, 0, original.z);
        }

        public static Vector3 DirectionTo(this Vector3 original, Vector3 destination)
        {
            return Vector3.Normalize(destination - original);
        }
        
        public static float DistanceTo(this Vector3 original, Vector3 destination)
        {
            return Vector3.Distance(original, destination);
        }
        
        public static float DistanceToFlat(this Vector3 original, Vector3 destination)
        {
            return Vector3.Distance(original.Flatten(), destination.Flatten());
        }
        
        // public static Vector2 Flatten2(this Vector3 original)
        // {
        //     return new Vector2(original.x, original.z);
        // }
    }
}