﻿using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Game.Extensions
{
    public static class PlayableDirectorExtensions
    {
        public static void BindTrack(this PlayableDirector director, string trackName, Object value)
        {
            var track = FindTrackByName(director, trackName);
            director.SetGenericBinding(track, value);
        }
        
        public static void SetTrackOffset(this PlayableDirector director, string trackName, Vector3 pos, Quaternion rot)
        {
            var track = FindTrackByName(director, trackName) as AnimationTrack;
            if (track == null)
            {
                return;
            }

            track.position = pos;
            track.rotation = rot;
        }
      
        public static void SetAnimationOffset(this PlayableDirector director, string trackName, Vector3 pos, Quaternion rot)
        {
            var track = FindTrackByName(director, trackName);

            foreach (var clip in track.GetClips())
            {

                var clipAsset = clip.asset as AnimationPlayableAsset;
                if (clipAsset == null)
                {
                    continue;
                }

                clipAsset.position = pos;
                clipAsset.rotation = rot;
            }
        }
        
        public static TrackAsset FindTrackByName(this PlayableDirector director, string trackName)
        {
            var asset = (TimelineAsset) director.playableAsset;
            return asset.GetOutputTracks().FirstOrDefault(track => track.name == trackName);
        }
    }
}