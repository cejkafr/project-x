﻿using UnityEngine;

namespace Game.Extensions
{
    public static class TransformExtensions
    {
        public static Vector3 DirectionTo(this Transform source, Transform destination)
        {
            return source.position.DirectionTo(destination.position);
        }

        public static float FacingAngle(this Transform source, Vector3 destination)
        {
            return Vector3.Angle(source.forward, destination - source.position);
        }
        
        public static float FacingAngleFlat(this Transform source, Vector3 destination)
        {
            return Vector3.Angle(source.forward.Flatten(), destination - source.position.Flatten());
        }
        
        // public static float FacingAngle2(this Transform source, Vector2 destination)
        // {
        //     return Vector2.Angle(source.forward.Flatten2(), destination - source.position.Flatten2());
        // }
    }
}