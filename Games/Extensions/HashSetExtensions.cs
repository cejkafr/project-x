﻿using System.Collections.Generic;

namespace Game.Extensions
{
    public static class HashSetExtensions
    {
        public static bool IsEmpty<T>(this HashSet<T> set)
        {
            return set.Count == 0;
        }
    }
}