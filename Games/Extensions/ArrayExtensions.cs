﻿using System;

namespace Game.Extensions
{
    public static class ArrayExtensions
    {
        public static void IterateOver<T>(this T[,] values, Action<int, int, T> action)
        {
            for (var y = 0; y < values.GetLength(1); y++)
            {
                for (var x = 0; x < values.GetLength(0); x++)
                {
                    action.Invoke(x, y, values[x, y]);
                }
            }
        }

        public static void IterateOver<T>(this T[,,] values, Action<int, int, int, T> action)
        {
            for (var z = 0; z < values.GetLength(2); z++)
            {
                for (var y = 0; y < values.GetLength(1); y++)
                {
                    for (var x = 0; x < values.GetLength(0); x++)
                    {
                        action.Invoke(x, y, z, values[x, y, z]);
                    }
                }
            }
        }
    }
}