﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Commons.Pools
{
    public class SimpleObjectPool : MonoBehaviour, IObjectPool
    {
        [Serializable]
        public struct PooledObject
        {
            public GameObject prefab;
            public int size;
            public bool resizeable;
        }

        private struct PoolData
        {
            public readonly Queue<GameObject> Queue;
            public readonly bool Resizeable;

            public PoolData(int initialSize, bool resizeable)
            {
                Queue = new Queue<GameObject>(initialSize);
                Resizeable = resizeable;
            }
        }
    
        private static Dictionary<GameObject, PoolData> PoolDictionary;

        public PooledObject[] settings;

        private void Awake()
        {
            PoolDictionary = new Dictionary<GameObject, PoolData>(settings.Length);
        
            foreach (var setting in settings)
            {
                CreatePool(setting.prefab, setting.size, setting.resizeable);
            }
        }

        public GameObject CreateFromPool(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            if (!PoolDictionary.ContainsKey(prefab))
            {
                throw new ArgumentException("Unknown pool " + gameObject.name + ".");
            }
        
            var pool = PoolDictionary[prefab];
            var inst = pool.Queue.Dequeue();
            pool.Queue.Enqueue(inst);

            inst.SetActive(false);
            
            inst.transform.position = position;
            inst.transform.rotation = rotation;

            inst.SetActive(true);
            
            return inst;
        }

        public GameObject CreateFromPool(GameObject prefab, Transform t)
        {
            return CreateFromPool(prefab, t.position, t.rotation);
        }

        public void CreatePool(GameObject prefab, int initialSize, bool resizeable)
        {
            var pool = new PoolData(initialSize, resizeable);
            PoolDictionary.Add(prefab, pool);

            for (var i = 0; i < initialSize; i++)
            {
                var inst = Instantiate(prefab, transform);
                inst.SetActive(false);
            
                pool.Queue.Enqueue(inst);
            }
        }
    }
}
