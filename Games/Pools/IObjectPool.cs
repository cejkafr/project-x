﻿using UnityEngine;

namespace Game.Commons.Pools
{
    public interface IObjectPool
    {
        GameObject CreateFromPool(GameObject prefab, Vector3 position, Quaternion rotation);
        GameObject CreateFromPool(GameObject prefab, Transform t);
        void CreatePool(GameObject prefab, int initialSize, bool resizeable);
    }
}