﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UniverseInitializers.UI
{
    public class UniverseInitializerReadyScreen : MonoBehaviour
    {
        [Serializable]
        public class ReadyEvent : UnityEvent
        {
        
        }
        
        public ReadyEvent onReady = new ReadyEvent();
        
        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }

        public void OnBtnClick()
        {
            onReady.Invoke();
        }
    }
}