﻿using System;
using System.Collections;
using Cinemachine;
using Commons;
using Game;
using Games;
using Sectors;
using Unity.Mathematics;
using UnityEngine;
using Universes;

namespace UniverseInitializers.UI
{
    public class UniverseInitializerManager : MonoBehaviour
    {
        [Serializable]
        public struct Bindings
        {
            public UniverseInitializerSeedScreen initializerSeedScreenScreen;
            public UniverseInitializerReadyScreen initializerReadyScreenScreen;
        }

        [SerializeField] private Bindings bindings;
        [SerializeField] private UniverseController universeController;
        [SerializeField] private StrategicMapController strategicMapController;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        [SerializeField] private int2 sectorCount;
        [SerializeField] private float cameraSpeed;

        private void OnEnable()
        {
            bindings.initializerSeedScreenScreen.SetActive(true);
            bindings.initializerReadyScreenScreen.SetActive(false);
            virtualCamera.enabled = false;
            
            GameManager.PlayMusic("MainMenu", .5f, .5f);
        }

        public void OnGenerate(string seed)
        {
            bindings.initializerSeedScreenScreen.SetActive(false);
            StartCoroutine(GenerateUniverse(seed));
        }

        public void OnStartPlayMode()
        {
            SetActive(false);
            strategicMapController.StartPlayMode();
        }
        
        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }

        private IEnumerator GenerateUniverse(string seed)
        {
            universeController.Init(seed, sectorCount);
            
            yield return new WaitForSecondsRealtime(.1f);
            
            universeController.LoadSector(
                universeController.Universe.Sectors[0], SectorLoadReason.LoadGame);
            
            yield return new WaitForSecondsRealtime(.1f);
            
            bindings.initializerReadyScreenScreen.SetActive(true);
            StartCoroutine(MoveCamera());
        }

        private IEnumerator MoveCamera()
        {
            virtualCamera.enabled = true;
            var trackedDolly = virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();

            while (gameObject.activeInHierarchy)
            {
                yield return new WaitForEndOfFrame();
                trackedDolly.m_PathPosition += cameraSpeed * Time.deltaTime;
            }
        }
    }
}