﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UniverseInitializers.UI
{
    public class UniverseInitializerSeedScreen : MonoBehaviour
    {
        [Serializable]
        public class SeedEvent : UnityEvent<string>
        {
        
        }
        
        [Serializable]
        public struct Bindings
        {
            public TMP_InputField seedField;
            public Button continueBtn;
        }

        [SerializeField] private Bindings bindings;
        [Space]
        public SeedEvent onSubmit = new SeedEvent();

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
        
        private void OnEnable()
        {
            bindings.seedField.text = Random.Range(int.MinValue, int.MaxValue).ToString();
            bindings.continueBtn.enabled = true;
        }

        public void OnValueChanged(string value)
        {
            bindings.continueBtn.enabled = value.Length < 1;
        }
        
        public void OnBtnSubmit()
        {
            if (bindings.seedField.text.Length < 1)
            {
                return;
            }
            
            print("Universe seed: " + bindings.seedField.text + ".");
            
            onSubmit.Invoke(bindings.seedField.text);
        }
    }
}