﻿namespace Battles
{
    public interface IDetectionProcessor
    {
        void Init(ITeam playerTeam);
    }
}