﻿using Battles.Cameras;
using Battles.GridBoard.UI;
using Commons;
using Game.Console;
using Game.Extensions;
using Games.Consoles;
using TacticalBattles.Ships;
using TacticalBattles.Ships.Commands;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Battles.Controllers
{
    [RequireComponent(typeof(IPlaneRaycaster))]
    [RequireComponent(typeof(PlayerShipController))]
    public class PlayerInputProcessor : MonoBehaviour, IPlayerInputProcessor
    {
        [SerializeField] private CinemachineRTSCamera cameraRig;
        [SerializeField] private GridBoardUi gridBoardUi;
        
        private PlayerShipController playerShipController;
        private IPlaneRaycaster raycaster;
        private IShip ship;
        private bool control;
        private bool angleMode;
        private bool cameraRotation;

        // TODO move to Ship
        public bool AngleMode
        {
            get => angleMode;
            set
            {
                angleMode = value;

                if (!angleMode)
                {
                    var controller = playerShipController.Selected[0];
                    controller.Engine.Mode = ShipEngineMode.Standard;
                }
            }
        }

        private void Awake()
        {
            playerShipController = GetComponent<PlayerShipController>();
            raycaster = GetComponent<IPlaneRaycaster>();
        }

        public void OnCameraPanning(InputAction.CallbackContext ctx)
        {
            if (control) return;
            cameraRig.Move(ctx.ReadValue<Vector2>());
        }

        public void OnCameraRotation(InputAction.CallbackContext ctx)
        {
            if (!cameraRotation) return;
            cameraRig.Rotate(ctx.ReadValue<Vector2>());
        }
        
        public void OnCameraRotationToggle(InputAction.CallbackContext ctx)
        {
            cameraRotation = ctx.performed;
            cameraRig.Rotate(float2.zero);
        }
        
        public void OnCameraZoom(InputAction.CallbackContext ctx)
        {
            cameraRig.Zoom(ctx.ReadValue<Vector2>());
        }
        
        public void OnSelect(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValueAsButton()) return;
            if (EventSystem.current.IsPointerOverGameObject()) return;
            
            // TODO doesnt belong here, refactor
            if (AngleMode)
            {
                ship = playerShipController.Selected[0];
                var cPos = raycaster.CursorPosition();

                ship.Engine.Direction = (cPos.Flatten() - ship.Position.Flatten()).normalized;
                ship.Engine.Mode = ShipEngineMode.HighVelocityTurn;

                angleMode = false;
                return;
            }

            var go = raycaster.RaycastCursor().GameObject;
            if (go == null)
            {
                playerShipController.DeselectAll();
                return;
            }

            ship = go.GetComponent<IShip>();
            if (ship == null)
            {
                ship = go.GetComponent<ShipMarker>()?.Ship;
            }

            if (ship == null)
            {
                playerShipController.DeselectAll();
                return;
            }

            if (ship.Team.IsHuman || DetectionLevel.Full == ship.DetectionLevel)
            {
                playerShipController.Select(ship);
            }
        }

        public void OnSelectMultiple(InputAction.CallbackContext ctx)
        {
            playerShipController.IsMultiSelect = ctx.ReadValueAsButton();
        }

        public void OnSelect1(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectByIndex(0);
        }
        
        public void OnSelect2(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectByIndex(1);
        }
        
        public void OnSelect3(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectByIndex(2);
        }
        
        public void OnSelect4(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectByIndex(3);
        }

        public void OnSelectNext(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectNext();
        }
        
        public void OnSelectAll(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.SelectAll();
        }
        
        public void OnCommand(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValueAsButton()) return;
            if (EventSystem.current.IsPointerOverGameObject()) return;
            
            if (AngleMode)
            {
                angleMode = false;
                gridBoardUi.GetShipMarker(playerShipController.Selected[0]).ShowAngle(false, Vector3.zero);
                return;
            }

            var target = raycaster.RaycastCursor();

            if (target.Success)
            {
                ship = target.GameObject?.GetComponent<IShip>();
                if (ship != null)
                {
                    if (!ship.Team.IsHuman)
                    {
                        playerShipController.AttackTarget(ship);
                    }
                }
                else
                {
                    playerShipController.IssueCommand(new MoveShipCommand(target.Point));
                }
            }
        }

        public void OnCommandMultiple(InputAction.CallbackContext ctx)
        {
            playerShipController.IsMultiCommand = ctx.ReadValueAsButton();
        }

        public void OnCommandPressed(InputAction.CallbackContext ctx)
        {
            control = ctx.performed;
        }
        
        public void OnControlAutoEngage(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.AutoEngage = !playerShipController.AutoEngage;
        }
        
        public void OnControlStop(InputAction.CallbackContext ctx)
        {
            if (!ctx.started) return;
            playerShipController.Stop();
        }
        
        public void OnEngineSkill1(InputAction.CallbackContext ctx)
        {
            if (ShipEngineMode.Overclock == playerShipController.Selected[0].Engine.Mode)
            {
                playerShipController.Selected[0].Engine.Mode = ShipEngineMode.Standard;
            }
            else
            {
                playerShipController.Selected[0].Engine.Mode = ShipEngineMode.Overclock;
            }
        }
        
        public void OnEngineSkill2(InputAction.CallbackContext ctx)
        {
            AngleMode = !AngleMode;
        }
        
        public void OnEngineSkill3(InputAction.CallbackContext ctx)
        {
            if (ShipEngineMode.Disabled == playerShipController.Selected[0].Engine.Mode)
            {
                playerShipController.Selected[0].Engine.Mode = ShipEngineMode.Standard;
            }
            else
            {
                playerShipController.Selected[0].Engine.Mode = ShipEngineMode.Disabled;
            }
        }
        
        public void OnTimePause(InputAction.CallbackContext ctx)
        {
            TimeManager.TogglePause();
        }

        public void OnTimeSlower(InputAction.CallbackContext ctx)
        {
            TimeManager.SlowerSpeed();
        }
        
        public void OnTimeNormal(InputAction.CallbackContext ctx)
        {
            TimeManager.NormalSpeed();
        }
        
        public void OnTimeFaster(InputAction.CallbackContext ctx)
        {
            TimeManager.FasterSpeed();
        }
        
        public void OnTimeFastest(InputAction.CallbackContext ctx)
        {
            TimeManager.FastestSpeed();
        }

        public void OnMenu(InputAction.CallbackContext ctx)
        {
            ConsoleManager.EnqueueCommand("menu 2");
        }

        private void Update()
        {
            if (AngleMode)
            {
                var c = playerShipController.Selected[0];
                var cPos = raycaster.CursorPosition();

                gridBoardUi.GetShipMarker(c).ShowAngle(true, cPos);
            }
        }
    }
}