﻿using Commons;
using TacticalBattles.Ships.Commands;

namespace Battles.Controllers
{
    public class SimpleEnemyController : IEnemyController
    {
        private readonly ITeam team;

        public SimpleEnemyController(ITeam team)
        {
            this.team = team;
        }

        public void Tick()
        {
            for (var i = 0; i < team.Ships.Count; i++)
            {
                var ship = team.Ships[i];
                if (ship.IsDestroyed) continue;

                // Select target
                var enemy = team.EnemyShips[0];
                var enemyRight = enemy.Ship.Transform.right;
                ship.TargettingProcessor.Target = enemy.Ship;

                var offset = (i % 2 == 1 ? enemyRight : enemyRight * -1) * 800f;
                var pos = enemy.Ship.Position2D + GeneralUtils.V3ToV2(offset);

                ship.CommandProcessor.Set(new MoveShipCommand(pos));
            }
        }
    }
}