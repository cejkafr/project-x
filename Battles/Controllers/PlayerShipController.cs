﻿using System;
using System.Collections.Generic;
using Commons.DataObjects.Ships;
using Commons.Ships;
using DataObjects.Ships;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.Events;
using IShip = TacticalBattles.Ships.IShip;

namespace Battles.Controllers
{
    public class PlayerShipController : MonoBehaviour
    {
        [Serializable]
        public class PlayerShipEvent : UnityEvent<IShip>
        {
            
        }
        
        public PlayerShipEvent onShipSelected;
        public PlayerShipEvent onShipDeselected;

        public ITeam Team { get; private set; }
        public List<IShip> Selected { get; } = new List<IShip>(3);

        public bool IsMultiSelect { get; set; }
        public bool IsMultiCommand { get; set; }

        public bool AutoEngage
        {
            get
            {
                if (Selected.Count == 1)
                {
                    return Selected[0].TargettingProcessor.AutoEngage;
                }
                
                var val = false;
                for (var i = 0; i < Selected.Count; i++)
                {
                    var ship = Selected[i];
                    if (i != 0 && ship.TargettingProcessor.AutoEngage && !val)
                    {
                        return false;
                    }

                    val = ship.TargettingProcessor.AutoEngage;
                }

                return val;
            }

            set { ExecuteOnSelected(s => s.TargettingProcessor.AutoEngage = value); }
        }

        public StanceSkillObject StanceSkill
        {
            get
            {
                if (Selected.Count == 1)
                {
                    return Selected[0].StanceSkill;
                }
                
                StanceSkillObject val = null;
                foreach (var ship in Selected)
                {
                    if (val != null && ship.StanceSkill != val)
                    {
                        return null;
                    }

                    val = ship.StanceSkill;
                }

                return val;
            }

            set { ExecuteOnSelected(s => s.StanceSkill = value); }
        }

        public void Init(ITeam team)
        {
            if (team == null)
            {
                Debug.LogError("Player is not defined.");
            }

            Team = team;
        }

        public void Select(IShip ship)
        {
            if (Selected.Contains(ship)) return;

            if (!IsMultiSelect)
            {
                DeselectAll();
            }

            Selected.Add(ship);
            ship.OnSelected();

            onShipSelected.Invoke(ship);
        }

        public void SelectAll()
        {
            foreach (var ship in Team.Ships)
            {
                Select(ship);
            }
        }

        public void SelectNext()
        {
            var index = 0;
            if (Selected.Count > 0)
            {
                index = Team.Ships.IndexOf(Selected[0]) + 1;
            }

            if (index >= Team.Ships.Count)
            {
                index = 0;
            }
            
            SelectByIndex(index);
        }

        public void SelectByIndex(int index)
        {
            if (Team.Ships.Count < index) return;

            DeselectAll();
            Select(Team.Ships[index]);
        }

        public void Deselect(IShip ship)
        {
            if (!Selected.Contains(ship)) return;

            Selected.Remove(ship);
            onShipDeselected.Invoke(ship);
        }

        public void DeselectAll()
        {
            var arr = Selected.ToArray();
            foreach (var s in arr)
            {
                Deselect(s);
            }
        }

        public void Stop()
        {
            ExecuteOnSelected(s => s.CommandProcessor.Clear());
        }

        public void AttackTarget(IShip target)
        {
            ExecuteOnSelected(s => s.TargettingProcessor.Target = target);
        }

        public void IssueCommand(IShipCommand command)
        {
            if (IsMultiCommand)
            {
                AddCommand(command);
            }
            else
            {
                SetCommand(command);
            }
        }

        public void AddCommand(IShipCommand command)
        {
            ExecuteOnSelected(s => s.CommandProcessor.Add(command));
        }

        public void SetCommand(IShipCommand command)
        {
            ExecuteOnSelected(s => s.CommandProcessor.Set(command));
        }

        private void ExecuteOnSelected(Action<IShip> action)
        {
            foreach (var ship in Selected)
            {
                action.Invoke(ship);
            }
        }
    }
}