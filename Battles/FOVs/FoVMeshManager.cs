﻿using System.Collections.Generic;
using UnityEngine;

namespace Battles.FOVs
{
    public class FoVMeshManager : MonoBehaviour
    {
        public GameObject fieldOfViewPrefab;

        public GameObject CreateFieldOfView(float viewRadius, float viewAngle, float resolution)
        {
            var mesh = CreateFieldOfViewMesh(viewRadius, viewAngle, resolution);

            var inst = Instantiate(fieldOfViewPrefab, Vector3.zero, Quaternion.identity);
            inst.GetComponent<MeshFilter>().mesh = mesh;
            
            return inst;
        }
        
        private Mesh CreateFieldOfViewMesh(float viewRadius, float viewAngle, float resolution)
        {
            var stepCount = Mathf.RoundToInt(viewAngle * resolution);
            var stepAngleSize = viewAngle / stepCount;

            var viewPoints = new List<Vector3>();

            for (var i = 0; i < stepCount; i++)
            {
                var angle = (transform.eulerAngles.y - viewAngle * .5f + stepAngleSize * i);
                var newViewCast = PointFromAngle(angle, true, viewRadius);
                viewPoints.Add(newViewCast);
            }

            var vertexCount = viewPoints.Count + 1;
            var vertices = new Vector3[vertexCount];
            var triangles = new int[(vertexCount - 2) * 3];

            vertices[0] = Vector3.zero;
            for (var i = 0; i < vertexCount - 1; i++)
            {
                vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
                if (i >= vertexCount - 2) continue;

                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }

            var mesh = new Mesh
            {
                vertices = vertices,
                triangles = triangles
            };
            mesh.RecalculateNormals();

            return mesh;
        }

        private Vector3 PointFromAngle(float angleInDegrees, bool angleIsGlobal, float viewRadius)
        {
            if (!angleIsGlobal)
                angleInDegrees += transform.eulerAngles.y;

            var dir = new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0f,
                Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));

            return transform.position + dir * viewRadius;
        }
    }
}