﻿using TacticalBattles.Ships;
using UnityEngine;

namespace Battles.FOVs
{
    public class FoVTrajectoryRenderer : MonoBehaviour, IFoVRenderer
    {
        private IShipWeapon shipWeapon;
        private LineRenderer[] lineRenderers;

        [SerializeField] private float lineWidth;
        [SerializeField] private Material material;
        [SerializeField] private Color color;
        
        private void Start()
        {
        }

        private void LateUpdate()
        {
            for (var i = 0; i < lineRenderers.Length; i++)
            {
                lineRenderers[i].SetPosition(0, shipWeapon.Outputs[i].position);
                lineRenderers[i].SetPosition(1, shipWeapon.Outputs[i].position + shipWeapon.Outputs[i].forward * shipWeapon.Component.Range);
            }
        }

        public void Init(IShipWeapon iShipWeapon)
        {
            shipWeapon = iShipWeapon;

            lineRenderers = new LineRenderer[this.shipWeapon.Outputs.Length];
            for (var i = 0; i <lineRenderers.Length; i++)
            {
                var inst = new GameObject("LineRenderer_" + i, typeof(LineRenderer));
                inst.transform.SetParent(transform);
                lineRenderers[i] = inst.GetComponent<LineRenderer>();
                lineRenderers[i].material = material;
                lineRenderers[i].startColor = color;
                lineRenderers[i].endColor = color;
                lineRenderers[i].startWidth = lineWidth;
                lineRenderers[i].endWidth = lineWidth;
            }
        }

        public void Show(bool val)
        {
            gameObject.SetActive(val);
        }
    }
}
