﻿using TacticalBattles.Ships;
using UnityEngine;

namespace Battles.FOVs
{
    public class FoVFieldRenderer : MonoBehaviour, IFoVRenderer
    {
        public void Init(IShipWeapon controller)
        {
            
        }

        public void Show(bool val)
        {
            gameObject.SetActive(val);
        }
    }
}
