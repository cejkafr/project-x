﻿using System.Collections.Generic;
using TacticalBattles.Ships;

namespace Battles
{
    public interface IShipDetector
    {
        bool Evaluate(EnemySettings enemySettings, IEnumerable<IShip> ships);
    }
}