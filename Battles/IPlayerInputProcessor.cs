﻿namespace Battles
{
    public interface IPlayerInputProcessor
    {
        bool AngleMode { get; set; }
    }
}