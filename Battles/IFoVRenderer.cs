﻿using TacticalBattles.Ships;

namespace Battles
{
    public interface IFoVRenderer
    {
        void Init(IShipWeapon controller);
        void Show(bool val);
    }
}