﻿using Commons;
using UnityEngine;
using Universes;

namespace Battles.Scenarios
{
    // TODO Move setup functions separate
    public class EliminationScenario : IScenario
    {
        private const float XPadding = 250;
        private const float ZPadding = 250;
        private const float YPosition = 100;
        
        private readonly TacticalBattleController tacticalBattle;
        private readonly PlayerCombat combat;
        private readonly ITeam humanTeam;
        private readonly ITeam enemyTeam;

        public bool IsWon { get; private set; }
        public bool IsLost { get; private set; }
        public bool IsCompleted => IsWon || IsLost;

        public EliminationScenario(TacticalBattleController iTacticalBattle, PlayerCombat iCombat)
        {
            tacticalBattle = iTacticalBattle;
            combat = iCombat;
            
            humanTeam = tacticalBattle.PlayerShip.Team;
            // TODO hardcoded faction position
            enemyTeam = tacticalBattle.TeamManager.Teams[1];
        }

        public void OnInit()
        {
            PlacePlayerShips();
            PlaceEnemyShips();
            PositionCamera();
        }

        public void OnStart()
        {
        }

        public void OnCompleted()
        {
            if (IsWon)
            {
                combat.Status = PlayerCombatStatus.Won;
                combat.Fleet.Destroy();
            }
            else
            {
                combat.Status = PlayerCombatStatus.Lost;
            }
        }

        public void Tick()
        {
            CheckLost();
            CheckWon();

            if (IsCompleted)
            {
                
            }
        }

        private void PlacePlayerShips()
        {
            var shipCount = humanTeam.Ships.Count;
            var halfSize = tacticalBattle.Size * 0.5f;
            var y = (halfSize.y * -1) + ZPadding;
            var xOffset = shipCount == 1 ? 0f : -((shipCount - 1) * XPadding / 2f);

            foreach (var ship in humanTeam.Ships)
            {
                ship.Transform.position = new Vector3(xOffset, YPosition, y);
                ship.Transform.rotation = Quaternion.Euler(0, 0, 0);
                xOffset += XPadding;
            }
        }

        private void PlaceEnemyShips()
        {
            var halfSize = tacticalBattle.Size * 0.5f;
            var xStart = Random.Range(-halfSize.x, halfSize.x);
            var yStart = Random.Range(0, halfSize.x);
            var xOffset = xStart;
            
            foreach (var ship in enemyTeam.Ships)
            {
                ship.Transform.position = new Vector3(xOffset, YPosition, yStart);
                ship.Transform.rotation = Quaternion.Euler(0, 180, 0);
                xOffset += XPadding;
            }
        }

        private void PositionCamera()
        {
            var halfSize = tacticalBattle.Size * 0.5f;
            tacticalBattle.Camera.GoTo(new Vector2(0, (halfSize.y * -1) + ZPadding));
        }

        private void CheckLost()
        {
            var shipCount = humanTeam.Ships.FindAll(ship => !ship.IsDestroyed).Count;
            IsLost = shipCount < 1;
        }

        private void CheckWon()
        {
            var shipCount = humanTeam.EnemyShips.FindAll(enemy => !enemy.Ship.IsDestroyed).Count;
            IsWon = IsLost = shipCount < 1;
        }
    }
}