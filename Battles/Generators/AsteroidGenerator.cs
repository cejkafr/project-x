﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TacticalBattle.Generators
{
    public class AsteroidGenerator : MonoBehaviour, IAsteroidGenerator
    {
        private readonly float3 halfOne = new float3(0.5f, 0.5f, 1);

        [SerializeField] private GameObject[] prefabs;
        [SerializeField] private GameObject plane;

        public GameObject Generate(int3 size, float intensity)
        {
            var parent = new GameObject("Asteroids");
            var halfSize = size * halfOne;

            for (var i = 0; i < 10; i++)
            {
                var prefab = RandomPrefab();
                var pos = RandomPosition(halfSize);
                var rot = RandomRotation();
                // var scale = RandomScale();

                var inst = Instantiate(prefab, pos, rot, parent.transform);
                // inst.transform.localScale = scale;
                var cBounds = inst.GetComponent<Collider>().bounds;

                var pInst = Instantiate(plane, inst.transform);
                pInst.transform.position = new Vector3(cBounds.center.x, 0.2f, cBounds.center.z);
                pInst.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                pInst.transform.localScale = new Vector3(cBounds.extents.x / 100, 1, cBounds.extents.z / 100);
            }

            return parent;
        }

        private GameObject RandomPrefab()
        {
            return prefabs[Random.Range(0, prefabs.Length)];
        }
        
        private Vector3 RandomPosition(float3 size)
        {
            return new Vector3(
                Random.Range(-size.x, +size.x),
                size.z, Random.Range(-size.y, +size.y));
        }

        public Quaternion RandomRotation()
        {
            return Quaternion.Euler(
                Random.Range(0, 360),
                Random.Range(0, 360), 
                Random.Range(0, 360));
        }

        private Vector3 RandomScale()
        {
            var scaleVal = Random.Range(3, 8);
            return new Vector3(scaleVal, scaleVal, scaleVal);
        }
        
    }
}