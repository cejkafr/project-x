﻿using Battles;
using Game;
using Game.DI;
using TacticalBattles;
using UnityEngine;

namespace TacticalBattle.Generators
{
    [RequireComponent(typeof(ILightGenerator))]
    [RequireComponent(typeof(IAsteroidGenerator))]
    [RequireComponent(typeof(ICloudGenerator))]
    public class LevelGenerator : ManagedMonoBehaviour, ILevelGenerator
    {
        [InjectComponent] private ILightGenerator lightGenerator;
        [InjectComponent] private IAsteroidGenerator asteroidGenerator;
        [InjectComponent] private ICloudGenerator cloudGenerator;

        public void Generate(LevelSettings s)
        {
            Clean();

            CreateBgObjects(s.backgroundObjects);
            
            var lightsGo = lightGenerator.Generate(s.lightSource);
            lightsGo.transform.SetParent(transform);
            
            var asteroidsGo = asteroidGenerator.Generate(s.size, s.asteroidIntensity);
            asteroidsGo.transform.SetParent(transform);

            var nebulasGo = cloudGenerator.Generate(s.size, s.cloudIntensity);
            nebulasGo.transform.SetParent(transform);
        }

        private void Clean()
        {
            foreach (Transform t in transform)
            {
                Destroy(t.gameObject);
            }
        }

        private void CreateBgObjects(LevelSettingsBgObject[] s)
        {
            var parent = new GameObject("BgObjects");
            parent.transform.SetParent(transform);

            for (var i = 0; i < s.Length; i++)
            {
                var bgObject = s[i];
                var inst = Instantiate(bgObject.prefab, bgObject.position,
                    Quaternion.Euler(bgObject.rotation), parent.transform);
                inst.transform.localScale = bgObject.scale;
            }
        }
    }
}