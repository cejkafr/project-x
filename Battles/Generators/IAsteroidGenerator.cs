﻿using Unity.Mathematics;
using UnityEngine;

namespace TacticalBattle.Generators
{
    public interface IAsteroidGenerator
    {
        GameObject Generate(int3 size, float intensity);
    }
}