﻿using Battles;
using TacticalBattles;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

namespace TacticalBattle.Generators
{
    public class LightGenerator : MonoBehaviour, ILightGenerator
    {
        [SerializeField] private float3 rimLight0Offset;
        [SerializeField] private float3 rimLight1Offset;

        public GameObject Generate(LevelSettingsLightSource s)
        {
            var parent = new GameObject("Lights");

            var mainLightGo = CreateLight("MainLight_0", s.rotation, s.color, s.intensity, true);
            mainLightGo.transform.parent = parent.transform;

            var rimLight0Rotation = s.rotation + rimLight0Offset;
            var rimLight0Go = CreateLight("RimLight_0", rimLight0Rotation, Color.blue, s.intensity * 0.4f);
            rimLight0Go.transform.parent = parent.transform;

            var rimLight1Rotation = s.rotation + rimLight1Offset;
            var rimLight1Go = CreateLight("RimLight_1", rimLight1Rotation, Color.red, s.intensity * 0.4f);
            rimLight1Go.transform.parent = parent.transform;

            return parent;
        }

        private GameObject CreateLight(string name, float3 rotation, Color color, float intensity, bool shadows = false)
        {
            var go = new GameObject(name);

            go.transform.rotation = Quaternion.Euler(rotation);
            var l = go.AddComponent<Light>();
            l.type = LightType.Directional;
            l.color = color;
            l.intensity = intensity;

            var lhd = go.AddComponent<HDAdditionalLightData>();
            lhd.type = HDLightType.Directional;
            lhd.color = color;
            lhd.SetIntensity(intensity, LightUnit.Lux);
            lhd.interactsWithSky = false;
            lhd.angularDiameter = 0f;
            lhd.EnableShadows(shadows);

            return go;
        }
    }
}