﻿using Unity.Mathematics;
using UnityEngine;

namespace TacticalBattle.Generators
{
    public interface ICloudGenerator
    {
        GameObject Generate(int3 size, float intensity);
    }
}