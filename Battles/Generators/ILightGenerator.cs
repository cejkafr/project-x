﻿using Battles;
using TacticalBattles;
using UnityEngine;

namespace TacticalBattle.Generators
{
    public interface ILightGenerator
    {
        GameObject Generate(LevelSettingsLightSource s);
    }
}