﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TacticalBattle.Generators
{
    public class CloudGenerator : MonoBehaviour, ICloudGenerator
    {
        private readonly float3 halfOne = new float3(0.5f, 0.5f, 1);

        [SerializeField] private GameObject[] prefabs;

        public GameObject Generate(int3 size, float intensity)
        {
            var parent = new GameObject("Clouds");
            var halfSize = size * halfOne;

            for (var i = 0; i < 5; i++)
            {
                var prefab = RandomPrefab();
                var pos = RandomPosition(halfSize);
                var rot = RandomRotation();
                var scale = RandomScale();

                var inst = Instantiate(prefab, pos, rot, parent.transform);
                inst.transform.localScale = scale;
            }

            return parent;
        }

        private GameObject RandomPrefab()
        {
            return prefabs[Random.Range(0, prefabs.Length)];
        }

        private Vector3 RandomPosition(float3 size)
        {
            return new Vector3(
                Random.Range(-size.x, +size.x),
                size.z, Random.Range(-size.y, +size.y));
        }

        public Quaternion RandomRotation()
        {
            return Quaternion.Euler(0, Random.Range(0, 360), 0);
        }

        private Vector3 RandomScale()
        {
            var scaleVal = Random.Range(3, 8);
            return new Vector3(scaleVal, scaleVal, scaleVal);
        }
    }
}