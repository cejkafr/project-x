﻿using Game.Commons.Combat;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles
{
    public interface IAttackable
    {
        ITeam Team { get; }

        DamageBundle TakeDamage(Vector3 point, IShipWeapon weapon);
    }
}