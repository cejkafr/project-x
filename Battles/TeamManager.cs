﻿using System.Collections.Generic;
using Battles.Ships;
using Commons.Factions;
using TacticalBattles;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles
{
    public class TeamManager : MonoBehaviour
    {
        [SerializeField] private ShipManager shipManager;

        public List<ITeam> Teams { get; } = new List<ITeam>(2);
        public ITeam HumanTeam { get; private set; }

        public ITeam Create(IFaction iFaction)
        {
            var team = new Team(iFaction);

            shipManager.onShipRegistered.AddListener(team.OnShipRegistered);
            shipManager.onShipDestroyed.AddListener(team.OnShipUnregistered);

            Teams.Add(team);

            if (team.IsHuman)
            {
                HumanTeam = team;
            }

            return team;
        }

        public ITeam Find(IFaction faction)
        {
            return Teams.Find(f => f.BaseObject == faction);
        }

        public void Clear()
        {
            foreach (var team in Teams)
            {
                shipManager.onShipRegistered.RemoveListener(team.OnShipRegistered);
                shipManager.onShipDestroyed.RemoveListener(team.OnShipUnregistered);
            }

            HumanTeam = null;
            Teams.Clear();
        }
    }
}