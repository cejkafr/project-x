﻿namespace Battles
{
    public interface IScenario
    {
        bool IsWon { get;  }
        bool IsLost { get; }
        bool IsCompleted { get; }

        void OnInit();
        void OnStart();
        void OnCompleted();
        void Tick();
    }
}