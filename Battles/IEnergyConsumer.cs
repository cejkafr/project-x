﻿using Games.Types;

namespace Battles
{
    public delegate void EnergyConsumerEvent(IEnergyConsumer consumer);
    
    public interface IEnergyConsumer
    {
        event EnergyConsumerEvent EnergyConsumptionChanged;
        
        int EnergyConsumption { get; }

        void OnReactorEnergyDepleted(MaxValue maxValue);
    }
}