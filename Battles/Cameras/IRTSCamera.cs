﻿using UnityEngine;

namespace Battles.Cameras
{
    public interface IRTSCamera
    {
        Camera Camera { get; }
        Vector2 BoardSize { get; set; }

        void Move(Vector2 dir);
        void Rotate(Vector2 delta);
        void Zoom(Vector2 delta);

        void GoTo(Vector2 iPos);
        void GoTo(Vector3 iPos);
    }
}