﻿using Cinemachine;
using Game.Extensions;
using UnityEngine;

namespace Battles.Cameras
{
    public class CinemachineRTSCamera : MonoBehaviour, IRTSCamera
    {
        private static readonly int Name = Shader.PropertyToID("_GridAlpha");
        
        [SerializeField] private Camera realCamera;
        [SerializeField] private CinemachineFreeLook freeLookCam;
        [SerializeField] private LayerMask cinematicLayerMask;

        [Header("Movement & Rotation")] 
        [SerializeField] private float movementSpeed = 10f;
        [SerializeField] private float movementTime = 5f;

        [Header("Grid")]
        [SerializeField] private MeshRenderer gridRenderer;
        [SerializeField] private float maxShowGridDistance = 350;
        [SerializeField] private float minShowGridDistance = 150;
        
        private int standardLayerMask;
        private Vector2 boardSize;
        private Vector2 moveDir;
        private Vector3 cameraDir;
        private Vector3 newPosition;
        private Vector3 forward;
        private Vector3 right;
        private float gridShowRange;
        private float gridAlpha;

        public Camera Camera => realCamera;
        public CinemachineFreeLook FreeLookCam => freeLookCam;
        public Transform CameraTransform { get; private set; }
        public Vector2 BoardSize
        {
            get => boardSize;
            set => boardSize = value * 0.5f;
        }

        private void Awake()
        {
            CameraTransform = realCamera.transform;
            standardLayerMask = realCamera.cullingMask;
        }

        private void Update()
        {
            Panning();
            
            transform.position = Vector3.Lerp(
                transform.position, newPosition,
                Time.unscaledDeltaTime * movementTime);
        }

        private void OnEnable()
        {
            gridShowRange = maxShowGridDistance - minShowGridDistance;
            HandleGridAlpha();
        }

        public void Move(Vector2 dir)
        {
            moveDir = dir;
        }

        public void Rotate(Vector2 delta)
        {
            FreeLookCam.m_XAxis.m_InputAxisValue = delta.x;
            FreeLookCam.m_YAxis.m_InputAxisValue = delta.y;
            
            HandleGridAlpha();
        }

        public void Zoom(Vector2 delta)
        {
            
        }
        
        private void Panning()
        {
            if (Mathf.Abs(moveDir.x) < .1f
                && Mathf.Abs(moveDir.y) < .1f) return;

            if (Mathf.Abs(moveDir.x) > 0.1f)
            {
                cameraDir = CameraTransform.right;
                right = new Vector3(cameraDir.x, 0f, cameraDir.z).normalized;
                newPosition += right * (moveDir.x * movementSpeed);
            }

            if (Mathf.Abs(moveDir.y) > 0.1f)
            {
                cameraDir = CameraTransform.forward;
                forward = new Vector3(cameraDir.x, 0f, cameraDir.z).normalized;
                newPosition += forward * (moveDir.y * movementSpeed);
            }

            if (boardSize != Vector2.zero) MoveOverSizeCorrection();
        }

        private void MoveOverSizeCorrection()
        {
            if (newPosition.x > boardSize.x)
            {
                newPosition = new Vector3(boardSize.x, newPosition.y, newPosition.z);
            }
            else if (newPosition.x < -boardSize.x)
            {
                newPosition = new Vector3(-boardSize.x, newPosition.y, newPosition.z);
            }

            if (newPosition.z > boardSize.y)
            {
                newPosition = new Vector3(newPosition.x, newPosition.y, boardSize.y);
            }
            else if (newPosition.z < -boardSize.y)
            {
                newPosition = new Vector3(newPosition.x, newPosition.y, -boardSize.y);
            }
        }

        public void GoTo(Vector2 iPos)
        {
            newPosition = transform.position.With(x: iPos.x, z: iPos.y);
        }
        
        public void GoTo(Vector3 iPos)
        {
            newPosition = transform.position.With(x: iPos.x, z: iPos.z);
        }
        
        private void HandleGridAlpha()
        {
            gridAlpha = 1f;
            if (CameraTransform.position.y < minShowGridDistance)
            {
                gridAlpha = 0f;
            } else if (CameraTransform.position.y < maxShowGridDistance)
            {
                gridAlpha = (CameraTransform.position.y - minShowGridDistance) / gridShowRange;
            }

            gridRenderer.material.SetFloat(Name, gridAlpha);
        }
    }
}