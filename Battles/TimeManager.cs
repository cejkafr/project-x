using System;
using Game.Commons;
using UnityEngine;
using UnityEngine.Events;

namespace Battles
{
    public enum TimeScale
    {
        Paused,
        Slower,
        Normal,
        Faster,
        Fastest,
    }

    public class TimeManagerEvent : UnityEvent
    {
        
    }
    
    public class TimeManager : Singleton<TimeManager>
    {
        private const float PausedScale = 0f;
        private const float SlowerScale = 0.5f;
        private const float NormalScale = 1f;
        private const float FasterScale = 1.5f;
        private const float FastestScale = 2f;
        
        private static float prevTimeScale = 1f;
        
        public static readonly TimeManagerEvent OnSpeedChange = new TimeManagerEvent();
        
        public static bool IsPaused => Time.timeScale == PausedScale;

//        public static TimeScale TimeScale
//        {
//            get
//            {
//                
//            }
//        }

        public static TimeScale CurrentTimeScale
        {
            get
            {
                switch (Time.timeScale)
                {
                    case PausedScale: return TimeScale.Paused;
                    case SlowerScale: return TimeScale.Slower;
                    case NormalScale: return TimeScale.Normal;
                    case FasterScale: return TimeScale.Faster;
                    case FastestScale: return TimeScale.Fastest;
                    default: throw new ArgumentException("Unknown time scale.");
                }
            }
        }

        public static float TimeScaledFloat(float val)
        {
            return val * Time.timeScale;
        }

        public static void Pause()
        {
            prevTimeScale = Time.timeScale;
            Time.timeScale = PausedScale;
            
            OnSpeedChange.Invoke();
        }

        public static void Resume()
        {
            Time.timeScale = prevTimeScale;
            
            OnSpeedChange.Invoke();
        }

        public static void TogglePause()
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        public static void SlowerSpeed()
        {
            Time.timeScale = SlowerScale;
            
            OnSpeedChange.Invoke();
        }
        
        public static void NormalSpeed()
        {
            Time.timeScale = NormalScale;
            
            OnSpeedChange.Invoke();
        }

        public static void FasterSpeed()
        {
            Time.timeScale = FasterScale;
            
            OnSpeedChange.Invoke();
        }

        public static void FastestSpeed()
        {
            Time.timeScale = FastestScale;
            
            OnSpeedChange.Invoke();
        }
    }
}