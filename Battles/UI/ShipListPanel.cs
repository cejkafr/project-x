﻿using System.Collections.Generic;
using System.Linq;
using Battles.Controllers;
using Battles.Ships;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace TacticalBattles.UI
{
    public class ShipListPanel : MonoBehaviour, IPanel
    {
        private readonly List<ShipListButton> buttons = new List<ShipListButton>(3);

        private PlayerShipController playerShipController;

        public GameObject buttonPrefab;

        public bool IsOpen => gameObject.activeInHierarchy;

        public void Init(ShipManager shipManager, PlayerShipController iPlayerShipController)
        {
            playerShipController = iPlayerShipController;

            shipManager.onShipRegistered.AddListener(OnShipRegistered);
            shipManager.onShipDestroyed.AddListener(OnShipDestroyed);
            
            playerShipController.onShipSelected.AddListener(OnShipActivated);
            playerShipController.onShipDeselected.AddListener(OnShipDeactivated);
        }
        
        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void OnShipRegistered(IShip shipController)
        {
            if (!shipController.Team.IsHuman) return;
            
            var inst = Instantiate(buttonPrefab, transform);
            var btn = inst.GetComponent<ShipListButton>();
            btn.OnButtonClicked.AddListener(OnButtonClicked);
                
            btn.SetShip(shipController);
                
            buttons.Add(btn);
        }

        public void OnShipDestroyed(IShip iShip)
        {
            if (!iShip.Team.IsHuman) return;

            var btn = FindButtonForShip(iShip);
            if (btn == null) return;

            buttons.Remove(btn);
            Destroy(btn.gameObject);
        }
        
        public void OnShipActivated(IShip iShip)
        {
            if (!iShip.Team.IsHuman) return;
            FindButtonForShip(iShip).Activate();
        }
        
        public void OnShipDeactivated(IShip iShip)
        {
            if (!iShip.Team.IsHuman) return;
            FindButtonForShip(iShip)?.Deactivate();
        }

        public void OnButtonClicked(IShip iShip)
        {
            playerShipController.Select(iShip);
        }

        private ShipListButton FindButtonForShip(IShip shipController)
        {
            return buttons.FirstOrDefault(button => button.Ship.Equals(shipController));
        }
    }
}
