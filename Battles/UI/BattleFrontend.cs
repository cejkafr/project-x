﻿using Battles.Controllers;
using Battles.Ships;
using TacticalBattles.UI;
using UnityEngine;

namespace Battles.UI
{
    public class BattleFrontend : MonoBehaviour
    {
        private CanvasGroup canvasGroup;
        private ShipPanel[] shipPanels;
        
        [SerializeField] private TimePanel timePanel;
        [SerializeField] private ShipListPanel shipListPanel;
        [SerializeField] private ShipManager shipManager; 
        [SerializeField] private PlayerShipController playerShipController;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            
            shipListPanel.Init(shipManager, playerShipController);

            shipPanels = FindObjectsOfType<ShipPanel>();
            foreach (var panel in shipPanels)
            {
                panel.Init(playerShipController);
                playerShipController.onShipSelected.AddListener(panel.OnShipActivated);
                playerShipController.onShipDeselected.AddListener(panel.OnShipDeactivated);
                panel.Close();
            }
        }

        public void Hide(bool value)
        {
            if (value)
            {
                canvasGroup.alpha = 0;
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }
            else
            {
                canvasGroup.alpha = 1;
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }
        }
        
        public void OpenEndDialog()
        {
            print("End");
        }
    }
}
