﻿using Battles;
using UnityEngine;
using UnityEngine.UI;

namespace TacticalBattles.UI
{
    public class TimePanel : MonoBehaviour, IPanel
    {
        public Toggle[] toggles;

        public bool IsOpen => gameObject.activeInHierarchy;
        
        private void Start()
        {
            TimeManager.OnSpeedChange.AddListener(OnSpeedChange);
            
            OnSpeedChange();
        }

        public void OnSpeedChange()
        {
            var toggle = toggles[(int) TimeManager.CurrentTimeScale];
            if (toggle.isOn) return;
            
            toggle.SetIsOnWithoutNotify(true);
        }
        
        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void OnPause()
        {
            TimeManager.Pause();
        }

        public void OnSlower()
        {
            TimeManager.SlowerSpeed();
        }

        public void OnNormal()
        {
            TimeManager.NormalSpeed();
        }

        public void OnFaster()
        {
            TimeManager.FasterSpeed();
        }

        public void OnFastest()
        {
            TimeManager.FastestSpeed();
        }
    }
}
