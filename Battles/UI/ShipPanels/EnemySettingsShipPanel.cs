﻿using System;
using Battles;
using Battles.Controllers;
using UnityEngine.UI;

namespace TacticalBattles.UI.ShipPanels
{
    public class EnemySettingsShipPanel : ShipPanel
    {
        [Serializable]
        public struct Bindings
        {
            public Toggle[] priorityToggles;
        }

        private PlayerShipController playerShip;

        public Bindings bindings;

        public void OnPriorityChanged(int value)
        {
            if (Ship == null) return;
            
            playerShip.Team.SetTargetPriority(Ship, (ShipTargetPriority) value);
        }

        public override void Init(PlayerShipController iPlayerShipController)
        {
            playerShip = iPlayerShipController;
            OpenFor = OpenMode.Enemy;
        }
        
        protected override void OnOpen()
        {
            var settings = playerShip.Team.FindEnemyByShip(Ship).Settings;

            bindings.priorityToggles[(int) settings.Priority].SetIsOnWithoutNotify(true);
        }
    }
}
