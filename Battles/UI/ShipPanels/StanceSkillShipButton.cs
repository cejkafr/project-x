﻿using Commons.DataObjects.Ships;
using Commons.Ships;
using Commons.UI;
using DataObjects.Ships;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.UI.ShipPanels
{
    public class StanceSkillShipButton : MonoBehaviour, ISkillShipButton<StanceSkillObject>, IPointerEnterHandler, IPointerExitHandler
    {
        public event SkillShipButtonEvent<StanceSkillObject> SkillShipButtonEvent;

        private StanceSkillObject skill;
        private Toggle toggle;

        [SerializeField] private Image icon;

        public bool IsOn => toggle.isOn;

        private void Awake()
        {
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(OnValueChanged);
        }

        public void Show(IShip iShip, StanceSkillObject iSkill)
        {
            skill = iSkill;

            icon.sprite = iSkill.icon;
            toggle.SetIsOnWithoutNotify(iShip.StanceSkill == iSkill); 
            
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnValueChanged(bool value)
        {
            if (!value)
            {
                return;
            }
            
            SkillShipButtonEvent?.Invoke(skill);
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            TextTooltip.Instance.Show(skill.title, skill.description);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            TextTooltip.Instance.Hide();
        }
    }
}