﻿using System;
using Battles.Controllers;
using UnityEngine.UI;

namespace TacticalBattles.UI.ShipPanels
{
    public class SettingsShipPanel : ShipPanel
    {
        [Serializable]
        public struct Bindings
        {
            public Toggle autoEngageToggle;
        }

        private PlayerShipController playerShipController;
        
        public Bindings bindings;

        public void OnAutoEngageChanged()
        {
            if (Ship == null) return;

            playerShipController.AutoEngage = bindings.autoEngageToggle.isOn;
        }

        public void OnStop()
        {
            if (Ship == null) return;
            
            playerShipController.Stop();
        }

        protected override void OnOpen()
        {
            bindings.autoEngageToggle.SetIsOnWithoutNotify(playerShipController.AutoEngage);
        }

        public override void Init(PlayerShipController iPlayerShipController)
        {
            playerShipController = iPlayerShipController;
        }
    }
}