﻿using System;
using Battles;
using Battles.Controllers;
using Battles.GridBoard.UI;
using Battles.Ships;
using Games.Types;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.UI;

namespace TacticalBattles.UI.ShipPanels
{
    public class EnergyShipPanel : ShipPanel
    {
        private enum ToggleFunction
        {
            Func1,
            Func2,
            Disable,
        }

        [Serializable]
        public struct Bindings
        {
            public Slider energySlider;
            public Toggle[] engineToggles;
            public Toggle[] weaponToggles;
            public Toggle[] shieldToggles;
        }

        private IPlayerInputProcessor inputProcessor;

        public Bindings bindings;

        public void OnEnergyPointsChanged(MaxValue maxValue)
        {
            bindings.energySlider.maxValue = maxValue.Max;
            bindings.energySlider.value = maxValue.Value;
        }

        public void OnShieldModeChanged(IShipShield shield)
        {
            RefreshButtons();
        }

        public void OnEngineModeChanged(IShipEngine engine)
        {
            RefreshButtons();
        }
        
        public void OnWeaponSystemModeChanged(IShipWeaponSystem engine)
        {
            RefreshButtons();
        }

        public void OnEngineToggleChanged(int value)
        {
            if (Ship == null) return;

            var e = bindings.engineToggles[value].isOn;

            switch ((ToggleFunction) value)
            {
                case ToggleFunction.Func1:
                    Ship.Engine.Mode = e ? ShipEngineMode.Overclock : ShipEngineMode.Standard;
                    break;
                case ToggleFunction.Func2:
                    inputProcessor.AngleMode = e;
                    break;
                case ToggleFunction.Disable:
                    Ship.Engine.Mode = e ? ShipEngineMode.Disabled : ShipEngineMode.Standard;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        public void OnWeaponToggleChanged(int value)
        {
            if (Ship == null) return;

            var e = bindings.weaponToggles[value].isOn;

            switch ((ToggleFunction) value)
            {
                case ToggleFunction.Func1:
                    Ship.WeaponSystem.Mode = e ? ShipWeaponMode.Overclock : ShipWeaponMode.Standard;
                    break;
                case ToggleFunction.Func2:
                    break;
                case ToggleFunction.Disable:
                    Ship.WeaponSystem.Mode = e ? ShipWeaponMode.Disabled : ShipWeaponMode.Standard;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        public void OnShieldToggleChanged(int value)
        {
            if (Ship == null) return;

            var e = bindings.shieldToggles[value].isOn;

            switch ((ToggleFunction) value)
            {
                case ToggleFunction.Func1:
                    Ship.Shield.Mode = e ? ShipShieldMode.Overclock : ShipShieldMode.Standard;
                    break;
                case ToggleFunction.Func2:
                    break;
                case ToggleFunction.Disable:
                    Ship.Shield.Mode = e ? ShipShieldMode.Disabled : ShipShieldMode.Standard;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        public override void Init(PlayerShipController iPlayerShipController)
        {
            // TODO refactor, use DI
            inputProcessor = FindObjectOfType<PlayerInputProcessor>();
        }

        protected override void OnOpen()
        {
            Ship.Reactor.Energy.ChangeEvent += OnEnergyPointsChanged;
            Ship.Shield.ModeChangedEvent += OnShieldModeChanged;
            Ship.Engine.ModeChangedEvent += OnEngineModeChanged;
            Ship.WeaponSystem.ModeChanged += OnWeaponSystemModeChanged;
            
            OnEnergyPointsChanged(Ship.Reactor.Energy);

            RefreshButtons();
        }

        protected override void OnClose()
        {
            if (Ship == null) return;

            Ship.Reactor.Energy.ChangeEvent -= OnEnergyPointsChanged;
            Ship.Shield.ModeChangedEvent -= OnShieldModeChanged;
            Ship.Engine.ModeChangedEvent -= OnEngineModeChanged;
            Ship.WeaponSystem.ModeChanged -= OnWeaponSystemModeChanged;
        }

        private void RefreshButtons()
        {
            bindings.engineToggles[(int) ToggleFunction.Func1]
                .SetIsOnWithoutNotify(ShipEngineMode.Overclock == Ship.Engine.Mode);
            bindings.engineToggles[(int) ToggleFunction.Func2]
                .SetIsOnWithoutNotify(ShipEngineMode.HighVelocityTurn == Ship.Engine.Mode);
            bindings.engineToggles[(int) ToggleFunction.Disable]
                .SetIsOnWithoutNotify(ShipEngineMode.Disabled == Ship.Engine.Mode);

            bindings.weaponToggles[(int) ToggleFunction.Func1]
                .SetIsOnWithoutNotify(ShipWeaponMode.Overclock == Ship.WeaponSystem.Mode);
            bindings.weaponToggles[(int) ToggleFunction.Disable]
                .SetIsOnWithoutNotify(ShipWeaponMode.Disabled == Ship.WeaponSystem.Mode);

            bindings.shieldToggles[(int) ToggleFunction.Func1]
                .SetIsOnWithoutNotify(ShipShieldMode.Overclock == Ship.Shield.Mode);
            bindings.shieldToggles[(int) ToggleFunction.Disable]
                .SetIsOnWithoutNotify(ShipShieldMode.Disabled == Ship.Shield.Mode);
            
            // TODO - ugly hack, move elsewhere
            if (ShipEngineMode.HighVelocityTurn != Ship.Engine.Mode)
            {
                var worldUi = FindObjectOfType<GridBoardUi>();
                worldUi.GetShipMarker(Ship).ShowAngle(false, Vector3.zero);
            }
        }
    }
}