﻿using Battles.Controllers;
using Commons.DataObjects.Ships;
using Commons.Ships;
using DataObjects.Ships;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.UI.ShipPanels
{
    public class StanceSkillShipPanel : SkillShipPanel<StanceSkillShipButton, StanceSkillObject>
    {
        private PlayerShipController playerShipController;
        
        public override void Init(PlayerShipController iPlayerShipController)
        {
            playerShipController = iPlayerShipController;
        }
        
        protected override void OnOpen()
        {
            Draw();
            
            Ship.StanceChanged += OnStanceChanged;
        }

        protected override void OnClose()
        {
            base.OnClose();

            if (Ship == null)
            {
                return;
            }

            Ship.StanceChanged -= OnStanceChanged;
        }

        public void Draw()
        {
            foreach (var stance in Ship.Stances)
            {
                var button = Next();
                button.Show(Ship, stance);
            }
        }

        protected override void OnButtonCreated(StanceSkillShipButton button)
        {
            button.SkillShipButtonEvent += OnValueChange;
        }

        private void OnStanceChanged(IShip ship)
        {
            Reset();
            Draw();
        }
        
        private void OnValueChange(StanceSkillObject stanceSkill)
        {
            playerShipController.StanceSkill = stanceSkill;
        }
    }
}