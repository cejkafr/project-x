﻿using System;
using Commons.Ships.Components;
using Commons.UI;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Battles.UI.ShipPanels
{
    public class DetailShipPanelSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private IShipWeapon controller;
        private IComponent component;

        [Serializable]
        public struct Bindings
        {
            public Image frameImage;
            public Image iconImage;
        }

        public Bindings bindings;

        public void OnPointerEnter(PointerEventData eventData)
        {
            controller?.ShowFoV(true);
            ComponentTooltip.Instance.Show(component);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            controller?.ShowFoV(false);
            ComponentTooltip.Instance.Hide();
        }

        public void Show(IComponent iComponent)
        {
            component = iComponent;
            controller = null;

            Show();
        }
        
        public void Show(IShipWeapon iController)
        {
            component = iController.Component;
            controller = iController;

            Show();
        }

        private void Show()
        {
            switch (component)
            {
                case EngineComponent c:
                    bindings.frameImage.color = Color.gray;
                    break;
                case ShieldComponent c:
                    bindings.frameImage.color = Color.blue;
                    break;
                case WeaponComponent c:
                    bindings.frameImage.color = Color.red;
                    break;
                case ReactorComponent c:
                    bindings.frameImage.color = Color.green;
                    break;
                default:
                    bindings.frameImage.color = Color.white;
                    break;
            }
            
            bindings.iconImage.sprite = component.Icon;
            bindings.iconImage.color = component.Icon != null ? Color.white : Color.clear;
            
            gameObject.SetActive(true); 
        }
    }
}
