﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Battles.Cameras;
using Battles.Controllers;
using Games.Types;
using TacticalBattles.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Battles.UI.ShipPanels
{
    public class DetailShipPanel : ShipPanel
    {
        [Serializable]
        public struct Bindings
        {
            public Image expandInactiveImage;
            public Image expandActiveImage;
            public TextMeshProUGUI nameText;
            public TextMeshProUGUI classNameText;
            public Slider hullPointsSlider;
            public Text hullPointsMaxValueText;
            public Text hullPointsValueText;
            public Slider shieldPointsSlider;
            public Text shieldPointsMaxValueText;
            public Text shieldPointsValueText;
            public GameObject expandedPanel;
            public Image portraitImage;
            public TextMeshProUGUI speedText;
            public TextMeshProUGUI rotationSpeedText;
            public TextMeshProUGUI scannerRangeText;
            public TextMeshProUGUI armorFrontText;
            public TextMeshProUGUI armorSideText;
            public TextMeshProUGUI armorRearText;
            public Transform componentGrid;
        }

        private RectTransform rectTransform;
        private readonly List<DetailShipPanelSlot> slots = new List<DetailShipPanelSlot>(10);

        public Bindings bindings;
        public float minimalHeight;
        public float expandedHeight;
        public GameObject componentSlotPrefab;

        private bool IsExpanded => rectTransform.sizeDelta.y > minimalHeight;

        public void OnExpandToggle()
        {
            if (IsExpanded)
            {
                Minimalize();
            }
            else
            {
                Expand();
            }
        }

        public void OnGoToClicked()
        {
            // TODO refactor
            FindObjectOfType<CinemachineRTSCamera>().GoTo(Ship.Position2D);
        }

        public void OnPointsChanged(MaxValue maxValue)
        {
            UpdateSliders();
        }

        protected override void OnOpen()
        {
            Ship.HullPoints.ChangeEvent += OnPointsChanged;
            Ship.Shield.Points.ChangeEvent += OnPointsChanged;

            bindings.nameText.text = Ship.BaseObject.Name;
            bindings.classNameText.text = Ship.BaseObject.ClassName;
            
            Minimalize();
            
            UpdateSliders();
            UpdatePortrait();
            UpdateStats();
            UpdateComponents();
        }

        protected override void OnClose()
        {
            if (Ship == null) return;
            Ship.HullPoints.ChangeEvent -= OnPointsChanged;
            Ship.Shield.Points.ChangeEvent -= OnPointsChanged;
        }

        public override void Init(PlayerShipController iPlayerShipController)
        {
            rectTransform = GetComponent<RectTransform>();
            OpenFor = OpenMode.All;
        }

        private void Minimalize()
        {
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, minimalHeight);
            bindings.expandInactiveImage.gameObject.SetActive(true);
            bindings.expandActiveImage.gameObject.SetActive(false);
            bindings.expandedPanel.SetActive(false);
        }

        private void Expand()
        {
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, expandedHeight);
            bindings.expandInactiveImage.gameObject.SetActive(false);
            bindings.expandActiveImage.gameObject.SetActive(true);
            bindings.expandedPanel.SetActive(true);
        }

        private void UpdateSliders()
        {
            bindings.hullPointsSlider.maxValue = Ship.HullPoints.Max;
            bindings.hullPointsMaxValueText.text =
                Ship.HullPoints.Max.ToString(CultureInfo.InvariantCulture);
            bindings.hullPointsSlider.value = Ship.HullPoints.Value;
            bindings.hullPointsValueText.text = Ship.HullPoints.Value.ToString(CultureInfo.InvariantCulture);

            bindings.shieldPointsSlider.maxValue = Ship.Shield.Points.Max;
            bindings.shieldPointsMaxValueText.text = Ship.Shield.Points.Max.ToString(CultureInfo.InvariantCulture);
            bindings.shieldPointsSlider.value = Ship.Shield.Points.Value;
            bindings.shieldPointsValueText.text = Ship.Shield.Points.Value.ToString(CultureInfo.InvariantCulture);
        }

        public void UpdatePortrait()
        {
            bindings.portraitImage.sprite = Ship.BaseObject.IconSide;
        }

        public void UpdateStats()
        {
            bindings.speedText.text = Ship.Engine.SpeedMax.ToString(CultureInfo.CurrentCulture);
            bindings.rotationSpeedText.text = Mathf.RoundToInt(Ship.Engine.RotationSpeedRad * Mathf.Rad2Deg).ToString(CultureInfo.CurrentCulture);
            bindings.scannerRangeText.text = Ship.Sensors.Range.ToString(CultureInfo.CurrentCulture);
            
            bindings.armorFrontText.text = Ship.BaseObject.FrontArmour.Value.ToString(CultureInfo.CurrentCulture);
            bindings.armorSideText.text = Ship.BaseObject.SideArmour.Value.ToString(CultureInfo.CurrentCulture);
            bindings.armorRearText.text = Ship.BaseObject.RearArmour.Value.ToString(CultureInfo.CurrentCulture);
        }
        
        private void HideComponents()
        {
            foreach (var slot in slots)
            {
                slot.gameObject.SetActive(false);
            }
        }
        
        private void UpdateComponents()
        {
            HideComponents();

            var i = 0;
            foreach (var c in Ship.BaseObject.GetComponents())
            {
                if (c == null) continue;

                if (slots.Count <= i)
                {
                    var inst = Instantiate(componentSlotPrefab, bindings.componentGrid);
                    slots.Add(inst.GetComponent<DetailShipPanelSlot>());
                }

                slots[i].Show(c);
                
                i++;
            }
            
            foreach (var c in Ship.WeaponSystem.Weapons)
            {
                if (c == null) continue;

                if (slots.Count <= i)
                {
                    var inst = Instantiate(componentSlotPrefab, bindings.componentGrid);
                    slots.Add(inst.GetComponent<DetailShipPanelSlot>());
                }

                slots[i].Show(c);
                
                i++;
            }
        }
    }
}