﻿using System.Collections.Generic;
using Commons.DataObjects.Ships;
using DataObjects.Ships;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.UI.ShipPanels
{
    public delegate void SkillShipButtonEvent<in T>(T skillObject);
    
    public interface ISkillShipButton<T> where T : SkillObject
    {
        event SkillShipButtonEvent<T> SkillShipButtonEvent;
        
        void Show(IShip iShip, T iSkill);
        void Hide();
    }

    public abstract class SkillShipPanel<T, TR> : ShipPanel where T : ISkillShipButton<TR> where TR : SkillObject
    {
        private int index;

        [SerializeField] private Transform gridBindings;
        [SerializeField] private GameObject buttonPrefab;

        public List<T> Buttons { get; } = new List<T>();
        
        protected T Next()
        {
            T button;

            if (index >= Buttons.Count)
            {
                var inst = Instantiate(buttonPrefab, gridBindings);
                button = inst.GetComponent<T>();
                button.Hide();
                Buttons.Add(button);

                OnButtonCreated(button);
            }

            button = Buttons[index];
            index++;
            
            return button;
        }

        protected virtual void OnButtonCreated(T button)
        {
            
        }
        
        protected void Reset()
        {
            foreach (var b in Buttons)
            {
                b.Hide();
            }

            index = 0;
        }
        
        protected override void OnClose()
        {
            Reset();
        }
    }
}