﻿using Battles.Controllers;
using DataObjects.Ships;

namespace TacticalBattles.UI.ShipPanels
{
    public class WeaponSkillShipPanel : SkillShipPanel<WeaponSkillShipButton, WeaponSkillObject>
    {
        protected override void OnOpen()
        {
            Draw();
        }

        public override void Init(PlayerShipController iPlayerShipController)
        {
        }

        public void Draw()
        {
            foreach (var weapon in Ship.WeaponSystem.Weapons)
            {
                if (weapon == null || !weapon.Component.HasSkills)
                {
                    continue;
                }

                var button = Next();
                button.Show(Ship, weapon.Component.Skills[0]);
            }
        }
    }
}