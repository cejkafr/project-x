﻿using Commons.UI;
using DataObjects.Ships;
using TacticalBattle;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.UI.ShipPanels
{
    public class WeaponSkillShipButton : MonoBehaviour, ISkillShipButton<WeaponSkillObject>, IPointerEnterHandler, IPointerExitHandler
    {
        public event SkillShipButtonEvent<WeaponSkillObject> SkillShipButtonEvent;

        private IShip ship;
        private WeaponSkillObject skill;
        private IShipWeapon shipWeapon;
        private Button button;

        [SerializeField] private Image icon;
        [SerializeField] private Slider slider;

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void OnGUI()
        {
            UpdateProgressBar();
        }

        public void Show(IShip iShip, WeaponSkillObject iSkill)
        {
            skill = iSkill;
            ship = iShip;
            shipWeapon = ship.WeaponSystem.GetWeaponAssociatedWithSkill(iSkill);

            // text.text = skill.title;

            UpdateProgressBar();

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnClick()
        {
            var weapon = ship.WeaponSystem.GetWeaponAssociatedWithSkill(skill);
            weapon.Shoot();

            SkillShipButtonEvent?.Invoke(skill);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            ComponentTooltip.Instance.Show(shipWeapon.Component);
            
            shipWeapon.ShowFoV(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            shipWeapon.ShowFoV(false);
            
            ComponentTooltip.Instance.Hide();
        }

        private void UpdateProgressBar()
        {
            if (shipWeapon.IsOnCooldown)
            {
//                m_Button.interactable = false;
                slider.gameObject.SetActive(true);
                slider.maxValue = shipWeapon.Component.AttackDelaySec;
                slider.value = shipWeapon.RemainingCooldownSec;
            }
            else
            {
//                m_Button.interactable = true;
                slider.gameObject.SetActive(false);
            }
        }
    }
}