﻿using Battles.Controllers;
using TacticalBattles.Ships;
using UnityEngine;

namespace TacticalBattles.UI
{
    public interface IPanel
    {
        bool IsOpen { get; }

        void Open();
        void Close();
    }
    
    public interface IShipPanel : IPanel
    {
        void Open(IShip iShip);
        void OnShipActivated(IShip iShip);
        void OnShipDeactivated(IShip iShip);
    }

    public abstract class ShipPanel : MonoBehaviour, IShipPanel
    {
        protected enum OpenMode
        {
            Player, Enemy, All,
        }
        
        public bool IsOpen => gameObject.activeInHierarchy;
        protected IShip Ship { get; private set; }
        protected OpenMode OpenFor { get; set; } = OpenMode.Player;

        public virtual void OnShipActivated(IShip iShip)
        {
            var isHuman = iShip.Team.IsHuman;
            if (OpenMode.Player == OpenFor && !isHuman || 
                OpenMode.Enemy == OpenFor && isHuman) return;

            Open(iShip);
        }

        public virtual void OnShipDeactivated(IShip iShip)
        {
            var isHuman = iShip.Team.IsHuman;
            if (OpenMode.Player == OpenFor && !isHuman || 
                OpenMode.Enemy == OpenFor && isHuman) return;

            Close();
        }

        public virtual void Init(PlayerShipController iPlayerShipController)
        {

        }

        public virtual void Open()
        {
            if (IsOpen) return;

            gameObject.SetActive(true);
            
            OnOpen();
        }

        public virtual void Open(IShip iShip)
        {
            if (iShip == Ship) return;
            
            Ship = iShip;

            Open();
        }

        public virtual void Close()
        {
            OnClose();
            
            Ship = null;
            gameObject.SetActive(false);
        }
        
        protected virtual void OnOpen()
        {
            
        }
        
        protected virtual void OnClose()
        {
            
        }
    }
}
