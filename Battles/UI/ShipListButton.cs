﻿using Battles.Cameras;
using TacticalBattle;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TacticalBattles.UI
{
    public class ShipListButtonEvent : UnityEvent<IShip>
    {
        
    }
    
    public class ShipListButton : MonoBehaviour//, IPointerEnterHandler, IPointerExitHandler
    {
        public readonly ShipListButtonEvent OnButtonClicked = new ShipListButtonEvent();

        private float lastClick;

        public Slider hpSlider;
        public Slider spSlider;
        public Image shipSprite;
        public Image highlight;

        public IShip Ship { get; private set; }

        private void OnGUI()
        {
            UpdateSliders();
        }

//        public void OnPointerEnter(PointerEventData eventData)
//        {
//            Tooltip.Instance.Show(ShipController.Ship.Name, ShipController.Ship.ClassName);
//        }
//
//        public void OnPointerExit(PointerEventData eventData)
//        {
//            Tooltip.Instance.Hide();
//        }

        public void SetShip(IShip shipController)
        {
            Ship = shipController;
            shipSprite.sprite = shipController.BaseObject.IconSide;
        }

        public void UpdateSliders()
        {
            hpSlider.maxValue = Ship.HullPoints.Max;
            hpSlider.value = Ship.HullPoints.Value;
            spSlider.maxValue = Ship.Shield.Points.Max;
            spSlider.value = Ship.Shield.Points.Value;
        }
        
        public void Activate()
        {
            highlight.gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            highlight.gameObject.SetActive(false);
        }

        public void OnButtonClick()
        {
            if (Time.time - lastClick < 0.5f)
            {
                // TODO Refactor
                FindObjectOfType<CinemachineRTSCamera>().GoTo(Ship.Position2D);
            }
            
            OnButtonClicked.Invoke(Ship);

            lastClick = Time.time;
        }
    }
}
