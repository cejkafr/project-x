﻿namespace Battles
{
    public interface IEnemyController
    {
        void Tick();
    }
}