﻿using UnityEngine;

namespace Battles.GridBoard.UI
{
    public class ShipAngleMarker : MonoBehaviour
    {
        public float angle;

        private void OnGUI()
        {
            transform.rotation = Quaternion.Euler(90f, 0f, angle);
        }
    }
}
