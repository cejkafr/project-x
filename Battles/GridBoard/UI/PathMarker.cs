﻿using UnityEngine;
using UnityEngine.UI;

namespace Battles.GridBoard.UI
{
    public class PathMarker : MonoBehaviour
    {
        public Image markerSprite;
        public LineRenderer lineRenderer;

        public Vector3 Position => transform.position;
    
        public void SetColor(Color color)
        {
            markerSprite.color = color;
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;
        }
    
        public void LineTo(Vector3 position)
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, position);
        }
    }
}
