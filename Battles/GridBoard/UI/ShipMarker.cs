﻿using System;
using Battles.Controllers;
using Game.Extensions;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Battles.GridBoard.UI
{
    public class ShipMarker : MonoBehaviour, IPointerClickHandler
    {
        [Serializable]
        public struct Bindings
        {
            public GameObject maxWeaponRangeRenderer;
            public GameObject scannerRangeRenderer;
            public Slider hullSlider;
            public Slider shieldSlider;
            public GameObject angleVisualisation;
            public ShipAngleMarker angleMarker;
        }
        
        private PlayerShipController playerShipController;
        private Image image;
        private LineRenderer lineRenderer;

        [SerializeField] private Bindings bindings;

        public Color colorActive;
        public Color colorInactive;

        public Vector3 Position => transform.position;
        public IShip Ship { get; private set; }

        private void Awake()
        {
            image = GetComponent<Image>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void Start()
        {
            // TODO refactor, nope
            playerShipController = FindObjectOfType<PlayerShipController>();
        }

        public void Setup(IShip ship)
        {
            Ship = ship;

            SetColor(ship.Team.Color + colorInactive);
            SetMaxWeaponRange(ship.WeaponSystem.Range.Max);
            SetScannerRange(ship.Sensors.Range);

            if (!ship.Team.IsHuman)
            {
                OnShipDetectionLevelChange(ship);
                ship.DetectionLevelChanged += OnShipDetectionLevelChange;
            }
        }

        private void SetColor(Color color)
        {
            image.color = color;
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;
        }

        public void LineTo(Vector3 position)
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, position);
        }

        private void SetMaxWeaponRange(float value)
        {
            bindings.maxWeaponRangeRenderer.transform.localScale
                = new Vector3(value / 5f, 1f, value / 5f);
        }

        private void SetScannerRange(float value)
        {
            bindings.scannerRangeRenderer.transform.localScale
                = new Vector3(value / 5f, 1f, value / 5f);
        }

        public void SetActive(bool value)
        {
            SetColor(Ship.Team.Color + (value ? colorActive : colorInactive));
            bindings.maxWeaponRangeRenderer.SetActive(value);
            bindings.scannerRangeRenderer.SetActive(value);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            playerShipController.Select(Ship);
        }
        
        public void OnShipDetectionLevelChange(IShip ship)
        {
            gameObject.SetActive(DetectionLevel.Full == ship.DetectionLevel);
        }

        public void UpdateSliders(IShip iShip)
        {
            bindings.hullSlider.maxValue = iShip.HullPoints.Max;
            bindings.hullSlider.value = iShip.HullPoints.Value;
            bindings.shieldSlider.maxValue = iShip.Shield.Points.Max;
            bindings.shieldSlider.value = iShip.Shield.Points.Value;
        }

        public void ShowAngle(bool value, Vector3 cursorPos)
        {
            var dir = (cursorPos.Flatten() - transform.position).normalized;
            var angle = Vector2.SignedAngle(new Vector2(dir.x, dir.z), Vector2.up) * -1;
            bindings.angleMarker.angle = angle;
            bindings.angleVisualisation.SetActive(value);
        }
    }
}