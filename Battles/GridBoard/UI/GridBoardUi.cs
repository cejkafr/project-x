﻿using System.Collections.Generic;
using Battles.Controllers;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles.GridBoard.UI
{
    // TODO refactor, very old
    public class GridBoardUi : MonoBehaviour
    {
        private readonly Dictionary<IShip, ShipMarker> shipMarkers
            = new Dictionary<IShip, ShipMarker>();
        private readonly Dictionary<IShip, List<PathMarker>> pathMarkers
            = new Dictionary<IShip, List<PathMarker>>();
        private readonly Dictionary<IShip, PathMarker> targetMarkers
            = new Dictionary<IShip, PathMarker>();
        
        [SerializeField] private GameObject pathMarkerPrefab;
        [SerializeField] private GameObject shipMarkerPrefab;
        [SerializeField] private PlayerShipController playerShipController;

        public void Init(Vector2 size)
        {
            GetComponent<RectTransform>().sizeDelta = size;
        }

        public void OnShipRegistered(IShip ship)
        {
            pathMarkers.Add(ship, new List<PathMarker>(1));
            targetMarkers.Add(ship, null);

            CreateShipMarker(ship);
        }

        public void OnShipUnregistered(IShip ship)
        {
            Destroy(shipMarkers[ship].gameObject);
            shipMarkers.Remove(ship);

            ClearShipTargetMarker(ship);
            targetMarkers.Remove(ship);

            ClearShipPathMarkers(ship);
            pathMarkers.Remove(ship);
        }

        public void OnShipActivated(IShip ship)
        {
            shipMarkers[ship].SetActive(true);
            CreateShipTargetMarker(ship);
            CreateShipPathMarkers(ship);

            ship.CommandProcessor.CommandChangedEvent += OnCommandChanged;
            ship.TargettingProcessor.TargetChangedEvent += OnTargetChanged;
        }

        public void OnShipDeactivated(IShip ship)
        {
            shipMarkers[ship].SetActive(false);
            ClearShipTargetMarker(ship);
            ClearShipPathMarkers(ship);
            
            ship.CommandProcessor.CommandChangedEvent -= OnCommandChanged;
            ship.TargettingProcessor.TargetChangedEvent -= OnTargetChanged;
        }

        public void OnCommandChanged(IShip ship)
        {
            if (!playerShipController.Selected.Contains(ship)) return;

            ClearShipPathMarkers(ship);
            CreateShipPathMarkers(ship);
        }

        public void OnTargetChanged(IShip ship)
        {
            if (!playerShipController.Selected.Contains(ship)) return;

            CreateShipTargetMarker(ship);
        }

        private void CreateShipPathMarkers(IShip ship)
        {
            if (ship.CommandProcessor.Queue.Count == 0) return;

            var fromPos = ship.Position2D;
            foreach (var command in ship.CommandProcessor.Queue)
            {
                var marker = CreatePathMarker(fromPos, command.Position);
                marker.SetColor(Color.blue);
                pathMarkers[ship].Add(marker);

                fromPos = command.Position;
            }
        }

        private void ClearShipPathMarkers(IShip ship)
        {
            var markers = pathMarkers[ship];
            foreach (var marker in markers)
            {
                Destroy(marker.gameObject);
            }

            markers.Clear();
        }

        private void CreateShipTargetMarker(IShip ship)
        {
            if (ship.TargettingProcessor.Target == null) return;

            if (targetMarkers[ship] == null)
            {
                var marker = CreatePathMarker(ship.Position2D, ship.TargettingProcessor.Target.Position2D);
                marker.SetColor(Color.red);
                targetMarkers[ship] = marker;
            }
        }

        private void ClearShipTargetMarker(IShip ship)
        {
            if (!targetMarkers.ContainsKey(ship) || targetMarkers[ship] == null) return;

            Destroy(targetMarkers[ship].gameObject);
            targetMarkers[ship] = null;
        }

        private PathMarker CreatePathMarker(Vector2 from, Vector2 to)
        {
            // TODO cache
            var go = Instantiate(pathMarkerPrefab, WorldToUiPosition(to),
                WorldToUiRotation(pathMarkerPrefab.transform.rotation), transform);
            var marker = go.GetComponent<PathMarker>();
            marker.LineTo(WorldToUiPosition(from));
            return marker;
        }

        public ShipMarker GetShipMarker(IShip shipController)
        {
            return shipMarkers[shipController];
        }

        public void OnGUI()
        {
            foreach (var shipController in shipMarkers.Keys)
            {
                var shipMarker = shipMarkers[shipController];
                if (!shipMarker.gameObject.activeInHierarchy) continue;
                
                UpdateShipMarker(shipController, shipMarker);
                if (targetMarkers[shipController] != null)
                    UpdateShipTargetMarker(shipController);
            }

            foreach (var ship in pathMarkers.Keys)
            {
                if (pathMarkers[ship].Count == 0) continue;
                var marker = pathMarkers[ship][0];
                if (Vector3.Distance(ship.Position, marker.Position) < 150f)
                {
                    pathMarkers[ship].RemoveAt(0);
                    Destroy(marker.gameObject);
                }

                if (pathMarkers[ship].Count == 0) continue;
                pathMarkers[ship][0].LineTo(WorldToUiPosition(ship.Position2D));
            }
        }

        private void CreateShipMarker(IShip ship)
        {
            if (shipMarkers.ContainsKey(ship)) return;

            var go = Instantiate(shipMarkerPrefab, transform);
            var marker = go.GetComponent<ShipMarker>();
            marker.Setup(ship);

            shipMarkers.Add(ship, marker);
        }

        private void UpdateShipMarker(IShip ship, ShipMarker marker)
        {
            marker.transform.position = WorldToUiPosition(ship.Position2D);
            marker.transform.rotation = WorldToUiRotation(ship.Rotation);

            marker.LineTo(ship.Position);
            marker.UpdateSliders(ship);
        }

        private void UpdateShipTargetMarker(IShip ship)
        {
            if (ship.TargettingProcessor.Target == null)
            {
                ClearShipTargetMarker(ship);
                return;
            }

            var marker = targetMarkers[ship];

            marker.transform.position = WorldToUiPosition(ship.TargettingProcessor.Target.Position2D);
            marker.LineTo(WorldToUiPosition(ship.Position2D));
        }

        private Vector3 WorldToUiPosition(Vector2 position)
        {
            return new Vector3(position.x, transform.position.y + 2f, position.y);
        }

        private Quaternion WorldToUiRotation(Quaternion rotation)
        {
            return Quaternion.Euler(transform.rotation.eulerAngles.x, rotation.eulerAngles.y, 0f);
        }
    }
}