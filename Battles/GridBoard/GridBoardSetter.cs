﻿using UnityEngine;

namespace Battles.GridBoard
{
    public class GridBoardSetter : MonoBehaviour
    {
        public void Init(Vector2 size)
        {
            transform.localScale = new Vector3(size.x / 10f, 1f, size.y / 10f);
        }
    }
}