﻿using TacticalBattle;

namespace TacticalBattles.Ships
{
    public interface IDetectionLevelChangeProcessor
    {
        void Evaluate();
        void SubOldValue(DetectionLevel value);
        void AddNewValue(DetectionLevel value);
    }
}