﻿using System;
using Battles;
using Commons.Ships.Components;
using Games.Types;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class ShipShield : MonoBehaviour, IShipShield
    {
        public event EnergyConsumerEvent EnergyConsumptionChanged;
        public event ShipShieldModeChanged ModeChangedEvent;
        
        private IShield component;
        private bool depleted;
        private float tmpTime;
        private bool recharging;
        private ShipShieldMode mode;

        [SerializeField] private Vector3 size;
        [SerializeField] private ShipShieldRenderer vfxRenderer;
        [SerializeField] private Transform[] locations;

        public Transform[] Locations => locations;

        public MaxValue Points { get; private set; }
        public float RechargeRate { get; private set; }
        public float RechargeDelay { get; private set; }
        public int EnergyConsumption { get; private set; }

        public bool Visible
        {
            get => vfxRenderer.Enabled;
            set => vfxRenderer.Enabled = value;
        }
        public ShipShieldMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                ResetMode();

                EnergyConsumptionChanged?.Invoke(this);
                ModeChangedEvent?.Invoke(this);
            }
        }

        public void Init(IShield i)
        {
            component = i;

            mode = ShipShieldMode.Standard;

            Points = new MaxValue(component.Capacity);
            Points.DepletedEvent += OnDepleted;
            ResetMode();

            vfxRenderer.Init(size);
            vfxRenderer.Enabled = Points.Max > 0;
        }

        public void Tick()
        {
            if (ShipShieldMode.Disabled == Mode) return;
            
            if (!recharging && depleted && Time.time >= tmpTime + RechargeDelay)
            {
                recharging = true;
            }

            else if (recharging)
            {
                Points.Value += RechargeRate * Time.deltaTime;
                if (Points.IsMax)
                {
                    recharging = false;
                    depleted = false;

                    vfxRenderer.Enabled = true;
                }
            }
        }

        private void ResetMode()
        {
            RechargeRate = component.RechargeRate;
            RechargeDelay = component.RechargeDelay;
            EnergyConsumption = component.EnergyConsumption;

            switch (mode)
            {
                case ShipShieldMode.Disabled:
                    EnergyConsumption = 0;
                    RechargeDelay = 9999;
                    RechargeRate = 0;
                    vfxRenderer.Enabled = false;
                    Points.ToMin();
                    break;
                case ShipShieldMode.Standard:
                    break;
                case ShipShieldMode.Overclock:
                    EnergyConsumption = Mathf.RoundToInt(EnergyConsumption * 2.5f);
                    RechargeDelay *= 0.5f;
                    RechargeRate *= 2f;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnDepleted(MaxValue maxValue)
        {
            vfxRenderer.Enabled = false;
            depleted = true;
            tmpTime = Time.time;
        }
        
        public void OnReactorEnergyDepleted(MaxValue maxValue)
        {
            switch (mode)
            {
                case ShipShieldMode.Disabled:
                case ShipShieldMode.Standard:
                    return;
                default:
                    Mode = ShipShieldMode.Standard;
                    break;
            }
        }
    }
}