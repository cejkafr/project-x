﻿namespace TacticalBattles.Ships
{
    internal enum CommandHandlerTickResult
    {
        Continue,
        Completed,
    }

    internal interface IShipCommandHandler
    {
        IShipCommand Command { get; }
        bool IsCommandQueued { get; }
        CommandHandlerTickResult Tick();
        void OnStart();
        void OnCompleted();
    }
}