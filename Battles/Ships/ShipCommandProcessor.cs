﻿using System.Collections.Generic;
using TacticalBattles.Ships.Commands;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class ShipCommandProcessor : IShipCommandProcessor
    {
        public event ShipCommandChanged CommandChangedEvent;
        public event CommandHandlerChanged CommandHandlerChangedEvent;

        private readonly IShip ship;
        private IShipCommandHandler commandHandler;

        public Queue<IShipCommand> Queue { get; } = new Queue<IShipCommand>(1);
        public bool HasCommands => Queue.Count > 0;
        public bool IsHandlingCommand => commandHandler != null;

        public ShipCommandProcessor(IShip ship)
        {
            this.ship = ship;
        }

        public void Add(IShipCommand cmd)
        {
            Queue.Enqueue(cmd);

            if (!IsHandlingCommand)
            {
                ExecuteNextCommand();
            }
            
            CommandChangedEvent?.Invoke(ship);
        }
        
        public void Clear()
        {
            ClearWithoutNotify();
            CommandChangedEvent?.Invoke(ship);
        }

        public void Set(IShipCommand cmd)
        {
            ClearWithoutNotify();
            Add(cmd);
        }
        
        public void Cancel()
        {
            if (!HasCommands) return;

            FinishCommandHandler();
            ExecuteNextCommand();
        }

        public void Tick()
        {
            if (!IsHandlingCommand) return;

            if (commandHandler.Tick() == CommandHandlerTickResult.Completed)
            {
                FinishCommandHandler();
                ExecuteNextCommand();
            }
        }

        private void ClearWithoutNotify()
        {
            Queue.Clear();

            if (commandHandler != null && commandHandler.IsCommandQueued)
            {
                FinishCommandHandler();
            }
        }
        
        private void ExecuteNextCommand()
        {
            if (!HasCommands)
            {
                // Debug.Log("No commands available, stopping ship.");
                return;
            }

            var command = Queue.Peek();
            if (ExecuteCommand(command)) return;

            Debug.Log("Command could not be executed, removing from queue.");
            Queue.Dequeue();
        }

        private bool ExecuteCommand(IShipCommand command)
        {
            commandHandler = CreateCommandHandler(command);
            if (commandHandler == null)
            {
                return false;
            }

            commandHandler.OnStart();
            
            CommandHandlerChangedEvent?.Invoke();
            
            return true;
        }

        private void FinishCommandHandler()
        {
            if (commandHandler == null) return;

            // print($"Command has been completed. ({m_CommandHandler})");

            commandHandler.OnCompleted();
            if (commandHandler.IsCommandQueued && HasCommands)
            {
                Queue.Dequeue();
            }

            commandHandler = null;
            
            CommandHandlerChangedEvent?.Invoke();
        }
        
        private IShipCommandHandler CreateCommandHandler(IShipCommand command)
        {
            switch (command)
            {
                case MoveShipCommand cmd:
                    return new MoveShipCommandHandler(ship, cmd);
                default:
                    Debug.LogError("Unknown command type. " + command);
                    return null;
            }
        }
    }
}