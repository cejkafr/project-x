﻿using System;
using Battles;
using Commons;
using Commons.Ships.Components;
using Game;
using Game.Commons.Audio;
using Games;
using Games.Types;
using UnityEngine;

namespace TacticalBattles.Ships
{
    // TODO refactor and remove MODEs into custom handlers
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(ShipThrusterController))]
    public class ShipEngine : MonoBehaviour, IShipEngine
    {
        private const float StoppingSpeed = 0.01f;

        public event EnergyConsumerEvent EnergyConsumptionChanged;
        public event ShipEngineModeChanged ModeChangedEvent;
        
        private IEngine component;
        private ShipThrusterController thrusterController;
        private Rigidbody rb;
        private Transform t;
        private ShipEngineMode mode;
        private float targetAngle;
        private bool accelerate;
        private SoundSystem.SoundHandle loopHandle = SoundSystem.SoundHandle.Invalid;

        [SerializeField] private Transform[] locations;

        public Transform[] Locations => locations;
        public bool IsNotMoving => Speed <= StoppingSpeed;
        public bool IsVisible
        {
            get => thrusterController.ThrustersEnabled;
            set
            {
                thrusterController.ThrustersEnabled = value;
                ToggleEngineSound();
            }
        }

        public bool IsEnabled => ShipEngineMode.Disabled != mode;

        public float Speed { get; private set; }
        public float SpeedMax { get; private set; }
        public float Acceleration { get; private set; }
        public float RotationSpeedRad { get; private set; }
        public Vector3 Velocity { get; private set; }
        public int EnergyConsumption { get; private set; }

        public bool Accelerate
        {
            get => accelerate;
            set
            {
                if (accelerate == value) return;
                accelerate = value;
                ToggleEngineSound();
            }
        }

        public Vector2 Direction { get; set; }
        public ShipEngineMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                ResetMode();

                EnergyConsumptionChanged?.Invoke(this);
                ModeChangedEvent?.Invoke(this);
            }
        }

        private void Awake()
        {
            thrusterController = GetComponent<ShipThrusterController>();
            rb = GetComponent<Rigidbody>();
            t = transform;
        }
        
        public void Init(IEngine i)
        {
            component = i;
            mode = ShipEngineMode.Standard;

            SyncThrusters();
            ResetMode();
        }

        public void Tick()
        {
            CalculateAcceleration();
            SyncThrusters();
        }

        public void Move()
        {
            rb.MovePosition(t.position + Velocity * Time.fixedDeltaTime);
            // PlayLoopSfx(ShipEngineMode.Disabled != mode && Speed > 0f);
        }

        public void Rotate()
        {
            // TODO migrate in some kind of handler, so we do not check this every frame
            if (ShipEngineMode.HighVelocityTurn == Mode)
            {
                var euler = t.rotation.eulerAngles.y;
                if (euler > 180) euler -= 360;
                euler *= -1;

                if ((targetAngle < 0f && euler < targetAngle) ||
                    (targetAngle > 0f && euler > targetAngle))
                {
                    Mode = ShipEngineMode.Standard;
                    return;
                }

                var right = t.right;
                Direction = GeneralUtils.V3ToV2(targetAngle < 0f ? right : right * -1);
            }

            if (Direction == Vector2.zero) return;
            var targetRotation = Quaternion.LookRotation(GeneralUtils.V2ToV3(Direction, 0f));
            transform.rotation = Quaternion.Slerp(transform.rotation,
                targetRotation, RotationSpeedRad * Time.deltaTime);
        }

        private void CalculateAcceleration()
        {
            // TODO migrate in some kind of handler, so we do not check this every frame
            Speed += (Mode == ShipEngineMode.Overclock
                      || Mode == ShipEngineMode.HighVelocityTurn
                      || Accelerate
                ? Acceleration
                : Acceleration * -1) * Time.deltaTime;
            Speed = Mathf.Clamp(Speed, 0f, SpeedMax);

            Velocity = Speed * t.forward;
        }

        private void ResetMode()
        {
            IsVisible = true;
            SpeedMax = component.MaxSpeed;
            Acceleration = component.Acceleration;
            RotationSpeedRad = component.RotationSpeedRad;
            EnergyConsumption = component.EnergyConsumption;

            switch (mode)
            {
                case ShipEngineMode.Disabled:
                    Acceleration = 0;
                    RotationSpeedRad = 0;
                    EnergyConsumption = 0;
                    IsVisible = false;
                    break;
                case ShipEngineMode.Standard:
                    break;
                case ShipEngineMode.Overclock:
                    SpeedMax *= 2;
                    Acceleration *= 3;
                    RotationSpeedRad /= 2;
                    EnergyConsumption = Mathf.RoundToInt(EnergyConsumption * 3f);
                    break;
                case ShipEngineMode.HighVelocityTurn:
                    RotationSpeedRad *= 2;
                    targetAngle = Vector2.SignedAngle(Direction, Vector2.up) * -1;
                    EnergyConsumption = Mathf.RoundToInt(EnergyConsumption * 3f);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            ToggleEngineSound();
        }

        private void ToggleEngineSound()
        {
            if (!Accelerate || !IsEnabled || !IsVisible)
            {
                if (!loopHandle.IsValid()) return;

                // print("STOP");

                GameManager.SoundSystem.Stop(loopHandle);
                loopHandle = SoundSystem.SoundHandle.Invalid;
            }
            
            if (loopHandle.IsValid()) return;

            // print("PLAY");
            loopHandle = GameManager.SoundSystem.Play(component.LoopSfx, locations[0]);
        }

        private void SyncThrusters()
        {
            var maxShipSpeed = TimeManager.TimeScaledFloat(SpeedMax);
            if (maxShipSpeed < StoppingSpeed) return;

            var speedPercent = Speed / TimeManager.TimeScaledFloat(SpeedMax);
            thrusterController.SetThrusters(speedPercent);
        }

        private void Tilt()
        {
            var angleToTarget = Vector3.Angle(transform.forward, Direction);
            float tilt;
            if (transform.InverseTransformPoint(transform.position).x < 0)
            {
                tilt = Mathf.Clamp(angleToTarget / 2f, 0f, 20f);
            }
            else
            {
                tilt = Mathf.Clamp(angleToTarget / 2f * -1, -20f, 0f);
            }

            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, tilt);
        }

        public void OnReactorEnergyDepleted(MaxValue maxValue)
        {
            switch (mode)
            {
                case ShipEngineMode.Disabled:
                case ShipEngineMode.Standard:
                    return;
                default:
                    Mode = ShipEngineMode.Standard;
                    break;
            }
        }
    }
}