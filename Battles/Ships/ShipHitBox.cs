﻿using Battles;
using Game.Commons.Combat;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class ShipHitBox : MonoBehaviour, IAttackable
    {
        private IShip ship;

        [SerializeField] private AttackVector attackVector;

        public ITeam Team => ship.Team;

        public void Init(IShip iShip)
        {
            ship = iShip;
        }
        
        public DamageBundle TakeDamage(Vector3 point, IShipWeapon weapon)
        {
            return ship.TakeDamage(attackVector, point, weapon);
        }
    }
}