﻿using System.Collections.Generic;

namespace TacticalBattles.Ships
{
    public delegate void ShipTargetChanged(IShip iShip);

    public interface IShipTargettingProcessor
    {
        event ShipTargetChanged TargetChangedEvent;
        
        HashSet<IShip> Targets { get; }
        IShip Target { get; set; }
        bool HasTarget { get; }
        bool AutoEngage { get; set; }
        
        void Init(IShip iShip);
        void Tick();

        bool CanTargetWith(int index, IShip target);
    }
}