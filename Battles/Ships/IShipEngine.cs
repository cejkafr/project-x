﻿using Commons.Ships.Components;
using UnityEngine;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public enum ShipEngineMode
    {
        Disabled, Standard, Overclock, HighVelocityTurn
    }
    
    public delegate void ShipEngineModeChanged(IShipEngine engine);
    
    public interface IShipEngine : IEnergyConsumer
    {
        event ShipEngineModeChanged ModeChangedEvent;

        Transform[] Locations { get; }
        bool IsNotMoving { get; }
        float Speed { get; }
        float SpeedMax { get; }
        float Acceleration { get; }
        float RotationSpeedRad { get; }
        Vector3 Velocity { get; }

        bool IsVisible { get; set; }
        bool Accelerate { get; set; }
        Vector2 Direction { get; set; }
        ShipEngineMode Mode { get; set; }

        void Init(IEngine i);
        void Tick();
        void Move();
        void Rotate();
    }
}