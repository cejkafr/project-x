﻿using System;
using System.Collections.Generic;
using TacticalBattles.Ships;
using UnityEngine;
using UnityEngine.Events;

namespace Battles.Ships
{
    public class ShipManager : MonoBehaviour
    {
        [Serializable]
        public class ShipEvent : UnityEvent<IShip>
        {
        }

        [SerializeField] private TeamManager teamManager;
        [Space] 
        public ShipEvent onShipRegistered = new ShipEvent();
        public ShipEvent onShipUnregistered = new ShipEvent();
        public ShipEvent onShipDestroyed = new ShipEvent();

        public List<IShip> Ships { get; } = new List<IShip>(2);

        public IShip Create(Commons.Ships.IShip iShip)
        {
            var pos3d = new Vector3(0, 100f, 0);
            var inst = Instantiate(iShip.GamePrefab, pos3d, Quaternion.identity, transform);
            var ship = inst.GetComponent<IShip>();
            var faction = teamManager.Find(iShip.Faction);

            ship.Init(iShip, faction);

            Ships.Add(ship);

            ship.Destroyed += OnShipDestroyed;

            onShipRegistered.Invoke(ship);

            return ship;
        }

        public void Remove(IShip ship)
        {
            Ships.Remove(ship);
            onShipUnregistered.Invoke(ship);
        }
        
        public void Destroy(IShip ship)
        {
            ship.DestroyObject();
            OnShipDestroyed(ship);
        }

        public void OnShipDestroyed(IShip shipController)
        {
            Remove(shipController);
            onShipDestroyed.Invoke(shipController);
        }
        
        public void Clear()
        {
            foreach (var ship in Ships)
            {
                ship.Destroyed -= OnShipDestroyed;
                onShipDestroyed.Invoke(ship);
                ship.DestroyObject();
            }
            
            Ships.Clear();
        }
    }
}