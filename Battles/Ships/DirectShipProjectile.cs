﻿using Battles;
using Game;
using Game.Commons.Audio;
using Game.Commons.Pools;
using Games;
using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class DirectShipProjectile : MonoBehaviour, IShipProjectile
    {
        private SimpleObjectPool pool;
        private float ttl;

        public GameObject muzzlePrefab;
        public GameObject explosionPrefab;
        public SoundDef shootFx;
        public SoundDef hitFx;

        public IShipWeapon Weapon { get; private set; }

        private void Awake()
        {
            pool = FindObjectOfType<SimpleObjectPool>();
        }

        public void Init(IShipWeapon iWeapon, Transform source)
        {
            Weapon = iWeapon;

            ttl = Time.time + Weapon.Component.ProjectileLiveTimeSec;

            gameObject.SetActive(true);
        }

        public void Init(IShipWeapon iWeapon, Transform source, Transform target)
        {
            Init(iWeapon, source);
        }

        public void AttackFx()
        {
            if (shootFx == null) return;
            GameManager.SoundSystem.Play(shootFx, transform.position);
        }

        public void AttackVfx()
        {
            if (muzzlePrefab == null) return;
            pool.CreateFromPool(muzzlePrefab, transform);
        }

        private void Update()
        {
            if (Time.time > ttl)
            {
                Detonate();
            }

            transform.Translate(Time.deltaTime * Weapon.Component.ProjectileSpeed * Vector3.forward);
        }

//        private void OnEnable()
//        {
//            
//        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Missile")) return;

            var attackable = other.GetComponent<IAttackable>();
            if (attackable != null)
            {
                if (Weapon.Team == attackable.Team) return;

                if (hitFx != null)
                    GameManager.SoundSystem.Play(hitFx, transform);

                var point = Vector3.zero;
                if (Physics.Raycast(transform.position, transform.forward, out var hit))
                {
                    point = hit.point;
                }
                
                var result = attackable.TakeDamage(point, Weapon);
                if (result.IsEmpty)
                {
                    Detonate();   
                }
            }
            else
            {
                Detonate();
            }
        }

        private void Detonate()
        {
            if (explosionPrefab)
            {
                pool.CreateFromPool(explosionPrefab, transform);
            }

            gameObject.SetActive(false);
        }
    }
}