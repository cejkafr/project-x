﻿using TacticalBattle;

namespace TacticalBattles.Ships
{
    public class DetectionLevelChangeProcessor : IDetectionLevelChangeProcessor
    {
        private readonly IShip ship;

        private int detectionCount;
        private int fullCount;

        public DetectionLevelChangeProcessor(IShip ship)
        {
            this.ship = ship;
        }

        public void Evaluate()
        {
            if (fullCount > 0 && DetectionLevel.Full != ship.DetectionLevel)
            {
                ship.OnDetectionLevelChanged(DetectionLevel.Full);
            } else if (detectionCount > 0 && DetectionLevel.Detected != ship.DetectionLevel)
            {
                ship.OnDetectionLevelChanged(DetectionLevel.Detected);
            }
        }

        public void SubOldValue(DetectionLevel value)
        {
            switch (value)
            {
                case DetectionLevel.Detected:
                    detectionCount--;
                    break;
                case DetectionLevel.Full:
                    fullCount--;
                    break;
            }
        }

        public void AddNewValue(DetectionLevel value)
        {
            switch (value)
            {
                case DetectionLevel.Detected:
                    detectionCount++;
                    break;
                case DetectionLevel.Full:
                    fullCount++;
                    break;
            }
        }
    }
}