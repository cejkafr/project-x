﻿using System.Collections.Generic;
using Commons.Ships.Components;
using JetBrains.Annotations;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public enum DetectionLevel
    {
        None, Detected, Full
    }

    public class DetectionObject
    {
        public readonly IShip Ship;

        public float Distance { get; private set; }
        public float FacingAngle { get; private set; }
        public DetectionLevel Level { get; private set; }
        public bool IsDetected => DetectionLevel.Detected == Level;
        public bool IsFull => DetectionLevel.Full == Level;

        public DetectionObject(IShip ship)
        {
            Ship = ship;
        }

        public void Update(float distance, float angle, DetectionLevel detectionLevel)
        {
            Distance = distance;
            FacingAngle = angle;
            Level = detectionLevel;
        }
    }

    public delegate void ShipSensorEvent(DetectionObject detectionObject);
    
    public interface IShipSensors : IEnergyConsumer
    {
        event ShipSensorEvent DetectionLevelChanged;
        
        Dictionary<IShip, DetectionObject> Objects { get; }

        int Range { get; }
        void Init(ISensors iSensors);
        void UpdateValue(IShip ship, float distance, [CanBeNull] IDetectionLevelChangeProcessor processor = null);
    }
}