﻿using Commons.Ships.Components;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public interface IShipAuxiliarySystem : IEnergyConsumer
    {
        int SensorRange { get; }

        void Init(ISensors i);
    }
}