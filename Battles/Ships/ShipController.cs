﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commons;
using Commons.Ships;
using Game.Commons.Combat;
using Games;
using Games.Types;
using TacticalBattles.Ships;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;
using Random = UnityEngine.Random;

namespace Battles.Ships
{
    [RequireComponent(typeof(IShipReactor))]
    [RequireComponent(typeof(IShipEngine))]
    [RequireComponent(typeof(IShipShield))]
    [RequireComponent(typeof(IShipWeaponSystem))]
    [RequireComponent(typeof(IShipTargettingProcessor))]
    [RequireComponent(typeof(IShipSensors))]
    public class ShipController : MonoBehaviour, IShip
    {
        public event ShipEvent DetectionLevelChanged;
        public event ShipEvent StanceChanged;
        public event ShipEvent Destroyed;

        private StanceSkillObject stance;

        [SerializeField] private GameObject shipRenderer;
        [SerializeField] private GameObject exposedShipRenderer;

        public Transform Transform => transform;
        public Vector2 Position2D { get; private set; }
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;

        public Commons.Ships.IShip BaseObject { get; private set; }
        public ITeam Team { get; private set; }

        public IShipCommandProcessor CommandProcessor { get; private set; }
        public IShipReactor Reactor { get; private set; }
        public IShipEngine Engine { get; private set; }
        public IShipShield Shield { get; private set; }
        public IShipWeaponSystem WeaponSystem { get; private set; }
        public IShipTargettingProcessor TargettingProcessor { get; private set; }
        public IShipSensors Sensors { get; private set; }

        public MaxValue HullPoints { get; private set; }

        public DetectionLevel DetectionLevel { get; private set; }
        public bool IsDestroyed { get; private set; }
        

        public IEnumerable<StanceSkillObject> Stances => BaseObject.Stances;
        public StanceSkillObject StanceSkill
        {
            get => stance;
            set
            {
                if (stance == value)
                {
                    return;
                }

                if (!Stances.Contains(value))
                {
                    return;
                }

                stance = value;
                StanceChanged?.Invoke(this);
            }
        }

        public List<IEffector> Effectors { get; } = new List<IEffector>(1);

        private void Awake()
        {
            Reactor = GetComponent<IShipReactor>();
            Engine = GetComponent<IShipEngine>();
            Shield = GetComponent<IShipShield>();
            WeaponSystem = GetComponent<IShipWeaponSystem>();
            TargettingProcessor = GetComponent<IShipTargettingProcessor>();
            Sensors = GetComponent<IShipSensors>();

            var armours = GetComponentsInChildren<ShipHitBox>();
            foreach (var armour in armours)
            {
                armour.Init(this);
            }
        }

        private void Start()
        {
        }

        public void Init(Commons.Ships.IShip ship, ITeam team)
        {
            BaseObject = ship;
            Team = team;

            HullPoints = new MaxValue(BaseObject.HullPoints);
            HullPoints.DepletedEvent += OnHullPointsDepleted;

            CommandProcessor = new ShipCommandProcessor(this);

            Reactor.Init(CompositeComponentFactory.Create(ship.ReactorSlots));

            Engine.Init(CompositeComponentFactory.Create(ship.EngineSlots));
            Reactor.RegisterConsumer(Engine);
            
            Shield.Init(CompositeComponentFactory.Create(ship.ShieldSlots));
            Reactor.RegisterConsumer(Shield);

            WeaponSystem.Init(ship.WeaponSlots, Team);
            StanceChanged += WeaponSystem.OnStanceChange;
            
            Reactor.RegisterConsumer(WeaponSystem);

            TargettingProcessor.Init(this); ;
            
            Sensors.Init(CompositeComponentFactory.Create(ship.AuxiliarySlots));
            Reactor.RegisterConsumer(Sensors);

            ResetRenderMode();
            
            if (Team.IsHuman)
            {
                CommandProcessor.CommandChangedEvent += OnCommand;
                TargettingProcessor.TargetChangedEvent += OnAttack;
            }
        }

        private void Update()
        {
            Position2D = GeneralUtils.V3ToV2(transform.position);
            
            Reactor.Tick();
            Shield.Tick();
            CommandProcessor.Tick();
            TargettingProcessor.Tick();
            Engine.Tick();
            Engine.Rotate();
        }

        private void FixedUpdate()
        {
            Engine.Move();
        }

        public void AddEffector(IEffector effector)
        {
            Effectors.Add(effector);
            effector.OnActivate(this);
        }

        public void RemoveEffector(IEffector effector)
        {
            Effectors.Remove(effector);
            effector.OnDeactivate(this);
        }

        public void Destroy()
        {
            IsDestroyed = true;
            CommandProcessor.Clear();
            Engine.IsVisible = false;
            enabled = false;

            if (Team.IsHuman)
            {
                GameManager.SoundSystem.Play(BaseObject.Pilot.quoteDestroyed);
            }

            Destroyed?.Invoke(this);
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }

        public DamageBundle TakeDamage(AttackVector vector, Vector3 point, IShipWeapon weapon)
        {
            return ShipDamageProcessor.Apply(this, vector, weapon.Component);
        }
        
        public Transform FindComponentTransform()
        {
            var value = Random.Range(0, 4);
            switch (value)
            {
                case 0:
                    return Reactor.Locations[Random.Range(0, Reactor.Locations.Length)];
                case 1:
                    return Engine.Locations[Random.Range(0, Engine.Locations.Length)];
                case 2:
                    return Shield.Locations[Random.Range(0, Shield.Locations.Length)];
                case 3:
                    return WeaponSystem.Locations[Random.Range(0, WeaponSystem.Locations.Length)].transform;
                default:
                    throw new ArgumentException("Unknown slot type.");
            }
        }
        
        private void OnCollisionStay(Collision other)
        {
            print($"Collision {Engine.Speed}");

        }

        private void ResetRenderMode()
        {
            if (Team.IsHuman)
            {
                return;
            }

            Shield.Visible = DetectionLevel.Full == DetectionLevel;
            Engine.IsVisible = DetectionLevel.Full == DetectionLevel;
            shipRenderer.SetActive(DetectionLevel.Full == DetectionLevel);

            exposedShipRenderer.SetActive(DetectionLevel.Detected == DetectionLevel);
        }

        #region Event Handling

        private void OnCommand(IShip ship)
        {
            GameManager.SoundSystem.Play(BaseObject.Pilot.quoteCommand);
        }
        
        private void OnAttack(IShip ship)
        {
            if (!ship.TargettingProcessor.HasTarget)
            {
                return;
            }

            GameManager.SoundSystem.Play(BaseObject.Pilot.quoteAttack);
        }
        
        private void OnHullPointsDepleted(MaxValue maxValue)
        {
            Destroy();
        }

        public void OnSelected()
        {
            if (!Team.IsHuman)
            {
                return;
            }

            GameManager.SoundSystem.Play(BaseObject.Pilot.quoteSelect);
        }

        public void OnDetectionLevelChanged(DetectionLevel value)
        {
            DetectionLevel = value;
            ResetRenderMode();
            DetectionLevelChanged?.Invoke(this);
        }

        #endregion
        
        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(BaseObject.Faction)}: {BaseObject.Faction}, {nameof(BaseObject)}: {BaseObject}";
        }
    }
}