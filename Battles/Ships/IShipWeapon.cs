﻿using Battles;
using Battles.Ships;
using Commons.Ships.Components;
using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public interface IShipWeapon
    {
        Transform Transform { get; }
        float YAngle { get; }
        IWeaponComponent Component { get; }
        ITeam Team { get; }
        Transform[] Outputs { get; }
        float TimeSinceAttackSec { get; }
        bool IsOnCooldown { get; }
        bool IsAutoFireReady { get; }
        float RemainingCooldownSec { get; }
        int EnergyConsumption { get; }
        float CooldownSec { get; }
        float Range { get; }
        float CriticalChance { get; }

        void Init(IWeaponComponent component, IShipWeaponSystem weaponSystem, IFoVRenderer iFoVRenderer);
        void Shoot(IShip target);
        void Shoot();
        void ShowFoV(bool val);
    }
}