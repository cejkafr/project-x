﻿using Commons.Ships.Components;
using Game.Commons.Combat;
using TacticalBattles.Ships.DamageProcessors;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public static class ShipDamageProcessor
    {
        private static readonly IDamageProcessor ShieldDamageProcessor = new ShieldDamageProcessor();
        private static readonly IDamageProcessor HullDamageProcessor = new HullDamageProcessor();

        public static DamageBundle Apply(IShip ship, AttackVector vector, IWeaponComponent weapon)
        {
            if (AttackVector.Shield == vector)
            {
                return IsShieldPenetrated(weapon)
                    ? weapon.Damage
                    : ShieldDamageProcessor.Apply(ship, vector, weapon);
            }

            return HullDamageProcessor.Apply(ship, vector, weapon);
        }

        private static bool IsShieldPenetrated(IWeaponComponent weapon)
        {
            var randomValue = Random.Range(0, 99);
            var penetrated = randomValue < weapon.ShieldPenetrationChance;
            Debug.Log($"Hit; Shield Penetration; {randomValue} > {weapon.ShieldPenetrationChance} = {penetrated}; ");
            return penetrated;
        }
    }
}