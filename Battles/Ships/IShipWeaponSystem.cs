﻿using Commons.Ships;
using DataObjects.Ships;
using TacticalBattles.Ships;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;

namespace Battles.Ships
{
    public enum ShipWeaponMode
    {
        Disabled, Standard, Overclock
    }

    public class WeaponRange
    {
        public float Min { get; set; }
        public float Max { get; set; }

        public WeaponRange(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }

    public delegate void ShipWeaponSystemEvent(IShipWeaponSystem engine);
    
    public interface IShipWeaponSystem : IEnergyConsumer
    {
        event ShipWeaponSystemEvent ModeChanged;
        
        Transform[] Locations { get; }
        WeaponShipSlot[] Slots { get; }
        IShipWeapon[] Weapons { get; }
        ShipWeaponMode Mode { get; set; }
        ITeam Team { get; }

        float AttackSpeedModifier { get; }
        float AttackPrecisionModifier { get; }
        float AttackCriticalChanceModifier { get; }
        
        WeaponRange Range { get; }

        void Init(WeaponShipSlot[] slots, ITeam team);

        void AttackWith(int index, IShip target);
        void Attack(IShip target);

        bool IsWeaponAutoFireReady(int index);

        IShipWeapon GetWeaponAssociatedWithSkill(WeaponSkillObject skillObject);

        void OnStanceChange(IShip ship);
    }
}
