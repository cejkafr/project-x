﻿using System.Collections.Generic;
using Commons.Ships.Components;
using Games.Types;
using UnityEngine;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public class ShipReactor : MonoBehaviour, IShipReactor
    {
        private IReactor component;
        private List<IEnergyConsumer> consumers;
        private float lastTickTime;

        [SerializeField] private Transform[] locations;

        public Transform[] Locations => locations;
        public int Power => component.Power;
        public int Capacity => component.Capacity;
        public float RechargeRate { get; private set; }
        public MaxValue Energy { get; private set; }

        public void Init(IReactor i)
        {
            component = i;
            consumers = new List<IEnergyConsumer>(5);

            ResetRechargeRate();
            
            Energy = new MaxValue(i.Capacity);
        }

        public void Tick()
        {
            EnergyLogic();

            lastTickTime = Time.time;
        }

        public void RegisterConsumer(IEnergyConsumer consumer)
        {
            if (consumers.Contains(consumer)) return;
            
            // print($"Reactor: Registering new consumer {iConsumer.EnergyConsumption}");
            
            consumers.Add(consumer);
            consumer.EnergyConsumptionChanged += OnEnergyConsumptionChanged;
            
            Energy.DepletedEvent += consumer.OnReactorEnergyDepleted;
            
            ResetRechargeRate();
        }

        public void UnregisterConsumer(IEnergyConsumer consumer)
        {
            if (!consumers.Contains(consumer)) return;
            
            Energy.DepletedEvent -= consumer.OnReactorEnergyDepleted;
            
            consumers.Remove(consumer);
            consumer.EnergyConsumptionChanged -= OnEnergyConsumptionChanged;

            ResetRechargeRate();
        }

        private void OnEnergyConsumptionChanged(IEnergyConsumer iConsumer)
        {
            ResetRechargeRate();
        }

        private void ResetRechargeRate()
        {
            RechargeRate = Power;
            foreach (var consumer in consumers)
            {
                RechargeRate -= consumer.EnergyConsumption;
            }
            
            // print($"Reactor: RechargeRate {RechargeRate}.");
        }

        private void EnergyLogic()
        {
            if (Energy.IsMax && RechargeRate > 0) return;

            var deltaTime = Time.time - lastTickTime;
            Energy.Value += RechargeRate * deltaTime;
        }
    }
}