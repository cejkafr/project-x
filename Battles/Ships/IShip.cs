﻿using System.Collections.Generic;
using Battles;
using Battles.Ships;
using Commons.DataObjects.Ships;
using Commons.Ships;
using DataObjects.Ships;
using Game.Commons.Combat;
using Games.Types;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public enum AttackVector
    {
        Front, Side, Rear, Shield,
    }

    public delegate void ShipEvent(IShip iShip);

    public interface IShip
    {
        event ShipEvent DetectionLevelChanged;
        event ShipEvent StanceChanged;
        event ShipEvent Destroyed;

        Transform Transform { get; }
        Vector2 Position2D { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }

        Commons.Ships.IShip BaseObject { get; }
        ITeam Team { get; }

        IShipCommandProcessor CommandProcessor  { get; }
        IShipReactor Reactor  { get; }
        IShipEngine Engine  { get; }
        IShipShield Shield  { get; }
        IShipWeaponSystem WeaponSystem  { get; }
        IShipTargettingProcessor TargettingProcessor  { get; }
        IShipSensors Sensors  { get; }

        MaxValue HullPoints  { get; }

        DetectionLevel DetectionLevel { get; }
        bool IsDestroyed { get; }
        
        IEnumerable<StanceSkillObject> Stances { get; }
        StanceSkillObject StanceSkill { get; set; }

        void Init(Commons.Ships.IShip ship, ITeam team);
        DamageBundle TakeDamage(AttackVector vector, Vector3 point, IShipWeapon weapon);
        Transform FindComponentTransform();

        void OnSelected();

        void OnDetectionLevelChanged(DetectionLevel value);

        void DestroyObject();
    }
}