﻿using System.Collections.Generic;

namespace TacticalBattles.Ships
{
    public delegate void ShipCommandChanged(IShip iShip);
    public delegate void CommandHandlerChanged();
    
    public interface IShipCommandProcessor
    {
        event ShipCommandChanged CommandChangedEvent;
        event CommandHandlerChanged CommandHandlerChangedEvent;
        
        Queue<IShipCommand> Queue { get; }
        bool HasCommands { get; }
        bool IsHandlingCommand { get; }
        
        void Add(IShipCommand cmd);
        void Set(IShipCommand cmd);
        void Cancel();
        void Clear();
        void Tick();

    }
}