﻿using UnityEngine;

namespace TacticalBattles.Ships
{
    public class ShipShieldRenderer : MonoBehaviour
    {
        private MeshRenderer mr;
        private Collider c;
        
        public bool Enabled
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        private void Awake()
        {
            mr = GetComponent<MeshRenderer>();
            c = GetComponent<Collider>();
        }

        public void Init(Vector3 size)
        {
            transform.localScale = size;
        }
    }
}