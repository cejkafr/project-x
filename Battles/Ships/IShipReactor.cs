﻿using Commons.Ships.Components;
using Games.Types;
using UnityEngine;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public interface IShipReactor
    {
        Transform[] Locations { get; }
        int Power { get; }
        int Capacity { get; }
        float RechargeRate { get; }
        MaxValue Energy { get; }

        void Init(IReactor i);
        void RegisterConsumer(IEnergyConsumer consumer);
        void UnregisterConsumer(IEnergyConsumer consumer);
        void Tick();
    }
}