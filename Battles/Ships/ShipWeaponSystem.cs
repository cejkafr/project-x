﻿using System;
using System.Linq;
using Battles.FOVs;
using Commons.Ships;
using Commons.Ships.Components;
using DataObjects.Ships;
using Games.Types;
using TacticalBattles.Ships;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;

namespace Battles.Ships
{
    public class ShipWeaponSystem : MonoBehaviour, IShipWeaponSystem
    {
        public event EnergyConsumerEvent EnergyConsumptionChanged;
        public event ShipWeaponSystemEvent ModeChanged;

        private ShipWeaponMode mode;
        private StanceSkillObject stanceSkill;

        [SerializeField] private Transform[] locations;

        public Transform[] Locations => locations;

        public WeaponShipSlot[] Slots { get; private set; }
        public IShipWeapon[] Weapons { get; private set; }
        public int EnergyConsumption { get; private set; }

        public ITeam Team { get; private set; }

        public float AttackSpeedModifier { get; private set; }
        public float AttackPrecisionModifier { get; private set; }
        public float AttackCriticalChanceModifier { get; private set; }

        public WeaponRange Range { get; } = new WeaponRange(0, 0);

        public ShipWeaponMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                ResetMode();

                EnergyConsumptionChanged?.Invoke(this);
                ModeChanged?.Invoke(this);
            }
        }

        public void Init(WeaponShipSlot[] slots, ITeam team)
        {
            Slots = slots;
            Weapons = new IShipWeapon[Slots.Length];
            Team = team;
            for (var i = 0; i < slots.Length; i++)
            {
                Equip(i, slots[i]);
            }

            mode = ShipWeaponMode.Standard;
            ResetMode();
        }

        public void AttackWith(int index, IShip target)
        {
            Weapons[index].Shoot(target);
        }

        public void Attack(IShip target)
        {
            foreach (var weapon in Weapons)
            {
                weapon?.Shoot(target);
            }
        }

        public bool IsWeaponAutoFireReady(int index)
        {
            return Weapons[index] != null && Weapons[index].IsAutoFireReady;
        }

        public IShipWeapon GetWeaponAssociatedWithSkill(WeaponSkillObject skillObject)
        {
            return Weapons.Where(weapon => weapon != null)
                .Where(weapon => weapon.Component.HasSkills)
                .FirstOrDefault(weapon => weapon.Component.Skills[0] == skillObject);
        }

        public void OnReactorEnergyDepleted(MaxValue maxValue)
        {
            switch (mode)
            {
                case ShipWeaponMode.Disabled:
                case ShipWeaponMode.Standard:
                    return;
                default:
                    Mode = ShipWeaponMode.Standard;
                    break;
            }
        }
        
        public void OnStanceChange(IShip ship)
        {
            stanceSkill = ship.StanceSkill;
            ResetMode();
        }

        private void Equip(int i, WeaponShipSlot slot)
        {
            if (slot.IsEmpty) return;
            var weapon = (IWeaponComponent) slot.Component;

            var inst = Instantiate(weapon.WeaponPrefab, locations[i]);
            var controller = inst.GetComponent<IShipWeapon>();

            // todo factory?
            IFoVRenderer fovRenderer;
            if (WeaponVisualisationMethod.Trajectory == weapon.Visualisation)
            {
                // Fix hardcoded path
                var prefab = Resources.Load<GameObject>("Prefabs/Battle/FOVTrajectory");
                fovRenderer = Instantiate(prefab, locations[i]).GetComponent<IFoVRenderer>();
                fovRenderer.Init(controller);
                fovRenderer.Show(false);
            }
            else
            {
                // TODO fix findobjectoftype
                var fovInst = FindObjectOfType<FoVMeshManager>().CreateFieldOfView(
                    weapon.Range, slot.FireAngle, 10f);
                fovInst.transform.SetParent(locations[i]);
                fovInst.transform.localPosition = Vector3.down * 96f;
                fovInst.transform.localRotation = Quaternion.identity;

                fovRenderer = fovInst.GetComponent<IFoVRenderer>();
                fovRenderer.Init(controller);
                fovRenderer.Show(false);
            }

            controller.Init(weapon, this, fovRenderer);
            Weapons[i] = controller;
            
            if (weapon.Range < Range.Min) Range.Min = weapon.Range;
            if (weapon.Range > Range.Max) Range.Max = weapon.Range;
        }

        private void ResetMode()
        {
            AttackSpeedModifier = 1;
            AttackPrecisionModifier = 1;
            AttackCriticalChanceModifier = 1;
            EnergyConsumption = Weapons.Where(controller => controller != null)
                .Sum(controller => controller.EnergyConsumption);

            if (stanceSkill != null)
            {
                AttackSpeedModifier = stanceSkill.attackSpeedMultiplier;
                AttackPrecisionModifier = stanceSkill.attackPrecisionMultiplier;
                AttackCriticalChanceModifier = stanceSkill.attackCriticalChanceMultiplier;
            }
            
            switch (mode)
            {
                case ShipWeaponMode.Disabled:
                    AttackSpeedModifier = 9999;
                    EnergyConsumption = 0;
                    break;
                case ShipWeaponMode.Standard:
                    break;
                case ShipWeaponMode.Overclock:
                    AttackSpeedModifier *= 0.5f;
                    EnergyConsumption *= 2;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}