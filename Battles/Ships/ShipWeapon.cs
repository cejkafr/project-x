﻿using System.Collections;
using System.Text;
using Battles;
using Battles.Ships;
using Commons;
using Commons.Ships.Components;
using Game.Commons.Pools;
using UnityEngine;

namespace TacticalBattles.Ships
{
    // TODO REFACTOR!
    public class ShipWeapon : MonoBehaviour, IShipWeapon
    {
        private SimpleObjectPool pool;
        private float lastAttackTime;
        private IFoVRenderer fovRenderer;

        [SerializeField] private Transform[] outputs;

        public Transform Transform => transform;
        public float YAngle { get; private set; }
        public IWeaponComponent Component { get; private set; }
        public IShipWeaponSystem WeaponSystem { get; private set; }
        public ITeam Team => WeaponSystem.Team;
        public Transform[] Outputs => outputs;
        public float TimeSinceAttackSec => Time.time - lastAttackTime;
        public bool IsOnCooldown => TimeSinceAttackSec < CooldownSec;
        public bool IsAutoFireReady => !Component.HasSkills && !IsOnCooldown;
        public float RemainingCooldownSec => CooldownSec - TimeSinceAttackSec;
        public int EnergyConsumption => Component.EnergyConsumption;

        public float CooldownSec => Component.AttackDelaySec * WeaponSystem.AttackSpeedModifier;
        public float Range => Component.Range;
        public float CriticalChance => Component.Range * WeaponSystem.AttackCriticalChanceModifier;

        public bool Enabled => ShipWeaponMode.Disabled != WeaponSystem.Mode;

        private void Awake()
        {
            // TODO refactor
            pool = FindObjectOfType<SimpleObjectPool>();
        }

        public void Init(IWeaponComponent component, IShipWeaponSystem weaponSystem, IFoVRenderer iFoVRenderer)
        {
            Component = component;
            WeaponSystem = weaponSystem;
            fovRenderer = iFoVRenderer;

            YAngle = transform.rotation.eulerAngles.y;
            
            lastAttackTime = Time.time - 200f;
        }

        public void Shoot(IShip target)
        {
            if (!Enabled) return;
            if (IsOnCooldown) return;

            lastAttackTime = Time.time;

            if (Component.Burst)
            {
                StartCoroutine(BurstFireCoroutine(target.Transform, target));
            }
            else
            {
                SpawnProjectiles(target.Transform, target);
            }
        }

        public void Shoot()
        {
            if (!Enabled) return;
            if (IsOnCooldown) return;

            lastAttackTime = Time.time;

            if (Component.Burst)
            {
                StartCoroutine(BurstFireCoroutine());
            }
            else
            {
                SpawnProjectiles();
            }
        }

        public void ShowFoV(bool val)
        {
            fovRenderer?.Show(val);
        }

        private void SpawnProjectiles()
        {
            for (var i = 0; i < outputs.Length; i++)
            {
                var projectile = SpawnProjectile(outputs[i]);
                if (i == 0)
                {
                    projectile.AttackFx();
                    projectile.AttackVfx();
                }
            }
        }

        private void SpawnProjectiles(Transform targetTransform, IShip target)
        {
            for (var i = 0; i < outputs.Length; i++)
            {
                var pos = TargetTransform(target, target.FindComponentTransform().position);
                var projectile = SpawnProjectile(outputs[i], targetTransform, pos);
                if (i == 0)
                {
                    projectile.AttackFx();
                    projectile.AttackVfx();
                }
            }
        }

        private IShipProjectile SpawnProjectile(Transform output)
        {
            var inst = pool.CreateFromPool(
                Component.ProjectilePrefab, output.position, output.rotation);

            var projectile = inst.GetComponent<IShipProjectile>();
            projectile.Init(this, output);

            return projectile;
        }

        private IShipProjectile SpawnProjectile(Transform output, Transform targetTransform, Vector3 targetPosition)
        {
            var dir = targetPosition - output.position;
            var rotation = Quaternion.LookRotation(dir, Vector3.up);
            var inst = pool.CreateFromPool(
                Component.ProjectilePrefab, output.position, rotation);

            var projectile = inst.GetComponent<IShipProjectile>();
            projectile.Init(this, output, targetTransform);

            return projectile;
        }

        private IEnumerator BurstFireCoroutine()
        {
            var wfs = new WaitForSeconds(Component.BurstDelaySec);

            for (var i = 0; i < Component.BurstCount; i++)
            {
                SpawnProjectiles();
                yield return wfs;
            }
        }

        private IEnumerator BurstFireCoroutine(Transform targetTransform, IShip target)
        {
            var wfs = new WaitForSeconds(Component.BurstDelaySec);

            for (var i = 0; i < Component.BurstCount; i++)
            {
                SpawnProjectiles(targetTransform, target);
                yield return wfs;
            }
        }

        private Vector3 TargetTransform(IShip target, Vector3 targetPosition)
        {
            var isHit = IsSuccessfulHit(Vector3.Distance(transform.position, targetPosition));
            if (!isHit) return targetPosition + RandomizeMissingHit();

            return GeneralUtils.FirstOrderIntercept(
                transform.position, Vector3.zero,
                Component.ProjectileSpeed,
                targetPosition,
                target.Engine.Velocity
            );
        }

        private bool IsSuccessfulHit(float distance)
        {
            var sb = new StringBuilder();
            sb.Append("Precision; ");
            if (Component.Precision.Length == 0)
            {
                sb.Append("Not using precision, auto hit.");
                print(sb);
                return true;
            }

            var precision = 0;
            foreach (var precisionSetting in Component.Precision)
            {
                precision = precisionSetting.precision;
                if (distance <= precisionSetting.distance)
                {
                    break;
                }
            }

            var newPrecision = precision * WeaponSystem.AttackPrecisionModifier;
            var random = Random.Range(0, 100);
            var hit = random <= newPrecision;

            sb.Append($"base {precision}; final {newPrecision}; random {random}; {hit}");
            print(sb);

            return hit;
        }

        private Vector3 RandomizeMissingHit()
        {
            return new Vector3(
                Random.Range(0, 0) * Random.Range(0, 2) * 2 - 1,
                Random.Range(0, 30) * Random.Range(0, 2) * 2 - 1,
                Random.Range(0, 0) * Random.Range(0, 2) * 2 - 1
            );
        }
    }
}