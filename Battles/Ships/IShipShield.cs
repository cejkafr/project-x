﻿using Commons.Ships.Components;
using Games.Types;
using UnityEngine;
using IEnergyConsumer = Battles.IEnergyConsumer;

namespace TacticalBattles.Ships
{
    public enum ShipShieldMode
    {
        Disabled, Standard, Overclock
    }
    
    public delegate void ShipShieldModeChanged(IShipShield engine);
    
    public interface IShipShield : IEnergyConsumer
    {
        event ShipShieldModeChanged ModeChangedEvent;
        
        Transform[] Locations { get; }
        MaxValue Points { get; }
        float RechargeRate { get; }
        float RechargeDelay { get; }

        bool Visible { get; set; }
        ShipShieldMode Mode { get; set; }
        
        void Init(IShield i);
        void Tick();
    }
}