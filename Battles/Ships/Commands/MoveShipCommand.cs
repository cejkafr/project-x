﻿using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships.Commands
{
    public readonly struct MoveShipCommand : IShipCommand
    {
        public Vector2 Position { get; }

        public MoveShipCommand(Vector2 position)
        {
            Position = position;
        }

        public override string ToString()
        {
            return $"MoveShipCommand: {{{nameof(Position)}: {Position}}}";
        }
    }
}