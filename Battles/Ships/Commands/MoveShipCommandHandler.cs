﻿using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships.Commands
{
    internal class MoveShipCommandHandler : IShipCommandHandler
    {
        private readonly IShip ship;
        private readonly MoveShipCommand command;
        // private readonly float startDistance;
        // private readonly float startAngle;

        public IShipCommand Command => command;
        public bool IsCommandQueued => true;

        public MoveShipCommandHandler(IShip c, MoveShipCommand cmd)
        {
            ship = c;
            command = cmd;

            // startDistance = Vector3.Distance(ship.Position2D, cmd.Position);
            // startAngle = GeneralUtils.ForwardAngleBetween(ship.Transform, cmd.Position);

//            Debug.Log("Move Command starting distance: " + _startDistance + "m; " +
//                      "Starting angle: " + _startAngle + "°.");
        }

        public CommandHandlerTickResult Tick()
        {
            if (Vector3.Distance(ship.Position2D, command.Position) < StoppingDistance())
            {
                return CommandHandlerTickResult.Completed;
            }

            ship.Engine.Direction = command.Position - ship.Position2D;
            
            return CommandHandlerTickResult.Continue;
        }

        public void OnStart()
        {
            ship.Engine.Accelerate = true;
        }

        public void OnCompleted()
        {
            ship.Engine.Accelerate = false;
        }

        private float StoppingDistance()
        {
            return 50f;
        }

        public override string ToString()
        {
            return $"MoveCommandHandler: {{{nameof(command)}: {command}}}";
        }
    }
}