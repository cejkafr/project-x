﻿using System.Collections;
using System.Collections.Generic;
using Battles;
using Game.Extensions;
using TacticalBattle;
using UnityEngine;
using UnityEngine.Profiling;

namespace TacticalBattles.Ships
{
    public class DetectionProcessor : MonoBehaviour, IDetectionProcessor
    {
        private ICollection<IShip> playerShips;
        private ICollection<IShip> enemyShips;
        private Dictionary<IShip, IDetectionLevelChangeProcessor> changeProcessors;
        private WaitForSeconds waitForSeconds;

        [SerializeField] private float tickRate = 0.5f;

        public void Init(ITeam playerTeam)
        {
            playerShips = playerTeam.Ships;
            
            waitForSeconds = new WaitForSeconds(tickRate);

            enemyShips = new HashSet<IShip>();
            changeProcessors = new Dictionary<IShip, IDetectionLevelChangeProcessor>(enemyShips.Count);
            foreach (var shipSettings in playerTeam.EnemyShips)
            {
                enemyShips.Add(shipSettings.Ship);
                changeProcessors.Add(shipSettings.Ship, new DetectionLevelChangeProcessor(shipSettings.Ship));
            }
            
            StartCoroutine(DetectCoroutine());
        }

        private IEnumerator DetectCoroutine()
        {
            while (true)
            {
                yield return waitForSeconds;
                Tick();
            }
        }

        public void Tick()
        {
            Profiler.BeginSample("Ship Detection Processor");

            foreach (var playerShip in playerShips)
            {
                foreach (var enemyShip in enemyShips)
                {
                    var changeProcessor = changeProcessors[enemyShip];
                    var distance = playerShip.Position.DistanceToFlat(enemyShip.Position);
                    playerShip.Sensors.UpdateValue(enemyShip, distance, changeProcessor);
                    changeProcessor.Evaluate();

                    enemyShip.Sensors.UpdateValue(playerShip, distance);
                }
            }
            
            Profiler.EndSample();
        }


    }
}