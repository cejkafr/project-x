﻿using System.Collections;
using System.Collections.Generic;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles.Ships
{
    public class ShipTargettingProcessor : MonoBehaviour, IShipTargettingProcessor
    {
        public event ShipTargetChanged TargetChangedEvent;

        private readonly WaitForSeconds autoEngageFrequency = new WaitForSeconds(.5f);
        private Coroutine autoEngageCoroutine;

        private IShip ship;
        private IShip target;
        private bool autoEngage;

        public HashSet<IShip> Targets { get; } = new HashSet<IShip>();

        public IShip Target
        {
            get => target;
            set
            {
                if (target == value) return;

                target = value;
                TargetChangedEvent?.Invoke(ship);
            }
        }

        public bool HasTarget => Target != null;

        public bool AutoEngage
        {
            get => autoEngage;
            set
            {
                if (autoEngage == value) return;

                autoEngage = value;

                if (autoEngageCoroutine != null)
                {
                    StopCoroutine(autoEngageCoroutine);
                    autoEngageCoroutine = null;
                }

                if (value)
                {
                    autoEngageCoroutine = StartCoroutine(AutoEngageCoroutine());
                }
            }
        }

        public void Init(IShip iShip)
        {
            ship = iShip;
        }

        public void Tick()
        {
            TargetTick();
            AutoEngageTick();
        }

        public bool CanTargetWith(int index, IShip t)
        {
            return IsInRange(index, t) && IsInAngle(index, t);
        }

        public bool IsInMaxRange(IShip t)
        {
            return ship.Sensors.Objects[t].Distance <= ship.WeaponSystem.Range.Max;
        }

        public bool IsInRange(int index, IShip t)
        {
            return ship.Sensors.Objects[t].Distance <= ship.WeaponSystem.Weapons[index].Range;
        }

        public bool IsInAngle(int index, IShip t)
        {
            var slot = ship.WeaponSystem.Slots[index];
            var angle = Mathf.Abs(ship.Sensors.Objects[t].FacingAngle - ship.WeaponSystem.Weapons[index].YAngle);
            return angle <= slot.FireAngle / 2;
        }

        private void TargetTick()
        {
            if (!HasTarget) return;

            if (Target.IsDestroyed)
            {
                Target = null;
                return;
            }

            if (!IsInMaxRange(Target)) return;

            for (var i = 0; i < ship.WeaponSystem.Weapons.Length; i++)
            {
                if (!ship.WeaponSystem.IsWeaponAutoFireReady(i)) continue;
                if (!CanTargetWith(i, Target)) continue;

                ship.WeaponSystem.AttackWith(i, Target);
            }
        }

        private void AutoEngageTick()
        {
            if (!autoEngage) return;

            foreach (var autoTarget in Targets)
            {
                if (autoTarget.IsDestroyed) continue;
                if (autoTarget == Target) continue;

                for (var i = 0; i < ship.WeaponSystem.Weapons.Length; i++)
                {
                    if (!ship.WeaponSystem.IsWeaponAutoFireReady(i)) continue;
                    if (!IsInAngle(i, autoTarget)) continue;

                    ship.WeaponSystem.AttackWith(i, autoTarget);
                }
            }
        }

        private IEnumerator AutoEngageCoroutine()
        {
            while (true)
            {
                yield return autoEngageFrequency;

                Targets.Clear();

                foreach (var enemyShip in ship.Team.EnemyShips)
                {
                    if (!IsInMaxRange(enemyShip.Ship)) continue;
                    Targets.Add(enemyShip.Ship);
                }
            }
        }
    }
}