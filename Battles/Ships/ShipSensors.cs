﻿using System.Collections.Generic;
using Battles;
using Commons.Ships.Components;
using Game.Extensions;
using Games.Types;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class ShipSensors : MonoBehaviour, IShipSensors
    {
        public event EnergyConsumerEvent EnergyConsumptionChanged;
        public event ShipSensorEvent DetectionLevelChanged;
        
        private ISensors sensors;

        public int EnergyConsumption => sensors.EnergyConsumption;
        public Dictionary<IShip, DetectionObject> Objects { get; private set; }
        public int Range => sensors.ShortRange;

        public void Init(ISensors iSensors)
        {
            sensors = iSensors;

            Objects = new Dictionary<IShip, DetectionObject>(2);
        }
        
        public void UpdateValue(IShip ship, float distance, IDetectionLevelChangeProcessor processor = null)
        {
            if (!Objects.ContainsKey(ship))
            {
                Objects.Add(ship, new DetectionObject(ship));
            }
            
            var o = Objects[ship];
            var angle = transform.FacingAngleFlat(ship.Position);
            var oldValue = o.Level;
            var newValue = DetectionLevel.Detected;
            if (distance <= Range)
            {
                newValue = DetectionLevel.Full;
            }
            var changed = oldValue != newValue;

            o.Update(distance, angle, newValue);

            if (changed)
            {
                processor?.SubOldValue(oldValue);
                processor?.AddNewValue(newValue);

                DetectionLevelChanged?.Invoke(o);
            }
        }

        public void OnReactorEnergyDepleted(MaxValue maxValue)
        {

        }
    }
}
