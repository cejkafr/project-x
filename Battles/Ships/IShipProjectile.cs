﻿using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public interface IShipProjectile
    {
        IShipWeapon Weapon { get; }
        
        void Init(IShipWeapon iWeapon, Transform source);
        void Init(IShipWeapon iWeapon, Transform source, Transform target);
        void AttackFx();
        void AttackVfx();
    }
}
