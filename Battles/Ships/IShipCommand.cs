﻿using UnityEngine;

namespace TacticalBattles.Ships
{
    public interface IShipCommand
    {
        Vector2 Position { get; }
    }
}