﻿using Battles;
using Game;
using Game.Commons.Audio;
using Games;
using TacticalBattle;
using UnityEngine;

namespace TacticalBattles.Ships
{
    public class BeamShipProjectile : MonoBehaviour, IShipProjectile
    {
        private readonly struct RaycastTargetResult
        {
            public readonly bool Hit;
            public readonly Vector3 Position;
            public readonly GameObject Go;

            public RaycastTargetResult(bool hit, Vector3 position, GameObject go)
            {
                Hit = hit;
                Position = position;
                Go = go;
            }
        }

        private Transform source;
        private Transform target;
        private float ttl;
        private RaycastTargetResult raycastTarget;

        public GameObject startFx;
        public GameObject endFx;
        public LineRenderer lineRenderer;
        public LayerMask layerMask;
        public SoundDef shootFx;
        public SoundDef hitFx;

        public IShipWeapon Weapon { get; private set; }
        public bool HasEnded => Time.time > ttl;

        public void Init(IShipWeapon iWeapon, Transform source)
        {
            Weapon = iWeapon;
            this.source = source;
            ttl = Time.time + Weapon.Component.ProjectileLiveTimeSec;

            raycastTarget = RaycastTarget();

            gameObject.SetActive(true);
            startFx.SetActive(true);
            endFx.SetActive(raycastTarget.Hit);
        }

        public void Init(IShipWeapon iWeapon, Transform source, Transform target)
        {
            this.target = target;
            Init(iWeapon, source);
        }

        public void AttackFx()
        {
            if (shootFx == null) return;
            GameManager.SoundSystem.Play(shootFx, transform.position);
        }

        public void AttackVfx()
        {
        }

        private void Update()
        {
            if (HasEnded)
            {
                DamageTarget();
                gameObject.SetActive(false);
                return;
            }

            Rendering();
        }

        private void DamageTarget()
        {
            if (!raycastTarget.Hit) return;
            
            var attackable = raycastTarget.Go.GetComponent<IAttackable>();
            if (attackable != null)
            {
                if (Weapon.Team == attackable.Team) return;
                
                if (hitFx != null)
                    GameManager.SoundSystem.Play(hitFx, transform);
                
                attackable.TakeDamage(raycastTarget.Position, Weapon);
            }
        }

        private void Rendering()
        {
            var startPos = source.position;
            var endPos = raycastTarget.Position;

            lineRenderer.SetPosition(0, startPos);
            lineRenderer.SetPosition(1, endPos);

            startFx.transform.position = startPos;
            endFx.transform.position = endPos;
        }

        private RaycastTargetResult RaycastTarget()
        {
            var t = transform;
            return Physics.Raycast(t.position, t.forward, out var hit,
                Weapon.Component.Range, layerMask, QueryTriggerInteraction.Collide)
                ? new RaycastTargetResult(true, hit.point, hit.collider.gameObject)
                : new RaycastTargetResult(false, target.position, null);
        }
    }
}