﻿using System.Text;
using Commons.Ships.Components;
using Game.Commons.Combat;
using Games.Types;
using TacticalBattle;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.Ships.DamageProcessors
{
    public class HullDamageProcessor : IDamageProcessor
    {
        public DamageBundle Apply(IShip ship, AttackVector vector, IWeaponComponent weapon)
        {
            var sb = new StringBuilder($"Hit; {vector}; ");

            var armour = ResolveArmourValue(ship, vector);
            var critical = IsCriticalHit(weapon);
            foreach (var damage in weapon.Damage.Values)
            {
                ship.HullPoints.Value -= critical ? CalculateCriticalDamage(damage, sb) : CalculateDamage(damage, armour, sb);
            }

            Debug.Log(sb);
            
            return DamageBundle.Empty;
        }

        private MaxValue ResolveArmourValue(IShip ship, AttackVector vector)
        {
            switch (vector)
            {
                case AttackVector.Front:
                    return ship.BaseObject.FrontArmour;
                case AttackVector.Rear:
                    return ship.BaseObject.RearArmour;
                default:
                    return ship.BaseObject.SideArmour;
            }
        }

        private bool IsCriticalHit(IWeaponComponent weapon)
        {
            return Random.Range(0, 100) < weapon.CriticalChance;
        }
        
        private float CalculateCriticalDamage(Damage damage, StringBuilder sb)
        {
            var damageType = damage.Type;
            var rawDamage = damage.ValueFromRange;
            var hullDamage = rawDamage * damageType.hullMultiplier;

            sb.Append($"{damageType.title}; {rawDamage} * {damageType.hullMultiplier} = {hullDamage}; CRITICAL!!!");

            return hullDamage;
        }

        private float CalculateDamage(Damage damage, MaxValue armour, StringBuilder sb)
        {
            var damageType = damage.Type;
            var rawDamage = damage.ValueFromRange;
            var armorDamage = rawDamage * damageType.armorMultiplier;
            var rawHullDamage = armorDamage - armorDamage * (armour.Value / 100f);
            var hullDamage = rawHullDamage * damageType.hullMultiplier;

            sb.Append($"{damageType.title}; {rawDamage} * {damageType.armorMultiplier} = {armorDamage}; ");
            sb.Append($"{armorDamage} -  {armorDamage} * ({armour.Value} / 100) = {rawHullDamage}; ");
            sb.Append($"{rawHullDamage} * {damageType.hullMultiplier} = {hullDamage}; ");

            return hullDamage;
        }
    }
}