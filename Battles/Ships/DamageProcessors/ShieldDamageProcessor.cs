﻿using System.Text;
using Commons.Ships.Components;
using Game.Commons.Combat;
using TacticalBattle;
using UnityEngine;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.Ships.DamageProcessors
{
    public class ShieldDamageProcessor : IDamageProcessor
    {
        public DamageBundle Apply(IShip ship, AttackVector vector, IWeaponComponent weapon)
        {
            var damage = weapon.Damage;
            if (ship.Shield.Points.IsMin) return damage;

            var sb = new StringBuilder("Hit; Shield; ");

            for (var i = 0; i < damage.Values.Length; i++)
            {
                ship.Shield.Points.Value -= CalculateDamage(damage.Values[i], sb);
                if (!ship.Shield.Points.IsMin) continue;

                sb.Append("depleted");
                Debug.Log(sb);

                return ReturnRemainingDamage(damage, i);
            }

            sb.Append("OK");
            Debug.Log(sb);

            return DamageBundle.Empty;
        }

        private float CalculateDamage(Damage damage, StringBuilder sb)
        {
            var rawDamage = damage.ValueFromRange;
            var damageType = damage.Type;
            var shieldDamage = rawDamage * damageType.shieldMultiplier;

            sb.Append($"{damageType.title}; {rawDamage} * {damageType.shieldMultiplier} = {shieldDamage}; ");

            return shieldDamage;
        }

        private DamageBundle ReturnRemainingDamage(DamageBundle bundle, int i)
        {
            var damageArray = new Damage[bundle.Values.Length - i];
            for (var r = i; r < bundle.Values.Length; r++)
            {
                damageArray[r - i] = bundle.Values[i];
            }

            return DamageBundle.Of(damageArray);
        }
    }
}