﻿using Commons.Ships.Components;
using Game.Commons.Combat;
using TacticalBattle;
using IShip = TacticalBattles.Ships.IShip;

namespace TacticalBattles.Ships.DamageProcessors
{
    public interface IDamageProcessor
    {
        DamageBundle Apply(IShip ship, AttackVector vector, IWeaponComponent weapon);
    }
}