﻿using Battles;
using Commons.Ships.Components;
using Games.Types;

namespace TacticalBattles.Ships
{
    public class ShipAuxiliarySystem : IShipAuxiliarySystem
    {
        public event EnergyConsumerEvent EnergyConsumptionChanged;

        private ISensors component;

        public int SensorRange => component.ShortRange;
        public int EnergyConsumption => component.EnergyConsumption;

        public void Init(ISensors i)
        {
            component = i;
        }

        public void OnReactorEnergyDepleted(MaxValue maxValue)
        {

        }
    }
}