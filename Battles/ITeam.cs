﻿using System.Collections.Generic;
using Commons.Factions;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles
{
    public enum ShipTargetPriority
    {
        Urgent = 0,
        VeryHigh = 1,
        High = 2,
        Medium = 3,
        Low = 4
    }

    public class EnemyShipSettings
    {
        public ShipTargetPriority Priority { get; set; }

        public EnemyShipSettings(ShipTargetPriority priority)
        {
            Priority = priority;
        }
    }

    public class EnemySettings
    {
        public IShip Ship { get; }
        public EnemyShipSettings Settings { get; set; }

        public EnemySettings(IShip iShip, EnemyShipSettings settings)
        {
            Ship = iShip;
            Settings = settings;
        }
    }

    public interface ITeam
    {
        IFaction BaseObject { get; }
        List<IShip> Ships { get; }
        List<EnemySettings> EnemyShips { get; }
        string Name { get; }
        Color Color { get; }
        bool IsHuman { get; }

        EnemySettings FindEnemyByShip(IShip iShip);
        void SetTargetPriority(IShip ship, ShipTargetPriority priority);
        void OnShipRegistered(IShip iShip);
        void OnShipUnregistered(IShip iShip);
    }
}