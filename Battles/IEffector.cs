﻿using TacticalBattles.Ships;

namespace Battles
{
    public interface IEffector
    {
        void OnActivate(IShip controller);
        void OnDeactivate(IShip controller);
        void Tick(IShip controller);
    }
}