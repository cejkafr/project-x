﻿using Commons.Presenters;
using UnityEngine;
using Universes;
using Universes.Presenters;

namespace Battles.Presenters
{
    public class BattlePresenter : MonoBehaviour, IBattlePresenter
    {
        [SerializeField] private GameObject lightSourcePrefab;

        private IBulkDisableObjectCache cache;
        private IBattleEnvPresenter envPresenter;
        private BattleObjectPresenter objectPresenter;
        
        private void Awake()
        {
            var cacheParent = new GameObject("Object Cache").transform;
            cache = new BulkDisableObjectCache(cacheParent);
            
            var positionSampler = new SimplePositionSamplerV1();
            var envObjectPresenter = new CachedObjectPresenter(cache);
            var layerPresenter = new ProceduralPresenter(positionSampler, envObjectPresenter);
            var lightSourcePresenter = new CachedLightSourcePresenter(cache, lightSourcePrefab);

            envPresenter = new BattleEnvPresenter(layerPresenter, lightSourcePresenter);   
            
            objectPresenter = new BattleObjectPresenter();
        }
        
        public void Present(Battle battle)
        {
            envPresenter.Present(battle);
        }

        public void Destroy()
        {
            cache.DisableAll();
        }
    }
}