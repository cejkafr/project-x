﻿using System;
using System.Collections;
using Battles.Cameras;
using Battles.Controllers;
using Battles.GridBoard;
using Battles.GridBoard.UI;
using Battles.Scenarios;
using Battles.Ships;
using Battles.UI;
using Commons;
using Commons.UI;
using Game;
using Game.Commons.UI;
using Game.Console;
using Games;
using Games.Consoles;
using TacticalBattles.UI;
using UnityEngine;
using UnityEngine.Events;
using Universes;

namespace Battles
{
    public class TacticalBattleController : MonoBehaviour
    {
        [Serializable]
        public class GameEvent : UnityEvent
        {
        }

        [SerializeField] private TeamManager teamManager;
        [SerializeField] private ShipManager shipManager;
        [SerializeField] private CinemachineRTSCamera cameraBehaviour;
        [SerializeField] private PlayerShipController playerShipController;
        [SerializeField] private BattleFrontend battleFrontend;

        [Space] 
        public GameEvent onBattleStarted = new GameEvent();
        public GameEvent onBattleEnded = new GameEvent();

        private IDetectionProcessor detectionProcessor;
        private IEnemyController enemyController;
        private PlayerData data;

        public IRTSCamera Camera => cameraBehaviour;
        public TeamManager TeamManager => teamManager;
        public ShipManager ShipManager => shipManager;
        public PlayerShipController PlayerShip => playerShipController;
        public BattleFrontend BattleFrontend => battleFrontend;
        public IScenario Scenario { get; private set; }
        public Vector2 Size { get; private set; }

        private void Awake()
        {
            GameManager.LevelLoaded += EndGame;

            detectionProcessor = GetComponent<IDetectionProcessor>();
            data = FindObjectOfType<PlayerData>();

            ConsoleManager.AddCommand("babandon", args
                =>
            {
                ExitTacticalBattle();
                ConsoleManager.SetOpen(false);
            }, "Instantly quit tactical battle.");

            ConsoleManager.AddCommand("bwin", args
                =>
            {
                foreach (var enemyShip in PlayerShip.Team.EnemyShips.ToArray())
                {
                    shipManager.Destroy(enemyShip.Ship);
                    ConsoleManager.SetOpen(false);
                }
            }, "Instantly win tactical battle.");
        }

        private void Start()
        {
        }

        public void OnMenuEvent(bool isOpen)
        {
            if (!gameObject.activeInHierarchy) return;
            
            if (isOpen)
            {
                TimeManager.Pause();
                BattleFrontend.Hide(true);
            }
            else
            {
                BattleFrontend.Hide(false);
                TimeManager.Resume();
            }
        }

        public void Init(Vector2 size)
        {
            Size = size;
        }

        public void OnBootstrapDone()
        {
            Scenario = new EliminationScenario(this, data.Player.Combat);
            Scenario.OnInit();
        }

        private void OnDestroy()
        {
            GameManager.LevelLoaded -= EndGame;
        }

        public void StartGame()
        {
            Scenario.OnStart();

            onBattleStarted.Invoke();
            TimeManager.NormalSpeed();

            var humanPlayer = PlayerShip.Team;

            foreach (var player in TeamManager.Teams)
            {
                if (player == humanPlayer) continue;
                enemyController = new SimpleEnemyController(player);
            }

            StartCoroutine(EnemyPlayerTickCoroutine());
            detectionProcessor.Init(playerShipController.Team);
        }

        public void EndGame()
        {
            Scenario.OnCompleted();
            onBattleEnded.Invoke();

            BattleFrontend.OpenEndDialog();
        }

        public void ExitTacticalBattle()
        {
            var universeController = FindObjectOfType<UniverseController>();
            universeController.LoadSector(universeController.CurrentSector, SectorLoadReason.TacticalBattle);
        }

        private IEnumerator EnemyPlayerTickCoroutine()
        {
            var wfs = new WaitForSeconds(.5f);

            while (true)
            {
                yield return wfs;

                Scenario.Tick();
                if (Scenario.IsCompleted)
                {
                    EndGame();
                    ExitTacticalBattle();
                    break;
                }

                enemyController.Tick();
            }
        }

        public void OnBattleLoaded(Battle battle)
        {
            gameObject.SetActive(true);

            var size = new Vector2(
                battle.Size.x * battle.Sector.Biome.tileSize, 
                battle.Size.y * battle.Sector.Biome.tileSize);

            cameraBehaviour.BoardSize = size;
            FindObjectOfType<GridBoardSetter>().Init(size);
            FindObjectOfType<GridBoardUi>().Init(size);
            Init(size);

            CreateFactions();
            CreateShips();

            OnBootstrapDone();

            StartGame();

            Loader.Instance.FadeIn(0.8f);
        }

        public void OnBattleUnloaded(Battle battle)
        {
            playerShipController.DeselectAll();

            teamManager.Clear();
            shipManager.Clear();
            
            gameObject.SetActive(false);
        }

        private void CreateFactions()
        {
            teamManager.Create(data.Player.Faction);
            teamManager.Create(data.Player.Combat.Fleet.Faction);

            playerShipController.Init(teamManager.HumanTeam);
        }

        private void CreateShips()
        {
            foreach (var ship in data.Player.Fleet.Ships)
            {
                shipManager.Create(ship);
            }

            foreach (var ship in data.Player.Combat.Fleet.Ships)
            {
                shipManager.Create(ship);
            }
        }
    }
}