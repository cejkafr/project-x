﻿using System;
using Unity.Mathematics;
using UnityEngine;

namespace Battles
{
    [Serializable]
    public struct LevelSettingsLightSource
    {
        public float3 rotation;
        public Color color;
        public float intensity;
    }
    
    [Serializable]
    public struct LevelSettingsBgObject
    {
        public float3 position;
        public float3 rotation;
        public float3 scale;
        public GameObject prefab;
    }
    
    [Serializable]
    public struct LevelSettings
    {
        public int3 size;
        public LevelSettingsLightSource lightSource;
        public LevelSettingsBgObject[] backgroundObjects;
        public float asteroidIntensity;
        public float cloudIntensity;
    }
    
    public interface ILevelGenerator
    {
        void Generate(LevelSettings s);
    }
}