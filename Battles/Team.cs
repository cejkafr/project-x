﻿using System.Collections.Generic;
using TacticalBattles;
using TacticalBattles.Ships;
using UnityEngine;

namespace Battles
{
    public class Team : ITeam
    {
        private readonly Dictionary<IShip, EnemySettings> enemyShipsMap;

        public Commons.Factions.IFaction BaseObject { get; }
        public List<IShip> Ships  { get; }
        public List<EnemySettings> EnemyShips { get; }
        public string Name => BaseObject.Name;
        public Color Color => BaseObject.Color;
        public bool IsHuman => BaseObject.IsHuman;

        public Team(Commons.Factions.IFaction iCommon)
        {
            BaseObject = iCommon;

            enemyShipsMap = new Dictionary<IShip, EnemySettings>(2);
            Ships = new List<IShip>(2);
            EnemyShips = new List<EnemySettings>(4);
        }

        public EnemySettings FindEnemyByShip(IShip iShip)
        {
            return enemyShipsMap[iShip];
        }

        public void SetTargetPriority(IShip ship, ShipTargetPriority priority)
        {
            var enemyShip = FindEnemyByShip(ship);
            enemyShip.Settings.Priority = priority;
            SortEnemyShipsByPriority();
        }

        public void OnShipRegistered(IShip iShip)
        {
            if (iShip.Team == this)
            {
                RegisterOwnedShip(iShip);
            }
            else
            {
                RegisterEnemyShip(iShip);
            }
        }

        public void OnShipUnregistered(IShip iShip)
        {
            if (iShip.Team == this)
            {
                UnregisterOwnedShip(iShip);
            }
            else
            {
                UnregisterEnemyShip(iShip);
            }
        }

        private void RegisterEnemyShip(IShip ship)
        {
            var combatSettings = new EnemyShipSettings( ShipTargetPriority.Medium);
            var enemyShip = new EnemySettings(ship, combatSettings);

            EnemyShips.Add(enemyShip);
            enemyShipsMap.Add(ship, enemyShip);

            SortEnemyShipsByPriority();
        }

        private void UnregisterEnemyShip(IShip ship)
        {
            var enemyShip = enemyShipsMap[ship];
            EnemyShips.Remove(enemyShip);
            enemyShipsMap.Remove(ship);

            SortEnemyShipsByPriority();
        }

        private void RegisterOwnedShip(IShip iShip)
        {
            Ships.Add(iShip);
        }

        private void UnregisterOwnedShip(IShip iShip)
        {
            Ships.Remove(iShip);
        }

        // TODO move sorter to external class
        private void SortEnemyShipsByPriority()
        {
            EnemyShips.Sort(delegate(EnemySettings enemy1, EnemySettings enemy2)
            {
//                var visibility1 = m_EnemyShips[ship1].Visibility.Visible;
//                var visibility2 = m_EnemyShips[ship2].Visibility.Visible;
                var priority1 = enemy1.Settings.Priority;
                var priority2 = enemy2.Settings.Priority;

//                if (visibility1 == visibility2) return 0;
                if (priority1 == priority2) return 0;
                return priority1 > priority2 ? 1 : -1;
            });
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}";
        }
    }
}